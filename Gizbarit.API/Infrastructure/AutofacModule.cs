﻿using System.Configuration;
using System.Reflection;
using Autofac;
using Gizbarit.DAL;
using Gizbarit.DAL.Facades.Document;
using Gizbarit.DAL.Interfaces;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Providers.Sign;
using Gizbarit.DAL.Services.Content;
using Gizbarit.DAL.Services.Document;
using Gizbarit.Utils.Email;

namespace Gizbarit.API.Infrastructure
{
    class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterServices(builder);
        }


        private static void RegisterServices(ContainerBuilder builder)
        {
            string dbConnectionString =
#if DEBUG
           ConfigurationManager.ConnectionStrings["DebugConnection"].ConnectionString;
#else
            ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
#endif

            builder.RegisterType<DataAccessFactory<GizbratDataContext>>().As<IDataAccessFactory>().WithParameter("connectionString", dbConnectionString).SingleInstance();
            builder.Register(c => c.Resolve<IDataAccessFactory>().CreateUnitOfWork()).As<IUnitOfWork>().InstancePerRequest();

            var asm = Assembly.Load("Gizbarit.DAL");
            builder.RegisterAssemblyTypes(asm).Where(t => t.Name.EndsWith("Service") || t.Name.EndsWith("Factory")).AsImplementedInterfaces().InstancePerRequest();

            builder.RegisterType<PdfProviderFactory>().As<IDocumentProviderFactory>().InstancePerRequest();
            builder.RegisterType<PdfSignProvider>().As<ISignProvider>().InstancePerRequest();
            builder.RegisterType<MailService>().As<IMailService>().InstancePerRequest();

            builder.RegisterType<DocumentFacade>()
                .As<IDocumentFacade>()
                .UsingConstructor(typeof(IDocumentService), typeof(IContentService),
                    typeof(IMailService), typeof(IDocumentProviderFactory),
                    typeof(ISignProvider))
                .InstancePerRequest();
        }
    }
}