﻿using System.Configuration;
using System.Reflection;
using Autofac;
using Gizbarit.DAL;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.API.Infrastructure
{
    public static class Injector
    {
        public static IContainer GetContainer()
        {
            var builder = new ContainerBuilder();
            
                string dbConnectionString =
#if DEBUG
           ConfigurationManager.ConnectionStrings["DebugConnection"].ConnectionString;
#else
            ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
#endif
            builder.RegisterType<DataAccessFactory<GizbratDataContext>>()
                .As<IDataAccessFactory>()
                .WithParameter("connectionString", dbConnectionString)
                .SingleInstance();

            builder.Register(c => c.Resolve<IDataAccessFactory>().CreateUnitOfWork())
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope().PropertiesAutowired();

            var asm = Assembly.Load("Gizbarit.DAL");
            builder.RegisterAssemblyTypes(asm)
                .Where(t => t.Name.EndsWith("Service") || t.Name.EndsWith("Factory"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope().PropertiesAutowired();
            return builder.Build();
        }
    }
}