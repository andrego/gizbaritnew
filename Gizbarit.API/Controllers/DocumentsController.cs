﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

using AutoMapper;
using Gizbarit.API.MapProfiles;
using Gizbarit.DAL.Common;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Facades.Document;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Models.Documents.Related;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.Utils;

namespace Gizbarit.API.Controllers
{
    [System.Web.Http.Authorize]
    [System.Web.Http.RoutePrefix("api/Documents")]
    public class DocumentsController :BaseDocumentController
    {

        private readonly DocumentUtils _documentUtils;

        private readonly Currency _currency;
        private readonly IDocumentFacade _documentFacade;

        public DocumentsController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService, IAdvanceService advanceService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService,
            IDocumentFacade documentFacade,
            ICatalogService catalogService
            ) :base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
 
            _currency = currencyService.Get("ILS");
            _documentFacade = documentFacade;

            _documentUtils = new DocumentUtils(_organizationService, _documentService, _documentItemService,
                _transactionService, _advanceService, _bankAccountService, _paymentTypeService, _paymentService,
                catalogService, currencyService);
        }


        /// <summary>
        /// Get documents by searchString
        /// </summary>
        /// <param name="searchString">string of searhing documents</param>
        /// <returns>IEnumerable of DocumentSearchResult</returns>
        [System.Web.Http.HttpGet]
        public IEnumerable<DocumentSearchResult> Get(string searchString=null)
        {
            var org = _organizationService.GetCurrent();
            var documents = org.Documents.Where(d => searchString == null ||
                ( 
                    d.Subject != null
                        ? d.Subject.ToLower().Contains(searchString.ToLower())
                        : false
                          || d.Client.Name.ToLower().Contains(searchString.ToLower())
                          || d.Number.ToString().Contains(searchString)
                          || d.DocumentItems.Any(it => it.Name.ToLower().Contains(searchString))
                    )).ToList();

            if (documents.Count == 0)
                documents = org.Documents.ToList();
            
            var itemsResult = Mapper.Map<IEnumerable<Document>, IEnumerable<DocumentSearchResult>>(documents);
            return itemsResult;
        }

        /// <summary>
        /// Get PriceOffers documents for client 
        /// </summary>
        /// <param name="id">Id type int of client id</param>
        /// <returns>IEnumerable of LinkedPreliminaryInvoice</returns>
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("{id}/PriceOffers")]
        public IEnumerable<LinkedPreliminaryInvoice> GetLinkedPriceOffers(int id)
        {
            var org = _organizationService.GetCurrent();
            var documents =
                org.Documents.Where(m => m.DocumentType == DocumentType.PriceOffer && !m.IsPaid && m.ClientID == id)
                .OrderByDescending(m => m.Number).ToList();
            if (documents.IsNotEmpty())
            {
                var mapper = MapperDocument.GetDocumentPreliminaryInvoicesMapper(MapperDocumentType.PriceOffers, _documentService, org);
                var linkedItems = mapper.Map<IEnumerable<Document>, IEnumerable<LinkedPreliminaryInvoice>>(documents);
                return linkedItems;
            }
            return null;
        }


        /// <summary>
        /// Get PreliminaryInvoices documents for client 
        /// </summary>
        /// <param name="id">Id type int of client id</param>
        /// <param name="showPaid">bool type (default value = true)</param>
        /// <returns>IEnumerable of LinkedPreliminaryInvoice</returns>
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("{id}/PreliminaryInvoices")]
        public IEnumerable<LinkedPreliminaryInvoice> GetLinkedPreliminaryInvoices(int id, bool showPaid = true)
        {

            var org = _organizationService.GetCurrent();
            var documents =
                org.Documents.Where(
                    m => m.DocumentType == DocumentType.PreliminaryInvoice && !m.IsPaid && m.ClientID == id);

            if (!showPaid)
                documents = documents.Where(m => m.PreliminaryInvoice.Paid == 0);

            var model = documents.OrderByDescending(m => m.Number).ToList();
            if (model.IsNotEmpty())
            {

                var mapper = MapperDocument.GetDocumentPreliminaryInvoicesMapper(MapperDocumentType.PreliminaryInvoices, _documentService,org);
                var linkedItems = mapper.Map<IEnumerable<Document>, IEnumerable<LinkedPreliminaryInvoice>>(model);
                return linkedItems;

            }

            return null;
        }

        /// <summary>
        /// Get LinkedDocuments  documents for client
        /// </summary>
        /// <param name="id">Id type int of client id</param>
        /// <param name="selectedDocumentIdStr">Guid type (default value = null)</param>
        /// <returns>IEnumerable of LinkedDocuments</returns>
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("{id}/DocumentsForReceipt")]
        public LinkedDocuments GetLinkedDocumentsForReceipt(int id, Guid? selectedDocumentIdStr=null)
        {
            var org = _organizationService.GetCurrent();
            var model = new LinkedDocuments();
            if (selectedDocumentIdStr != null)
            {
                var docItems = org.Documents.Where(d => d.ID == selectedDocumentIdStr)
                    .OrderByDescending(d => d.Number).ToList();

             var mapper = MapperDocument.GetDocumentReceiptMapper(MapperDocumentType.LinkedSelectedDocument,
                    _documentService);
                model.SelectedDocumentItem = mapper.Map<Document, LinkedDocumentReceipt>(docItems.FirstOrDefault());
           }
            
            var docPreliminary = org.Documents.Where(d => d.ClientID == id && !d.IsPaid &&
                                    d.DocumentType == DocumentType.PreliminaryInvoice)
                                    .OrderByDescending(d => d.Number).ToList();

            var preliminaryMapper = MapperDocument.GetDocumentReceiptMapper(MapperDocumentType.LinkedPreliminaryInvoices,
               _documentService);
            model.PreliminaryInvoices =
               preliminaryMapper.Map<IEnumerable<Document>, List<LinkedDocumentReceipt>>(docPreliminary);

            var docTaxInvoice =
                org.Documents.Where(d => d.ClientID == id && !d.IsPaid && d.DocumentType == DocumentType.TaxInvoice)
                    .OrderByDescending(d => d.Number).ToList();

            var taxInvoicesMapper = MapperDocument.GetDocumentReceiptMapper(MapperDocumentType.LinkedTaxInvoices,
                _documentService);
            model.TaxInvoices = taxInvoicesMapper.Map<IEnumerable<Document>, List<LinkedDocumentReceipt>>(docTaxInvoice);
            
            return model;
        }


        /// <summary>
        /// Get FileStreamResult of Document
        /// </summary>
        /// <param name="id">Id type Guid of doument</param>
        /// <param name="isCopyRequested">bool type (default value = false)</param>
        /// <returns>FileStreamResult</returns>
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("{id}/Print")]
        [ResponseType(typeof(FileStreamResult))]
        public async Task<IHttpActionResult> Print(Guid id, bool isCopyRequested = false)
        {
            var document = _documentService.GetUserDocuments().FirstOrDefault(d=>d.ID == id);
            if (document == null)
                return BadRequest("Document not found");

            var model = _documentFacade.GetForPrint(id, isCopyRequested);
            
            FileStreamResult fs = new FileStreamResult(model.GetStream(), model.Type);
            return Ok(fs);
        }

        /// <summary>
        /// Send Document to e-mail
        /// </summary>
        /// <param name="id">Id type Guid of document</param>
        /// <param name="customText">string type (default value = empty string)</param>
        /// <param name="isCopyRequest">bool type (default value = false)</param>
        /// <returns>IHttpActionResult</returns>
        [System.Web.Http.Route("{id}/Send")]
        [System.Web.Http.HttpGet]
        public async Task<IHttpActionResult> Send(Guid id, string customText = "", bool isCopyRequest = false)
        {
            var document = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == id);
            if (document == null)
                return BadRequest("Document not found");

            _documentFacade.SendByMail(id, customText, isCopyRequest);
            LockDocumentForEditing(document.ID, DocumentStatusType.Send);
            
            return Ok();
        }


        [System.Web.Http.NonAction]
        public void LockDocumentForEditing(Guid id, DocumentStatusType status = DocumentStatusType.Printed)
        {
            var document = _documentService.GetUserDocuments()
                .Include(d => d.Payments)
                .Include(d => d.Organization)
                .First(d => d.ID == id);
            if (status != DocumentStatusType.Printed || status != DocumentStatusType.Send)
            {
                status = DocumentStatusType.Printed;
            }

            if (document != null)
            {
                document.Status = status;
                ProcessDocumentBanking(document);

                _documentService.Commit(true);
               
            }
            
        }

        [System.Web.Http.NonAction]
        private void ProcessPayments(Document document)
        {
            var bankAccount = _bankAccountService.GetDefaultAccount(User.Identity.GetOrgId());
            foreach (var payment in document.Payments.Where(p => p.PaymentType == 1 || p.PaymentType == 2))
            {
                var refDocument = document.DocumentReferences.FirstOrDefault();

                _transactionService.Create(new BankTransaction
                {
                    OrganizationId = User.Identity.GetOrgId(),
                    ID = Guid.NewGuid(),
                    Amount = Math.Abs(payment.Amount),
                    BankId = bankAccount.ID,
                    Credit = payment.Amount > 0,
                    DateInserted = payment.Date,
                    Details =
                        String.Format("{0} {1} {2} ({3})",
                            Translations.Bank.Payment,
                            Translations.Errors.PaymentForReceipt,
                            document.Number,
                            document.Client.Name),
                    DateUpdated = payment.Date,
                    CurrencyId = "ILS"
                });

            }
            _transactionService.Commit(true);

        }

        [System.Web.Http.NonAction]
        private void ProcessDocumentBanking(Document document)
        {
            if (!document.IsPaymentProcessed)
            {
                var org = _organizationService.GetCurrent();
                switch (document.DocumentType)
                {
                    case DocumentType.TaxInvoice:
                        _documentUtils.AddTaxesOnDocumentCreate(document.ToBaseDocument(org), document.Number, document.DocumentType);
                        break;
                    case DocumentType.Receipt:
                        ProcessPayments(document);
                        break;
                    case DocumentType.TaxInvoiceReceipt:
                        _documentUtils.AddTaxesOnDocumentCreate(document.ToBaseDocument(org), document.Number, document.DocumentType);
                        ProcessPayments(document);
                        break;
                     
                }
                document.IsPaymentProcessed = true;
            }
        }

    }
}
