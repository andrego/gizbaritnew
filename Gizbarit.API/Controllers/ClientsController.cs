﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Organisation;
using Gizbarit.DAL.Services.Clients;
using Gizbarit.DAL.Services.Organisation;


namespace Gizbarit.API.Controllers
{
    /// <summary>
    /// Endpoint to work with clients
    /// </summary>
    [Authorize]
    public class ClientsController : ApiController
    {
        private readonly IOrganisationService _organizationService;
        private readonly IClientService _clientService;
        public ClientsController(IOrganisationService organisationService, IClientService clientService)
        {
            _organizationService = organisationService;
            _clientService = clientService;
        }


        /// <summary>
        /// Get clients by name
        /// </summary>
        /// <param name="name">name of searching client</param>
        /// <returns>IEnumerable of ClientViewModel</returns>
        [HttpGet]
        public IEnumerable<ClientViewModel> Get(string name = null)
        {
            var org = _organizationService.GetCurrent();
            var clients = org.Clients.Where(m => name == null || m.Name.ToLower().Contains(name.ToLower())).ToList();
            if (clients.Any())
            {
                var items = Mapper.Map<IEnumerable<Client>, IEnumerable<ClientViewModel>>(clients);
                return items;
            }
            return null;
        }

        // PUT: api/Clients
        /// <summary>
        /// PUT client detail
        /// </summary>
        /// <param name="item">item type of ClientViewModel</param>
        /// <returns>item type of ClientViewModel</returns>

        [HttpPut]
        [ResponseType(typeof(ClientViewModel))]
        public async Task<IHttpActionResult> Put([FromBody] ClientViewModel item)
        {
            if (ModelState.IsValid)
            {
                var org = _organizationService.GetCurrent();
                var changeItem = org.Clients.FirstOrDefault(el=>el.ID == item.Id);
                if (changeItem == null)
                    return BadRequest();

                var clientItem = Mapper.Map<ClientViewModel, Client>(item);
                Mapper.Map<Client, Client>(clientItem, changeItem);
                _organizationService.Commit(true);
                return Ok(item);

            }
            return BadRequest(ModelState);
        }

        // POST: api/Catalog
        /// <summary>
        /// Create Client item
        /// </summary>
        /// <param name="itemClientViewModel">ClientViewMode item model</param>
        /// <returns>Return created client item in case of success.</returns>

        [HttpPost]
        [ResponseType(typeof(ClientViewModel))]
        public async Task<IHttpActionResult> Post([FromBody]ClientViewModel itemClientViewModel)
        {
            if (ModelState.IsValid)
            {
                var org = _organizationService.GetCurrent();

                var clientItem = Mapper.Map<ClientViewModel, Client>(itemClientViewModel);

                //Can't update database with out date
                clientItem.DateCreated = DateTime.Now;
                org.Clients.Add(clientItem);

                _organizationService.Commit(true);

                return Ok(Mapper.Map<Client, ClientViewModel>(clientItem));
            }

            return BadRequest(ModelState);
        }

        // DELETE: api/Clients/2002
        /// <summary>
        /// delete client item
        /// </summary>
        /// <param name="id">Client item id</param>
        /// <returns>Return IHttpActionResult</returns>
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var organization = _organizationService.GetCurrent();
            var item = organization.Clients.FirstOrDefault(m => m.ID == id);
            if (item != null)
            {
                _clientService.Remove(item, true);
                return Ok();
            }
            return BadRequest();
        }
    }
}
