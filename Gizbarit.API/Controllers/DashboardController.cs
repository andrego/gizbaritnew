﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Account.Bank;
using Gizbarit.DAL.Models.Dashboard;
using Gizbarit.DAL.Models.Organisation;
using Gizbarit.DAL.Models.Payment;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.Utils;

namespace Gizbarit.API.Controllers
{
    [Authorize]
    public class DashboardController : ApiController
    {
        private readonly IBankAccountService _bankAccountService;
        private readonly IDocumentService _documentService;
        public DashboardController(IBankAccountService bankAccountService, IDocumentService documentService)
        {
            _bankAccountService = bankAccountService;
            _documentService = documentService;
        }

        /// <summary>
        /// Get account and required payments
        /// </summary>
        /// <returns>DashboardViewModel</returns>
        [HttpGet]
        public async Task<DashboardViewModel> Get()
        {
            var orgId = User.Identity.GetOrgId();
            var accountEntities = _bankAccountService.GetUserAccounts(orgId).ToList();
            var accounts = new List<BankAccountShort>();
            var firstDayOfmonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            foreach (var account in accountEntities)
            {
               accounts.Add(new BankAccountShort
               {
                   Account = account.BankName.Name,
                   Balance = await _bankAccountService.GetBalance(account.ID, DateTime.Today),
                   Number = account.AccountNumber,
                   BalanceMonth = await _bankAccountService.GetBalance(account.ID, firstDayOfmonth),
                   Transactions = Mapper.Map<List<BankTransaction>, List<TransactionShortViewModel>>(account.BankTransactions.Where(t=>t.DateInserted > firstDayOfmonth).ToList())
                  
               });
            }
            var currentYear = DateTime.Today.Year;

            var preliminaryInvoices =
                _documentService.GetAll().Include(d => d.Payments).Include(d => d.Client)
                    .Where(
                        d =>
                            d.OrganizationID == orgId && d.DateCreated.Year == currentYear &&
                            (d.DocumentType == DocumentType.PreliminaryInvoice ||
                             ((d.DocumentType == DocumentType.TaxInvoice ||
                               d.DocumentType == DocumentType.TaxInvoiceReceipt) &&
                              (d.DocumentReferences.Count == 0 ||
                               d.DocumentReferences.FirstOrDefault().Type != DocumentType.PreliminaryInvoice))))
                               .OrderByDescending(d => d.DateCreated);
           var payments = preliminaryInvoices.ToList().Select(c=> c.ToSettlementsViewModel(_documentService)).Select(p=> new PaymentShortViewModel
           {
               Amount = p.Deduction,
               Date = p.PaymentDate,
               Client = p.ClientName
           });

            return new DashboardViewModel
            {
                Accounts = accounts,
                Payments = payments
            };
        }


        /// <summary>
        /// Get account forecast
        /// </summary>
        /// <returns>List of AccountForecastViewModel</returns>
        [HttpGet]
        [Route("api/Dashboard/Forecast")]
        public async Task<IEnumerable<AccountForecastViewModel>> Forecast()
        {
            var orgId = User.Identity.GetOrgId();
            var accountEntities = _bankAccountService.GetUserAccounts(orgId).ToList();
            var accounts = new List<AccountForecastViewModel>();
            var today = DateTime.Today;
            var tomorrow = DateTime.Today.AddDays(1);
            foreach (var account in accountEntities)
            {
                var exceed = await _bankAccountService.GetLimitExceedInfo(account.ID);
                accounts.Add(new AccountForecastViewModel
                {
                    Account = account.BankName.Name,
                    Number = account.AccountNumber,
                    Today = await _bankAccountService.GetBalance(account.ID, today),
                    Tomorrow = await _bankAccountService.GetBalance(account.ID, tomorrow),
                    Exceeding_date = exceed?.Date ?? new DateTime()
            });
            }
            return accounts;
        }
    }
}
