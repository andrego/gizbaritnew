﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Gizbarit.API.ProfilesMap;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.Utils;

namespace Gizbarit.API.Controllers.Documents
{
    [Authorize]
    public class InvoiceReceiptController : BaseDocumentController
    {
     
        public InvoiceReceiptController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            ICatalogService catalogService) : 
            base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
           
        }



        // GET: api/InvoiceReceipt/9fab6e14-de98-45fb-b683-be6379e245c9
        /// <summary>
        /// Get InvoiceReceipt document
        /// </summary>
        /// <param name="id">Id type Guid of InvoiceReceipt Document</param>
        /// <returns>model of InvoiceReceiptViewModel</returns>
        [HttpGet]
        [Route("api/InvoiceReceipt/{id}")]
        [ResponseType(typeof(InvoiceReceiptViewModel))]
        public IHttpActionResult Get(Guid id)
        {

            var org = _organizationService.GetCurrent();
            var document = org.Documents.FirstOrDefault(d => d.ID == id);
          
            if (document == null)
                return null;

            var mapperInvoiceReceipt = MapperInvoiceReceipt.GetInvoiceReceiptMapper(org, _paymentTypeService);
            var model = mapperInvoiceReceipt.Map<Document, InvoiceReceiptViewModel>(document);
           
            return Ok(model);
        }

        /// <summary>
        /// Insert InvoiceReceipt document
        /// </summary>
        /// <param name="model">model type InvoiceReceiptViewModel</param>
        /// <returns>InvoiceReceiptViewModel of new document if success</returns>
        // POST: api/InvoiceReceipt
        [HttpPost]
        [ResponseType(typeof(InvoiceReceiptViewModel))]
        [Route("api/InvoiceReceipt")]
        public async Task<IHttpActionResult> Create([FromBody] InvoiceReceiptViewModel model)
        {
            if (ModelIsValid(model))
            {
                _documentUtils.AddClient(model.BaseDocument.ClientInfo);
                var document = model.ToDocument(_currency, HttpContext.Current.Request.UserHostAddress);

                Document currentDocument;
                var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                                    !_documentService.GetUserDocuments()
                                        .Any(x=>x.ID == model.BaseDocument.Id);
                if (isNewDocument)
                {
                    currentDocument = document;
                    InitInvoiceReceipt(currentDocument);
                    currentDocument.Organization = _organizationService.GetCurrent();
                    currentDocument.Client = _organizationService.GetCurrent().Clients
                        .FirstOrDefault(c=>c.ID == currentDocument.ClientID);
                    currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.TaxInvoiceReceipt);

                    if (User.Identity.GetOrgType() == Enums.BusinessType.Exemption)
                        currentDocument.IsSmallBusiness = true;

                    _documentService.Create(currentDocument, true);
                }
                else
                {
                    currentDocument = _documentService.GetUserDocuments().FirstOrDefault(d=>d.ID == document.ID);
                    if (currentDocument == null)
                        return BadRequest();

                    currentDocument.Update(document);
                    if (currentDocument.InvoiceReceipt != null)
                        currentDocument.InvoiceReceipt.Paid = 0;

                    _documentUtils.CleanDocumentItems(currentDocument);
                    _documentUtils.CleanDocumentPayments(currentDocument);
                }

                _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);
                var distinctCatalogItems = model.
                    IncludedCatalogItems.Where(i => i.DocumentId != null && i.DocumentId != currentDocument.ID)
                    .GroupBy(i => i.DocumentId)
                    .Select(g => g.First());

                if (distinctCatalogItems.Any())
                {
                    foreach (var item in distinctCatalogItems)
                    {
                        _documentUtils.AddDocumentReferences(currentDocument, item.DocumentId,
                            DocumentType.PriceOffer, DocumentType.PreliminaryInvoice);
                    }
                }
                else if(currentDocument.DocumentReferences.Count > 0)
                {
                    currentDocument.InvoiceReceipt.Paid +=
                        currentDocument.DocumentReferences.Sum(r => r.CreditAmount) ?? 0;

                }

                foreach (var payment in model.Payments)
                {
                    currentDocument.InvoiceReceipt.Paid += payment.Sum;
                    _documentUtils.AdDocumentPayments(payment, currentDocument.ID
                        , currentDocument.ClientID);
                }

                currentDocument.InvoiceReceipt.Deduction = currentDocument.Total -
                                                           currentDocument.InvoiceReceipt.Paid;
                _documentService.Update(currentDocument,true);
                return Get(currentDocument.ID);

            }
            return BadRequest(ModelState);
        }

        [NonAction]
        public void InitInvoiceReceipt(Document d)
        {
            d.InvoiceReceipt = new InvoiceReceipt
            {
                Deduction = d.Total
            };
        }


        [NonAction]
        public bool ModelIsValid(InvoiceReceiptViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.TaxInvoiceReceipt,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }

                if (model.BaseDocument.PriceWithTax != model.PaymentsTotal)
                {
                    ModelState.AddModelError("", Translations.Errors.SumPaymentsHasToBeLess);
                }
            }
            return ModelState.IsValid;
        }
    }
}
