﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using AutoMapper;
using Gizbarit.API.ProfilesMap;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Models.Payment;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;



namespace Gizbarit.API.Controllers.Documents
{
    [Authorize]
    public class ReceiptController : BaseDocumentController
    {
      

        public ReceiptController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            ICatalogService catalogService) :
                base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
           
        }

        //GET: api/Receipt/2A6FFD96-D289-4402-ADB5-5BC690B87F6B
        /// <summary>
        /// Get Receipt document
        /// </summary>
        /// <param name="id">Id type Guid of Receipt Document</param>
        /// <returns>model of ReceiptViewModel</returns>
        [HttpGet]
        [ResponseType(typeof(ReceiptViewModel))]
        [Route("api/Receipt/{id}")]
        public IHttpActionResult Get(Guid id)
        {
            var org = _organizationService.GetCurrent();
            var document = org.Documents.FirstOrDefault(el => el.ID == id);

            if (document == null)
                return BadRequest("Document not found");
  
            var model = new ReceiptViewModel()
            {
               
                BaseDocument = document.ToBaseDocument(org),
                PaymentTypes = Getter.GetPaymentTypes(_paymentTypeService),
                Payments = document.Payments.ToPayments()
            };
            if (document.Receipt.ReferenceDocument != null)
            {
                var refDoc = org.Documents.FirstOrDefault(d => d.ID == document.Receipt.ReferenceDocument);
                if (refDoc != null)
                {
                    model.SelectedDocumentItem = Mapper.Map<Document, BaseDocument>(refDoc); // not sure for true mapping

                }
            }
            else
                model.OtherDescription = document.ExternalComments;
            
            return Ok(model);
        }

        //POST: api/Receipt
        /// <summary>
        /// Insert Receipt document
        /// </summary>
        /// <param name="model">model type ReceiptViewModel</param>
        /// <returns>ReceiptViewModel of new document if success</returns>

        [HttpPost]
        [Route("api/Receipt")]
        [ResponseType(typeof(ReceiptViewModel))]
        public async Task<IHttpActionResult> Create([FromBody]ReceiptViewModel model)
        {
            if (!ModelIsValid(model))
            {
                return BadRequest(ModelState);
            }
            _documentUtils.AddClient(model.BaseDocument.ClientInfo);
           var org = _organizationService.GetCurrent();

            var document = org.Documents.FirstOrDefault(d=>d.ID == model.BaseDocument.Id);
            double deduction = 0;

            if (document == null) // new document
            {
                Document refDocument = null;
                document = model.ToDocument(_currency);
                if (!string.IsNullOrEmpty(model.OtherDescription))
                {
                    document.ExternalComments = model.OtherDescription;
                }
                else
                {
                    refDocument = _documentService.Get(model.SelectedDocumentItem.Id);
                    if (refDocument == null)
                        return BadRequest();
                    try
                    {
                        switch (refDocument.DocumentType)
                        {
                            case DocumentType.TaxInvoice:
                                {
                                    deduction = ProcessTaxInvoiceDocument(document, refDocument, model.PaymentsTotal);
                                    break;
                                }
                            case DocumentType.PreliminaryInvoice:
                                {
                                    deduction = ProcessPreliminaryinvoice(document, refDocument, model.PaymentsTotal);
                                    break;
                                }
                        }
                    }
                    catch (InvalidProgramException)
                    {
                        ModelState.AddModelError("",
                            $"{Translations.Errors.SumPaymentsHasToBeLess}: {refDocument.Total}");

                        return BadRequest(ModelState);
                    }

                }

                document.Receipt = new Receipt()
                {
                    ReferenceDocument = refDocument?.ID,
                    Deduction = deduction
                };
                document.Number = _documentService.GetLastDocumentNumber(DocumentType.Receipt);
                _documentService.Create(document, true);

            }
            else
            {
                if (document.IsOriginalGenerated())
                    return BadRequest();


                if (!string.IsNullOrWhiteSpace(document.ExternalComments))
                    document.ExternalComments = model.OtherDescription;
                else
                {
                    var refDocument = _documentService.Get(model.SelectedDocumentItem.Id);
                    if (refDocument == null)
                        return BadRequest();

                    try
                    {
                        switch (refDocument.DocumentType)
                        {
                            case DocumentType.TaxInvoice:
                                {
                                    deduction = ProcessTaxInvoiceDocument(document, refDocument, model.PaymentsTotal);
                                    break;
                                }
                            case DocumentType.PreliminaryInvoice:
                                {
                                    deduction = ProcessPreliminaryinvoice(document, refDocument, model.PaymentsTotal);
                                    break;
                                }
                        }
                    }
                    catch (InvalidProgramException)
                    {
                        ModelState.AddModelError("",
                            $"{Translations.Errors.SumPaymentsHasToBeLess}: {refDocument.Total}");

                        return BadRequest();
                    }
                    document.Receipt.Deduction = deduction;
                }
                _documentService.Update(document, true);
            }
            SavePayments(model, document);
            return Get(document.ID);           
         
           
        }

        [NonAction]
        private void SavePayments(ReceiptViewModel model, Document document)
        {
            var documentPayments = document.Payments.ToList();
            foreach (var payment in documentPayments)
                _paymentService.Remove(payment, true);

            var receiptMapper = MapperReceipt.GetReceiptMapper(document);

            foreach (var payment in model.Payments)
            {
                _paymentService.Create(receiptMapper.Map<PaymentViewModel, Payment>(payment));
               
            }
            _paymentTypeService.Commit(true);
        }

        [NonAction]
        private double ProcessPreliminaryinvoice(Document document, Document referenceDocument, double totalPayments)
        {
            double amountToReduce = 0;
            if (!document.IsNew())
            {
                amountToReduce = document.Total;
            }

            if (referenceDocument.PreliminaryInvoice.Paid - amountToReduce + totalPayments > referenceDocument.Total)
            {
                throw new InvalidProgramException();
            }

            referenceDocument.PreliminaryInvoice.Paid -= amountToReduce;
            referenceDocument.PreliminaryInvoice.Paid += totalPayments;

            var deduction = referenceDocument.Total - referenceDocument.PreliminaryInvoice.Paid;
            if (referenceDocument.PreliminaryInvoice.Paid == referenceDocument.Total)
            {
                referenceDocument.IsPaid = true;
            }
            if (document.DocumentReferences.All(d => d.BaseDocumentId != referenceDocument.ID))
            {
                document.DocumentReferences.Add(new DocumentReference
                {
                    BaseDocumentId = referenceDocument.ID,
                    Type = referenceDocument.DocumentType,
                    CreditAmount = deduction
                });
            }

            return deduction;
        }

        [NonAction]
        private double ProcessTaxInvoiceDocument(Document document, Document referenceDocument, double totalPayments)

        {
            if (!referenceDocument.IsPaid)
            {
                double amountToReduce = 0;
                if (!document.IsNew())
                {
                    amountToReduce = document.Total;

                }

                if (referenceDocument.Invoice.Paid - amountToReduce + totalPayments > referenceDocument.Total)
                {
                    throw new InvalidProgramException();
                }

                referenceDocument.Invoice.Paid -= amountToReduce;

                referenceDocument.Invoice.Paid += totalPayments;

                var deduction = referenceDocument.Total - referenceDocument.Invoice.Paid;

                if (referenceDocument.Invoice.Paid == referenceDocument.Total)
                {
                    referenceDocument.IsPaid = true;
                }

                if (document.DocumentReferences.All(d => d.BaseDocumentId != referenceDocument.ID))
                {
                    document.DocumentReferences.Add(new DocumentReference
                    {
                        BaseDocumentId = referenceDocument.ID,
                        Type = referenceDocument.DocumentType,
                        CreditAmount = deduction
                    });
                }

                return deduction;
            }
            return 0;

        }

      
        [NonAction]
        public bool ModelIsValid(ReceiptViewModel model)
        {
            FilterModeState(ModelState, new[] { "model.BaseDocument.Subject" });
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.Receipt,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }

                if ((model.SelectedDocumentItem == null || model.SelectedDocumentItem.Id == default(Guid)) &&
                    String.IsNullOrEmpty(model.OtherDescription))
                {
                    ModelState.AddModelError("", Translations.Errors.SelectSourceDocument);
                }

                if (model.Payments.Any(p => p.PaymentType == 0))
                {
                    ModelState.AddModelError("", Translations.Errors.NoPaymentMethodSelected);
                }
            }
            return ModelState.IsValid;

        }
    }
}
