﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Gizbarit.API.ProfilesMap;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;

namespace Gizbarit.API.Controllers.Documents
{
    public class WaybillController : BaseDocumentController
    {
       

        public WaybillController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            ICatalogService catalogService) :
            base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
            
        }

        // GET: api/Waybill/9fab6e14-de98-45fb-b683-be6379e245c9
        /// <summary>
        /// Get Waybill document
        /// </summary>
        /// <param name="id">Id type Guid of Document</param>
        /// <returns>model of WayBillViewModel</returns>
        [HttpGet]
        [ResponseType(typeof(WayBillViewModel))]
        [Route("api/Waybill/{id}")]
        public IHttpActionResult Get(Guid id)
        {
            var org = _organizationService.GetCurrent();
            var document = org.Documents.FirstOrDefault(el => el.ID == id);
            if (document == null)
                return null;

            var mapperWaybill = MapperWaybill.GetWaybillMapper(org);
            var model = mapperWaybill.Map<Document, WayBillViewModel>(document);

            return Ok(model);
        }



        // POST: api/waybill
        /// <summary>
        /// Create waybill document
        /// </summary>
        /// <param name="model">WayBillViewModel model</param>
        /// <returns>Return created WayBillViewModel document in case of success.</returns>

        [HttpPost]
        [Route("api/Waybill")]
        [ResponseType(typeof (WayBillViewModel))]
        public async Task<IHttpActionResult> Create([FromBody] WayBillViewModel model)
        {
            if (ModelIsValid(model))
            {
                _documentUtils.AddClient(model.BaseDocument.ClientInfo);
                var document = model.ToDocument(_currency);

                Document currentDocument;
                var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                                    !_documentService.GetUserDocuments()
                                        .Any(x => x.ID == model.BaseDocument.Id);
                if (isNewDocument)
                {
                    currentDocument = document;
                    InitWayBill(currentDocument);
                    currentDocument.Organization = _organizationService.GetCurrent();
                    currentDocument.Client =
                        _organizationService.GetCurrent().Clients.FirstOrDefault(c => c.ID == currentDocument.ClientID);
                    currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.Waybill);

                    _documentService.Create(currentDocument, true);
                }
                else
                {
                    currentDocument = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == document.ID);
                    if (currentDocument == null)
                        return BadRequest("Document not found");

                    if (currentDocument.IsOriginalGenerated())
                        return BadRequest();

                    currentDocument.Update(document);
                    _documentUtils.CleanDocumentItems(currentDocument);
                }

                if (!CheckTaxCheme(model, document))
                {
                    ModelState.AddModelError("", Translations.Errors.DifferentTaxInclusionScheme);
                    return BadRequest(ModelState);
                }

                _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);
                _documentUtils.ProcessToPreliminaryInvoice(currentDocument);
                return Get(currentDocument.ID);

            }
            return BadRequest(ModelState);
        }

        [NonAction]
        private bool CheckTaxCheme(WayBillViewModel model, Document document)
        {
            var thisMonthPreliminaryInvoice =
                _documentService.GetAutoCreatedPreliminaryInvoice(model.BaseDocument.ClientInfo.Id,
                        model.BaseDocument.DateCreated);

            var isNewDocument = (thisMonthPreliminaryInvoice == null);
            if (isNewDocument)
                return true;

            return thisMonthPreliminaryInvoice.IsTaxIncluded == document.IsTaxIncluded;
        }

        [NonAction]
        public void InitWayBill(Document d)
        {
            d.Waybill = new Waybill();
        }

        [NonAction]
        public bool ModelIsValid(WayBillViewModel model)
        {
           
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.Waybill,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }
            }
            return ModelState.IsValid;
        }

    }
}
