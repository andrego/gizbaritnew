﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Gizbarit.API.ProfilesMap;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Other;

namespace Gizbarit.API.Controllers.Documents
{
    [Authorize]
    public class PriceOfferController : BaseDocumentController
    {
     
        public PriceOfferController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            ICatalogService catalogService

                                    ) : base(organizationService, currencyService,
                                        documentService, documentItemService,
                                        transactionService, bankAccountService,
                                        paymentTypeService, paymentService, advanceService, catalogService)
        {
          
        }

        //GET: api/PriceOffer/6DEEEBCE-B905-4B46-8093-25156F77DD41
        /// <summary>
        /// Get PriceOffer document
        /// </summary>
        /// <param name="id">Id type Guid of PriceOffer Document</param>
        /// <returns>PriceOfferViewModel of document</returns>
        [HttpGet]
        [ResponseType(typeof(PriceOfferViewModel))]
        [Route("api/PriceOffer/{id}")]
        public IHttpActionResult Get(Guid id)
        {
            var org = _organizationService.GetCurrent();
            var document = org.Documents.FirstOrDefault(el => el.ID == id);

            if (document == null)
                return null;

            var priceMapper = MapperPriceOffer.GetPriceOfferMapper(org, _organizationService);
            var model = priceMapper.Map<Document, PriceOfferViewModel>(document);
            return Ok(model);

        }

        //POST: api/PriceOffer
        /// <summary>
        /// Insert PriceOffer document
        /// </summary>
        /// <param name="model">model type PriceOfferViewModel</param>
        /// <returns>PriceOfferViewModel of new document if success</returns>
      
        [HttpPost]
        [Route("api/PriceOffer")]
        [ResponseType(typeof(PriceOfferViewModel))]
        public async Task<IHttpActionResult> Create([FromBody]PriceOfferViewModel model)
        {
            if (ModelIsValid(model))
            {
                
                var org = _organizationService.GetCurrent();
                _documentUtils.AddClient(model.BaseDocument.ClientInfo);

                if (model.BaseDocument.IsExport)
                {
                    var currencyFromDb = _currencyService.Get(model.BaseDocument.Currency);
                    if (currencyFromDb != null)
                        _currency = currencyFromDb;
                }

                var document = model.ToDocument(_currency);

                Document currentDocument;

                var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                    !_documentService.GetUserDocuments()
                                     .Any(x => x.ID == model.BaseDocument.Id);

                if (isNewDocument)
                {
                    currentDocument = document;
                    InitPriceOffer(currentDocument);
                    currentDocument.Organization = org;
                    currentDocument.Client =
                        org.Clients.FirstOrDefault(c => c.ID == currentDocument.ClientID);

                    currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.PriceOffer);
                    _documentService.Create(currentDocument, true);
                }
                else
                {
                    currentDocument = org.Documents.FirstOrDefault(d => d.ID == document.ID);
                    if (currentDocument == null)
                        return BadRequest();

                    if (currentDocument.InvoiceReceipt != null)
                    {
                        currentDocument.InvoiceReceipt.Paid = 0;
                    }

                    currentDocument.Update(document);
                    _documentUtils.CleanDocumentItems(currentDocument);
                }

                _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);
                _documentService.Update(currentDocument, true); 
                
                return Get(currentDocument.ID);
            }
            return BadRequest(ModelState);
        }

        [NonAction]
        public void InitPriceOffer(Document d)
        {
            d.PriceOffer = new PriceOffer();
        }

 
        [NonAction]
        public bool ModelIsValid(PriceOfferViewModel model)
        {

            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.PriceOffer,
                        model.BaseDocument.DateCreated))
                {

                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }
            }

            return ModelState.IsValid;
        }

    }
}
