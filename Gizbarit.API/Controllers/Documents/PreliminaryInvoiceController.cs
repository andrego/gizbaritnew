﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Gizbarit.API.ProfilesMap;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;

namespace Gizbarit.API.Controllers.Documents
{

    [Authorize]
    public class PreliminaryInvoiceController : BaseDocumentController
    {
     
        public PreliminaryInvoiceController(IOrganisationService organizationService, ICurrencyService currencyService,
           IDocumentService documentService, IDocumentItemService documentItemService,
           ITransactionService transactionService,
           IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
           IPaymentService paymentService, IAdvanceService advanceService,
            ICatalogService catalogService) :
           base(organizationService, currencyService,
               documentService, documentItemService,
               transactionService,
               bankAccountService, paymentTypeService,
               paymentService, advanceService, catalogService)
        {
           
        }
        // GET: api/PreliminaryInvoice/660448ff-7d87-487f-84c3-40bc661ff9b2
        /// <summary>
        /// Get PreliminaryInvoice document
        /// </summary>
        /// <param name="id">Id type Guid of PreliminaryInvoice Document</param>
        /// <returns>if success document type of PreliminaryInvoiceViewModel</returns>
        [HttpGet]
        [ResponseType(typeof(PreliminaryInvoiceViewModel))]
        [Route("api/PreliminaryInvoice/{id}")]
        public IHttpActionResult Get(Guid id)
        {
            var org = _organizationService.GetCurrent();
            var document = org.Documents.FirstOrDefault(el => el.ID == id);
            if (document == null)
                return null;

            var preliminaryInvoiceMapper = MapperPreliminaryInvoice.GetPreliminaryInvoiceMapper(org);
            var model = preliminaryInvoiceMapper.Map<Document, PreliminaryInvoiceViewModel>(document);

            return Ok(model);
        }



        /// <summary>
        /// Insert PreliminaryInvoice document
        /// </summary>
        /// <param name="model">model type PreliminaryInvoiceViewModel</param>
        /// <returns>PreliminaryInvoiceViewModel of new document if success</returns>
        [HttpPost]
        [ResponseType(typeof(PreliminaryInvoiceViewModel))]
        [Route("api/PreliminaryInvoice")]
        public async Task<IHttpActionResult> Create([FromBody] PreliminaryInvoiceViewModel model)
        {
            if (ModelIsValid(model))
            {
                _documentUtils.AddClient(model.BaseDocument.ClientInfo);
                var document = model.ToDocument(_currency);
                Document currentDocument;
                var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                                    !_documentService.GetUserDocuments()
                                        .Any(x => x.ID == model.BaseDocument.Id);

                if (isNewDocument)
                {
                    currentDocument = document;
                    InitPreliminaryInvoice(currentDocument);
                    currentDocument.Organization = _organizationService.GetCurrent();
                    currentDocument.Client =
                        _organizationService.GetCurrent().Clients.FirstOrDefault(c => c.ID == currentDocument.ClientID);
                    currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.PreliminaryInvoice);

                    _documentService.Create(currentDocument, true);
                }
                else
                {
                    currentDocument = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == document.ID);
                    if (currentDocument == null)
                        return BadRequest("Document not found");

                    currentDocument.Update(document);
                    _documentUtils.CleanDocumentItems(currentDocument);
                    _documentService.Commit(true);
                }

                _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);

                var distinctCatalogItems = model.IncludedCatalogItems.Where(i => i.DocumentId != null
                                                                                 && i.DocumentId != currentDocument.ID)
                    .GroupBy(i => i.DocumentId)
                    .Select(g=>g.First());

                if (distinctCatalogItems.Any())
                {
                    foreach (var item in distinctCatalogItems)
                    {
                        _documentUtils.AddDocumentReferences(currentDocument, item.DocumentId, DocumentType.PriceOffer, DocumentType.PreliminaryInvoice);
                    }
                }
                _documentService.Update(currentDocument, true);
                return Get(currentDocument.ID);
            }
            return BadRequest(ModelState);
        }


        [NonAction]
        public void InitPreliminaryInvoice(Document d)
        {
            d.PreliminaryInvoice = new PreliminaryInvoice { IsAutoCreated = false };
        }

        [NonAction]
        public bool ModelIsValid(PreliminaryInvoiceViewModel model)
        {
           
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.PreliminaryInvoice,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }
            }
            return ModelState.IsValid;
        }

    }
}
