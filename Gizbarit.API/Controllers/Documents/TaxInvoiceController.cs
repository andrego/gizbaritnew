﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Gizbarit.API.ProfilesMap;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;

namespace Gizbarit.API.Controllers.Documents
{
    [Authorize]
    public class TaxInvoiceController : BaseDocumentController
    {
        

        public TaxInvoiceController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            ICatalogService catalogService) :
            base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
            
        }



        // GET: api/TaxInvoice/9fab6e14-de98-45fb-b683-be6379e245c9
        /// <summary>
        /// Get TaxInvoice document
        /// </summary>
        /// <param name="id">Id type Guid of Document</param>
        /// <returns>model of TaxInvoiceViewModel</returns>
        [HttpGet]
        [Route("api/TaxInvoice/{id}")]
        [ResponseType(typeof(TaxInvoiceViewModel))]
        public IHttpActionResult Get(Guid id)
        {
            var org = _organizationService.GetCurrent();
            var document = org.Documents.FirstOrDefault(el => el.ID == id);
            if (document == null)
                return null;

            var mapperTaxInvoice = MapperTaxInvoices.GetTaxInvoicesMapper(org);
            var model = mapperTaxInvoice.Map<Document, TaxInvoiceViewModel>(document);

            return Ok(model);
        }



        // POST: api/TaxInvoice
        /// <summary>
        /// Create TaxInvoice document
        /// </summary>
        /// <param name="model">TaxInvoiceViewModel model</param>
        /// <returns>Return created TaxInvoiceViewModel document in case of success.</returns>
        [HttpPost]
        [Route("api/TaxInvoice")]
        [ResponseType(typeof(TaxInvoiceViewModel))]
        public async Task<IHttpActionResult> Create([FromBody] TaxInvoiceViewModel model)
        {
            if (ModelIsValid(model))
            {
                _documentUtils.AddClient(model.BaseDocument.ClientInfo);

                // add Currency To Document
                if (model.BaseDocument.IsExport)
                {
                    var currencyFromDb = _currencyService.Get(model.BaseDocument.Currency);
                    if (currencyFromDb != null)
                        _currency = currencyFromDb;
                }

                var document = model.ToDocument(_currency);
                Document currentDocument;
                var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                                    !_documentService.GetUserDocuments()
                                        .Any(x => x.ID == model.BaseDocument.Id);

                if (isNewDocument)
                {
                    currentDocument = document;
                    InitTaxInvoice(currentDocument);
                    currentDocument.Organization = _organizationService.GetCurrent();
                    currentDocument.Client =
                        _organizationService.GetCurrent().Clients.FirstOrDefault(c => c.ID == currentDocument.ClientID);
                    currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.TaxInvoice);

                    _documentService.Create(currentDocument, true);
                }
                else
                {
                    currentDocument = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == document.ID);
                    if (currentDocument == null)
                        return BadRequest("document not found");

                    if (currentDocument.IsOriginalGenerated())
                        return BadRequest("You can't edit document that was already send");

                    currentDocument.Update(document);
                    _documentUtils.CleanDocumentItems(currentDocument);
                }

                _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);

                var distinctCatalogItems =
                    model.IncludedCatalogItems.Where(i => i.DocumentId != null && i.DocumentId != currentDocument.ID)
                        .GroupBy(i => i.DocumentId)
                        .Select(g => g.First());

                if (distinctCatalogItems.Any())
                {
                    foreach (var item in distinctCatalogItems)
                    {
                        _documentUtils.AddDocumentReferences(currentDocument, item.DocumentId, DocumentType.PriceOffer, DocumentType.PreliminaryInvoice);
                    }
                }
                _documentService.Update(currentDocument, true);

                return Get(currentDocument.ID);
            }
            return BadRequest(ModelState);
        }

        [NonAction]
        public bool ModelIsValid(TaxInvoiceViewModel model)
        {
            
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.TaxInvoice,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }
            }
            return ModelState.IsValid;
        }

        [NonAction]
        public void InitTaxInvoice(Document d)
        {
            d.Invoice = new Invoice();
        }
    }
}
