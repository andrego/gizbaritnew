﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Models.Organisation;
using Gizbarit.DAL.Services.Organisation;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Models;

namespace Gizbarit.API.Controllers
{
    [System.Web.Http.Authorize]
    public class CatalogController : ApiController
    {
        private readonly IOrganisationService _organisationService;
        private readonly ICatalogService _catalogService;


        public CatalogController(IOrganisationService organisationService, ICatalogService catalogService)
        {
            _organisationService = organisationService;
            _catalogService = catalogService;

        }

        // GET: api/Catalog
        /// <summary>
        /// Get user catalog items
        /// </summary>
        /// <returns>IEnumerable of CatalogItemViewModel</returns>
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(IEnumerable<CatalogItemViewModel>))]
        public IHttpActionResult Get()
        {
            var org = _organisationService.GetCurrent();

            var items = Mapper.Map<IEnumerable<CatalogItem>, IEnumerable<CatalogItemViewModel>>(org.CatalogItems.Where(c=>c.Active));

            return Ok(items);
        }

        /// <summary>
        /// Verify catalog item quantity in Stock
        /// </summary>
        /// <returns>Http result code 200 and CatalogCountView if product exists, Http 404</returns>
        [System.Web.Http.Route("api/Catalog/VerifyCountInStock/")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(CatalogCountView))]
        public IHttpActionResult CatalogItemCount(string code)
        {
            if (string.IsNullOrEmpty(code))
                return BadRequest("code is null or empty");

            var org = _organisationService.GetCurrent();
            var catalogItems = org.StoreInvoices.SelectMany(i => i.Items.Where(o => o.CatalogItem.Code == code)).ToArray();

            if (!catalogItems.Any())
                return Content(HttpStatusCode.NoContent,"");

            var item = catalogItems.FirstOrDefault().ToViewModel();
            var count = catalogItems.Sum(i => i.Quantity);

            return Ok(new CatalogCountView()
            {
                Code = code,
                Count = count - item.UsedQuantity ?? 0
            });
        }

        // PUT: api/Catalog
        /// <summary>
        /// PUT item details
        /// </summary>
        /// <param name="item">item type CatalogItemViewModel</param>
        /// <returns>item type CatalogItemViewModel</returns>
        [System.Web.Http.HttpPut]
        [ResponseType(typeof(CatalogItemViewModel))]
        public IHttpActionResult Put([FromBody] CatalogItemViewModel item)
        {
            if (ModelState.IsValid)
            {
                var org = _organisationService.GetCurrent();
                CatalogItem changeItem = org.CatalogItems.FirstOrDefault(el => el.ID == item.ID);
                if (changeItem == null)
                    return BadRequest();

                var catalogItem = Mapper.Map<CatalogItemViewModel, CatalogItem>(item);
                Mapper.Map<CatalogItem, CatalogItem>(catalogItem, changeItem);

                _organisationService.Commit(true);
                return Ok(item);

            }
            return BadRequest(ModelState);
        }

        // GET: api/Catalog/5
        /// <summary>
        /// Get item details
        /// </summary>
        /// <param name="id">Id if existing catalog item</param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(CatalogItemViewModel))]
        public IHttpActionResult Get(int id)
        {

            var org = _organisationService.GetCurrent();
            var item = Mapper.Map<CatalogItem, CatalogItemViewModel>(org.CatalogItems.FirstOrDefault(m => m.ID == id));
            return Ok(item);
        }

        // POST: api/Catalog
        /// <summary>
        /// Create catalog item
        /// </summary>
        /// <param name="item">Catalog item model</param>
        /// <returns>Return created catalog item in case of success.</returns>
        [System.Web.Http.HttpPost]
        [ResponseType(typeof(CatalogItemViewModel))]
        public async Task<IHttpActionResult> Post([FromBody]CatalogItemViewModel item)
        {
            if (ModelState.IsValid)
            {
                var org = _organisationService.GetCurrent();

                var catalogItem = Mapper.Map<CatalogItemViewModel, CatalogItem>(item);

                org.CatalogItems.Add(catalogItem);
                _organisationService.Commit(true);
                return Get(catalogItem.ID);
            }
            return BadRequest(ModelState);
        }


        // DELETE: api/Catalog/5//
        /// <summary>
        /// delete catalog item
        /// </summary>
        /// <param name="id">Catalog item id</param>
        /// <returns>Return IHttpActionResult</returns>
        [System.Web.Http.HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {

            var organization = _organisationService.GetCurrent();
            var item = organization.CatalogItems.FirstOrDefault(m => m.ID == id);
            if (item != null)
            {
                _catalogService.Remove(item, true);
                return Ok();
            }
            return BadRequest();

        }
    }
}
