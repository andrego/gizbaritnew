﻿using System.Web.Http;

namespace Gizbarit.API.Controllers
{
    /// <summary>
    /// Just for test controller
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Test")]
    public class TestController : ApiController
    {

        // GET: api/Catalog
        /// <summary>
        /// Test Accept-Language header
        /// </summary>
        /// <returns>String 'April' in requested locale</returns>
        public IHttpActionResult Get()
        {
            return Ok(Translations.General.April);
        }
    }
}
