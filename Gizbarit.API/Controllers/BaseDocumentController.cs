﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;

namespace Gizbarit.API.Controllers
{
    public class BaseDocumentController:ApiController
    {
        protected readonly IOrganisationService _organizationService;
        protected readonly IDocumentService _documentService;
        protected readonly IDocumentItemService _documentItemService;
        protected readonly ITransactionService _transactionService;
        protected readonly IBankAccountService _bankAccountService;
        protected readonly IPaymentTypeService _paymentTypeService;
        protected readonly IPaymentService _paymentService;
        protected readonly IAdvanceService _advanceService;
        protected readonly ICurrencyService _currencyService;

        protected Currency _currency;
        protected readonly DocumentUtils _documentUtils;

        protected bool IsSend { get; set; }
        protected bool IsPrint { get; set; }
        protected bool IsSendCopy { get; set; }

        public BaseDocumentController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            ICatalogService catalogService)
        {
            _organizationService = organizationService;
            _documentService = documentService;
            _documentItemService = documentItemService;
            _transactionService = transactionService;
            _bankAccountService = bankAccountService;
            _paymentTypeService = paymentTypeService;
            _paymentService = paymentService;
            _advanceService = advanceService;
            _currencyService = currencyService;

            _currency = currencyService.Get("ILS");
            _documentUtils = new DocumentUtils(_organizationService, _documentService, _documentItemService,
                _transactionService, _advanceService, _bankAccountService, _paymentTypeService, _paymentService,
                catalogService, currencyService);
        }

        protected void FilterModeState(ModelStateDictionary modelState, string[] keysToNotValidate)
        {
            List<string> keysTobeRemoved = new List<string>();

            if (keysToNotValidate != null && keysToNotValidate.Any())
            {
                foreach (var key in keysToNotValidate)
                {
                    keysTobeRemoved.AddRange(
                        modelState.Where(x => String.Compare(key, 0, x.Key, 0, key.Length) == 0)
                            .Select(x => x.Key)
                            .ToList());
                }
            }
            foreach (var item in keysTobeRemoved)
            {
                modelState.Remove(item);
            }
        }

    }
}