﻿using System.Web.Http;
using Gizbarit.API.Results;
using Microsoft.Owin.Security.OAuth;

namespace Gizbarit.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SerializerSettings.DateFormatString = "dd.MM.yyyy";
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(
                new GizbaritDateTimeConverter());
        }
    }
}
