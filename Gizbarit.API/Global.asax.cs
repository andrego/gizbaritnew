﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.WebApi;
using Gizbarit.API.Infrastructure;

namespace Gizbarit.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            SetupIocContainer();
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            string culture = Request.UserLanguages?.FirstOrDefault();
            CultureInfo ci;
            if (culture == null)
            {
                ci = new CultureInfo("en-US");
            }
            else
            {
                try
                {
                    ci = new CultureInfo(culture);
                }
                catch (CultureNotFoundException)
                {
                    ci = new CultureInfo("en-US");
                }
            }
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
        }

        private static void SetupIocContainer()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModule<AutofacModule>();

            IContainer container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
