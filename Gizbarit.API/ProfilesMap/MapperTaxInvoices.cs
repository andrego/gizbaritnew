﻿using System.Linq;
using AutoMapper;
using AutoMapper.Configuration;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Utils;

namespace Gizbarit.API.ProfilesMap
{
    public static class MapperTaxInvoices
    {
        public static IMapper GetTaxInvoicesMapper(Organization organization)
        {
            var confExpressMap = new MapperConfigurationExpression();
            var buildMap = confExpressMap.CreateMap<Document, TaxInvoiceViewModel>();
            buildMap.ForMember(d => d.BaseDocument, opt => opt.MapFrom(el => el.ToBaseDocument(organization)));
            buildMap.ForMember(d => d.CatalogItems, opt => opt.UseValue(organization.CatalogItems.Select(c => c.ToCatalogItemViewModel()).ToList()));
            buildMap.ForMember(d => d.IncludedCatalogItems,
                opt => opt.MapFrom(d => d.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList()));
            buildMap.ForMember(d => d.PaymentDueDateDays, opt => opt.MapFrom(el => Getter.GetPaymentDueDays(el)));
            buildMap.ForMember(d => d.OfferExtras, opt => opt.MapFrom(el => el.InternalComments));


            var mapperConfiguration = new MapperConfiguration(confExpressMap);
            return mapperConfiguration.CreateMapper();
        }
    }
}