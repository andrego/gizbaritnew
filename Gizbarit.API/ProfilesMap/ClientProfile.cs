﻿using AutoMapper;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Organisation;

namespace Gizbarit.API.MapProfiles
{
    public class ClientProfile:Profile
    {
        public override string ProfileName => "ClientProfile";

        public ClientProfile()
        {
            CreateMap<Client, ClientViewModel>();
            CreateMap<ClientViewModel, Client>();

            CreateMap<Client, Client>()
                .ForMember(el => el.BalanceAdjusts, opt => opt.Ignore())
                .ForMember(el => el.Organization, opt => opt.Ignore())
                .ForMember(el => el.OrganizationID, opt => opt.Ignore())
                .ForMember(el => el.Documents, opt => opt.Ignore())
                .ForMember(el => el.Retainer, opt => opt.Ignore())
                .ForMember(el => el.Payments, opt => opt.Ignore())
                .ForMember(el=>el.DateCreated, opt=>opt.Ignore());
        }
    }
}