﻿using System.Linq;
using AutoMapper;
using AutoMapper.Configuration;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Utils;

namespace Gizbarit.API.ProfilesMap
{
    public static  class MapperInvoiceReceipt
    {
        public static IMapper GetInvoiceReceiptMapper(Organization org, IPaymentTypeService paymentTypeService)
        {
            var confExpressMap = new MapperConfigurationExpression();
            var buildMap = confExpressMap.CreateMap<Document, InvoiceReceiptViewModel>();
            buildMap.ForMember(d => d.BaseDocument, opt => opt.MapFrom(el => el.ToBaseDocument(org)));
            buildMap.ForMember(d => d.CatalogItems, opt => opt.UseValue(org.CatalogItems.Select(c=>c.ToCatalogItemViewModel()).ToList()));
            buildMap.ForMember(d => d.IncludedCatalogItems,
                  opt => opt.MapFrom(el => el.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList()));
            buildMap.ForMember(d => d.PaymentDueDateDays, opt => opt.MapFrom(el => Getter.GetPaymentDueDays(el)));
            buildMap.ForMember(d => d.PaymentTypes, opt => opt.UseValue(Getter.GetPaymentTypes(paymentTypeService)));
            buildMap.ForMember(d => d.Payments, opt => opt.MapFrom(el => el.Payments.ToPayments()));
            buildMap.ForMember(d => d.OfferExtras, opt => opt.MapFrom(el => el.InternalComments));
           


            var mapperConfiguration = new MapperConfiguration(confExpressMap);
            return mapperConfiguration.CreateMapper();
        }
    }
}