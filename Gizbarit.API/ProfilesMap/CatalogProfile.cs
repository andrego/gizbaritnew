﻿using AutoMapper;
using Gizbarit.DAL.Entities.Catalog;
using CatalogItemViewModel = Gizbarit.DAL.Models.Organisation.CatalogItemViewModel;


namespace Gizbarit.API.MapProfiles
{
    public class CatalogProfile:Profile
    {
        
        public override string ProfileName => "CatalogProfile";

        public CatalogProfile()
        {
            CreateMap<CatalogItem, CatalogItemViewModel>();
            CreateMap<CatalogItemViewModel, CatalogItem>();

            CreateMap<CatalogItem, CatalogItem>()
                .ForMember(el => el.Organization, opt => opt.Ignore())
                .ForMember(el=> el.OrganizationId, opt=>opt.Ignore());
        }
    }
}