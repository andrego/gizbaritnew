﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Models.Account.Bank;

namespace Gizbarit.API.ProfilesMap
{
    public class DashboardProfile:Profile
    {
        public override string ProfileName => "DashboardProfile";

        public DashboardProfile()
        {
            CreateMap<BankAccount, BankAccountShort>()
                .ForMember(s=>s.Number, o=>o.MapFrom(d=>d.AccountNumber))
                .ForMember(s=>s.Account, o=>o.MapFrom(d=>d.BankName.Name));

            CreateMap<BankTransaction, TransactionShortViewModel>()
                .ForMember(s => s.Currency, o => o.MapFrom(d => d.Currency.ID))
                .ForMember(s => s.Date, o => o.MapFrom(d => d.DateInserted))
                .ForMember(s => s.Description, o => o.MapFrom(d => d.Details));

        }
    }
}