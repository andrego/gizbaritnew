﻿using System;
using System.Linq;
using AutoMapper;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;

namespace Gizbarit.API.MapProfiles
{
    public class DocumentProfile:Profile
    {
        public override string ProfileName => "DocumentProfile";

        public DocumentProfile()
        {
            CreateMap<Document, DocumentSearchResult>()
                .ForMember(x => x.Type, opt => opt.MapFrom(el => el.DocumentType))
                .ForMember(x => x.ClientName, opt => opt.MapFrom(el => el.Client.Name))
                .ForMember(x => x.Date, opt => opt.MapFrom(el => el.DateCreated))
                .ForMember(x => x.Price, opt => opt.MapFrom(el => el.Total))
                .ForMember(x => x.IsUsed, opt => opt.MapFrom(el => el.Status == DocumentStatusType.Send))
                .ForMember(x => x.Subject,
                    opt => opt.MapFrom(el => el.DocumentType != DocumentType.Receipt
                        ? el.Subject
                        : el.Receipt.ReferenceDocument != null
                            ? String.Format("For {0} {1}",
                                el.DocumentReferences.FirstOrDefault(
                                    d => d.BaseDocumentId == el.Receipt.ReferenceDocument).BaseDocument.DocumentType ==
                                DocumentType.TaxInvoice
                                    ? "tax invoice #"
                                    : "preliminary invoice #",
                                el.DocumentReferences.FirstOrDefault(
                                    d => d.BaseDocumentId == el.Receipt.ReferenceDocument).BaseDocument.Number)
                            : el.ExternalComments));

            CreateMap<Document, BaseDocument>()
                .ForMember(x => x.PriceWithTax, opt => opt.MapFrom(el => el.Total));
        }
       
    }
}