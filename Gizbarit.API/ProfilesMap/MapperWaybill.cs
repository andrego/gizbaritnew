﻿using System.Linq;
using AutoMapper;
using AutoMapper.Configuration;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Documents;

namespace Gizbarit.API.ProfilesMap
{
    public static class MapperWaybill
    {
        public static IMapper GetWaybillMapper(Organization organization)
        {
            var confExpressMap = new MapperConfigurationExpression();
            var buildMap = confExpressMap.CreateMap<Document, WayBillViewModel>();
            buildMap.ForMember(d => d.BaseDocument, opt => opt.MapFrom(el => el.ToBaseDocument(organization)));
            buildMap.ForMember(d => d.CatalogItems, opt => opt.UseValue(organization.CatalogItems.Select(c => c.ToCatalogItemViewModel()).ToList()));
            buildMap.ForMember(d => d.IncludedCatalogItems,
                opt => opt.MapFrom(d => d.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList()));
           
            var mapperConfiguration = new MapperConfiguration(confExpressMap);
            return mapperConfiguration.CreateMapper();
        }
    }
}