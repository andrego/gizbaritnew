﻿using System.Linq;
using AutoMapper;
using AutoMapper.Configuration;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Models.Documents.Related;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Utils;

namespace Gizbarit.API.MapProfiles
{
    public static class MapperDocument
    {

        public static IMapper GetDocumentReceiptMapper(MapperDocumentType docType, IDocumentService docService)
        {
            var confExpressMap = new MapperConfigurationExpression();
            var buidMap = confExpressMap.CreateMap<Document, LinkedDocumentReceipt>();
            buidMap.ForMember(x => x.TotalPrice, opt => opt.MapFrom(el => el.TotalwithoutTax));
            buidMap.ForMember(x => x.PriceWithTax, opt => opt.MapFrom(el => el.Total));
            switch (docType)
            {
                case MapperDocumentType.LinkedSelectedDocument:
                    {
                        buidMap.ForMember(x => x.Paid,
                            opt => opt.MapFrom(el => (el.DocumentType == DocumentType.PreliminaryInvoice
                                ? el.PreliminaryInvoice.Paid
                                : el.Invoice.Paid) + docService.GetInvoiceTotalPrepaymentSum(el.ID)));
                        break;
                    }

                case MapperDocumentType.LinkedPreliminaryInvoices:
                    {
                        buidMap.ForMember(x => x.Paid, opt => opt.MapFrom(el => el.PreliminaryInvoice.Paid));
                        break;
                    }

                case MapperDocumentType.LinkedTaxInvoices:
                    {
                        buidMap.ForMember(x => x.Paid,
                            opt =>
                                opt.MapFrom(el => el.Invoice.Paid + docService.GetInvoiceTotalPrepaymentSum(el.ID)));
                        break;
                    }
            }
            var mapperConfiguration = new MapperConfiguration(confExpressMap);
            return mapperConfiguration.CreateMapper();
        }


        public static IMapper GetDocumentPreliminaryInvoicesMapper(MapperDocumentType docType, IDocumentService docService, Organization org)
        {
           
            var confExpressMap = new MapperConfigurationExpression();
            var buidMap = confExpressMap.CreateMap<Document, LinkedPreliminaryInvoice>();
            
            buidMap.ForMember(x => x.OfferExtras, opt => opt.MapFrom(el => el.InternalComments));
            buidMap.ForMember(x => x.PaymentDueDateDays, opt => opt.MapFrom(el => Getter.GetPaymentDueDays(el)));
            buidMap.ForMember(x => x.IncludedCatalogItems, opt => opt.MapFrom(el => el.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList()));
            buidMap.ForMember(x => x.BaseDocument, opt => opt.MapFrom(el => el.ToBaseDocument(org)));
            if (docType == MapperDocumentType.PreliminaryInvoices)
                buidMap.ForMember(x => x.Paid, opt => opt.MapFrom(el => docService.GetDocumentReceiptsSum(el.ID)));
                       
    
            var mapperConfiguration = new MapperConfiguration(confExpressMap);
            
            return mapperConfiguration.CreateMapper();
        }
    }

    public enum MapperDocumentType
    {
        PriceOffers,
        PreliminaryInvoices,
        LinkedSelectedDocument,
        LinkedPreliminaryInvoices,
        LinkedTaxInvoices

    }
}