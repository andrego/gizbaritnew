﻿using AutoMapper;
using AutoMapper.Configuration;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Models.Payment;

namespace Gizbarit.API.ProfilesMap
{
    public static class MapperReceipt
    {

        public static IMapper GetReceiptMapper(Document document)
        {
            var confExpressMap = new MapperConfigurationExpression();
            var buildMap = confExpressMap.CreateMap<PaymentViewModel,Payment>();
            buildMap.ForMember(d => d.Amount, opt => opt.MapFrom(el => el.Sum));
            buildMap.ForMember(d => d.DocumentID, opt => opt.UseValue(document.ID));
            buildMap.ForMember(d => d.Date, opt => opt.MapFrom(el => el.DateOperation));
            buildMap.ForMember(d => d.PaymentNumber, opt=>opt.MapFrom(el => el.CheckNumber));
            buildMap.ForMember(d => d.PayerID, opt => opt.UseValue(document.ClientID));

            var mapperConfiguration = new MapperConfiguration(confExpressMap);
            return mapperConfiguration.CreateMapper();

           
        }
    }
}