﻿using AutoMapper;
using Gizbarit.API.MapProfiles;
using Gizbarit.API.ProfilesMap;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Gizbarit.API.Startup))]

namespace Gizbarit.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            ConfigureAuth(app);
            ConfigAutoMap();
        }

        private void ConfigAutoMap()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<CatalogProfile>();
                cfg.AddProfile<ClientProfile>();
                cfg.AddProfile<DocumentProfile>();
                cfg.AddProfile<DashboardProfile>();
            });
        

        }
    }
}
