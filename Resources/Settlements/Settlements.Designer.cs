﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Translations {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Settlements {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Settlements() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Settlements.Settlements", typeof(Settlements).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Balance due.
        /// </summary>
        public static string BalanceDue {
            get {
                return ResourceManager.GetString("BalanceDue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Check number.
        /// </summary>
        public static string CheckNo {
            get {
                return ResourceManager.GetString("CheckNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Client.
        /// </summary>
        public static string Client {
            get {
                return ResourceManager.GetString("Client", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Content of the preliminary invoice.
        /// </summary>
        public static string Content {
            get {
                return ResourceManager.GetString("Content", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delivery subject.
        /// </summary>
        public static string DeliverySubject {
            get {
                return ResourceManager.GetString("DeliverySubject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Supplier name.
        /// </summary>
        public static string EnterName {
            get {
                return ResourceManager.GetString("EnterName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invoice amount.
        /// </summary>
        public static string ExportInvoiceAmount {
            get {
                return ResourceManager.GetString("ExportInvoiceAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Receipt amount.
        /// </summary>
        public static string ExportReceiptAmount {
            get {
                return ResourceManager.GetString("ExportReceiptAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to invoice.
        /// </summary>
        public static string Invoice {
            get {
                return ResourceManager.GetString("Invoice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invoice amount.
        /// </summary>
        public static string InvoiceAmount {
            get {
                return ResourceManager.GetString("InvoiceAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invoice date.
        /// </summary>
        public static string InvoiceDate {
            get {
                return ResourceManager.GetString("InvoiceDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invoice number.
        /// </summary>
        public static string InvoiceNo {
            get {
                return ResourceManager.GetString("InvoiceNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Opening Date.
        /// </summary>
        public static string OpeningDate {
            get {
                return ResourceManager.GetString("OpeningDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create an order.
        /// </summary>
        public static string OrderProduct {
            get {
                return ResourceManager.GetString("OrderProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Paid.
        /// </summary>
        public static string Paid {
            get {
                return ResourceManager.GetString("Paid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment amount.
        /// </summary>
        public static string PaymentAmount {
            get {
                return ResourceManager.GetString("PaymentAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date of payment.
        /// </summary>
        public static string PaymentDate {
            get {
                return ResourceManager.GetString("PaymentDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preliminary invoice amount.
        /// </summary>
        public static string PreliminaryInvoiceAmount {
            get {
                return ResourceManager.GetString("PreliminaryInvoiceAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preliminary invoice date.
        /// </summary>
        public static string PreliminaryInvoiceDate {
            get {
                return ResourceManager.GetString("PreliminaryInvoiceDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preliminary invoice.
        /// </summary>
        public static string PreliminaryInvoiceNo {
            get {
                return ResourceManager.GetString("PreliminaryInvoiceNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preliminary invoice.
        /// </summary>
        public static string PreliminaryInvoiceNumber {
            get {
                return ResourceManager.GetString("PreliminaryInvoiceNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prepayment amount.
        /// </summary>
        public static string PrepaymentAmount {
            get {
                return ResourceManager.GetString("PrepaymentAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Receipt amount.
        /// </summary>
        public static string ReceiptAmount {
            get {
                return ResourceManager.GetString("ReceiptAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select or enter invoce number.
        /// </summary>
        public static string SelectInvoiceNo {
            get {
                return ResourceManager.GetString("SelectInvoiceNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose waybill number.
        /// </summary>
        public static string SelectWaybillNo {
            get {
                return ResourceManager.GetString("SelectWaybillNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Supplier.
        /// </summary>
        public static string Supplier {
            get {
                return ResourceManager.GetString("Supplier", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address.
        /// </summary>
        public static string SupplierAddress {
            get {
                return ResourceManager.GetString("SupplierAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone number.
        /// </summary>
        public static string SupplierPhone {
            get {
                return ResourceManager.GetString("SupplierPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tax invoice.
        /// </summary>
        public static string TaxInvoice {
            get {
                return ResourceManager.GetString("TaxInvoice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tax invoice amount.
        /// </summary>
        public static string TaxInvoiceAmount {
            get {
                return ResourceManager.GetString("TaxInvoiceAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to tax invoice-receipt.
        /// </summary>
        public static string TaxInvoiceReceipt {
            get {
                return ResourceManager.GetString("TaxInvoiceReceipt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total include past year.
        /// </summary>
        public static string TotalIncludePastYear {
            get {
                return ResourceManager.GetString("TotalIncludePastYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Waybill amount.
        /// </summary>
        public static string WaybillAmount {
            get {
                return ResourceManager.GetString("WaybillAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Waybill date.
        /// </summary>
        public static string WaybillDate {
            get {
                return ResourceManager.GetString("WaybillDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Waybill number.
        /// </summary>
        public static string WaybillNo {
            get {
                return ResourceManager.GetString("WaybillNo", resourceCulture);
            }
        }
    }
}
