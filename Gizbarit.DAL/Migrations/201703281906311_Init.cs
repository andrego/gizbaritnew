namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PreliminaryInvoices", newName: "ProformaInvoices");
            RenameTable(name: "dbo.Logs", newName: "Log");
            DropForeignKey("dbo.SupplierInvoices", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierReceipts", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierWaybills", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierReceipts", "SupplierInvoiceID", "dbo.SupplierInvoices");
            DropForeignKey("dbo.SupplierInvoices", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.SupplierReceipts", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.Suppliers", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.SupplierWaybills", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.Payments", "PayerID", "dbo.Clients");
            DropForeignKey("dbo.DocumentItems", "WaybillID", "dbo.Waybills");
            DropForeignKey("dbo.DocumentReferences", "BaseDocumentId", "dbo.Documents");
            DropForeignKey("dbo.PriceOffers", "ID", "dbo.Documents");
            DropForeignKey("dbo.Waybills", "ID", "dbo.Documents");
            DropForeignKey("dbo.DocumentItems", "Document_ID", "dbo.Documents");
            DropForeignKey("dbo.Invoices", "ID", "dbo.Documents");
            DropForeignKey("dbo.InvoiceReceipts", "ID", "dbo.Documents");
            DropForeignKey("dbo.Receipts", "ID", "dbo.Documents");
            DropForeignKey("dbo.Organizations", "BusinessTypeId", "dbo.BusinessTypes");
            DropIndex("dbo.Documents", new[] { "ClientID" });
            DropIndex("dbo.Organizations", new[] { "BusinessTypeId" });
            DropIndex("dbo.SupplierInvoices", new[] { "OrganizationID" });
            DropIndex("dbo.SupplierInvoices", new[] { "SupplierID" });
            DropIndex("dbo.SupplierReceipts", new[] { "OrganizationID" });
            DropIndex("dbo.SupplierReceipts", new[] { "SupplierID" });
            DropIndex("dbo.SupplierReceipts", new[] { "SupplierInvoiceID" });
            DropIndex("dbo.Suppliers", new[] { "OrganizationID" });
            DropIndex("dbo.SupplierWaybills", new[] { "OrganizationID" });
            DropIndex("dbo.SupplierWaybills", new[] { "SupplierID" });
            DropIndex("dbo.Payments", new[] { "PayerID" });
            DropIndex("dbo.DocumentItems", new[] { "WaybillID" });
            DropIndex("dbo.DocumentItems", new[] { "Document_ID" });
            DropIndex("dbo.Waybills", new[] { "ID" });
            DropIndex("dbo.DocumentReferences", new[] { "BaseDocumentId" });
            DropIndex("dbo.Invoices", new[] { "ID" });
            DropIndex("dbo.InvoiceReceipts", new[] { "ID" });
            DropIndex("dbo.PriceOffers", new[] { "ID" });
            DropIndex("dbo.Receipts", new[] { "ID" });
            DropIndex("dbo.Contents", "IX_OrgId_DocId");
            RenameColumn(table: "dbo.DocumentReferences", name: "ReferenceDocumentId", newName: "BaseDocument");
            RenameIndex(table: "dbo.DocumentReferences", name: "IX_ReferenceDocumentId", newName: "IX_BaseDocument");
            DropPrimaryKey("dbo.BusinessTypes");
            DropPrimaryKey("dbo.DocumentReferences");
            DropPrimaryKey("dbo.Invoices");
            DropPrimaryKey("dbo.InvoiceReceipts");
            DropPrimaryKey("dbo.Receipts");
            CreateTable(
                "dbo.InvoiceCredit",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Number = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ID, t.Number })
                .ForeignKey("dbo.Documents", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceOrder",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Number = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Documents", t => t.ID, cascadeDelete: true)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceQuote",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Number = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Documents", t => t.ID, cascadeDelete: true)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceShip",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Number = c.Long(nullable: false),
                        DepartureTime = c.Time(precision: 0),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Documents", t => t.ID, cascadeDelete: true)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.LogErrors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(),
                        Message = c.String(),
                        Date = c.DateTime(),
                        OrgId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Advances", "InvoiceReceiptNumber", c => c.Int());
            AddColumn("dbo.Advances", "InvoiceSupplierNumber", c => c.Int());
            AddColumn("dbo.Documents", "RoundAmount", c => c.Double());
            AddColumn("dbo.Documents", "IsNominal", c => c.Boolean());
            AddColumn("dbo.Organizations", "ExpirationDate", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.DocumentReferences", "ReferenceDocument", c => c.Guid(nullable: false));
            AddColumn("dbo.Invoices", "Number", c => c.Long(nullable: false));
            AddColumn("dbo.Invoices", "OrganizationID", c => c.Int(nullable: false));
            AddColumn("dbo.Invoices", "Document_ID", c => c.Guid(nullable: false));
            AddColumn("dbo.InvoiceReceipts", "Number", c => c.Long(nullable: false));
            AddColumn("dbo.InvoiceReceipts", "OrganizationID", c => c.Int(nullable: false));
            AddColumn("dbo.InvoiceReceipts", "Document_ID", c => c.Guid(nullable: false));
            AddColumn("dbo.ProformaInvoices", "Number", c => c.Long(nullable: false));
            AddColumn("dbo.Receipts", "Number", c => c.Long(nullable: false));
            AddColumn("dbo.Receipts", "OrganizationID", c => c.Int(nullable: false));
            AddColumn("dbo.Receipts", "Document_ID", c => c.Guid(nullable: false));
            AddColumn("dbo.Log", "Code", c => c.Byte(nullable: false));
            AddColumn("dbo.Log", "UserId", c => c.Int(nullable: false));
            AddColumn("dbo.Log", "AdditionalInfo", c => c.String(maxLength: 50));
            AlterColumn("dbo.Documents", "Subject", c => c.String(maxLength: 50));
            AlterColumn("dbo.Documents", "ClientID", c => c.Int());
            AlterColumn("dbo.Documents", "Total", c => c.Double());
            AlterColumn("dbo.Documents", "TaxPercentage", c => c.Double());
            AlterColumn("dbo.Documents", "TotalTaxAmount", c => c.Double());
            AlterColumn("dbo.Documents", "Status", c => c.Byte());
            AlterColumn("dbo.Documents", "Discount", c => c.Double());
            AlterColumn("dbo.Documents", "Number", c => c.Long());
            AlterColumn("dbo.Organizations", "TaxRate", c => c.Double());
            AlterColumn("dbo.Organizations", "BusinessTypeId", c => c.Byte(nullable: false));
            AlterColumn("dbo.BusinessTypes", "ID", c => c.Byte(nullable: false, identity: true));
            AlterColumn("dbo.CatalogItems", "Name", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.Payments", "PayerID", c => c.String(maxLength: 50));
            AlterColumn("dbo.DocumentItems", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.DocumentItems", "TotalTax", c => c.Double());
            AlterColumn("dbo.DocumentItems", "Code", c => c.String(maxLength: 15));
            AlterColumn("dbo.DocumentReferences", "Type", c => c.Short(nullable: false));
            AlterColumn("dbo.Log", "Date", c => c.DateTime(nullable: false));
            AddPrimaryKey("dbo.BusinessTypes", "ID");
            AddPrimaryKey("dbo.DocumentReferences", new[] { "BaseDocument", "ReferenceDocument" });
            AddPrimaryKey("dbo.Invoices", new[] { "Number", "OrganizationID" });
            AddPrimaryKey("dbo.InvoiceReceipts", new[] { "Number", "OrganizationID" });
            AddPrimaryKey("dbo.Receipts", new[] { "Number", "OrganizationID" });
            CreateIndex("dbo.Documents", "ClientID");
            CreateIndex("dbo.Organizations", "BusinessTypeId");
            CreateIndex("dbo.Invoices", "Document_ID");
            CreateIndex("dbo.InvoiceReceipts", "Document_ID");
            CreateIndex("dbo.Receipts", "Document_ID");
            AddForeignKey("dbo.Invoices", "Document_ID", "dbo.Documents", "ID", cascadeDelete: true);
            AddForeignKey("dbo.InvoiceReceipts", "Document_ID", "dbo.Documents", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Receipts", "Document_ID", "dbo.Documents", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Organizations", "BusinessTypeId", "dbo.BusinessTypes", "ID", cascadeDelete: true);
            DropColumn("dbo.Advances", "ForDocumentType");
            DropColumn("dbo.Advances", "SupplierInvoiceNumber");
            DropColumn("dbo.Documents", "TotalWithoutRounding");
            DropColumn("dbo.Documents", "IsPaid");
            DropColumn("dbo.Documents", "IsPaymentProcessed");
            DropColumn("dbo.Documents", "IsTaxIncluded");
            DropColumn("dbo.Documents", "IsRounded");
            DropColumn("dbo.Documents", "IsSmallBusiness");
            DropColumn("dbo.Organizations", "NameEng");
            DropColumn("dbo.Organizations", "AddressEng");
            DropColumn("dbo.Organizations", "PaidPeriodExpirationDate");
            DropColumn("dbo.Organizations", "TrialPeriodExpirationDate");
            DropColumn("dbo.Organizations", "RegisterType");
            DropColumn("dbo.Organizations", "Status",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DefaultValue", "Active" },
                });
            DropColumn("dbo.CatalogItems", "ImagePath");
            DropColumn("dbo.DocumentItems", "WaybillID");
            DropColumn("dbo.DocumentItems", "ImagePath");
            DropColumn("dbo.DocumentItems", "Document_ID");
            DropColumn("dbo.DocumentReferences", "BaseDocumentId");
            DropColumn("dbo.Log", "Type");
            DropColumn("dbo.Log", "Source");
            DropColumn("dbo.Log", "Message");
            DropColumn("dbo.Log", "OrgId");
            DropTable("dbo.SupplierInvoices");
            DropTable("dbo.SupplierReceipts");
            DropTable("dbo.Suppliers");
            DropTable("dbo.SupplierWaybills");
            DropTable("dbo.Waybills");
            DropTable("dbo.PriceOffers");
            DropTable("dbo.Contents");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OrganizationId = c.Int(),
                        DocumentId = c.Guid(),
                        Name = c.String(nullable: false, maxLength: 256),
                        Category = c.Int(nullable: false),
                        Type = c.String(nullable: false, maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        Expired = c.DateTime(nullable: false),
                        Description = c.String(),
                        Data = c.Binary(),
                        ExtraData = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PriceOffers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Waybills",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DepartureTime = c.Time(precision: 0),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierWaybills",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SupplyDescription = c.String(maxLength: 400),
                        Number = c.Long(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Amount = c.Double(nullable: false),
                        OrganizationID = c.Int(nullable: false),
                        SupplierID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150),
                        Phone = c.String(nullable: false, maxLength: 20),
                        Address = c.String(nullable: false, maxLength: 400),
                        OrganizationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierReceipts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Amount = c.Double(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Number = c.String(maxLength: 50),
                        OrganizationID = c.Int(nullable: false),
                        SupplierID = c.Int(nullable: false),
                        SupplierInvoiceID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierInvoices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Number = c.Long(nullable: false),
                        Amount = c.Double(nullable: false),
                        IsConfirmed = c.Boolean(nullable: false),
                        Description = c.String(maxLength: 500),
                        OrganizationID = c.Int(nullable: false),
                        SupplierID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Log", "OrgId", c => c.Int());
            AddColumn("dbo.Log", "Message", c => c.String());
            AddColumn("dbo.Log", "Source", c => c.String());
            AddColumn("dbo.Log", "Type", c => c.Int());
            AddColumn("dbo.DocumentReferences", "BaseDocumentId", c => c.Guid(nullable: false));
            AddColumn("dbo.DocumentItems", "Document_ID", c => c.Guid());
            AddColumn("dbo.DocumentItems", "ImagePath", c => c.String());
            AddColumn("dbo.DocumentItems", "WaybillID", c => c.Guid());
            AddColumn("dbo.CatalogItems", "ImagePath", c => c.String());
            AddColumn("dbo.Organizations", "Status", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "Active")
                    },
                }));
            AddColumn("dbo.Organizations", "RegisterType", c => c.Int(nullable: false));
            AddColumn("dbo.Organizations", "TrialPeriodExpirationDate", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.Organizations", "PaidPeriodExpirationDate", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.Organizations", "AddressEng", c => c.String());
            AddColumn("dbo.Organizations", "NameEng", c => c.String(maxLength: 50));
            AddColumn("dbo.Documents", "IsSmallBusiness", c => c.Boolean(nullable: false));
            AddColumn("dbo.Documents", "IsRounded", c => c.Boolean(nullable: false));
            AddColumn("dbo.Documents", "IsTaxIncluded", c => c.Boolean(nullable: false));
            AddColumn("dbo.Documents", "IsPaymentProcessed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Documents", "IsPaid", c => c.Boolean(nullable: false));
            AddColumn("dbo.Documents", "TotalWithoutRounding", c => c.Double(nullable: false));
            AddColumn("dbo.Advances", "SupplierInvoiceNumber", c => c.Int());
            AddColumn("dbo.Advances", "ForDocumentType", c => c.Int());
            DropForeignKey("dbo.Organizations", "BusinessTypeId", "dbo.BusinessTypes");
            DropForeignKey("dbo.Receipts", "Document_ID", "dbo.Documents");
            DropForeignKey("dbo.InvoiceReceipts", "Document_ID", "dbo.Documents");
            DropForeignKey("dbo.Invoices", "Document_ID", "dbo.Documents");
            DropForeignKey("dbo.InvoiceShip", "ID", "dbo.Documents");
            DropForeignKey("dbo.InvoiceQuote", "ID", "dbo.Documents");
            DropForeignKey("dbo.InvoiceOrder", "ID", "dbo.Documents");
            DropForeignKey("dbo.InvoiceCredit", "ID", "dbo.Documents");
            DropIndex("dbo.Receipts", new[] { "Document_ID" });
            DropIndex("dbo.InvoiceShip", new[] { "ID" });
            DropIndex("dbo.InvoiceReceipts", new[] { "Document_ID" });
            DropIndex("dbo.InvoiceQuote", new[] { "ID" });
            DropIndex("dbo.InvoiceOrder", new[] { "ID" });
            DropIndex("dbo.InvoiceCredit", new[] { "ID" });
            DropIndex("dbo.Invoices", new[] { "Document_ID" });
            DropIndex("dbo.Organizations", new[] { "BusinessTypeId" });
            DropIndex("dbo.Documents", new[] { "ClientID" });
            DropPrimaryKey("dbo.Receipts");
            DropPrimaryKey("dbo.InvoiceReceipts");
            DropPrimaryKey("dbo.Invoices");
            DropPrimaryKey("dbo.DocumentReferences");
            DropPrimaryKey("dbo.BusinessTypes");
            AlterColumn("dbo.Log", "Date", c => c.DateTime());
            AlterColumn("dbo.DocumentReferences", "Type", c => c.Int(nullable: false));
            AlterColumn("dbo.DocumentItems", "Code", c => c.String(maxLength: 50));
            AlterColumn("dbo.DocumentItems", "TotalTax", c => c.Double(nullable: false));
            AlterColumn("dbo.DocumentItems", "Name", c => c.String(nullable: false, maxLength: 3000));
            AlterColumn("dbo.Payments", "PayerID", c => c.Int());
            AlterColumn("dbo.CatalogItems", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.BusinessTypes", "ID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Organizations", "BusinessTypeId", c => c.Int(nullable: false));
            AlterColumn("dbo.Organizations", "TaxRate", c => c.Double(nullable: false));
            AlterColumn("dbo.Documents", "Number", c => c.Long(nullable: false));
            AlterColumn("dbo.Documents", "Discount", c => c.Double(nullable: false));
            AlterColumn("dbo.Documents", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.Documents", "TotalTaxAmount", c => c.Double(nullable: false));
            AlterColumn("dbo.Documents", "TaxPercentage", c => c.Double(nullable: false));
            AlterColumn("dbo.Documents", "Total", c => c.Double(nullable: false));
            AlterColumn("dbo.Documents", "ClientID", c => c.Int(nullable: false));
            AlterColumn("dbo.Documents", "Subject", c => c.String(maxLength: 4000));
            DropColumn("dbo.Log", "AdditionalInfo");
            DropColumn("dbo.Log", "UserId");
            DropColumn("dbo.Log", "Code");
            DropColumn("dbo.Receipts", "Document_ID");
            DropColumn("dbo.Receipts", "OrganizationID");
            DropColumn("dbo.Receipts", "Number");
            DropColumn("dbo.ProformaInvoices", "Number");
            DropColumn("dbo.InvoiceReceipts", "Document_ID");
            DropColumn("dbo.InvoiceReceipts", "OrganizationID");
            DropColumn("dbo.InvoiceReceipts", "Number");
            DropColumn("dbo.Invoices", "Document_ID");
            DropColumn("dbo.Invoices", "OrganizationID");
            DropColumn("dbo.Invoices", "Number");
            DropColumn("dbo.DocumentReferences", "ReferenceDocument");
            DropColumn("dbo.Organizations", "ExpirationDate");
            DropColumn("dbo.Documents", "IsNominal");
            DropColumn("dbo.Documents", "RoundAmount");
            DropColumn("dbo.Advances", "InvoiceSupplierNumber");
            DropColumn("dbo.Advances", "InvoiceReceiptNumber");
            DropTable("dbo.LogErrors");
            DropTable("dbo.InvoiceShip");
            DropTable("dbo.InvoiceQuote");
            DropTable("dbo.InvoiceOrder");
            DropTable("dbo.InvoiceCredit");
            AddPrimaryKey("dbo.Receipts", "ID");
            AddPrimaryKey("dbo.InvoiceReceipts", "ID");
            AddPrimaryKey("dbo.Invoices", "ID");
            AddPrimaryKey("dbo.DocumentReferences", new[] { "BaseDocumentId", "ReferenceDocumentId" });
            AddPrimaryKey("dbo.BusinessTypes", "ID");
            RenameIndex(table: "dbo.DocumentReferences", name: "IX_BaseDocument", newName: "IX_ReferenceDocumentId");
            RenameColumn(table: "dbo.DocumentReferences", name: "BaseDocument", newName: "ReferenceDocumentId");
            CreateIndex("dbo.Contents", new[] { "OrganizationId", "DocumentId" }, name: "IX_OrgId_DocId");
            CreateIndex("dbo.Receipts", "ID");
            CreateIndex("dbo.PriceOffers", "ID");
            CreateIndex("dbo.InvoiceReceipts", "ID");
            CreateIndex("dbo.Invoices", "ID");
            CreateIndex("dbo.DocumentReferences", "BaseDocumentId");
            CreateIndex("dbo.Waybills", "ID");
            CreateIndex("dbo.DocumentItems", "Document_ID");
            CreateIndex("dbo.DocumentItems", "WaybillID");
            CreateIndex("dbo.Payments", "PayerID");
            CreateIndex("dbo.SupplierWaybills", "SupplierID");
            CreateIndex("dbo.SupplierWaybills", "OrganizationID");
            CreateIndex("dbo.Suppliers", "OrganizationID");
            CreateIndex("dbo.SupplierReceipts", "SupplierInvoiceID");
            CreateIndex("dbo.SupplierReceipts", "SupplierID");
            CreateIndex("dbo.SupplierReceipts", "OrganizationID");
            CreateIndex("dbo.SupplierInvoices", "SupplierID");
            CreateIndex("dbo.SupplierInvoices", "OrganizationID");
            CreateIndex("dbo.Organizations", "BusinessTypeId");
            CreateIndex("dbo.Documents", "ClientID");
            AddForeignKey("dbo.Organizations", "BusinessTypeId", "dbo.BusinessTypes", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Receipts", "ID", "dbo.Documents", "ID", cascadeDelete: true);
            AddForeignKey("dbo.InvoiceReceipts", "ID", "dbo.Documents", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Invoices", "ID", "dbo.Documents", "ID", cascadeDelete: true);
            AddForeignKey("dbo.DocumentItems", "Document_ID", "dbo.Documents", "ID");
            AddForeignKey("dbo.Waybills", "ID", "dbo.Documents", "ID", cascadeDelete: true);
            AddForeignKey("dbo.PriceOffers", "ID", "dbo.Documents", "ID", cascadeDelete: true);
            AddForeignKey("dbo.DocumentReferences", "BaseDocumentId", "dbo.Documents", "ID", cascadeDelete: true);
            AddForeignKey("dbo.DocumentItems", "WaybillID", "dbo.Waybills", "ID");
            AddForeignKey("dbo.Payments", "PayerID", "dbo.Clients", "ID");
            AddForeignKey("dbo.SupplierWaybills", "OrganizationID", "dbo.Organizations", "ID");
            AddForeignKey("dbo.Suppliers", "OrganizationID", "dbo.Organizations", "ID");
            AddForeignKey("dbo.SupplierReceipts", "OrganizationID", "dbo.Organizations", "ID");
            AddForeignKey("dbo.SupplierInvoices", "OrganizationID", "dbo.Organizations", "ID");
            AddForeignKey("dbo.SupplierReceipts", "SupplierInvoiceID", "dbo.SupplierInvoices", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SupplierWaybills", "SupplierID", "dbo.Suppliers", "ID");
            AddForeignKey("dbo.SupplierReceipts", "SupplierID", "dbo.Suppliers", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SupplierInvoices", "SupplierID", "dbo.Suppliers", "ID");
            RenameTable(name: "dbo.Log", newName: "Logs");
            RenameTable(name: "dbo.ProformaInvoices", newName: "PreliminaryInvoices");
        }
    }
}
