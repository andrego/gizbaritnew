namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GizbaritMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderDetails", "CatalogItemId", "dbo.CatalogItems");
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.ContentNodes", "ParentId", "dbo.ContentNodes");
            DropForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems");
            DropIndex("dbo.Orders", new[] { "SupplierId" });
            DropIndex("dbo.OrderDetails", new[] { "CatalogItemId" });
            DropIndex("dbo.OrderDetails", new[] { "OrderId" });
            DropIndex("dbo.ContentNodes", "IX_NAME_CULTURE_Parent");
            CreateTable(
                "dbo.Advertisements",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        PathToLogo = c.String(),
                        Title = c.String(),
                        Language = c.Int(nullable: false),
                        Organization_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.OrganizationInfoes",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Site = c.String(),
                        Facebook = c.String(),
                        Instagram = c.String(),
                        LinkedIn = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.ID)
                .Index(t => t.ID);
            
            AddForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems", "ID");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.ContentNodes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ContentNodes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                        CultureCode = c.String(nullable: false, maxLength: 256),
                        Url = c.String(maxLength: 1024),
                        Title = c.String(nullable: false, maxLength: 256),
                        Description = c.String(maxLength: 1024),
                        Content = c.String(),
                        ParentId = c.Guid(),
                        Status = c.Int(nullable: false),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CatalogItemId = c.Int(nullable: false),
                        Quantity = c.Double(nullable: false),
                        OrderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        SupplierId = c.Int(nullable: false),
                        WasProcessed = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems");
            DropForeignKey("dbo.OrganizationInfoes", "ID", "dbo.Organizations");
            DropForeignKey("dbo.Advertisements", "Organization_ID", "dbo.Organizations");
            DropIndex("dbo.OrganizationInfoes", new[] { "ID" });
            DropIndex("dbo.Advertisements", new[] { "Organization_ID" });
            DropTable("dbo.OrganizationInfoes");
            DropTable("dbo.Advertisements");
            CreateIndex("dbo.ContentNodes", new[] { "Name", "CultureCode", "ParentId" }, unique: true, name: "IX_NAME_CULTURE_Parent");
            CreateIndex("dbo.OrderDetails", "OrderId");
            CreateIndex("dbo.OrderDetails", "CatalogItemId");
            CreateIndex("dbo.Orders", "SupplierId");
            AddForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ContentNodes", "ParentId", "dbo.ContentNodes", "ID");
            AddForeignKey("dbo.Orders", "SupplierId", "dbo.Suppliers", "ID", cascadeDelete: true);
            AddForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders", "ID", cascadeDelete: true);
            AddForeignKey("dbo.OrderDetails", "CatalogItemId", "dbo.CatalogItems", "ID", cascadeDelete: true);
        }
    }
}
