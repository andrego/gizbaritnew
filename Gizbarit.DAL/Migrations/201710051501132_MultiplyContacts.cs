namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MultiplyContacts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientEmailContacts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        ClientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.ClientPhoneContacts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Phone = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        ClientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClientPhoneContacts", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.ClientEmailContacts", "ClientId", "dbo.Clients");
            DropIndex("dbo.ClientPhoneContacts", new[] { "ClientId" });
            DropIndex("dbo.ClientEmailContacts", new[] { "ClientId" });
            DropTable("dbo.ClientPhoneContacts");
            DropTable("dbo.ClientEmailContacts");
        }
    }
}
