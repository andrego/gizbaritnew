namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationGizbarit : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems");
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        SupplierId = c.Int(nullable: false),
                        WasProcessed = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CatalogItemId = c.Int(nullable: false),
                        Quantity = c.Double(nullable: false),
                        OrderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CatalogItems", t => t.CatalogItemId, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.CatalogItemId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.ContentNodes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                        CultureCode = c.String(nullable: false, maxLength: 256),
                        Url = c.String(maxLength: 1024),
                        Title = c.String(nullable: false, maxLength: 256),
                        Description = c.String(maxLength: 1024),
                        Content = c.String(),
                        ParentId = c.Guid(),
                        Status = c.Int(nullable: false),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContentNodes", t => t.ParentId)
                .Index(t => new { t.Name, t.CultureCode, t.ParentId }, unique: true, name: "IX_NAME_CULTURE_Parent");
            
           // AddColumn("dbo.CatalogItems", "SupplierPrice", c => c.Double(nullable: false));
           // AddColumn("dbo.CatalogItems", "SellingPrice", c => c.Double(nullable: false));
            AddForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems", "ID", cascadeDelete: true);
           // DropColumn("dbo.StoreCatalogItems", "SupplierPrice");
           // DropColumn("dbo.StoreCatalogItems", "Price");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StoreCatalogItems", "Price", c => c.Double(nullable: false));
            AddColumn("dbo.StoreCatalogItems", "SupplierPrice", c => c.Double(nullable: false));
            DropForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems");
            DropForeignKey("dbo.ContentNodes", "ParentId", "dbo.ContentNodes");
            DropForeignKey("dbo.Orders", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderDetails", "CatalogItemId", "dbo.CatalogItems");
            DropIndex("dbo.ContentNodes", "IX_NAME_CULTURE_Parent");
            DropIndex("dbo.OrderDetails", new[] { "OrderId" });
            DropIndex("dbo.OrderDetails", new[] { "CatalogItemId" });
            DropIndex("dbo.Orders", new[] { "SupplierId" });
            DropColumn("dbo.CatalogItems", "SellingPrice");
            DropColumn("dbo.CatalogItems", "SupplierPrice");
            DropTable("dbo.ContentNodes");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Orders");
            AddForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems", "ID");
        }
    }
}
