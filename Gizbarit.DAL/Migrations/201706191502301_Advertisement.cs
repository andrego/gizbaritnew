namespace Gizbarit.DAL.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Advertisement : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advertisements",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        PathToLogo = c.String(),
                        Title = c.String(),
                        Language = c.Int(nullable: false),
                        Organization_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.OrganizationInfoes",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Site = c.String(),
                        Facebook = c.String(),
                        Instagram = c.String(),
                        LinkedIn = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.ID)
                .Index(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrganizationInfoes", "ID", "dbo.Organizations");
            DropForeignKey("dbo.Advertisements", "Organization_ID", "dbo.Organizations");
            DropIndex("dbo.OrganizationInfoes", new[] { "ID" });
            DropIndex("dbo.Advertisements", new[] { "Organization_ID" });
            DropTable("dbo.OrganizationInfoes");
            DropTable("dbo.Advertisements");
        }
    }
}
