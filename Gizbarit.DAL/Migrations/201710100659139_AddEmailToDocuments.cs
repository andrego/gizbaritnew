namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailToDocuments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Documents", "Email");
        }
    }
}
