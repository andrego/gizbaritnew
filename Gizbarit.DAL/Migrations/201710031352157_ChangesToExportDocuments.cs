namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesToExportDocuments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "PointOfDelivery", c => c.String());
            AddColumn("dbo.Documents", "ExportAditionInfo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Documents", "ExportAditionInfo");
            DropColumn("dbo.Documents", "PointOfDelivery");
        }
    }
}
