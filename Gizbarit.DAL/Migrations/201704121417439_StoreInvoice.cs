namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StoreInvoice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StoreInvoices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Number = c.Long(nullable: false),
                        Organization_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.StoreCatalogItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SupplierPrice = c.Double(nullable: false),
                        Quantity = c.Double(nullable: false),
                        CatalogItemId = c.Int(nullable: false),
                        StoreInvoiceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CatalogItems", t => t.CatalogItemId, cascadeDelete: true)
                .ForeignKey("dbo.StoreInvoices", t => t.StoreInvoiceId, cascadeDelete: true)
                .Index(t => t.CatalogItemId)
                .Index(t => t.StoreInvoiceId);
            
            AddColumn("dbo.Suppliers", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StoreInvoices", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.StoreCatalogItems", "StoreInvoiceId", "dbo.StoreInvoices");
            DropForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems");
            DropIndex("dbo.StoreCatalogItems", new[] { "StoreInvoiceId" });
            DropIndex("dbo.StoreCatalogItems", new[] { "CatalogItemId" });
            DropIndex("dbo.StoreInvoices", new[] { "Organization_ID" });
            DropColumn("dbo.Suppliers", "Email");
            DropTable("dbo.StoreCatalogItems");
            DropTable("dbo.StoreInvoices");
        }
    }
}
