namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPhoneToDocuments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "Phone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Documents", "Phone");
        }
    }
}
