namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GizbaritMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Advertisements", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationInfoes", "ID", "dbo.Organizations");
            DropForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems");
            DropIndex("dbo.Advertisements", new[] { "Organization_ID" });
            DropIndex("dbo.OrganizationInfoes", new[] { "ID" });
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        SupplierId = c.Int(nullable: false),
                        WasProcessed = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CatalogItemId = c.Int(nullable: false),
                        Quantity = c.Double(nullable: false),
                        OrderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CatalogItems", t => t.CatalogItemId, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.CatalogItemId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.ContentNodes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                        CultureCode = c.String(nullable: false, maxLength: 256),
                        Url = c.String(maxLength: 1024),
                        Title = c.String(nullable: false, maxLength: 256),
                        Description = c.String(maxLength: 1024),
                        Content = c.String(),
                        ParentId = c.Guid(),
                        Status = c.Int(nullable: false),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContentNodes", t => t.ParentId)
                .Index(t => new { t.Name, t.CultureCode, t.ParentId }, unique: true, name: "IX_NAME_CULTURE_Parent");
            
            AddForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems", "ID", cascadeDelete: true);
            DropTable("dbo.Advertisements");
            DropTable("dbo.OrganizationInfoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.OrganizationInfoes",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Site = c.String(),
                        Facebook = c.String(),
                        Instagram = c.String(),
                        LinkedIn = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Advertisements",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        PathToLogo = c.String(),
                        Title = c.String(),
                        Language = c.Int(nullable: false),
                        Organization_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems");
            DropForeignKey("dbo.ContentNodes", "ParentId", "dbo.ContentNodes");
            DropForeignKey("dbo.Orders", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderDetails", "CatalogItemId", "dbo.CatalogItems");
            DropIndex("dbo.ContentNodes", "IX_NAME_CULTURE_Parent");
            DropIndex("dbo.OrderDetails", new[] { "OrderId" });
            DropIndex("dbo.OrderDetails", new[] { "CatalogItemId" });
            DropIndex("dbo.Orders", new[] { "SupplierId" });
            DropTable("dbo.ContentNodes");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Orders");
            CreateIndex("dbo.OrganizationInfoes", "ID");
            CreateIndex("dbo.Advertisements", "Organization_ID");
            AddForeignKey("dbo.StoreCatalogItems", "CatalogItemId", "dbo.CatalogItems", "ID");
            AddForeignKey("dbo.OrganizationInfoes", "ID", "dbo.Organizations", "ID");
            AddForeignKey("dbo.Advertisements", "Organization_ID", "dbo.Organizations", "ID", cascadeDelete: true);
        }
    }
}
