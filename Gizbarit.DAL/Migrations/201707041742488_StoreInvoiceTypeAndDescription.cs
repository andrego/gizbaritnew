namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StoreInvoiceTypeAndDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StoreInvoices", "Description", c => c.String());
            AddColumn("dbo.StoreInvoices", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StoreInvoices", "Type");
            DropColumn("dbo.StoreInvoices", "Description");
        }
    }
}
