// <auto-generated />
namespace Gizbarit.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ContactsForClients1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ContactsForClients1));
        
        string IMigrationMetadata.Id
        {
            get { return "201710050637096_ContactsForClients1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
