using System.Collections.Generic;
using Gizbarit.DAL.Entities.Organization;

namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Gizbarit.DAL.GizbratDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Gizbarit.DAL.GizbratDataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var transactionsList = context.BankTransactions.Where(m => m.Currency == null).ToList();
            if (transactionsList.Count != 0)
            {
                var defaultCurrency = context.Currencies.FirstOrDefault(m => m.ID == "ILS");
                transactionsList.ForEach(m => m.Currency = defaultCurrency);

            }

            var clientsList = context.Clients.ToList();
            foreach (var item in clientsList)
            {
                if (item.ClientPhoneContacts.Count == 0 && item.Phone != String.Empty)
                {
                    
                    item.ClientPhoneContacts.Add(new ClientPhoneContact()
                    {
                        Phone = item.Phone,
                        IsActive = true
                    });
                }

                if (item.ClientEmailContacts.Count == 0 && item.Email != String.Empty)
                {
                    
                    item.ClientEmailContacts.Add(new ClientEmailContact()
                    {
                        Email = item.Email,
                        IsActive = true
                    });
                }
            }

            context.SaveChanges();

        }
    }
}
