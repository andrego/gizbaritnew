namespace Gizbarit.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTimeOfDelivery : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "TimeOfDelivery", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Documents", "TimeOfDelivery");
        }
    }
}
