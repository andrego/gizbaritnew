﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Models.Payment
{
    public class PaymentViewModel
    {
        public int idPayment { get; set; }
        public byte PaymentType { get; set; }
        public string AccountNumber { get; set; }
        public string BranchName { get; set; }
        public string BankName { get; set; }
        public string CheckNumber { get; set; }
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "PaymentSumRequired")]
        public double Sum { get; set; }
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "PaymentDateRequired")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime DateOperation { get; set; }

        public int? CreditCardType { get; set; }
    }

    public class PaymentShortViewModel
    {
        public string Client { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime Date { get; set; }
        public double Amount { get; set; }
    }
}