﻿namespace Gizbarit.DAL.Models.Payment
{
    public class PaymentTypeViewModel
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}