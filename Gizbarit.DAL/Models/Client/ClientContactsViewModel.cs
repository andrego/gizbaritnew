﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizbarit.DAL.Models.Client
{
   public class ClientContactsViewModel
    {
        public string ClientName { get; set; }
        public int ClientId { get; set; }
        public List<ClientContactView> Emails { get; set; }
        public List<ClientContactView> Phones { get; set; } 
    }

    public class ClientContactView
    {
        public int Id { get; set; }
        public string Contact { get; set; }
    }
}
