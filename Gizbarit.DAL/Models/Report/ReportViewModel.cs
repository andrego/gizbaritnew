﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Documents;

namespace Gizbarit.DAL.Models.Report
{
    public class ReportViewModel
    {
        public bool ReciptSelected { get; set; }
        public bool InvoiceSelected { get; set; }
        public bool InvoiceReciptSelected { get; set; }

        public bool PriceOfferSelected { get; set; }
        public bool WaybillSelected { get; set; }
        public bool PreliminaryInvoiceSelected { get; set; }

        /*
        PriceOffer,
        Waybill,
        PreliminaryInvoice,
        TaxInvoice,
        Receipt,
        TaxInvoiceReceipt
        */
        [Required(ErrorMessage = "*")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Required(ErrorMessage = "*")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        [Required(ErrorMessage = "*")]
        [EmailAddress]
        public string Email { get; set; }

        [EmailAddress]
        public string BccEmail { get; set; }

        public  Organization Organization { get; set; }

        public bool IsVatCharge { get; set; }

        public bool IsSended { get; set; }

        public ICollection<BaseDocument> Recipts { get; set; } = new List<BaseDocument>();
        public ICollection<BaseDocument> Invoices { get; set; } = new List<BaseDocument>();
        public ICollection<BaseDocument> InvoiceRecipts { get; set; } = new List<BaseDocument>();

        public static ReportViewModel Create(Organization org)
        {
            var instance = new ReportViewModel();

            if (org.IsVatCharge)
            {
                instance.ReciptSelected = true;
                instance.InvoiceSelected = true;
            }

            instance.InvoiceReciptSelected = true;
            instance.Organization = org;
            
            instance.FromDate = DateTime.UtcNow.Date.AddYears(-1);
            instance.ToDate = DateTime.UtcNow.Date;
            instance.IsVatCharge = org.IsVatCharge;

            return instance;
        }

    }

    public class TaxAuthorityReportViewModel
    {
        [Required(ErrorMessage = "*")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Required(ErrorMessage = "*")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        public IEnumerable<ContentBase> Contents { get; set; } = new List<ContentBase>();

        public ContentBase Content { get; set; } = new ContentBase();
    }
}