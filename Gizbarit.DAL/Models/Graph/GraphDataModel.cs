﻿using System;
using System.Collections.Generic;

namespace Gizbarit.DAL.Models.Graph
{
    public class BankGraphDataModel
    {
        public double Limit { get; set; }
        public List<GraphDataElement> GraphDaysData { get; set; }
        public List<GraphDataElement> GraphMonthData { get; set; }
    }

    public class DashboardGraphDataModel
    {
        public Dictionary<string, List<GraphDataElement>> GraphData { get; set; }
    }

    public class GraphDataElement
    {
        public DateTime Date { get; set; }
        public double Amount { get; set; }
    }
}