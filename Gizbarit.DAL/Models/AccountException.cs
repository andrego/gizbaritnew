﻿using System;

namespace Gizbarit.DAL.Models
{
    public class AccountException
    {
            public DateTime Date { get; set; }
            public double DueValue { get; set; }
    }
}
