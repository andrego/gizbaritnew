﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizbarit.DAL.Models.Account.Bank;

namespace Gizbarit.DAL.Models.Dashboard
{
    public class AccountForecastViewModel
    {
        public string Account { get; set; }
        public string Number { get; set; }
        public double Today { get; set; }
        public double Tomorrow { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime Exceeding_date { get; set; }
    }
}
