﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizbarit.DAL.Models.Account.Bank;
using Gizbarit.DAL.Models.Payment;

namespace Gizbarit.DAL.Models.Dashboard
{
    public class DashboardViewModel
    {
        public IEnumerable<BankAccountShort> Accounts { get; set; } 
        public IEnumerable<PaymentShortViewModel>  Payments { get; set; }
    }
}
