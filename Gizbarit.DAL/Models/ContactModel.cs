﻿using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Models
{
    public class ContactModel
    {
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "EmailIsRequired")]
        public string Message { get; set; }
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "EmailIsRequired")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "InvelidEmail")]
        public string Email { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
    }
}