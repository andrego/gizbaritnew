﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizbarit.DAL.Models.Documents;

namespace Gizbarit.DAL.Models.CustomerPayment
{
    public class CustomerPaymentViewModel
    {
        public BaseDocument BaseDocument { get; set; }
      
        public DateTime PaymentDay { get; set; }
        public double PaymentSum { get; set; }
    }
}
