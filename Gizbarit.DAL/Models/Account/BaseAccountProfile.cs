﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.Utils;

namespace Gizbarit.DAL.Models.Account
{
    public class BaseAccountProfile : BaseAccount
    {
        public BaseAccountProfile()
        {
            TempLogoId = Randomizer.GetRandomString(10);

            VatPeriod = new List<SelectListItem>
            {
               new SelectListItem {Text =  Translations.Register.EveryMonth, Value = "1"},
                new SelectListItem {Text = Translations.Register.Every2Month, Value = "2"}

            };
            TaxPeriod = new List<SelectListItem>
            {
                new SelectListItem {Text =  Translations.Register.EveryMonth, Value = "1"},
                new SelectListItem {Text = Translations.Register.Every2Month, Value = "2"},

            };
            RegisterTypes = new List<SelectListItem>
            {
                new SelectListItem {Text = Translations.Register.Company, Value = ((int)Enums.RegisterType.Company).ToString()},
                new SelectListItem {Text = Translations.Register.Private, Value = ((int)Enums.RegisterType.Private).ToString()},
                new SelectListItem {Text = Translations.Register.Partnership, Value = ((int)Enums.RegisterType.Partnership).ToString()},
                new SelectListItem {Text = Translations.Register.NonProfit, Value = ((int)Enums.RegisterType.NonProfit).ToString()},

            };
            VatPeriodId = 1;
            TaxPeriodId = 1;
            RegisterTypeId = 0;


        }

        public int Id { get; set; }

        public string TempLogoId { get; set; }
        public string LogoExtension { get; set; }

        public string PathToLogo { get; set; }

        public int VatTypeId { get; set; }
        public IEnumerable<SelectListItem> VatType { get; set; }
        public int VatPeriodId { get; set; }
        public IEnumerable<SelectListItem> VatPeriod { get; set; }
        public int IncomeTax { get; set; }
        public int TaxPeriodId { get; set; }
        public IEnumerable<SelectListItem> TaxPeriod { get; set; }
        public int BusinesTypeId { get; set; } = 1;
        public IEnumerable<SelectListItem> BusinesTypes { get; set; }
        public int RegisterTypeId { get; set; }
        public IEnumerable<SelectListItem> RegisterTypes { get; set; }

        public void SetBusinessTypes(IEnumerable<BusinessType> btypes)
        {
            BusinesTypes = btypes.Select(bt => new SelectListItem
            {
                Text = bt.Name,
                Value = bt.ID.ToString()
            });
        }

    }
}