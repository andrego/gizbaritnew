﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Gizbarit.DAL.Models.Account.Bank
{
    public class BankAccountViewModel
    {

        public int Id { get; set; }
        public int BankId { get; set; }

        public IEnumerable<SelectListItem> Bank { get; set; }


        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "AccountNumberRequired")]
        public string AccountNo { get; set; }

        [Display(Name = "Account Balance")]
        public double AccountBalance { get; set; }

        [Display(Name = "Limit")]
        public double Limit { get; set; }

        public int IsActive { get; set; }

        public int OrgId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "BranchNameIsRequired")]
        public string Branch { get; set; }

        public bool IsDeleted { get; set; }
    }

    public class BankAccountMiniViewModel
    {
        public int Id { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public bool IsDefault { get; set; }
        public double Limit { get; set; }
    }


}