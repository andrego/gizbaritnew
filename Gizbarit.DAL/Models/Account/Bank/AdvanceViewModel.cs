﻿using System;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Documents;

namespace Gizbarit.DAL.Models.Account.Bank
{
    public class AdvanceViewModel
    {
        public int Id { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime CreatedDate { get; set; }
        public DocumentType? DocumentType { get; set; }
        public string Description { get; set; }
        public int? InvoiceNumber { get; set; }
        public Guid? InvoiceId { get; set; }
        public int? SupplierInvoiceNumber { get; set; }
        public double Amount { get; set; }
        public double? VatAmount { get; set; }
        public double? AdvanceAmount { get; set; }
        public bool Credit { get; set; }

    }
}