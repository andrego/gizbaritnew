﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Models.Account.Bank
{
    public class TransactionViewModel
    {
        public Guid Id { get; set; }
        public int AccountId { get; set; }
        public bool Credit { get; set; }
        public double Amount { get; set; }
        public string Description { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime DateOperation { get; set; }

        public string Currency { get; set; }
       

    }
    public class EditTransactionViewModel:TransactionViewModel
    {
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime? DateEnd { get; set; }

        
    }

    public class TransactionShortViewModel
    {
        public string Description { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public string Currency { get; set; }
    }

}