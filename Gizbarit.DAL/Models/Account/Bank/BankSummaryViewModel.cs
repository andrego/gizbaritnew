﻿using System.Collections.Generic;

namespace Gizbarit.DAL.Models.Account.Bank
{
    public class BankSummaryViewModel
    {
        public List<BankSummary> Banks { get; set; }
    }

    public class BankSummary : BankAccountMiniViewModel
    {
        public BankSummary()
        {
            Values = new double[12];
        }
        public double[] Values { get; set; }
    }

}