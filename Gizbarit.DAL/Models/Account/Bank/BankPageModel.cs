﻿using System.Collections.Generic;

namespace Gizbarit.DAL.Models.Account.Bank
{
    public class BankPageModel
    {
        public List<BankAccountMiniViewModel> Accounts { get; set; }
        public double MonthInitialBalance { get; set; }
        public List<TransactionViewModel> Transactions { get; set; }

        public string Currency { get; set; }
       
    }


}