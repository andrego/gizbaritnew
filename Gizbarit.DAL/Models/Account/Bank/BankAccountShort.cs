﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizbarit.DAL.Models.Account.Bank
{
    public class BankAccountShort
    {
        public string Account { get; set; }
        public string Number { get; set; }
        public double Balance { get; set; }
        public double BalanceMonth { get; set; }

        public IEnumerable<TransactionShortViewModel> Transactions { get; set; } 

    }
}
