﻿using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Models.Account
{
    public class BaseAccount
    {
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "FirstNameIsRequired")]
        [DataType(DataType.Text)]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "LastNameIsRequiered")]
        [DataType(DataType.Text)]
        [Display(Name = "LastName")]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "EmailIsRequired")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "InvelidEmail")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "EmailValidationRequiered")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "InvelidEmail")]
        [System.ComponentModel.DataAnnotations.Compare("Email", ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "EmailsMustMatch")]
        public string EmailValidation { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "PasswordRequired")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "PasswordValidationRequired")]
        [Display(Name = @"Validation")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "PasswordMustMatch")]
        public string PasswordValidation { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "BusinessNumberRequired")]
        [RegularExpression("^([0-9]{9})$", ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "InvalidBusinessNumber")]
        [DataType(DataType.Text)]
        [Display(Name = "OrganizationId")]
        public string BusinessId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "BusinessNumberRequired")]
        [RegularExpression("^([0-9]{9})$", ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "InvalidBusinessNumber")]
        [DataType(DataType.Text)]
        [Display(Name = "OrganizationId")]
        [System.ComponentModel.DataAnnotations.Compare("BusinessId", ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "BusinessNumberMustMatch")]
        public string BusinessIdValidation { get; set; }

        [Display(Name = "BusinessName")]
        public string BusinessName { get; set; }

        public string BusinessNameEng { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "PhoneRequired")]
        [DataType(DataType.Text)]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "AddressRequiered")]
        [Display(Name = "Street")]
        public string Street { get; set; }

        public string StreetEng { get; set; }

        [Display(Name = "Zip")]
        public string Zip { get; set; }

    }
}