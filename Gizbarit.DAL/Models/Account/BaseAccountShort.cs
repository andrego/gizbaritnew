﻿using Gizbarit.Utils;

namespace Gizbarit.DAL.Models.Account
{
    public class BaseAccountShort
    {
        public string PathToLogo { get; set; }
        public string BusinesName { get; set; }
        public string BusinesNameEng { get; set; }
        public string Address { get; set; }
        public string AddressEng { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string BusinessId { get; set; }
        public Enums.RegisterType RegisterType { get; set; }
        public string BookKeppengEmail { get; set; }

    }
}