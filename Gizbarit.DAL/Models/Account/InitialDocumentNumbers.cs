﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Models.Account
{
    public class InitialDocumentNumbers
    {
        [Range(0, Int32.MaxValue, ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "WrongDocumentNumber")]
        public int InvoiceNo { get; set; }
        [Range(0, Int32.MaxValue, ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "WrongDocumentNumber")]
        public int InvoiceQouteNo { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "WrongDocumentNumber")]
        public int InvoiceShipNo { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "WrongDocumentNumber")]
        public int ProformaInvoiceNo { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "WrongDocumentNumber")]
        public int InvoiceReceiptNo { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "WrongDocumentNumber")]
        public int ReceiptNo { get; set; }

        


    }
}