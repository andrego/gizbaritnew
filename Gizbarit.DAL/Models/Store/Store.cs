﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Gizbarit.DAL.Entities.Store;

namespace Gizbarit.DAL.Models.Store
{
    public class StoreCatalogItemViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Code { get; set; }
        public double Quantity { get; set; }
        public double? UsedQuantity { get; set; }
        public string Supplier { get; set; }
        public int SupplierId { get; set; }
        public double SupplierPrice { get; set; }
        public double Price { get; set; }
    }

    public class StoreDataViewModel
    {
        public StoreDataViewModel()
        {
            Items = new List<StoreCatalogItemViewModel>
            {
                new StoreCatalogItemViewModel()
            };
        }
        public int SupplierId { get; set; }
        public long InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string Description { get; set; }
        public StoreInvoiceType Type { get; set; }
        public List<StoreCatalogItemViewModel> Items { get; set; }

        public double GetItemsPriceWithVat(double vatAmount = 17)
        {
            return Items.Sum(i => i.SupplierPrice*i.Quantity)*(1 + vatAmount/100);
        }
    }

    public class OrderDetailsViewModel
    {
        public OrderDetailsViewModel()
        {
            Items = new List<StoreCatalogItemViewModel>();
        }
        public int Id { get; set; }
        public int? SupplierId { get; set; }

        public DocumentActionType Action { get; set; }

        public string CultureCode { get; set; } = "he";

        public string Email { get; set; }

        public List<StoreCatalogItemViewModel> Items { get; set; }
    }

    public enum DocumentActionType
    {
        Save = 1,
        Print,
        Send
    }
}
