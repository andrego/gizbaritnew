﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Account;
using Gizbarit.DAL.Models.Account.Bank;
using Gizbarit.Utils;

namespace Gizbarit.DAL.Models
{
    public class RegisterModel
    {
        public RegisterModel()
        {
            AccountProfile = new BaseAccountProfile();
            BankAccounts = new List<BankAccountViewModel>();
            DocumentNumbers = new InitialDocumentNumbers();

            TempLogoId = Randomizer.GetRandomString(10);
        }
        public BaseAccountProfile AccountProfile { get; set; }

        public IList<BankAccountViewModel> BankAccounts { get; set; }
        public InitialDocumentNumbers DocumentNumbers { get; set; }
        public string TempLogoId { get; set; }
        public string LogoExtension { get; set; }

        //[MustBeTrue(ErrorMessage = "You must accept the terms and conditions")]
        [RegularExpression("True", ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "AcceptTermsAndConditions")]
        [DisplayName("Accept terms and conditions")]
        public bool AcceptsTerms { get; set; }
        public void SetBankNames(IEnumerable<BankName> bankNames)
        {
            if (BankAccounts.Any())
            {
                foreach (var bankAccount in BankAccounts)
                {
                    bankAccount.Bank =
                        bankNames.Select(bn => new SelectListItem {Text = bn.Name, Value = bn.ID.ToString()}).ToList();
                }
            }
            else
            {
                var ba = new BankAccountViewModel
                {
                    Bank = bankNames.Select(bn => new SelectListItem { Text = bn.Name, Value = bn.ID.ToString() }).ToList()
                };
                BankAccounts.Add(ba);
            }
            
        }

        public void SetBusinesTypes(IEnumerable<BusinessType> btypes)
        {
            AccountProfile.SetBusinessTypes(btypes);
        }

        public bool IsSmallBusines()
        {
            return AccountProfile.BusinesTypeId == 2;
        }

    }
}