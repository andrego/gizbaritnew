﻿namespace Gizbarit.DAL.Models
{
    public class CatalogCountView
    {
        public string Code { get; set; }
        public double Count { get; set; }
    }
}
