﻿using System;
using System.Collections.Generic;

namespace Gizbarit.DAL.Models.Settlements
{
    public class SupplierWaybillViewModel
    {
        public int Id { get; set; }
        public string SupplyDescription { get; set; }
        public long Number { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public int IsActive { get; set; }
    }

    public class SupplierInvoiceViewModel
    {
        public int Id { get; set; }
        public long? Number { get; set; }
        public double? Total { get; set; }
        public bool IsConfirmed { get; set; }
        public DateTime? Date { get; set; }
    }

    public class SupplierReceiptViewModel
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public double? Amount { get; set; }
        public DateTime? Date { get; set; }
    }

    public class SupplierWaybillContainer
    {
        public List<SupplierWaybillViewModel> Waybills { get; set; }
        public SupplierInvoiceViewModel Invoice { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int SupplierId { get; set; }
    }

    public class SupplierReceiptContainer
    {
        public SupplierInvoiceViewModel Invoice  { get; set; }
        public int SupplierId { get; set; }
        public string SupplyDescription { get; set; }
        public IEnumerable<SupplierReceiptViewModel> Receipts { get; set; } 
    }


}