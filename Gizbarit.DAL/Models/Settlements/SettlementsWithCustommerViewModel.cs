﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Organisation;

namespace Gizbarit.DAL.Models.Settlements
{
    public interface IClients
    {
        IEnumerable<ClientViewModel> Clients { get; set; }
    }
    public class SettlementsWithCustommerViewModel
    {
        public long? PreliminaryInvoiceNumber { get; set; }
        public Guid PreliminaryInvoice { get; set; }
        public DateTime PreliminaryInvoiceCreationDate { get; set; }
        public string ClientName { get; set; }
        public string Subject { get; set; }
        public double TotalWithVat { get; set; }
        public long? InvoiceNumber { get; set; }
        public Guid Invoice { get; set; }
        public DocumentType InvoiceType { get; set; }
        public double InvoiceTotal { get; set; }
        [IgnoreDataMember]
        public bool MainDocument { get; private set; }

        public double? ReceiptTotal { get; set; }
        public double InvoiceTotalReceiptSum { get; set; }
        public double TotalPaid { get; set; }

        public string Currency { get; set; }

        public double Deduction
        {
            get
            {
                if (InvoiceTotal != 0)
                {
                    return InvoiceTotalReceiptSum == 0
                        ? InvoiceTotal - TotalPaid - (ReceiptTotal ?? 0)
                        : InvoiceTotal - TotalPaid - InvoiceTotalReceiptSum;
                }
                return TotalWithVat - TotalPaid - (ReceiptTotal ?? 0);
            }
        }

        public DateTime PaymentDate { get; set; }

        public void SetMainDocument()
        {
            MainDocument = true;
        }

    }

    public class SettlementsWithCustommerViewModelWrapper : IClients
    {
        public IEnumerable<ClientViewModel> Clients { get; set; }
        public IEnumerable<SettlementsWithCustommerViewModel> Items { get; set; }
        public double PastYearDeduction { get; set; }

        public bool IsExport { get; set; }
    }

    [Serializable]
    public class SettlemetsByClient
    {
        [IgnoreDataMember]
        public int ClientId { get; set; }
        [DisplayName]
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
        public string ClientPhone { get; set; }
        public double TotalInvoice { get; set; }
        public double TotalPaid { get; set; }
    }

    public class SettlementsWaybillWrapper : IClients
    {
        public IEnumerable<ClientViewModel> Clients { get; set; }
        public List<SettlementsWaybillViewModel> Waybils { get; set; }

        public SettlementsWaybillWrapper()
        {
            Clients = new List<ClientViewModel>();
            Waybils = new List<SettlementsWaybillViewModel>();
        }
    }

    public class SettlementsWaybillViewModel
    {
        public long Number { get; set; }
        public Guid WaybillId { get; set; }
        public DateTime Date { get; set; }
        public string ClientName { get; set; }
        public double Amount { get; set; }
        public long PreliminaryInvoiceNumber { get; set; }
        public Guid PreliminaryInvoiceId { get; set; }
        public DateTime PreliminaryInvoiceDate { get; set; }
        public double TotalAmount { get; set; }
        public string Subject { get; set; }

    }
}