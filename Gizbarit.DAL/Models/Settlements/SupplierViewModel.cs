﻿using System;
using System.Runtime.Serialization;

namespace Gizbarit.DAL.Models.Settlements
{
    public class SupplierViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }

    [Serializable]
    public class SummaryBySupplier
    {
        public string SupplierName { get; set; }
        [IgnoreDataMember]
        public int SupplierId { get; set; }
        public long InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string DeliverySubject { get; set; }
        public double InvoiceAmount { get; set; }
        public double Paid { get; set; }

        public double BalanceDue
        {
            get { return InvoiceAmount - Paid; }
        }
    }

    public class SupplierInvoiceDetailsViewModel
    {
        public long Number { get; set; }
        public DateTime Date { get; set; }
        public string Subject { get; set; }
        public double Amount { get; set; }
        public double Paid { get; set; }
    }
}