﻿using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Advertisement;
using Gizbarit.Utils;

namespace Gizbarit.DAL.Models.Advertisement
{
    public class AdvertisementModelView
    {
        public AdvertisementModelView()
        {
            TempLogoId = Randomizer.GetRandomString(10);
        }

        [Required]
        public string Text { get; set; }
        public string PathToLogo { get; set; }
        [Required]
        public string Title { get; set; }

        [Required]
        public AdvertiseLanguages? Language { get; set; }

        public string Site { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string LinkedIn { get; set; }


        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }

        public string TempLogoId { get; set; }
        public string LogoExtension { get; set; }

    }


}
