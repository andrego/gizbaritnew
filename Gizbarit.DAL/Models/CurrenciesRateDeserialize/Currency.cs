﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Gizbarit.DAL.Models.CurrenciesRateDeserialize
{
    [Serializable]
    public class Currency
    {
        [XmlElement(ElementName = "NAME")]
        public string Name { get; set; }

        [XmlElement(ElementName = "UNIT")]
        public int Unit { get; set; }

        [XmlElement(ElementName = "CURRENCYCODE")]
        public string CurrencyCode { get; set; }

        [XmlElement(ElementName = "COUNTRY")]
        public string Country { get; set; }

        [XmlElement(ElementName = "RATE")]
        public double Rate { get; set; }

        [XmlElement(ElementName = "CHANGE")]
        public double Change { get; set; }
    }
}
