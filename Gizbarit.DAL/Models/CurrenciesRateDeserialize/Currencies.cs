﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Gizbarit.DAL.Entities;

namespace Gizbarit.DAL.Models.CurrenciesRateDeserialize
{
    [Serializable]
    [XmlRoot("CURRENCIES")]
    public class Currencies
    {
        [XmlElement(ElementName = "LAST_UPDATE")]
        public string LastUpdate { get; set; }

        [XmlElement(ElementName = "CURRENCY")]
        public List<Currency> Currency = new List<Currency>();
    }
}
