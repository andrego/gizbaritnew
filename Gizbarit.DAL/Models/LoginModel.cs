﻿using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Business number")]
        [DataType(DataType.Text)]
        public string BusinessNumber { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
    }

    public class AdminLoginViewModel
    {
        [Required]
        [Display(Name = "User Name")]
        [DataType(DataType.EmailAddress)]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}