﻿using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Models.Organisation
{
    public class CatalogItemViewModel :IHasCodeAndId
    {
        public int ID { get; set; }
        [StringLength(50)]
        public string Code { get; set; }
        public string Name { get; set; }
        public double? Price { get; set; }
        public double? PriceAfterTax { get; set; }
        public double? VatPercentage { get; set; }
        public bool Active { get; set; }
        public string ImagePath { get; set; }
    }
}