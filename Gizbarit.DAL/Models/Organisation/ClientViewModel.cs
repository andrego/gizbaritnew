﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Models.Organisation
{
    public class ClientViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "ClientNameRequired")]
        [StringLength(50)]
        public string Name { get; set; }
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "SelectClient")]
        [StringLength(50)]
        public string UniqueId { get; set; }
        public string Email { get; set; }
        
        public string Phone { get; set; }

        public string Fax { get; set; }

        public string City { get; set; }

        public string Zip { get; set; }

        [StringLength(300)]
        public string Address { get; set; }

        public string Cell { get; set; }

        public double RetainerAmount { get; set; }

        public IEnumerable<string> AdditionEmails { get; set; } 
        public IEnumerable<string> AdditionPhones { get; set; } 

    }

    public class ClientEditViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UniqueId { get; set; }
        public string Address { get; set; }
        public double? RetainerAmount { get; set; }

    }
}