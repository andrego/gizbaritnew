﻿using System;
using Gizbarit.DAL.Entities.Documents;

namespace Gizbarit.DAL.Models.Documents
{
    public class DocumentSearchResult
    {
        public Guid Id { get; set; }
        public long Number { get; set; }
        public string Subject { get; set; }
        public DocumentType Type { get; set; }
        public string ClientName { get; set; }
        public DateTime Date { get; set; }
        public bool IsSmallBusiness { get; set; }
        public bool IsUsed { get; set; }
        public double Price { get; set; }
    }

    public class DocumentSearchModel
    {
        public string Query { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
    }
}