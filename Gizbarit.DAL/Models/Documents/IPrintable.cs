﻿namespace Gizbarit.DAL.Models.Documents
{
    public interface IPrintable
    {
        bool Print { get; set; } 
    }
}
