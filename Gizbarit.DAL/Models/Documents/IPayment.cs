﻿using System.Collections.Generic;
using Gizbarit.DAL.Models.Payment;

namespace Gizbarit.DAL.Models.Documents
{
    public interface IPayment
    {
        IEnumerable<PaymentTypeViewModel> PaymentTypes { get; set; }
        IEnumerable<PaymentViewModel> Payments { get; set; }
        double PaymentsTotal { get; set; }
    }
}
