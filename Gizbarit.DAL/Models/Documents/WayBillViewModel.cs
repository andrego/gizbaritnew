﻿using System.Collections.Generic;
using Gizbarit.DAL.Models.Documents.Related;

namespace Gizbarit.DAL.Models.Documents
{
    public class WayBillViewModel:ICatalog
    {
        public WayBillViewModel()
        {
            BaseDocument = new BaseDocument();
        }
        public BaseDocument BaseDocument { get; set; }

        public IEnumerable<DocumentCatalogItemViewModel> CatalogItems { get; set; }
        public IEnumerable<DocumentCatalogItemViewModel> IncludedCatalogItems { get; set; }
    }
}