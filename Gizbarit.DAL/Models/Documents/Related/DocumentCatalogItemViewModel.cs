﻿using System;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Models.Documents.Related
{
    public class DocumentCatalogItemViewModel:IHasCodeAndNullableId
    {
        public Guid? DocumentId { get; set; }
        public int? ID { get; set; }
        [StringLength(50)]
        public string Code { get; set; }
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "CatalogItemNameRequired")]
        public string Name { get; set; }
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "CatalogItemPriceRequired")]
        public double Price { get; set; }
        public double SupplierPrice { get; set; }
        public double? Discount { get; set; }
        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "CatalogItemQuantityRequired")]
        public double Quantity { get; set; }
        public int InUse { get; set; }
        public string ImagePath { get; set; }

        public string Currency { get; set; }
        public string DeliveryTermsOfPayments { get; set; }
        public string TermsOfPaymentType { get; set; }

        public string PointOfDelivery { get; set; }
        public string TimeOfDelivery { get; set; }
        public string ExportAditionInfo { get; set; }


    }
}