﻿using System.Collections.Generic;

namespace Gizbarit.DAL.Models.Documents.Related
{
    public class LinkedDocuments
    {
        public List<LinkedDocumentReceipt> TaxInvoices { get; set; }
        public List<LinkedDocumentReceipt> PreliminaryInvoices { get; set; }
        public LinkedDocumentReceipt SelectedDocumentItem { get; set; }
    }

    public class LinkedDocumentReceipt : BaseDocument
    {
        public double Paid { get; set; }
    }

    public class LinkedPreliminaryInvoice
    {

        public BaseDocument BaseDocument { get; set; }
        public IEnumerable<DocumentCatalogItemViewModel> IncludedCatalogItems { get; set; }
        public double Paid { get; set; }
        public int PaymentDueDateDays { get; set; }
        public string OfferExtras { get; set; }
    }
}