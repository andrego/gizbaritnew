﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Models.Payment;

namespace Gizbarit.DAL.Models.Documents
{
    public class ReceiptViewModel:IPayment
    {
        public ReceiptViewModel()
        {
            BaseDocument = new BaseDocument();
        }
        public BaseDocument BaseDocument { get; set; }
        public BaseDocument SelectedDocumentItem { get; set; }
        public IEnumerable<PaymentTypeViewModel> PaymentTypes { get; set; }
        public IEnumerable<PaymentViewModel> Payments { get; set; }
        public double PaymentsTotal { get; set; }
        public string OtherDescription { get; set; }
    }

    public class ReceiptViewModelContainer
    {
        public Guid Id { get; set; }
        public long Number { get; set; }
        public string ClientName { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime DateCreated { get; set; }
        public double Amount { get; set; }
        public double Deduction { get; set; }
        public bool IsPrinted { get; set; }
        public long? PreliminaryInvoice { get; set; }
        public long? TaxInvoice { get; set; }
        public Guid? ReferenceDocumentId { get; set; }
    }
}