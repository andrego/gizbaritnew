﻿namespace Gizbarit.DAL.Models.Documents
{
    public interface ISendable
    {
        bool Send { get; set; }
        bool SendCopy { get; set; }
    }
}
