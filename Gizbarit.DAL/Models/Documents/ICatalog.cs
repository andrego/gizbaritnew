﻿using System.Collections.Generic;
using Gizbarit.DAL.Models.Documents.Related;

namespace Gizbarit.DAL.Models.Documents
{
    public interface ICatalog
    {
        IEnumerable<DocumentCatalogItemViewModel> CatalogItems { get; set; }
        IEnumerable<DocumentCatalogItemViewModel> IncludedCatalogItems { get; set; }
    }
}
