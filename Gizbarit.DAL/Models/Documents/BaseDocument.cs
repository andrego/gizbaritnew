﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Account;
using Gizbarit.DAL.Models.Organisation;

namespace Gizbarit.DAL.Models.Documents
{
    public class BaseDocument:IPrintable, ISendable
    {
        public BaseDocument()
        {
            IsTaxIncluded = false;
        }
        public Guid Id { get; set; } //= Guid.NewGuid();
        public int OrganizationId { get; set; }
        public DocumentType DocumentType { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "DescriptionRequired")]
        [MaxLength(4000, ErrorMessageResourceType = typeof(Translations.Errors), ErrorMessageResourceName = "MaxLength100")]
        public string Subject { get; set; }
        public long Number { get; set; }
        public BaseAccountShort BusinessInfo { get; set; }
        public IEnumerable<ClientViewModel> ClientsToChoose { get; set; }
        public ClientViewModel ClientInfo { get; set; }
        [DisplayFormat(DataFormatString = "{0:0,0.00}", ApplyFormatInEditMode = true)]
        public double TotalPrice { get; set; }
        public double TotalWithoutRounding { get; set; }

        public bool IsTaxIncluded { get; set; }
        public bool IsSmallBusiness { get; set; }
        public double Tax { get; set; }
        [DisplayFormat(DataFormatString = "{0:0,0.00}", ApplyFormatInEditMode = true)]
        public double TaxAmount { get; set; }
        [DisplayFormat(DataFormatString = "{0:0,0.00}", ApplyFormatInEditMode = true)]
        public double PriceWithTax { get; set; }
        public bool IsRounded{ get; set; }

        public bool InUse { get; set; }

        public DateTime? PaymentDueDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime DateCreated { get; set; }

        public DocumentStatusType Status { get; set; }

        public bool IsCopy => Status == DocumentStatusType.Printed || Status == DocumentStatusType.Send;
        public bool IsNew => Id == Guid.Empty;
        public bool Print { get; set; }
        public bool Send { get; set; }
        public bool SendCopy { get; set; }


        // extension for export document
        
       
        public string Currency { get; set; }

       
        public DeliveryTermsType DeliveryTerms { get; set; }

        
        public TermsOfPaymentType TermsOfPayment { get; set; }

        public bool IsExport { get; set; }

        
        public string PointOfDelivery { get; set; }
        public string TimeOfDelivery { get; set; }

        public string ExportAditionInfo { get; set; }

        public List<string> CurrencyList = new List<string>(); 
    }

    
}