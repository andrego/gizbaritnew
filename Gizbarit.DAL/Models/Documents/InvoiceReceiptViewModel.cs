﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Models.Documents.Related;
using Gizbarit.DAL.Models.Payment;

namespace Gizbarit.DAL.Models.Documents
{
    public class InvoiceReceiptViewModel: ICatalog, IPayment
    {
        public InvoiceReceiptViewModel()
        {
            BaseDocument = new BaseDocument();
        }
        public BaseDocument BaseDocument { get; set; }
        public IEnumerable<DocumentCatalogItemViewModel> CatalogItems { get; set; }
        public IEnumerable<DocumentCatalogItemViewModel> IncludedCatalogItems { get; set; }
        public IEnumerable<PaymentTypeViewModel> PaymentTypes { get; set; }
        public IEnumerable<PaymentViewModel> Payments { get; set; }
        public string OfferExtras { get; set; }
        public double PaymentsTotal { get; set; }
        public int? PaymentDueDateDays { get; set; }
        public bool IsSmallBusiness { get; set; }

    }

    public class InvoiceReceiptViewModelContainer
    {
        public Guid Id { get; set; }
        public long Number { get; set; }
        public long TaxInvoice { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime DateCreated { get; set; }
        public string Description { get; set; }
        public bool IsPrinted { get; set; }
        public double Total { get; set; }
        public double VatAmount { get; set; }
        public double TotalWithVat { get; set; }
        public string ClientName { get; set; }
    }
}