﻿using System.Collections.Generic;
using Gizbarit.DAL.Models.Documents.Related;

namespace Gizbarit.DAL.Models.Documents
{
    public class TaxInvoiceViewModel:ICatalog
    {
        public TaxInvoiceViewModel()
        {
            BaseDocument = new BaseDocument();
        }
        public BaseDocument BaseDocument { get; set; }
        public string OfferExtras { get; set; }
        public int? PaymentDueDateDays { get; set; }
        public IEnumerable<DocumentCatalogItemViewModel> CatalogItems { get; set; }
        public IEnumerable<DocumentCatalogItemViewModel> IncludedCatalogItems { get; set; }
    }
}