using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Web.Caching;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Entities.Advertisement;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Models.CustomerPayment;

namespace Gizbarit.DAL
{
    public partial class GizbratDataContext : DbContext
    {
        public GizbratDataContext()
#if TEST
            : base("TestConnection")
#elif DEBUG
            : base("DebugConnection")
#else
            : base("DefaultConnection")
#endif
        {
            Trace.TraceInformation(this.Database.Connection.ConnectionString);
        }

        public GizbratDataContext(string connectionString)
            : base(connectionString)
        {
#if TEST
           Database.SetInitializer(new MigrateDatabaseToLatestVersion<GizbratDataContext, Gizbarit.DAL.Migrations.Configuration>("TestConnection"));
#elif DEBUG
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GizbratDataContext, Gizbarit.DAL.Migrations.Configuration>("DebugConnection"));
#else
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GizbratDataContext, Gizbarit.DAL.Migrations.Configuration>("DefaultConnection"));
#endif

            //TODO: Remove for migraion
            //      Database.SetInitializer<GizbratDataContext>(new CreateDatabaseIfNotExists<GizbratDataContext>());
        }



        public virtual DbSet<AdminSiteMessage> AdminSiteMessages { get; set; }
        public virtual DbSet<AdminUser> AdminUsers { get; set; }
        public virtual DbSet<Advance> Advances { get; set; }
        public virtual DbSet<AssociatedEmail> AssociatedEmails { get; set; }
        public virtual DbSet<BalanceAdjust> BalanceAdjusts { get; set; }
        public virtual DbSet<BankAccountInitialBalanceYearly> BankAccountInitialBalanceYearlies { get; set; }
        public virtual DbSet<BankAccount> BankAccounts { get; set; }
        public virtual DbSet<BankName> BankNames { get; set; }
        public virtual DbSet<BankTransaction> BankTransactions { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Bundle> Bundles { get; set; }
        public virtual DbSet<BundleType> BundleTypes { get; set; }
        public virtual DbSet<BusinessType> BusinessTypes { get; set; }
        public virtual DbSet<CatalogItem> CatalogItems { get; set; }
        public virtual DbSet<CategoryDrawing> CategoryDrawings { get; set; }
        public virtual DbSet<ClearingCompany> ClearingCompanies { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<CreditCompany> CreditCompanies { get; set; }
        public virtual DbSet<Credit> Credits { get; set; }
        public virtual DbSet<CsvLog> CsvLogs { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<CurrenciesRate> CurrenciesRates { get; set; }
        public virtual DbSet<Deposit> Deposits { get; set; }
        public virtual DbSet<DocumentItem> DocumentItems { get; set; }
        public virtual DbSet<DocumentNumbering> DocumentNumberings { get; set; }
        public virtual DbSet<DocumentReference> DocumentReferences { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<ExpirationDateHistory> ExpirationDateHistories { get; set; }
        public virtual DbSet<ExpirationUpdateReason> ExpirationUpdateReasons { get; set; }
        public virtual DbSet<GeneralClient> GeneralClients { get; set; }
        public virtual DbSet<InquiryStatus> InquiryStatuses { get; set; }
        public virtual DbSet<PriceOffer> PriceOffers { get; set; }
        public virtual DbSet<InvoiceReceipt> InvoiceReceipts { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<Waybill> Waybills { get; set; }
        public virtual DbSet<InvoiceStatus> InvoiceStatuses { get; set; }
        public virtual DbSet<LeadCellNumber> LeadCellNumbers { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<OrganizationOccupationType> OrganizationOccupationTypes { get; set; }
        public virtual DbSet<OrganizationPayPal> OrganizationPayPals { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<OrganizationsClearingAccount> OrganizationsClearingAccounts { get; set; }
        public virtual DbSet<OrganizationsCreditCard> OrganizationsCreditCards { get; set; }
        public virtual DbSet<OrganizationsCreditCount> OrganizationsCreditCounts { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<PaymentType> PaymentTypes { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<PermissionType> PermissionTypes { get; set; }
        public virtual DbSet<PreliminaryInvoice> ProformaInvoices { get; set; }
        public virtual DbSet<Receipt> Receipts { get; set; }
        public virtual DbSet<Retainer> Retainers { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<UserInquiry> UserInquiries { get; set; }
        public virtual DbSet<ClientsPaymentDetaile> ClientsPaymentDetailes { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<DrawingCategory> DrawingCategories { get; set; }
        public virtual DbSet<ExportHistory> ExportHistories { get; set; }
        public virtual DbSet<Page> Pages { get; set; }
        public virtual DbSet<PrintOption> PrintOptions { get; set; }

        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<SupplierInvoice> SupplierInvoices { get; set; }
        public virtual DbSet<SupplierWaybill> SupplierWaybills { get; set; }

        public virtual DbSet<ContentBase> Contents { get; set; }
        public virtual DbSet<StoreCatalogItem> StoreCatalogItems { get; set; }
        public virtual DbSet<StoreInvoice> StoreInvoices { get; set; }
        public virtual DbSet<Order> Orders { get; set; }

        public virtual DbSet<OrganizationInfo> OrganizationInfos { get; set; }
        public virtual DbSet<Advertisement> Advertisements { get; set; }


//        public virtual DbQuery<PdfContent> PdfContents { get; set; }
//        public virtual DbQuery<CertificateContent> Certificates { get; set; }
//        public virtual DbQuery<ImageContent> Images { get; set; }
        public virtual DbSet<ContentNode> ContentNodes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BankAccount>()
                .HasMany(e => e.BankAccountInitialBalanceYearlies)
                .WithRequired(e => e.BankAccount)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BankAccount>()
                .HasMany(e => e.BankTransactions)
                .WithRequired(e => e.BankAccount)
                .HasForeignKey(e => e.BankId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BankAccount>()
                .HasMany(e => e.Deposits)
                .WithRequired(e => e.BankAccount1)
                .HasForeignKey(e => e.BankAccount)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Bundle>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Bundle>()
                .Property(e => e.Descritption)
                .IsUnicode(false);

            //modelBuilder.Entity<Document>()
            //    .HasMany(e => e.WaybillItems)
            //    .WithOptional(e => e.Waybill)
            //    .HasForeignKey(e => e.WaybillID);

            modelBuilder.Entity<Bundle>()
                .HasMany(e => e.Organizations)
                .WithRequired(e => e.Bundle)
                .HasForeignKey(e => e.BundleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BankAccount>()
                .HasRequired(e => e.BankName)
                .WithMany(e => e.BankAccounts)
                .HasForeignKey(e => e.BankNameId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<BundleType>()
                .HasMany(e => e.Bundles)
                .WithRequired(e => e.BundleType)
                .HasForeignKey(e => e.Type)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BusinessType>()
                .HasMany(e => e.Organizations)
                .WithRequired(e => e.BusinessType)
                .HasForeignKey(e => e.BusinessTypeId);

            modelBuilder.Entity<ClearingCompany>()
                .HasMany(e => e.OrganizationsClearingAccounts)
                .WithRequired(e => e.ClearingCompany)
                .HasForeignKey(e => e.CompanyType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.BalanceAdjusts)
                .WithRequired(e => e.Client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Retainers)
                .WithRequired(e => e.Client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Currency>()
                .Property(e => e.ID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .HasMany(e => e.CurrenciesRates)
                .WithRequired(e => e.Currency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Currency>()
                .HasMany(e => e.Documents)
                .WithRequired(e => e.Currency)
                .HasForeignKey(e => e.CurrencyId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CurrenciesRate>()
                .Property(e => e.Symbol)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.UserIP)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .HasOptional(e => e.Deposit)
                .WithRequired(e => e.Document);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.DocumentItems)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.DocumentReferences)
                .WithRequired(e => e.BaseDocument)
                .HasForeignKey(e => e.BaseDocumentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.DocumentReferences)
                .WithRequired(e => e.ReferenceDocument)
                .HasForeignKey(e => e.ReferenceDocumentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Document>()
                .HasOptional(e => e.InvoiceReceipt)
                .WithRequired(e => e.Document);

            modelBuilder.Entity<Document>()
                .HasOptional(e => e.PriceOffer)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Document>()
                .HasOptional(e => e.InvoiceReceipt)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Document>()
                .HasOptional(e => e.Invoice)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Document>()
                .HasOptional(e => e.Waybill)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.Payments)
                .WithRequired(e => e.Document)
                .HasForeignKey(e => e.DocumentID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.Payments1)
                .WithOptional(e => e.Document1)
                .HasForeignKey(e => e.DepositID);

            modelBuilder.Entity<Document>()
                .HasOptional(e => e.PreliminaryInvoice)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Document>()
                .HasOptional(e => e.Receipt)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ExpirationDateHistory>()
                .Property(e => e.Comment)
                .IsFixedLength();

            modelBuilder.Entity<ExpirationUpdateReason>()
                .HasMany(e => e.ExpirationDateHistories)
                .WithRequired(e => e.ExpirationUpdateReason)
                .HasForeignKey(e => e.UpdateReason)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GeneralClient>()
                .HasMany(e => e.Documents)
                .WithOptional(e => e.GeneralClient)
                .HasForeignKey(e => e.GeneralClientId);

            modelBuilder.Entity<InquiryStatus>()
                .HasMany(e => e.UserInquiries)
                .WithRequired(e => e.InquiryStatus1)
                .HasForeignKey(e => e.InquiryStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Waybill>()
                .Property(e => e.DepartureTime)
                .HasPrecision(0);

            modelBuilder.Entity<InvoiceStatus>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<InvoiceStatus>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<LeadCellNumber>()
                .Property(e => e.CountryCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<LeadCellNumber>()
                .Property(e => e.AreaCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<LeadCellNumber>()
                .Property(e => e.Mobile)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.UniqueID)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.Phone1)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.Phone2)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Advertisements)
                .WithRequired(e => e.Organization);
                

            modelBuilder.Entity<Organization>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Advances)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.AssociatedEmails)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.CatalogItems)
                .WithRequired(e => e.Organization)
                .HasForeignKey(e => e.OrganizationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.BalanceAdjusts)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.BankAccounts)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.BankTransactions)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Clients)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.CreditCompanies)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Credits)
                .WithRequired(e => e.Organization)
                .HasForeignKey(e => e.OrganizationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.CsvLogs)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DocumentNumbering>()
                .HasRequired(e => e.Organization)
                .WithMany(e => e.DocumentNumberings)
                .HasForeignKey(e => e.OrganizationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasOptional(f => f.OrganizationInfo)
                .WithRequired(s => s.Organization);
                
                          
                
            
            modelBuilder.Entity<Organization>()
                .HasMany(e => e.ExpirationDateHistories)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasOptional(e => e.OrganizationsClearingAccount)
                .WithRequired(e => e.Organization);

            modelBuilder.Entity<Organization>()
                .HasOptional(e => e.OrganizationsCreditCard)
                .WithRequired(e => e.Organization);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.OrganizationsCreditCounts)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Payments)
                .WithOptional(e => e.Payer)
                .HasForeignKey(e => e.PayerID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Documents)
                .WithRequired(e => e.Client)
                .HasForeignKey(e => e.ClientID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Permissions)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasOptional(e => e.PrintOption)
                .WithRequired(e => e.Organization);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Retainers)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Suppliers)
                .WithRequired(e => e.Organization)
                .HasForeignKey(e => e.OrganizationID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.SupplierInvoices)
                .WithRequired(e => e.Organization)
                .HasForeignKey(e => e.OrganizationID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.SupplierWaybills)
                .WithRequired(e => e.Organization)
                .HasForeignKey(e => e.OrganizationID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.SupplierReceipts)
                .WithRequired(e => e.Organization)
                .HasForeignKey(e => e.OrganizationID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .Property(x => x.Status)
                .HasColumnAnnotation("DefaultValue", OrganizationStatus.Active)
                .IsRequired();

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.Waybills)
                .WithRequired(e => e.Supplier)
                .HasForeignKey(e => e.SupplierID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.Invoices)
                .WithRequired(e => e.Supplier)
                .HasForeignKey(e => e.SupplierID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.Receipts)
                .WithRequired(e => e.Supplier)
                .HasForeignKey(e => e.SupplierID);

            modelBuilder.Entity<SupplierInvoice>()
                .HasMany(e => e.Receipts)
                .WithRequired(e => e.SupplierInvoice)
                .HasForeignKey(e => e.SupplierInvoiceID);

            modelBuilder.Entity<Payment>()
                .Property(e => e.ExpirationDate)
                .IsFixedLength();

            modelBuilder.Entity<PaymentType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PaymentType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<PermissionType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PermissionType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ContentBase>().ToTable("Contents");

            modelBuilder.Entity<Organization>()
                .HasMany(c => c.StoreInvoices)
                .WithRequired(c => c.Organization)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<StoreInvoice>()
                .HasMany(c=>c.Items)
                .WithRequired(c=>c.StoreInvoice)
                .HasForeignKey(c=>c.StoreInvoiceId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Supplier>()
                .HasMany(c => c.StoreInvoices)
                .WithRequired(c => c.Supplier)
                .HasForeignKey(c => c.SupplierId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<CatalogItem>()
                .HasMany(c => c.StoreCatalogItems)
                .WithRequired(c => c.CatalogItem)
                .HasForeignKey(c => c.CatalogItemId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<Supplier>()
               .HasMany(c => c.Orders)
               .WithRequired(c => c.Supplier)
               .HasForeignKey(c => c.SupplierId)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<Order>()
              .HasMany(c => c.OrderDetails)
              .WithRequired(c => c.Order)
              .HasForeignKey(c => c.OrderId)
              .WillCascadeOnDelete(true);

            modelBuilder.Entity<CatalogItem>()
              .HasMany(c => c.StoreCatalogItems)
              .WithRequired(c => c.CatalogItem)
              .HasForeignKey(c => c.CatalogItemId)
              .WillCascadeOnDelete(true);

            modelBuilder.Entity<ContentNode>()
                .HasOptional(x => x.Parent);

            modelBuilder.Entity<ContentNode>()
                .HasMany(c => c.Childs)
                .WithOptional(c => c.Parent)
                .HasForeignKey(c => c.ParentId);


        
            modelBuilder.Entity<Currency>()
                .HasMany(m => m.BankTransactions)
                .WithRequired(m => m.Currency)
                .HasForeignKey(m => m.CurrencyId)
                .WillCascadeOnDelete();

            
            modelBuilder.Entity<Client>()
                .HasMany(m => m.ClientEmailContacts)
                .WithRequired(m => m.Client)
                .HasForeignKey(m => m.ClientId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Client>()
                .HasMany(m => m.ClientPhoneContacts)
                .WithRequired(m => m.Client)
                .HasForeignKey(m => m.ClientId)
                .WillCascadeOnDelete();

            //            modelBuilder.Entity<PdfContent>().ToTable("Contents");
            //            modelBuilder.Entity<ImageContent>().ToTable("Contents");
            //            modelBuilder.Entity<CertificateContent>().ToTable("Contents");
        }

        public static GizbratDataContext Create()
        {
            return new GizbratDataContext();
        }

        public static void Seed()
        {
            string connection = null;
            try
            {
                using (var db = GizbratDataContext.Create())
                {
                    connection = db.Database.Connection.ConnectionString;
                    SeedInternal(db);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Seed failed:" + connection, ex);
            }
        }

        private static void SeedInternal(GizbratDataContext db)
        {
            var isAdminExists = db.AdminUsers.Any();

            var orgs = db.Organizations.Where(x => x.Status == 0).ToArray();

            foreach (var organization in orgs)
            {
                organization.Status = OrganizationStatus.Active;
                db.Organizations.AddOrUpdate(organization);
            }

            db.SaveChanges();

            if (isAdminExists)
            {
                return;
            }

            var users = new List<AdminUser>()
                {
                    new AdminUser()
                    {
                        UserName = "dyachenkoav@gmail.com",
                        Email = "dyachenkoav@gmail.com",
                        Password = "servisall"
                    },
                    new AdminUser()
                    {
                        UserName = "vaikra@barak.net.il",
                        Password = "toref012",
                        Email = "vaikra@barak.net.il"
                    },
                    new AdminUser() {UserName = "deabgo@gmail.com", Password = "tash97", Email = "denisabg@gmail.com"},
                };

            db.AdminUsers.AddRange(users);
            db.SaveChanges();
        }

    }
}

