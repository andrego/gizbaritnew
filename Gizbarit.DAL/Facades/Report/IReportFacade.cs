﻿using System;
using System.Collections.Generic;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Providers.Document;

namespace Gizbarit.DAL.Facades.Report
{
    public class ReportSpecification
    {
        public int? OrganizationId { get; set; }

        public ICollection<DocumentType> DocumentTypes { get; set; } = new List<DocumentType>();

        public DateTime FromDate { get; set; } = DateTime.UtcNow.Date.AddYears(-1);
        public DateTime ToDate { get; set; } = DateTime.UtcNow.Date;

        public ICollection<string> Recipients { get; set; } = new List<string>();

        public bool IsCombined { get; set; } = false;

        public DocumentProviderArgs Args { get; set; } = new DocumentProviderArgs() { IsCombinedReport = false };

    }

    public class DocumentReportSpecification
    {
        public DocumentType DocumentType { get; set; } = DocumentType.TaxInvoice;
        public DateTime FromDate { get; set; } = DateTime.UtcNow.Date.AddMonths(-1);
        public DateTime ToDate { get; set; } = DateTime.UtcNow.Date;
    }

    public interface IReportFacade
    {
        ICollection<Entities.Documents.Document> GetDocuments(DocumentReportSpecification spec);
        void SendByMail(ReportSpecification spec);
        ContentBase GetForPrint(ReportSpecification spec);

        IEnumerable<ContentBase> GetTaxAuthorityReport(ReportSpecification spec);

        ContentBase GetDocumentsCombinedReport(ReportSpecification spec);
    }
}
