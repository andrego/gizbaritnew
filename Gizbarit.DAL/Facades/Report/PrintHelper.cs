﻿using System;
using System.Web;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Organization;

public class PrintHelper
{
    public static PdfContent CreatePdfInspection(HttpServerUtility server, Organization currUser, string[] sumUp)
    {
        try
        {
            PdfDocument document = new PdfDocument();

            PdfSharp.Pdf.PdfPage page = document.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            XPen pen = new XPen(XColors.Black, 2);

            XPdfFontOptions options = new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always);
            XFont font = new XFont("Arial", 18, XFontStyle.Bold, options);
            XTextFormatter tf = new XTextFormatter(gfx);
            tf.Alignment = XParagraphAlignment.Right;
            XRect r = new XRect();

            gfx.DrawRectangle(pen, 5, 5, page.Width - 10, page.Height - 10);

            r.X = page.Width - 450;
            r.Y = 20;
            r.Width = 400;
            tf.Alignment = XParagraphAlignment.Right;
            font = new XFont("Arial", 18, XFontStyle.Bold, options);

            tf.DrawString(Reverse("הפקת קבצים במבנה אחיד עבור : "), font, XBrushes.Black, r);

            r.X = page.Width - 210;
            r.Y = 60;
            r.Width = 150;
            font = new XFont("Arial", 16, XFontStyle.Regular, options);
            tf.DrawString(Reverse("מספר עוסק מורשה : "), font, XBrushes.Black, r);

            r.X = page.Width - 400;
            tf.DrawString(currUser.UniqueID, font, XBrushes.Black, r);

            r.X = page.Width - 210;
            r.Y = 80;
            tf.DrawString(Reverse("שם בית העסק : "), font, XBrushes.Black, r);

            // MAKE BUSINAME WITH MORE LENGTH.
            r.X = page.Width - 600;
            r.Width = 350;
            tf.DrawString(Reverse(currUser.Name), font, XBrushes.Black, r);

            r.Y = 100;
            r.X = page.Width - 240;
            r.Width = 180;
            tf.DrawString(Reverse("הנתונים נשמרו בנתיב הבא : "), font, XBrushes.Black, r);


            string yearFi = DateTime.Now.Year.ToString().Substring(2, 2);
            string month = DateTime.Now.Month.ToString();
            string day = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string minute = DateTime.Now.Minute.ToString();
            if (month.Length == 1)
                month = "0" + month;
            if (day.Length == 1)
                day = "0" + day;
            if (hour.Length == 1)
                hour = "0" + hour;
            if (minute.Length == 1)
                minute = "0" + minute;
            string pathShow = "E:\\OPENFRMT\\" + currUser.UniqueID.ToString() + "." + yearFi + "\\" + month + day + hour + minute;

            r.X = page.Width - 600;
            r.Width = 350;
            tf.DrawString(pathShow, font, XBrushes.Black, r);


            r.Y = 120;
            r.X = page.Width - 210;
            r.Width = 150;
            tf.DrawString(Reverse("טווח תאריכים : "), font, XBrushes.Black, r);

            r.X = page.Width - 600;
            r.Width = 350;
            tf.DrawString((sumUp[1] + " - " + sumUp[2]), font, XBrushes.Black, r);

            r.Y = 180;
            r.X = 0;
            r.Width = page.Width;
            tf.Alignment = XParagraphAlignment.Center;
            tf.DrawString(Reverse("פירוט סך כל סוגי הרשומות שנוצרו בקובץ") + (" BKMVDATA.txt : "), font, XBrushes.Black, r);
            tf.Alignment = XParagraphAlignment.Right;

            r.Y = 210;
            font = new XFont("Arial", 14, XFontStyle.Bold, options);
            r.X = page.Width - 230;
            r.Width = 100;
            tf.DrawString(Reverse("קוד רשומה"), font, XBrushes.Black, r);
            r.X = page.Width - 380;
            r.Width = 150;
            tf.DrawString(Reverse("תיאור רשומה"), font, XBrushes.Black, r);
            r.X = page.Width - 480;
            r.Width = 100;
            tf.DrawString(Reverse("סך רשומות"), font, XBrushes.Black, r);

            font = new XFont("Arial", 14, XFontStyle.Regular, options);

            r.Y += 20;
            r.X = page.Width - 230;
            r.Width = 100;
            tf.DrawString("A100", font, XBrushes.Black, r);
            r.X = page.Width - 380;
            r.Width = 150;
            tf.DrawString(Reverse("רשומת פתיחה"), font, XBrushes.Black, r);
            r.X = page.Width - 480;
            r.Width = 100;
            tf.DrawString("1", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 230;
            r.Width = 100;
            tf.DrawString("C100", font, XBrushes.Black, r);
            r.X = page.Width - 380;
            r.Width = 150;
            tf.DrawString(Reverse("כותרת מסמך"), font, XBrushes.Black, r);
            r.X = page.Width - 480;
            r.Width = 100;
            tf.DrawString(sumUp[6], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 230;
            r.Width = 100;
            tf.DrawString("D110", font, XBrushes.Black, r);
            r.X = page.Width - 380;
            r.Width = 150;
            tf.DrawString(Reverse("פרטי מסמך"), font, XBrushes.Black, r);
            r.X = page.Width - 480;
            r.Width = 100;
            tf.DrawString(sumUp[7], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 230;
            r.Width = 100;
            tf.DrawString("D120", font, XBrushes.Black, r);
            r.X = page.Width - 380;
            r.Width = 150;
            tf.DrawString(Reverse("פרטי קבלות"), font, XBrushes.Black, r);
            r.X = page.Width - 480;
            r.Width = 100;
            tf.DrawString(sumUp[8], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 230;
            r.Width = 100;
            tf.DrawString("B110", font, XBrushes.Black, r);
            r.X = page.Width - 380;
            r.Width = 150;
            tf.DrawString(Reverse("חשבון בהנהלת חשבונות"), font, XBrushes.Black, r);
            r.X = page.Width - 480;
            r.Width = 100;
            tf.DrawString(sumUp[4], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 230;
            r.Width = 100;
            tf.DrawString("M100", font, XBrushes.Black, r);
            r.X = page.Width - 380;
            r.Width = 150;
            tf.DrawString(Reverse("פריטים במלאי"), font, XBrushes.Black, r);
            r.X = page.Width - 480;
            r.Width = 100;
            tf.DrawString(sumUp[5], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 230;
            r.Width = 100;
            tf.DrawString("Z900", font, XBrushes.Black, r);
            r.X = page.Width - 380;
            r.Width = 150;
            tf.DrawString(Reverse("רשומת סיום"), font, XBrushes.Black, r);
            r.X = page.Width - 480;
            r.Width = 100;
            tf.DrawString("1", font, XBrushes.Black, r);

            gfx.DrawRectangle(pen, 130, 210, 360, 165);
            gfx.DrawRectangle(pen, 130, 210, 360, 18);

            font = new XFont("Arial", 14, XFontStyle.Regular, options);
            r.Y = page.Height - 46;
            r.X = 0;
            r.Width = page.Width;
            tf.Alignment = XParagraphAlignment.Center;

            string madeBy = hour + ":" + minute + Reverse(" בשעה ") + DateTime.Now.ToString("dd/MM/yy") + " ,00186701 " + Reverse("הנתונים הופקו באמצעות תוכנת גיזברית , תעודת רישום ");

            tf.DrawString(madeBy, font, XBrushes.Black, r);

            string path = server.MapPath("~/OPENFRMT/" + currUser.ID + "/" + "inspection.pdf");
            if (File.Exists(path))
                File.Delete(path);

            document.Save(path);

            var pdfContent = new PdfContent()
            {
                Name = "inspection.pdf",
                ExtraData = "/OPENFRMT/" + currUser.ID + "/" + "inspection.pdf"
            };


            return pdfContent;


        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public static PdfContent CreatePdfInspectionNumbers(HttpServerUtility server, Organization currUser, string[] sumUp)
    {
        try
        {
            PdfDocument document = new PdfDocument();

            PdfSharp.Pdf.PdfPage page = document.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            XPen pen = new XPen(XColors.Black, 2);

            XPdfFontOptions options = new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always);
            XFont font = new XFont("Arial", 18, XFontStyle.Bold, options);
            XTextFormatter tf = new XTextFormatter(gfx);
            tf.Alignment = XParagraphAlignment.Right;
            XRect r = new XRect();

            string yearFi = DateTime.Now.Year.ToString().Substring(2, 2);
            string month = DateTime.Now.Month.ToString();
            string day = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string minute = DateTime.Now.Minute.ToString();
            if (month.Length == 1)
                month = "0" + month;
            if (day.Length == 1)
                day = "0" + day;
            if (hour.Length == 1)
                hour = "0" + hour;
            if (minute.Length == 1)
                minute = "0" + minute;

            gfx.DrawRectangle(pen, 5, 5, page.Width - 10, page.Height - 10);

            r.X = page.Width - 450;
            r.Y = 20;
            r.Width = 400;
            tf.Alignment = XParagraphAlignment.Right;
            font = new XFont("Arial", 18, XFontStyle.Bold, options);

            tf.DrawString(Reverse("הפקת קבצים במבנה אחיד עבור : "), font, XBrushes.Black, r);

            r.X = page.Width - 210;
            r.Y = 60;
            r.Width = 150;
            font = new XFont("Arial", 16, XFontStyle.Regular, options);
            tf.DrawString(Reverse("מספר עוסק מורשה : "), font, XBrushes.Black, r);

            r.X = page.Width - 400;
            tf.DrawString(currUser.UniqueID, font, XBrushes.Black, r);

            r.X = page.Width - 210;
            r.Y = 80;
            tf.DrawString(Reverse("שם בית העסק : "), font, XBrushes.Black, r);

            // MAKE BUSINAME WITH MORE LENGTH.
            r.X = page.Width - 600;
            r.Width = 350;
            tf.DrawString(Reverse(currUser.Name), font, XBrushes.Black, r);

            r.Y = 100;
            r.X = page.Width - 210;
            r.Width = 150;
            tf.DrawString(Reverse("טווח תאריכים : "), font, XBrushes.Black, r);

            r.X = page.Width - 600;
            r.Width = 350;
            tf.DrawString((sumUp[1] + " - " + sumUp[2]), font, XBrushes.Black, r);

            r.Y = 130;
            r.X = 0;
            r.Width = page.Width;
            tf.Alignment = XParagraphAlignment.Center;
            tf.DrawString(Reverse("פירוט סך הרשומות שהופקו בדו''ח"), font, XBrushes.Black, r);
            tf.Alignment = XParagraphAlignment.Right;

            r.Y = 150;
            font = new XFont("Arial", 14, XFontStyle.Bold, options);

            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString(Reverse("מספר מסמך"), font, XBrushes.Black, r);

            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("סוג מסמך"), font, XBrushes.Black, r);

            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString(Reverse("סה''כ כמותי"), font, XBrushes.Black, r);

            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString(Reverse("סה''כ כספי בש''ח"), font, XBrushes.Black, r);


            font = new XFont("Arial", 14, XFontStyle.Regular, options);


            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("100", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("הזמנה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("200", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("תעודת משלוח"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString(sumUp[9], font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString( Reverse(" ש''ח") + sumUp[10], font, XBrushes.Black, r);


            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("205", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("תעודת משלוח סוכן"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("210", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("תעודת החזרה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("300", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("חשבונית/חשבונית עסקה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString(sumUp[7], font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString( Reverse(" ש''ח") + sumUp[8], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("305", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("חשבונית-מס"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString(sumUp[3], font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString( Reverse(" ש''ח") + sumUp[4], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("310", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("חשבונית ריכוז"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("320", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("חשבונית מס/קבלה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString(sumUp[11], font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString( Reverse(" ש''ח") + sumUp[12], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("330", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("חשבונית מס זיכוי"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString(sumUp[13], font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString( Reverse(" ש''ח") + sumUp[14], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("340", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("חשבונית שריון"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("345", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("חשבונית סוכן"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("400", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("קבלה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString(sumUp[5], font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString( Reverse(" ש''ח") + sumUp[6], font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("405", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("קבלה על תרומות"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("410", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("יציאה מקופה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("420", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("הפקדת בנק"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("500", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("הזמנת רכש"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("600", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("תעודת משלוח רכש"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("610", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("הכרזת רכש"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("700", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("חשבונית מס רכש"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("710", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("זיכוי רכש"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("800", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("יתרת פתיחה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("810", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("כניסה כללית למלאי"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("820", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("יציאה כללית מהמלאי"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("830", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("העברה בין מחסנים"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("840", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("עדכון בעקבות ספירה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("900", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("דו''ח ייצור-כניסה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            r.Y += 20;
            r.X = page.Width - 180;
            r.Width = 100;
            tf.DrawString("910", font, XBrushes.Black, r);
            r.X = page.Width - 330;
            r.Width = 150;
            tf.DrawString(Reverse("דו''ח ייצור-יציאה"), font, XBrushes.Black, r);
            r.X = page.Width - 430;
            r.Width = 100;
            tf.DrawString("0", font, XBrushes.Black, r);
            r.X = page.Width - 630;
            r.Width = 200;
            tf.DrawString("0", font, XBrushes.Black, r);

            gfx.DrawRectangle(pen, 50, 150, 490, 565);
            gfx.DrawRectangle(pen, 50, 150, 490, 18);

            font = new XFont("Arial", 14, XFontStyle.Regular, options);
            r.Y = page.Height - 46;
            r.X = 0;
            r.Width = page.Width;
            tf.Alignment = XParagraphAlignment.Center;

            string madeBy = hour + ":" + minute + Reverse(" בשעה ") + DateTime.Now.ToString("dd/MM/yy") + " ,00186701 " + Reverse("הנתונים הופקו באמצעות תוכנת גיזברית , תעודת רישום ");

            //string madeBy = "הנתונים הופקו באמצעות תוכנת גיזברית , תעודת רישום : 00186701, בתאריך " + DateTime.Now.ToString("dd/MM/yy") + " בשעה " + hour + ":" + minute;

            tf.DrawString(madeBy, font, XBrushes.Black, r);

            string path = server.MapPath("~/OPENFRMT/" + currUser.ID + "/" + "self_inspection.pdf");
            if (File.Exists(path))
                File.Delete(path);

            document.Save(path);


            var pdfContent = new PdfContent()
            {
                Name = "self_inspection.pdf",
                ExtraData = "/OPENFRMT/" + currUser.ID + "/" + "self_inspection.pdf"
            };

            return pdfContent;
        }
        catch (Exception err)
        {
            throw;
        }
    }
    public static string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }

}