﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Facades.Document;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Providers.Report;
using Gizbarit.DAL.Resources.Mail;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils.Email;

namespace Gizbarit.DAL.Facades.Report
{
    public partial class ReportFacade : IReportFacade
    {
        private readonly IDocumentService _documentService;
        private readonly IOrganisationService _organisationService;
        private readonly IMailService _mailService;
        private readonly IReportProvider _provider = new PdfReportProvider();

        private readonly IDocumentFacade _documentFacade;
        //private readonly IReportProvider _provider = new NullReportProvider();

        public ReportFacade(
            IOrganisationService organisationService,
            IDocumentService documentService,
            IMailService mailService,
            IDocumentFacade documentFacade)
        {
            _organisationService = organisationService;
            _documentService = documentService;
            _mailService = mailService;
            _documentFacade = documentFacade;
        }

        public ICollection<Entities.Documents.Document> GetDocuments(DocumentReportSpecification spec)
        {
            if (spec == null)
            {
                throw new ArgumentNullException(nameof(spec));
            }

            var docs = _documentService.GetUserDocuments()
                .Include(d => d.Client)
                .Include(d => d.Payments)
                .Include(d => d.Payments1)
                .Where(x => x.DocumentType == spec.DocumentType)
                .Where(x => x.ActualCreationDate >= spec.FromDate && x.ActualCreationDate <= spec.ToDate)
                .Where(x => x.Status == DocumentStatusType.Printed || x.Status == DocumentStatusType.Send)
                .OrderBy(d => d.Number)
                .ToList();

            return docs;
        }

        public ICollection<Entities.Documents.Document> GetDocuments(DocumentType type, DateTime fromDate,
            DateTime toDate, int? organizationId = null)
        {
            fromDate = fromDate.Date;
            toDate = toDate.AddDays(1).Date;

            var query = _documentService.GetUserDocuments(organizationId)
                .Include(d => d.Payments)
                .Include(d => d.Payments1)
                .Include(d => d.Client)
                .Where(x => x.DocumentType == type)
                .Where(x => x.ActualCreationDate >= fromDate.Date && x.ActualCreationDate < toDate.Date);


            switch (type)
            {
                case DocumentType.Waybill:
                case DocumentType.TaxInvoice:
                case DocumentType.Receipt:
                case DocumentType.TaxInvoiceReceipt:
                    query = query.Where(x => x.Status == DocumentStatusType.Printed ||
                                             x.Status == DocumentStatusType.Send);
                    break;
            }

            var docs = query.OrderBy(d => d.Number).ToList();

            if (type != DocumentType.Receipt)
            {
                return docs;
            }

            foreach (var document in docs)
            {
                var id = document.Receipt?.ReferenceDocument;

                if (id == null)
                {
                    continue;
                }

                var refDocument = _documentService.Get(id.Value);

                switch (refDocument.DocumentType)
                {
                    case DocumentType.PreliminaryInvoice:
                        document.PreliminaryInvoice = new PreliminaryInvoice()
                        {
                            Document = refDocument,
                            ID = refDocument.ID,
                        };
                        break;
                    case DocumentType.TaxInvoice:
                        document.Invoice = new Invoice()
                        {
                            Document = refDocument,
                            ID = refDocument.ID
                        };
                        break;
                }
            }

            return docs;
        }

        public void SendByMail(ReportSpecification spec)
        {
            var organization = _organisationService.GetCurrent();
            var pdfContent = GetForPrint(spec);
            var message = new MailMessage();
            var attachment = new Attachment(pdfContent.GetStream(), pdfContent.Name, pdfContent.Type);


            //TODO: Get from resource
            var text =
                $"{organization.Name} - {TemplateResource.DescriptionText} {spec.FromDate:dd.MM.yyyy} - {spec.ToDate:dd.MM.yyyy}";
            var template = new StringBuilder(TemplateResource.ReportMailTemplate);
            template
                .Replace("{documentName}", text);

            message.From = new MailAddress(organization.Email);
            message.IsBodyHtml = true;
            message.Subject = text;
            message.Body = template.ToString();
            message.Attachments.Add(attachment);

            foreach (var recipient in spec.Recipients)
            {
                try
                {
                    message.To.Add(recipient);
                }
                catch
                {
                    // ignored
                }
            }

            message.To.Add(organization.Email);

            _mailService.Send(message);
        }

        public ContentBase GetForPrint(ReportSpecification spec)
        {
            var args = new ReportProviderArgs()
            {
                Organization = _organisationService.GetCurrent(),
                FromDate = spec.FromDate,
                ToDate = spec.ToDate,

                Invoices = spec.DocumentTypes.Contains(DocumentType.TaxInvoice)
                    ? GetDocuments(DocumentType.TaxInvoice, spec.FromDate, spec.ToDate)
                    : new List<Entities.Documents.Document>(),

                Reciepts = spec.DocumentTypes.Contains(DocumentType.Receipt)
                    ? GetDocuments(DocumentType.Receipt, spec.FromDate, spec.ToDate)
                    : new List<Entities.Documents.Document>(),

                InvoiceReciepts = spec.DocumentTypes.Contains(DocumentType.TaxInvoiceReceipt)
                    ? GetDocuments(DocumentType.TaxInvoiceReceipt, spec.FromDate, spec.ToDate)
                    : new List<Entities.Documents.Document>(),
            };

            var pdfContent = _provider.Create(args);

            return pdfContent;
        }

        public IEnumerable<ContentBase> GetTaxAuthorityReport(ReportSpecification spec)
        {
            var result = this.SetReportsForIrs(spec.FromDate, spec.ToDate, false);

            return result;
        }

        public ContentBase GetDocumentsCombinedReport(ReportSpecification spec)
        {
            var fromDate = spec.FromDate.Date;
            var toDate = spec.ToDate.AddDays(1).Date;
            spec.Args.IsCombinedReport = true;

            //get docs ID
            var docIds = _documentService.GetUserDocuments()
                .Where(x => x.ActualCreationDate >= fromDate && x.ActualCreationDate < toDate)
                .Where(x => x.Status == DocumentStatusType.Printed || x.Status == DocumentStatusType.Send)
                .Where(x => x.OrganizationID == spec.OrganizationId)
                .OrderBy(d => d.Number)
                .Select(x => x.ID)
                .ToList();


            //TODO: Generate One zip with many files or one pdf file
            
            var pdfs = docIds.Select(id => this._documentFacade.GetForPrint(id, true, true)).ToList();
            var bytesList = pdfs.Select(x => x.Data).ToList();
            var result = new PdfContent()
            {
                Name = $"{Guid.NewGuid()}.pdf",
                Data = PdfProviderBase.ConcatAndAddContent(bytesList)
            };

            return result;

        }
    }
}