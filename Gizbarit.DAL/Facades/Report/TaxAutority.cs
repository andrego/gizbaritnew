﻿using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Organization;

namespace Gizbarit.DAL.Facades.Report
{
    using CustomExtensions;
    using Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;

    namespace CustomExtensions
    {
        //Extension methods must be defined in a static class 
        public static class StringExtension
        {
            // This is the extension method. 
            // The first parameter takes the "this" modifier
            // and specifies the type for which the method is defined. 
            public static string RemoveEntries(this string str)
            {
                return str
                    .Replace(",", "")
                    .Replace(".", "")
                    .Replace("+", "")
                    .Replace("-", "");
            }

            public static string AddPaddingAndSign(this string str, int size, string charToFill = " ", string sign = "")
            {
                str = string.IsNullOrWhiteSpace(str) ? string.Empty : str;


                if (string.IsNullOrWhiteSpace(sign))
                {
                    while (str.Length < size)
                    {
                        str = charToFill + str;
                    }
                    return str;
                }
                else
                {
                    while (str.Length < size)
                    {
                        if (str.Length != size - 1)
                        {
                            str = charToFill + str;
                        }
                        else
                        {
                            str = sign + str;
                        }
                    }
                    return str;
                }
            }
        }
    }


    /// <summary>
    /// Summary description for UnicFilesServices
    /// </summary>
    public partial class ReportFacade
    {
        public IEnumerable<ContentBase> SetReportsForIrs(DateTime startDate, DateTime endDate, bool otherBranches)
        {
            var curUser = _organisationService.GetCurrent();
            var organizationId = curUser.ID;
            int //totFormsNum = 0,
                runningNumber = 2,
                dealsCount = 0,         // ----- חשבונית/חשבונית עסקה -- 300
                deliveryCount = 0,      //------ תעודת משלוח         ---- 200  
                taxCount = 0,           // ----- חשבונית-מס          ---- 305 
                taxCreditCount = 0,     // ----- חשבונית מס זיכוי   ---- 330 
                taxReceiptCount = 0,    // ----- חשבונית מס / קבלה  ---- 320
                recCount = 0,           // ----- קבלה   C100-400
                itemsCount = 0,  
                taxRecieptRecCount = 0,
                itemsRecCount = 0,      // ---- קבלה    D120-400
                clCount = 0;            // ---- הזמנה   B110-100

            int curRandNumber = Math.Abs(Guid.NewGuid().GetHashCode()); 
            double taxRateForUser = 1 + curUser.TaxRate / 100;

            //preleminary invoice   C100-300
            //
            // ------- חשבונית/חשבונית עסקה
            //
            Document[] dealsList =
                GetAllDealFormsByUserAndDates(organizationId, startDate, endDate);

            //Waybill               C100-200 
            //
            // ------ תעודת משלוח
            //
            var deliveryList =
                GetAllDeliveryFormsByUserAndDates(organizationId, startDate, endDate);

            //invoice               C100-305
            //
            // ------- חשבונית-מס
            //
            Document[] taxList = 
                GetAllTaxFormsByUserAndDates(organizationId, startDate, endDate)
                .Where(t => t.TotalwithoutTax >= 0)
                .ToArray();

            //invoice refund        C100-330
            //
            // ----- חשבונית מס זיכוי  C100-330
            // --- חשבונית מס זיכוי     D110-330
            //
            Document[] taxCreditList = 
                GetAllTaxFormsByUserAndDates(organizationId, startDate, endDate)
                .Where(t => t.TotalwithoutTax < 0)
                .ToArray();

            //invoicerecipt         C100-320
            //
            // ------ חשבונית מס / קבלה     
            //
            Document[] taxReceiptList =
                GetAllTaxReceiptFormsByUserAndDates(organizationId, startDate, endDate);

            //recipt                C100-400
            //
            // ----- קבלה    
            //
            Document[] recList =
                GetAllReceiptsByUserAndDates(organizationId, startDate, endDate);


            Client[] clList = null;

            string[][] dealsDet = null;             // ---- חשבונית/חשבונית עסקה       C100-300
            string[][] deliveryDet = null;          // ---- תעודת משלוח                 C100-200 
            string[][] taxDet = null;               // ---- חשבונית-מס                  C100-305 
            string[][] taxCreditDet = null;         // ---- חשבונית מס זיכוי           C100-330 
            string[][] taxReceiptDet = null;        // ---- חשבונית מס / קבלה          C100-320
            string[][] recDet = null;               // ---- קבלה   C100-400

            string[][] itemsDet = null;             // ---- חשבונית/חשבונית עסקה      D110-300
            string[][] itemsRecDet = null;          // ---- קבלה                        D120-400
            string[][] itemsTaxRecDet = null;       // ---- חשבונית מס / קבלה          D120-320
            string[][] itemsLongList = null;        //                                  "M100"

            string[][] acountManage = null;         // ---- הזמנה                       B110-100

            string[] iniDet = new string[6];        /*         INI SUMUP INFO       */

            string[] sumUpForm = new string[11];


            //
            // ------- חשבונית/חשבונית עסקה         C100-300
            //
            if (dealsList.Any())
            {
                dealsCount = dealsList.Length;
                int i = 0;
               // totFormsNum += dealsCount;

                dealsDet = new string[dealsCount][];
                foreach (var curDeal in dealsList)
                {
                    itemsCount += curDeal.DocumentItems.Count;
                    string year = curDeal.DateCreated.Year.ToString();
                    string month = curDeal.DateCreated.Month.ToString();
                    if (month.Length == 1)
                    {
                        month = "0" + month;
                    }

                    string day = curDeal.DateCreated.Day.ToString();
                    if (day.Length == 1)
                    {
                        day = "0" + day;
                    }

                    string hourDeal = curDeal.DateCreated.Hour.ToString();
                    if (hourDeal.Length == 1)
                    {
                        hourDeal = "0" + hourDeal;
                    }

                    string minuteDeal = curDeal.DateCreated.Minute.ToString();
                    if (minuteDeal.Length == 1)
                    {
                        minuteDeal = "0" + minuteDeal;
                    }

                    var client = curDeal.Client;
                    dealsDet[i] = new string[29];
                    dealsDet[i][0] = "C100";
                    dealsDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                    dealsDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                    dealsDet[i][3] = "300";
                    dealsDet[i][4] = curDeal.Number.ToString().AddPaddingAndSign(20, "0");
                    dealsDet[i][5] = year + month + day;
                    dealsDet[i][6] = hourDeal + minuteDeal;
                    dealsDet[i][7] = client.Name.AddPaddingAndSign(50);
                    dealsDet[i][8] = client.Address.AddPaddingAndSign(90);
                    dealsDet[i][9] = "".AddPaddingAndSign(38);
                    dealsDet[i][9] = "IL" + dealsDet[i][9];
                    dealsDet[i][10] = client.Phone.AddPaddingAndSign(15);
                    dealsDet[i][11] = "000000000";
                    dealsDet[i][12] = year + month + day;
                    dealsDet[i][13] = "+00000000000000";
                    dealsDet[i][14] = "   ";

                    if (curDeal.Total >= 0)
                    {
                        dealsDet[i][15] =
                            curDeal.TotalwithoutTax.ToString("N2").RemoveEntries().AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        dealsDet[i][15] =
                            (curDeal.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    dealsDet[i][16] = "+00000000000000";

                    if (curDeal.TotalwithoutTax >= 0)
                    {
                        dealsDet[i][17] =
                            curDeal.TotalwithoutTax.ToString("N2").RemoveEntries().AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        dealsDet[i][17] =
                            (curDeal.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curDeal.TotalTaxAmount >= 0)
                    {
                        dealsDet[i][18] = curDeal.TotalTaxAmount.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        dealsDet[i][18] =
                            (curDeal.TotalTaxAmount * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curDeal.TotalTaxAmount >= 0)
                    {
                        dealsDet[i][19] = curDeal.Total.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        dealsDet[i][19] =
                            (curDeal.Total * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    dealsDet[i][20] = "+00000000000";
                    dealsDet[i][21] = client.UniqueId.AddPaddingAndSign(15);
                    dealsDet[i][22] = "          ";
                    dealsDet[i][23] = " ";
                    dealsDet[i][24] = year + month + day;
                    dealsDet[i][25] = "       ";

                    string name = curUser.Name + " " + curUser.LastName;
                    int nameI = 0;
                    dealsDet[i][26] = "";
                    while (dealsDet[i][26].Length < 9)
                    {
                        if (name.Length > nameI)
                        {
                            dealsDet[i][26] += name[nameI];
                        }
                        else
                        {
                            dealsDet[i][26] = " " + dealsDet[i][26];
                        }

                        nameI++;

                    }

                    dealsDet[i][27] = curDeal.Number.ToString().AddPaddingAndSign(7, "0");
                    dealsDet[i][28] = "             ";
                    runningNumber++;
                    i++;
                }
            }

            //
            //------תעודת משלוח     C100-200 
            //
            if (deliveryList.Any())
            {
                deliveryCount = deliveryList.Length;
                int i = 0;

                deliveryDet = new string[deliveryCount /*+ 1*/][];
                foreach (var curDelivery in deliveryList)
                {
                    itemsCount += curDelivery.DocumentItems.Count;

                    string year = curDelivery.DateCreated.Year.ToString();
                    string month = curDelivery.DateCreated.Month.ToString();
                    if (month.Length == 1)
                    {
                        month = "0" + month;
                    }

                    string day = curDelivery.DateCreated.Day.ToString();
                    if (day.Length == 1)
                    {
                        day = "0" + day;
                    }

                    string hourDeal = curDelivery.DateCreated.Hour.ToString();
                    if (hourDeal.Length == 1)
                    {
                        hourDeal = "0" + hourDeal;
                    }

                    string minuteDeal = curDelivery.DateCreated.Minute.ToString();
                    if (minuteDeal.Length == 1)
                    {
                        minuteDeal = "0" + minuteDeal;
                    }

                    var client = curDelivery.Client;
                    deliveryDet[i] = new string[29];
                    deliveryDet[i][0] = "C100";
                    deliveryDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                    deliveryDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                    deliveryDet[i][3] = "200";
                    deliveryDet[i][4] = curDelivery.Number.ToString().AddPaddingAndSign(20, "0");
                    deliveryDet[i][5] = year + month + day;
                    deliveryDet[i][6] = hourDeal + minuteDeal;
                    deliveryDet[i][7] = client.Name.AddPaddingAndSign(50);

                    deliveryDet[i][8] = client.Address.AddPaddingAndSign(90);
                    deliveryDet[i][9] = "".AddPaddingAndSign(38);
                    deliveryDet[i][9] = "IL" + deliveryDet[i][9];
                    deliveryDet[i][10] = client.Phone.AddPaddingAndSign(15);
                    deliveryDet[i][11] = "000000000";
                    deliveryDet[i][12] = year + month + day;
                    deliveryDet[i][13] = "+00000000000000";
                    deliveryDet[i][14] = "   ";

                    if (curDelivery.Total >= 0)
                    {
                        deliveryDet[i][15] =
                            curDelivery.TotalwithoutTax.ToString("N2")
                                .RemoveEntries()
                                .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        deliveryDet[i][15] =
                            (curDelivery.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    deliveryDet[i][16] = "+00000000000000";

                    if (curDelivery.TotalwithoutTax >= 0)
                    {
                        deliveryDet[i][17] =
                            curDelivery.TotalwithoutTax.ToString("N2")
                                .RemoveEntries()
                                .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        deliveryDet[i][17] =
                            (curDelivery.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curDelivery.TotalTaxAmount >= 0)
                    {
                        deliveryDet[i][18] =
                            curDelivery.TotalTaxAmount.ToString("N2")
                                .RemoveEntries()
                                .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        deliveryDet[i][18] =
                            (curDelivery.TotalTaxAmount * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curDelivery.TotalTaxAmount >= 0)
                    {
                        deliveryDet[i][19] = curDelivery.Total.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        deliveryDet[i][19] =
                            (curDelivery.Total * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    deliveryDet[i][20] = "+00000000000";
                    deliveryDet[i][21] = client.UniqueId.AddPaddingAndSign(15);
                    deliveryDet[i][22] = "          ";
                    deliveryDet[i][23] = " ";
                    deliveryDet[i][24] = year + month + day;
                    deliveryDet[i][25] = "       ";

                    string name = curUser.Name + " " + curUser.LastName;
                    int nameI = 0;
                    deliveryDet[i][26] = "";

                    while (deliveryDet[i][26].Length < 9)
                    {
                        if (name.Length > nameI)
                        {
                            deliveryDet[i][26] += name[nameI];
                        }
                        else
                        {
                            deliveryDet[i][26] = " " + deliveryDet[i][26];
                        }

                        nameI++;
                    }

                    deliveryDet[i][27] = curDelivery.Number.ToString().AddPaddingAndSign(7, "0");
                    deliveryDet[i][28] = "             ";
                    runningNumber++;
                    i++;
                }
            }

            //
            // ------- חשבונית-מס       C100-305 
            //
            if (taxList.Any())
            {
                taxCount = taxList.Length;

                taxDet = new string[taxCount][];
                int i = 0;

                foreach (var curTax in taxList)
                {
                    itemsCount += curTax.DocumentItems.Count;

                    string year = curTax.DateCreated.Year.ToString();
                    string month = curTax.DateCreated.Month.ToString();
                    if (month.Length == 1)
                    {
                        month = "0" + month;
                    }

                    string day = curTax.DateCreated.Day.ToString();
                    if (day.Length == 1)
                    {
                        day = "0" + day;
                    }

                    string hourDeal = curTax.DateCreated.Hour.ToString();
                    if (hourDeal.Length == 1)
                    {
                        hourDeal = "0" + hourDeal;
                    }

                    string minuteDeal = curTax.DateCreated.Minute.ToString();
                    if (minuteDeal.Length == 1)
                    {
                        minuteDeal = "0" + minuteDeal;
                    }

                    var client = curTax.Client;
                    taxDet[i] = new string[29];
                    taxDet[i][0] = "C100";
                    taxDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                    taxDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                    taxDet[i][3] = "305";
                    taxDet[i][4] = curTax.Number.ToString().AddPaddingAndSign(20, "0");
                    taxDet[i][5] = year + month + day;
                    taxDet[i][6] = hourDeal + minuteDeal;
                    taxDet[i][7] = client.Name.AddPaddingAndSign(50);

                    taxDet[i][8] = client.Address.AddPaddingAndSign(90);
                    taxDet[i][9] = "".AddPaddingAndSign(38);
                    taxDet[i][9] = "IL" + taxDet[i][9];
                    taxDet[i][10] = client.Phone.AddPaddingAndSign(15);
                    taxDet[i][11] = "000000000";
                    taxDet[i][12] = year + month + day;
                    taxDet[i][13] = "+00000000000000";
                    taxDet[i][14] = "   ";

                    if (curTax.Total >= 0)
                    {
                        taxDet[i][15] = curTax.TotalwithoutTax.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxDet[i][15] =
                            (curTax.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    taxDet[i][16] = "+00000000000000";

                    if (curTax.TotalwithoutTax >= 0)
                    {
                        taxDet[i][17] = curTax.TotalwithoutTax.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxDet[i][17] =
                            (curTax.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curTax.TotalTaxAmount >= 0)
                    {
                        taxDet[i][18] = curTax.TotalTaxAmount.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxDet[i][18] =
                            (curTax.TotalTaxAmount * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curTax.TotalTaxAmount >= 0)
                    {
                        taxDet[i][19] = curTax.Total.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxDet[i][19] =
                            (curTax.Total * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    taxDet[i][20] = "+00000000000";
                    taxDet[i][21] = curTax.Client.UniqueId.AddPaddingAndSign(15);
                    taxDet[i][22] = "          ";
                    taxDet[i][23] = " ";
                    taxDet[i][24] = year + month + day;
                    taxDet[i][25] = "       ";

                    string name = curUser.Name + " " + curUser.LastName;
                    int nameI = 0;
                    taxDet[i][26] = "";
                    while (taxDet[i][26].Length < 9)
                    {
                        if (name.Length > nameI)
                        {
                            taxDet[i][26] += name[nameI];
                        }
                        else
                        {
                            taxDet[i][26] = " " + taxDet[i][26];
                        }

                        nameI++;
                    }

                    taxDet[i][27] = curTax.Number.ToString().AddPaddingAndSign(7, "0");
                    taxDet[i][28] = "             ";
                    runningNumber++;
                    i++;
                }
            }


            //
            // ----- חשבונית מס זיכוי       C100-330 
            //
            if (taxCreditList != null)
            {
                taxCreditCount = taxCreditList.Length;

                taxCreditDet = new string[taxCreditCount][];
                int i = 0;

                foreach (var curTax in taxCreditList)
                {
                    itemsCount += curTax.DocumentItems.Count;

                    string year = curTax.DateCreated.Year.ToString();
                    string month = curTax.DateCreated.Month.ToString();
                    if (month.Length == 1)
                    {
                        month = "0" + month;
                    }

                    string day = curTax.DateCreated.Day.ToString();
                    if (day.Length == 1)
                    {
                        day = "0" + day;
                    }

                    string hourDeal = curTax.DateCreated.Hour.ToString();
                    if (hourDeal.Length == 1)
                    {
                        hourDeal = "0" + hourDeal;
                    }

                    string minuteDeal = curTax.DateCreated.Minute.ToString();

                    if (minuteDeal.Length == 1)
                    {
                        minuteDeal = "0" + minuteDeal;
                    }

                    taxCreditDet[i] = new string[29];
                    var client = curTax.Client;

                    taxCreditDet[i][0] = "C100";
                    taxCreditDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                    taxCreditDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                    taxCreditDet[i][3] = "330";
                    taxCreditDet[i][4] = curTax.Number.ToString().AddPaddingAndSign(20, "0");
                    taxCreditDet[i][5] = year + month + day;
                    taxCreditDet[i][6] = hourDeal + minuteDeal;
                    taxCreditDet[i][7] = client.Name.AddPaddingAndSign(50);


                    taxCreditDet[i][8] = client.Address.AddPaddingAndSign(90);
                    taxCreditDet[i][9] = "".AddPaddingAndSign(38);
                    taxCreditDet[i][9] = "IL" + taxCreditDet[i][9];
                    taxCreditDet[i][10] = client.Phone.AddPaddingAndSign(15);
                    taxCreditDet[i][11] = "000000000";
                    taxCreditDet[i][12] = year + month + day;
                    taxCreditDet[i][13] = "+00000000000000";
                    taxCreditDet[i][14] = "   ";
                    if (curTax.Total >= 0)
                    {
                        taxCreditDet[i][15] =
                            curTax.TotalwithoutTax.ToString("N2").RemoveEntries().AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxCreditDet[i][15] =
                            (curTax.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    taxCreditDet[i][16] = "+00000000000000";

                    if (curTax.TotalwithoutTax >= 0)
                    {
                        taxCreditDet[i][17] =
                            curTax.TotalwithoutTax.ToString("N2").RemoveEntries().AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxCreditDet[i][17] =
                            (curTax.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curTax.TotalTaxAmount >= 0)
                    {
                        taxCreditDet[i][18] =
                            curTax.TotalTaxAmount.ToString("N2").RemoveEntries().AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxCreditDet[i][18] =
                            (curTax.TotalTaxAmount * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curTax.TotalTaxAmount >= 0)
                    {
                        taxCreditDet[i][19] = curTax.Total.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxCreditDet[i][19] =
                            (curTax.Total * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    taxCreditDet[i][20] = "+00000000000";
                    taxCreditDet[i][21] = client.UniqueId.AddPaddingAndSign(15);
                    taxCreditDet[i][22] = "          ";
                    taxCreditDet[i][23] = " ";
                    taxCreditDet[i][24] = year + month + day;
                    taxCreditDet[i][25] = "       ";

                    string name = curUser.Name + " " + curUser.LastName;
                    int nameI = 0;
                    taxCreditDet[i][26] = "";
                    while (taxCreditDet[i][26].Length < 9)
                    {
                        if (name.Length > nameI)
                        {
                            taxCreditDet[i][26] += name[nameI];
                        }
                        else
                        {
                            taxCreditDet[i][26] = " " + taxCreditDet[i][26];
                        }

                        nameI++;
                    }

                    taxCreditDet[i][27] = curTax.Number.ToString().AddPaddingAndSign(7, "0");
                    taxCreditDet[i][28] = "             ";
                    runningNumber++;
                    i++;
                }
            }


            //
            // ----- חשבונית מס / קבלה      C100-320
            //
            if (taxReceiptList.Any())
            {
                taxReceiptCount = taxReceiptList.Length;

                taxReceiptDet = new string[taxReceiptCount + 1][];
                int i = 0;

                foreach (var curTaxRec in taxReceiptList)
                {
                    itemsCount += curTaxRec.DocumentItems.Count;

                    string year = curTaxRec.DateCreated.Year.ToString();
                    string month = curTaxRec.DateCreated.Month.ToString();
                    if (month.Length == 1)
                    {
                        month = "0" + month;
                    }

                    string day = curTaxRec.DateCreated.Day.ToString();
                    if (day.Length == 1)
                    {
                        day = "0" + day;
                    }

                    string hourDeal = curTaxRec.DateCreated.Hour.ToString();
                    if (hourDeal.Length == 1)
                    {
                        hourDeal = "0" + hourDeal;
                    }

                    string minuteDeal = curTaxRec.DateCreated.Minute.ToString();
                    if (minuteDeal.Length == 1)
                    {
                        minuteDeal = "0" + minuteDeal;
                    }

                    var client = curTaxRec.Client;

                    taxReceiptDet[i] = new string[29];

                    taxReceiptDet[i][0] = "C100";
                    taxReceiptDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                    taxReceiptDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                    taxReceiptDet[i][3] = "320";
                    taxReceiptDet[i][4] = curTaxRec.Number.ToString().AddPaddingAndSign(20, "0");
                    taxReceiptDet[i][5] = year + month + day;
                    taxReceiptDet[i][6] = hourDeal + minuteDeal;
                    taxReceiptDet[i][7] = client.Name.AddPaddingAndSign(50);

                    taxReceiptDet[i][8] = client.Address.AddPaddingAndSign(90);
                    taxReceiptDet[i][9] = "".AddPaddingAndSign(38);
                    taxReceiptDet[i][9] = "IL" + taxReceiptDet[i][9];
                    taxReceiptDet[i][10] = client.Phone.AddPaddingAndSign(15);
                    taxReceiptDet[i][11] = "000000000";
                    taxReceiptDet[i][12] = year + month + day;
                    taxReceiptDet[i][13] = "+00000000000000";
                    taxReceiptDet[i][14] = "   ";

                    if (curTaxRec.Total >= 0)
                    {
                        taxReceiptDet[i][15] =
                            curTaxRec.TotalwithoutTax.ToString("N2").RemoveEntries()
                                .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxReceiptDet[i][15] =
                            (curTaxRec.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    taxReceiptDet[i][16] = "+00000000000000";

                    if (curTaxRec.TotalwithoutTax >= 0)
                    {
                        taxReceiptDet[i][17] =
                            curTaxRec.TotalwithoutTax.ToString("N2").RemoveEntries()
                                .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxReceiptDet[i][17] =
                            (curTaxRec.TotalwithoutTax * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curTaxRec.TotalTaxAmount >= 0)
                    {
                        taxReceiptDet[i][18] =
                            curTaxRec.TotalTaxAmount.ToString("N2").RemoveEntries().AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxReceiptDet[i][18] =
                            (curTaxRec.TotalTaxAmount * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (curTaxRec.TotalTaxAmount >= 0)
                    {
                        taxReceiptDet[i][19] = curTaxRec.Total.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        taxReceiptDet[i][19] =
                            (curTaxRec.Total * -1).ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    taxReceiptDet[i][20] = "+00000000000";
                    taxReceiptDet[i][21] = client.UniqueId.AddPaddingAndSign(15);
                    taxReceiptDet[i][22] = "          ";
                    taxReceiptDet[i][23] = " ";
                    taxReceiptDet[i][24] = year + month + day;
                    taxReceiptDet[i][25] = "       ";

                    string name = curUser.Name + " " + curUser.LastName;
                    int nameI = 0;
                    taxReceiptDet[i][26] = "";
                    while (taxReceiptDet[i][26].Length < 9)
                    {
                        if (name.Length > nameI)
                        {
                            taxReceiptDet[i][26] += name[nameI];
                        }
                        else
                        {
                            taxReceiptDet[i][26] = " " + taxReceiptDet[i][26];
                        }

                        nameI++;
                    }

                    taxReceiptDet[i][27] = curTaxRec.Number.ToString().AddPaddingAndSign(7, "0");
                    taxReceiptDet[i][28] = "             ";
                    runningNumber++;
                    i++;
                }
            }

            //
            // ----- קבלה   C100-400
            //
            if (recList.Any())
            {
                recCount = recList.Length;
                
                recDet = new string[recCount][];
                int i = 0;

                foreach (var curRec in recList)
                {
                    itemsRecCount += curRec.Payments.Count;

                    string year = curRec.DateCreated.Year.ToString();
                    string month = curRec.DateCreated.Month.ToString();
                    if (month.Length == 1)
                        month = "0" + month;
                    string day = curRec.DateCreated.Day.ToString();
                    if (day.Length == 1)
                        day = "0" + day;
                    string hourDeal = curRec.DateCreated.Hour.ToString();
                    if (hourDeal.Length == 1)
                        hourDeal = "0" + hourDeal;
                    string minuteDeal = curRec.DateCreated.Minute.ToString();
                    if (minuteDeal.Length == 1)
                        minuteDeal = "0" + minuteDeal;

                    var client = curRec.Client;
                    recDet[i] = new string[29];
                    recDet[i][0] = "C100";
                    recDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                    recDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                    recDet[i][3] = "400";
                    recDet[i][4] = curRec.Number.ToString().AddPaddingAndSign(20, "0");
                    recDet[i][5] = year + month + day;
                    recDet[i][6] = hourDeal + minuteDeal;
                    recDet[i][7] = client.Name.AddPaddingAndSign(50);


                    recDet[i][8] = client.Address.AddPaddingAndSign(90);
                    recDet[i][9] = "".AddPaddingAndSign(38);
                    recDet[i][9] = "IL" + recDet[i][9];
                    recDet[i][10] = client.Phone.AddPaddingAndSign(15);
                    recDet[i][11] = "000000000";
                    recDet[i][12] = year + month + day;
                    recDet[i][13] = "+00000000000000";
                    recDet[i][14] = "   ";
                    recDet[i][15] = curRec.Total.ToString("N2").RemoveEntries().AddPaddingAndSign(15, "0", "+");
                    recDet[i][16] = "+00000000000000";
                    recDet[i][17] = curRec.Total.ToString("N2").RemoveEntries().AddPaddingAndSign(15, "0", "+");
                    recDet[i][18] = "0".AddPaddingAndSign(15, "0", "+");
                    recDet[i][19] = curRec.Total.ToString("N2").RemoveEntries().AddPaddingAndSign(15, "0", "+");
                    recDet[i][20] = "+00000000000";
                    recDet[i][21] = client.UniqueId.AddPaddingAndSign(15);
                    recDet[i][22] = "          ";
                    recDet[i][23] = " ";
                    recDet[i][24] = year + month + day;
                    recDet[i][25] = "       ";

                    string name = curUser.Name + " " + curUser.LastName;
                    int nameI = 0;

                    recDet[i][26] = "";
                    while (recDet[i][26].Length < 9)
                    {
                        if (name.Length > nameI)
                        {
                            recDet[i][26] += name[nameI];
                        }
                        else
                        {
                            recDet[i][26] = "-" + recDet[i][26];
                        }

                        nameI++;
                    }
                    recDet[i][27] = curRec.Number.ToString().AddPaddingAndSign(7, "0");
                    recDet[i][28] = "             ";
                    runningNumber++;
                    i++;
                }
            }

            /*         INI SUMUP INFO       */
            string sumHeaders = (runningNumber - 2).ToString().AddPaddingAndSign(15, "0");

            iniDet[0] = "C100" + sumHeaders;
            /*         For PDF DATA Checking       */
            var sumForPdfC100 = runningNumber - 2;
            /*                             */






            if (itemsCount != 0)
            {
                itemsDet = new string[itemsCount][];
                int i = 0;

                //
                // ----     חשבונית/חשבונית עסקה    D110-300
                //
                if (dealsList != null)
                {
                    foreach (var curDeal in dealsList)
                    {
                        int itemNumInForm = 1;

                        string year = curDeal.DateCreated.Year.ToString();
                        string month = curDeal.DateCreated.Month.ToString();
                        if (month.Length == 1)
                        {
                            month = "0" + month;
                        }

                        string day = curDeal.DateCreated.Day.ToString();
                        if (day.Length == 1)
                        {
                            day = "0" + day;
                        }

                        foreach (var curItem in curDeal.DocumentItems)
                        {
                            // ----     חשבונית/חשבונית עסקה    D110-300
                            itemsDet[i] = new string[24];
                            itemsDet[i][0] = "D110";
                            itemsDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsDet[i][3] = "300";
                            itemsDet[i][4] = curDeal.Number.ToString().AddPaddingAndSign(20, "0");
                            itemsDet[i][5] = itemNumInForm.ToString().AddPaddingAndSign(4, "0");
                            itemsDet[i][6] = "000";
                            itemsDet[i][7] = "                    ";
                            itemsDet[i][8] = "2";
                            itemsDet[i][9] = curItem.ID.ToString().AddPaddingAndSign(20);

                            if (curItem.Name.Length > 30)
                            {
                                itemsDet[i][10] =
                                    curItem
                                    .Name
                                    .Replace("\r\n", "")
                                    .Replace("\t", "")
                                    .Substring(0, 29)
                                    .AddPaddingAndSign(30);
                            }
                            else
                            {
                                itemsDet[i][10] = 
                                    curItem
                                    .Name
                                    .Replace("\r\n", "")
                                    .Replace("\t", "")
                                    .AddPaddingAndSign(30);
                            }

                            itemsDet[i][11] = "".AddPaddingAndSign(50);
                            itemsDet[i][12] = "".AddPaddingAndSign(30);
                            itemsDet[i][13] = "יחידה".AddPaddingAndSign(20);

                            if (curItem.Quantity >= 0)
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "+");
                            }
                            else
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "-");
                            }

                            double curItemPrice = curItem.Price;

                            if (curItem.Discount > 0)
                            {
                                var curItemPriceDiscount = 100 - curItem.Discount;

                                curItemPrice = 
                                    (double)(curItemPrice * curItemPriceDiscount)/100;
                            }

                            if (curItemPrice >= 0)
                            {
                                if (curDeal.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                    curItemPrice.ToString("N2").RemoveEntries()
                                    .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                            }
                            else
                            {
                                if (curDeal.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                        (curItemPrice * -1).ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser * -1)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                }
                            }

                            itemsDet[i][16] = "+00000000000000";

                            if (curItem.Total >= 0)
                            {
                                if (curDeal.IsTaxIncluded == false )
                                {
                                    itemsDet[i][17] =
                                        curItem.Total.ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    if  (curItem.Discount > 0)
                                    {
                                        itemsDet[i][17] =
                                            (curItemPrice * curItem.Quantity / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                    }
                                    else
                                    {
                                        itemsDet[i][17] =
                                            Math.Round(curItem.Total / taxRateForUser)
                                                .ToString("N2")
                                                .RemoveEntries()
                                                .AddPaddingAndSign(15, "0", "+");
                                    }
                                }
                            }

                            else
                            {
                                if (curDeal.IsTaxIncluded == false)
                                {
                                    itemsDet[i][17] =
                                        (curItem.Total * -1).ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    if (curItem.Discount > 0)
                                    {
                                        itemsDet[i][17] =
                                            (curItemPrice * curItem.Quantity / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                    }
                                    else
                                    {
                                        itemsDet[i][17] =
                                            Math.Round(curItem.Total / taxRateForUser)
                                                .ToString("N2")
                                                .RemoveEntries()
                                                .AddPaddingAndSign(15, "0", "-");
                                    }
                                }
                            }

                            if (curUser.City == "אילת")
                            {
                                itemsDet[i][18] = "0000";
                            }
                            else
                            {
                                string iDet = curUser.TaxRate.ToString("0000");
                                itemsDet[i][18] = iDet.AddPaddingAndSign(4, "0");
                            }

                            itemsDet[i][19] = "       ";
                            itemsDet[i][20] = year + month + day;
                            itemsDet[i][21] = curDeal.Number.ToString().AddPaddingAndSign(7, "0");
                            itemsDet[i][22] = "       ";
                            itemsDet[i][23] = "                     ";
                            itemNumInForm++;
                            runningNumber++;
                            i++;
                        }
                    }
                }


                //
                //  תעודת משלוח D110-200
                //

                if (deliveryList != null)
                {
                    foreach (var curDelivery in deliveryList)
                    {
                        int itemNumInForm = 1;

                        string year = curDelivery.DateCreated.Year.ToString();
                        string month = curDelivery.DateCreated.Month.ToString();
                        if (month.Length == 1)
                        {
                            month = "0" + month;
                        }

                        string day = curDelivery.DateCreated.Day.ToString();
                        if (day.Length == 1)
                        {
                            day = "0" + day;
                        }

                        var delList = curDelivery.DocumentItems;

                        foreach (var curItem in delList)
                        {

                            itemsDet[i] = new string[24];
                            itemsDet[i][0] = "D110";
                            itemsDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsDet[i][3] = "200";
                            itemsDet[i][4] = curDelivery.Number.ToString().AddPaddingAndSign(20, "0");
                            itemsDet[i][5] = itemNumInForm.ToString().AddPaddingAndSign(4, "0");
                            itemsDet[i][6] = "000";
                            itemsDet[i][7] = "                    ";
                            itemsDet[i][8] = "2";
                            itemsDet[i][9] = curItem.ID.ToString().AddPaddingAndSign(20);

                            if (curItem.Name.Length > 30)
                            {
                                itemsDet[i][10] = curItem.Name.Substring(0, 29).AddPaddingAndSign(30);
                            }
                            else
                            {
                                itemsDet[i][10] = curItem.Name.AddPaddingAndSign(30);
                            }

                            itemsDet[i][11] = "".AddPaddingAndSign(50);
                            itemsDet[i][12] = "".AddPaddingAndSign(30);
                            itemsDet[i][13] = "יחידה".AddPaddingAndSign(20);

                            if (curItem.Quantity >= 0)
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "+");
                            }
                            else
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "-");
                            }

                            double curItemPrice = curItem.Price;

                            if (curItem.Discount > 0)
                            {
                                var curItemPriceDiscount = 100 - curItem.Discount;

                                curItemPrice = (double)(curItemPrice * curItemPriceDiscount)/100;
                            }

                            if (curItemPrice >= 0)
                            {
                                if (curDelivery.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                        curItemPrice.ToString("N2").RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                            }
                            else
                            {
                                if (curDelivery.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                        (curItemPrice * -1).ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser * -1)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                }
                            }

                            itemsDet[i][16] = "+00000000000000";

                            if (curItem.Total >= 0)
                            {
                                if (curDelivery.IsTaxIncluded == false)
                                {
                                    itemsDet[i][17] =
                                        curItem.Total.ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    itemsDet[i][17] =
                                        Math.Round(curItem.Total / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                            }
                            else
                            {
                                if (curDelivery.IsTaxIncluded == false)
                                {
                                    itemsDet[i][17] =
                                        (curItem.Total * -1).ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    itemsDet[i][17] =
                                        Math.Round(curItem.Total / taxRateForUser * -1)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                }
                            }

                            if (curUser.City == "אילת")
                            {
                                itemsDet[i][18] = "0000";
                            }
                            else
                            {
                                string iDet = curUser.TaxRate.ToString("0000");
                                itemsDet[i][18] = iDet.AddPaddingAndSign(4, "0");
                            }

                            itemsDet[i][19] = "       ";
                            itemsDet[i][20] = year + month + day;
                            itemsDet[i][21] = curDelivery.Number.ToString().AddPaddingAndSign(7, "0");
                            itemsDet[i][22] = "       ";
                            itemsDet[i][23] = "                     ";
                            itemNumInForm++;
                            runningNumber++;
                            i++;
                        }
                    }
                }


                //
                // ---- חשבונית-מס  D110-305
                //

                if (taxList != null)
                {
                    foreach (var curTax in taxList)
                    {
                        int itemNumInForm = 1;

                        string year = curTax.DateCreated.Year.ToString();
                        string month = curTax.DateCreated.Month.ToString();
                        if (month.Length == 1)
                        {
                            month = "0" + month;
                        }

                        string day = curTax.DateCreated.Day.ToString();
                        if (day.Length == 1)
                        {
                            day = "0" + day;
                        }

                        foreach (var curItem in curTax.DocumentItems)
                        {

                            itemsDet[i] = new string[24];
                            itemsDet[i][0] = "D110";
                            itemsDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsDet[i][3] = "305";
                            itemsDet[i][4] = curTax.Number.ToString().AddPaddingAndSign(20, "0");
                            itemsDet[i][5] = itemNumInForm.ToString().AddPaddingAndSign(4, "0");
                            itemsDet[i][6] = "000";
                            itemsDet[i][7] = "                    ";
                            itemsDet[i][8] = "2";
                            itemsDet[i][9] = curItem.ID.ToString().AddPaddingAndSign(20);

                            if (curItem.Name.Length > 30)
                            {
                                itemsDet[i][10] = curItem.Name.Substring(0, 29).AddPaddingAndSign(30);
                            }
                            else
                            {
                                itemsDet[i][10] = curItem.Name.AddPaddingAndSign(30);
                            }

                            itemsDet[i][11] = "".AddPaddingAndSign(50);
                            itemsDet[i][12] = "".AddPaddingAndSign(30);
                            itemsDet[i][13] = "יחידה".AddPaddingAndSign(20);

                            if (curItem.Quantity >= 0)
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "+");
                            }
                            else
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "-");
                            }

                            double curItemPrice = curItem.Price;

                            if (curItem.Discount > 0)
                            {
                                var curItemPriceDiscount = 100 - curItem.Discount;

                                curItemPrice = (double)(curItemPrice * curItemPriceDiscount)/100;
                            }

                            if (curItemPrice >= 0)
                            {
                                if (curTax.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                        curItemPrice.ToString("N2").RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                            }
                            else
                            {
                                if (curTax.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                        (curItemPrice * -1).ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser * -1)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                }
                            }
                            itemsDet[i][16] = "+00000000000000";

                            if (curItem.Total >= 0)
                            {
                                if (curTax.IsTaxIncluded == false)
                                {
                                    itemsDet[i][17] =
                                        curItem.Total.ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    itemsDet[i][17] =
                                        Math.Round(curItem.Total / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                            }
                            else
                            {
                                if (curTax.IsTaxIncluded == false)
                                {
                                    itemsDet[i][17] =
                                        (curItem.Total * -1).ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    itemsDet[i][17] =
                                        Math.Round(curItem.Total / taxRateForUser * -1)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                }
                            }

                            if (curUser.City == "אילת")
                            {
                                itemsDet[i][18] = "0000";
                            }
                            else
                            {
                                string iDet = curUser.TaxRate.ToString("0000");
                                itemsDet[i][18] = iDet.AddPaddingAndSign(4, "0");/* + "00"*/
                            }
                            itemsDet[i][19] = "       ";
                            itemsDet[i][20] = year + month + day;
                            itemsDet[i][21] = curTax.Number.ToString().AddPaddingAndSign(7, "0");
                            itemsDet[i][22] = "       ";
                            itemsDet[i][23] = "                     ";
                            itemNumInForm++;
                            runningNumber++;
                            i++;
                        }

                    }
                }


                //
                // --- חשבונית מס זיכוי     D110-330
                if (taxCreditList != null)
                {
                    foreach (var curTax in taxCreditList)
                    {
                        int itemNumInForm = 1;

                        string year = curTax.DateCreated.Year.ToString();
                        string month = curTax.DateCreated.Month.ToString();
                        if (month.Length == 1)
                        {
                            month = "0" + month;
                        }

                        string day = curTax.DateCreated.Day.ToString();

                        if (day.Length == 1)
                        {
                            day = "0" + day;
                        }

                        foreach (var curItem in curTax.DocumentItems)
                        {
                            // --- חשבונית מס זיכוי
                            itemsDet[i] = new string[24];
                            itemsDet[i][0] = "D110";
                            itemsDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsDet[i][3] = "330";
                            itemsDet[i][4] = curTax.Number.ToString().AddPaddingAndSign(20, "0");
                            itemsDet[i][5] = itemNumInForm.ToString().AddPaddingAndSign(4, "0");
                            itemsDet[i][6] = "000";
                            itemsDet[i][7] = "                    ";
                            itemsDet[i][8] = "2";
                            itemsDet[i][9] = curItem.ID.ToString().AddPaddingAndSign(20);

                            if (curItem.Name.Length > 30)
                            {
                                itemsDet[i][10] = curItem.Name.Substring(0, 29).AddPaddingAndSign(30);
                            }
                            else
                            {
                                itemsDet[i][10] = curItem.Name.AddPaddingAndSign(30);
                            }

                            itemsDet[i][11] = "".AddPaddingAndSign(50);
                            itemsDet[i][12] = "".AddPaddingAndSign(30);
                            itemsDet[i][13] = "יחידה".AddPaddingAndSign(20);

                            if (curItem.Quantity >= 0)
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "+");
                            }
                            else
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "-");
                            }

                            double curItemPrice = curItem.Price;

                            if (curItem.Discount > 0)
                            {
                                var curItemPriceDiscount = 100 - curItem.Discount;

                                curItemPrice = (double)(curItemPrice * curItemPriceDiscount)/100;
                            }

                            if (curItemPrice >= 0)
                            {
                                if (curTax.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                        curItemPrice.ToString("N2").RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                            }
                            else
                            {
                                if (curTax.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                        (curItemPrice * -1).ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser * -1)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                }
                            }
                            itemsDet[i][16] = "+00000000000000";

                            if (curItem.Total >= 0)
                            {
                                if (curTax.IsTaxIncluded == false)
                                {
                                    itemsDet[i][17] =
                                        curItem.Total.ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    itemsDet[i][17] =
                                        Math.Round(curItem.Total / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                            }
                            else
                            {
                                if (curTax.IsTaxIncluded == false)
                                {
                                    itemsDet[i][17] =
                                        (curItem.Total * -1).ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    itemsDet[i][17] =
                                        Math.Round(curItem.Total / taxRateForUser * -1)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                }
                            }

                            if (curUser.City == "אילת")
                            {
                                itemsDet[i][18] = "0000";
                            }
                            else
                            {
                                string iDet = curUser.TaxRate.ToString("0000");
                                itemsDet[i][18] = iDet.AddPaddingAndSign(4, "0");/* + "00"*/
                            }

                            itemsDet[i][19] = "       ";
                            itemsDet[i][20] = year + month + day;
                            itemsDet[i][21] = curTax.Number.ToString().AddPaddingAndSign(7, "0");
                            itemsDet[i][22] = "       ";
                            itemsDet[i][23] = "                     ";
                            itemNumInForm++;
                            runningNumber++;
                            i++;
                        }

                    }
                }


                //
                // ---- חשבונית מס / קבלה   D110-320
                //
                if (taxReceiptList != null)
                {
                    foreach (var curTaxRec in taxReceiptList)
                    {
                        int itemNumInForm = 1;

                        string year = curTaxRec.DateCreated.Year.ToString();
                        string month = curTaxRec.DateCreated.Month.ToString();
                        if (month.Length == 1)
                        {
                            month = "0" + month;
                        }

                        string day = curTaxRec.DateCreated.Day.ToString();
                        if (day.Length == 1)
                        {
                            day = "0" + day;
                        }


                        // ---- חשבונית מס / קבלה   D110-320
                        foreach (var curItem in curTaxRec.DocumentItems)
                        {

                            itemsDet[i] = new string[24];
                            itemsDet[i][0] = "D110";
                            itemsDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsDet[i][3] = "320";
                            itemsDet[i][4] = curTaxRec.Number.ToString().AddPaddingAndSign(20, "0");
                            itemsDet[i][5] = itemNumInForm.ToString().AddPaddingAndSign(4, "0");
                            itemsDet[i][6] = "000";
                            itemsDet[i][7] = "                    ";
                            itemsDet[i][8] = "2";
                            itemsDet[i][9] = curItem.ID.ToString().AddPaddingAndSign(20);

                            if (curItem.Name.Length > 30)
                            {
                                itemsDet[i][10] = curItem.Name.Substring(0, 29).AddPaddingAndSign(30);
                            }
                            else
                            {
                                itemsDet[i][10] = curItem.Name.AddPaddingAndSign(30);
                            }

                            itemsDet[i][11] = "".AddPaddingAndSign(50);
                            itemsDet[i][12] = "".AddPaddingAndSign(30);
                            itemsDet[i][13] = "יחידה".AddPaddingAndSign(20);

                            if (curItem.Quantity >= 0)
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "+");
                            }
                            else
                            {
                                itemsDet[i][14] =
                                    (curItem.Quantity.ToString("N2").RemoveEntries() + "00")
                                    .AddPaddingAndSign(17, "0", "-");
                            }

                            double curItemPrice = curItem.Price;

                            if (curItem.Discount > 0)
                            {
                                var curItemPriceDiscount = 100 - curItem.Discount;

                                curItemPrice = (double)(curItemPrice * curItemPriceDiscount)/100;
                            }

                            if (curItemPrice >= 0)
                            {
                                if (curTaxRec.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                        curItemPrice
                                        .ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                }
                            }
                            else
                            {
                                if (curTaxRec.IsTaxIncluded == false)
                                {
                                    itemsDet[i][15] =
                                        (curItemPrice * -1)
                                        .ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    itemsDet[i][15] =
                                        Math.Round(curItemPrice / taxRateForUser * -1)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                }
                            }
                            itemsDet[i][16] = "+00000000000000";

                            if (curItem.Total >= 0)
                            {
                                if (curTaxRec.IsTaxIncluded == false)
                                {
                                    itemsDet[i][17] =
                                        curItem.Total
                                        .ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "+");
                                }
                                else
                                {
                                    if (curItem.Discount > 0)
                                    {
                                        itemsDet[i][17] =
                                            (curItemPrice * curItem.Quantity / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "+");
                                    }
                                    else
                                    {
                                        itemsDet[i][17] =
                                            Math.Round(curItem.Total / taxRateForUser)
                                                .ToString("N2")
                                                .RemoveEntries()
                                                .AddPaddingAndSign(15, "0", "+");
                                    }
                                }
                            }
                            else
                            {
                                if (curTaxRec.IsTaxIncluded == false)
                                {
                                    itemsDet[i][17] =
                                        (curItem.Total * -1)
                                        .ToString("N2")
                                        .RemoveEntries()
                                        .AddPaddingAndSign(15, "0", "-");
                                }
                                else
                                {
                                    if (curItem.Discount > 0)
                                    {
                                        itemsDet[i][17] =
                                            (curItemPrice * curItem.Quantity / taxRateForUser)
                                            .ToString("N2")
                                            .RemoveEntries()
                                            .AddPaddingAndSign(15, "0", "-");
                                    }
                                    else
                                    {
                                        itemsDet[i][17] =
                                            Math.Round(curItem.Total / taxRateForUser)
                                                .ToString("N2")
                                                .RemoveEntries()
                                                .AddPaddingAndSign(15, "0", "-");
                                    }
                                }
                            }

                            if (curUser.City == "אילת")
                            {
                                itemsDet[i][18] = "0000";
                            }
                            else
                            {
                                string iDet = curUser.TaxRate.ToString("0000");
                                itemsDet[i][18] = iDet.AddPaddingAndSign(4, "0");
                            }

                            itemsDet[i][19] = "       ";
                            itemsDet[i][20] = year + month + day;
                            itemsDet[i][21] = curTaxRec.Number.ToString().AddPaddingAndSign(7, "0");
                            itemsDet[i][22] = "       ";
                            itemsDet[i][23] = "                     ";
                            itemNumInForm++;        
                            runningNumber++;
                            i++;
                        }

                    }
                }


            }

            /*         INI SUMUP INFO       */
            string sumItems = itemsCount.ToString().AddPaddingAndSign(15, "0");
            iniDet[1] = "D110" + sumItems;
            /*                             */


            //
            // ---- חשבונית מס / קבלה   D120-320
            if (taxReceiptCount > 0)
            {
                var index = taxReceiptList.SelectMany(x => x.Payments).Count(); //taxReceiptCount + taxCreditCount + taxCount + 1; 

                taxRecieptRecCount = index;

                itemsTaxRecDet = new string[index][];

                int i = 0;
                foreach (var curTaxRec in taxReceiptList)
                {
                    int itemNumInTaxRec = 1;
                    string taxRecYear = curTaxRec.DateCreated.Year.ToString();
                    string taxRecMonth = curTaxRec.DateCreated.Month.ToString();
                    string taxRecDay = curTaxRec.DateCreated.Day.ToString();


                    if (taxRecMonth.Length == 1)
                    {
                        taxRecMonth = "0" + taxRecMonth;
                    }

                    if (taxRecDay.Length == 1)
                    {
                        taxRecDay = "0" + taxRecDay;
                    }

                    foreach (var curItem in curTaxRec.Payments)
                    {
                        string year = curItem.Date.Year.ToString();
                        string month = curItem.Date.Month.ToString();
                        if (month.Length == 1)
                            month = "0" + month;
                        string day = curItem.Date.Day.ToString();
                        if (day.Length == 1)
                            day = "0" + day;

                        itemsTaxRecDet[i] = new string[16];
                        itemsTaxRecDet[i][0] = "D120";
                        itemsTaxRecDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                        itemsTaxRecDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                        itemsTaxRecDet[i][3] = "320";
                        itemsTaxRecDet[i][4] = curTaxRec.Number.ToString().AddPaddingAndSign(20, "0");
                        itemsTaxRecDet[i][5] = itemNumInTaxRec.ToString().AddPaddingAndSign(4, "0");

                        itemsTaxRecDet[i][6] = GetPaymentType(curItem.PaymentType);

                        if (itemsTaxRecDet[i][6] == "2")
                        {
                            string bankNum = "", branchNum = "", accountNum = "", checkNum = "";

                            if (curItem.BankName?.Length > 10)
                            {
                                bankNum = curItem.BankName.Substring(0, 10);
                            }
                            else
                            {
                                while (bankNum.Length < 10)
                                {
                                    bankNum = bankNum.Length + bankNum;
                                }
                            }
                            if (curItem.BranchName?.Length > 10)
                            {
                                branchNum = curItem.BranchName.Substring(0, 10);
                            }
                            else
                            {
                                while (branchNum.Length < 10)
                                {
                                    branchNum = branchNum.Length + branchNum;
                                }
                            }
                            if (curItem.AccountNumber?.Length > 15)
                            {
                                accountNum = curItem.AccountNumber.Substring(0, 15);
                            }
                            else
                            {
                                int tempRun = 3;
                                while (accountNum.Length < 15)
                                {
                                    accountNum = tempRun + accountNum;
                                }
                            }

                            // Check number
                            if (curItem.PaymentNumber?.Length > 10)
                            {
                                checkNum = curItem.PaymentNumber.Substring(0, 10);
                            }
                            else
                            {
                                while (checkNum.Length < 10)
                                {
                                    checkNum = checkNum.Length + checkNum;
                                }
                            }

                            itemsTaxRecDet[i][6] = 
                                itemsTaxRecDet[i][6] + bankNum + branchNum + accountNum + checkNum;
                        }
                        else
                        {
                            while (itemsTaxRecDet[i][6].Length < 46)
                            {
                                itemsTaxRecDet[i][6] += "0";
                            }
                        }

                        itemsTaxRecDet[i][7] = year + month + day;

                        itemsTaxRecDet[i][8] = curItem.Amount.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                        // CARD TYPE-  UNKNOWN.
                        itemsTaxRecDet[i][9] = "0";
                        // CARD NAME.
                        itemsTaxRecDet[i][10] = "                    ";
                        // TRANSACTION TYPE.
                        itemsTaxRecDet[i][11] = "1";
                        itemsTaxRecDet[i][12] = "       ";
                        itemsTaxRecDet[i][13] = taxRecYear + taxRecMonth + taxRecDay;
                        itemsTaxRecDet[i][14] = curTaxRec.Number.ToString().AddPaddingAndSign(7, "0");
                        itemsTaxRecDet[i][15] = "".AddPaddingAndSign(60);
                        itemNumInTaxRec++;
                        runningNumber++;
                        i++;
                    }
                }
            }
 
            //
            // ---- קבלה    D120-400
            if (itemsRecCount != 0)
            {
                itemsRecDet = new string[itemsRecCount][];
                int i = 0;
                foreach (var curRec in recList)
                {
                    int itemNumInRec = 1;
                    string recYear = curRec.DateCreated.Year.ToString();
                    string recMonth = curRec.DateCreated.Month.ToString();
                    string recDay = curRec.DateCreated.Day.ToString();

                    if (recMonth.Length == 1)
                        recMonth = "0" + recMonth;
                    if (recDay.Length == 1)
                        recDay = "0" + recDay;

                    foreach (var curItem in curRec.Payments)
                    {
                        string year = curItem.Date.Year.ToString();
                        string month = curItem.Date.Month.ToString();
                        if (month.Length == 1)
                            month = "0" + month;
                        string day = curItem.Date.Day.ToString();
                        if (day.Length == 1)
                            day = "0" + day;


                        itemsRecDet[i] = new string[16];
                        itemsRecDet[i][0] = "D120";
                        itemsRecDet[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                        itemsRecDet[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                        itemsRecDet[i][3] = "400";
                        itemsRecDet[i][4] = curRec.Number.ToString().AddPaddingAndSign(20, "0");
                        itemsRecDet[i][5] = itemNumInRec.ToString().AddPaddingAndSign(4, "0");

                        itemsRecDet[i][6] = GetPaymentType(curItem.PaymentType);

                        if (itemsRecDet[i][6] == "2")
                        {
                            string bankNum = "", branchNum = "", accountNum = "", checkNum = "";
                            if (curItem.BankName?.Length > 10)
                            {
                                bankNum = curItem.BankName.Substring(0, 10);
                            }
                            else
                            {
                                while (bankNum.Length < 10)
                                {
                                    bankNum = bankNum.Length + bankNum;
                                }
                            }
                            if (curItem.BranchName?.Length > 10)
                            {
                                branchNum = curItem.BranchName.Substring(0, 10);
                            }
                            else
                            {
                                while (branchNum.Length < 10)
                                {
                                    branchNum = branchNum.Length + branchNum;
                                }
                            }
                            if (curItem.AccountNumber?.Length > 15)
                            {
                                accountNum = curItem.AccountNumber.Substring(0, 15);
                            }
                            else
                            {
                                int tempRun = 3;
                                while (accountNum.Length < 15)
                                {
                                    accountNum = tempRun + accountNum;
                                }
                            }
                            if (curItem.PaymentNumber?.Length > 10)
                            {
                                checkNum = curItem.PaymentNumber.Substring(0, 10);
                            }
                            else
                            {
                                while (checkNum.Length < 10)
                                {
                                    checkNum = checkNum.Length + checkNum;
                                }
                            }

                            itemsRecDet[i][6] = 
                                itemsRecDet[i][6] + bankNum + branchNum + accountNum + checkNum;
                        }
                        else
                        {
                            while (itemsRecDet[i][6].Length < 46)
                            {
                                itemsRecDet[i][6] += "0";
                            }
                        }

                        itemsRecDet[i][7] = year + month + day;

                        itemsRecDet[i][8] = curItem.Amount.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");

                        // CARD TYPE-  UNKNOWN.
                        itemsRecDet[i][9] = "0";

                        // CARD NAME.
                        itemsRecDet[i][10] = "                    ";

                        // TRANSACTION TYPE.
                        itemsRecDet[i][11] = "1";
                        itemsRecDet[i][12] = "       ";
                        itemsRecDet[i][13] = recYear + recMonth + recDay;
                        itemsRecDet[i][14] = curRec.Number.ToString().AddPaddingAndSign(7, "0");
                        itemsRecDet[i][15] = "".AddPaddingAndSign(60);
                        itemNumInRec++;
                        runningNumber++;
                        i++;
                    }
                }
            }


            /*         INI SUMUP INFO       */
            string sumItemsRec = 
                (taxRecieptRecCount + itemsRecCount)
                .ToString()
                .AddPaddingAndSign(15, "0");

            iniDet[2] = "D120" + sumItemsRec;
            /*                             */


            //
            // ---- הזמנה   B110-100
            if (curUser.Clients != null)
            {
                clList = curUser.Clients.ToArray();
                clCount = clList.Length;

                int i = 0;
                acountManage = new string[clCount][];
                foreach (var curCl in clList)
                {
                    acountManage[i] = new string[20];

                    acountManage[i][0] = "B110";
                    acountManage[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                    acountManage[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                    acountManage[i][3] = curCl.UniqueId.AddPaddingAndSign(15);
                    acountManage[i][4] = curCl.Name.AddPaddingAndSign(50);
                    acountManage[i][5] = "100            ";
                    acountManage[i][6] = "לקוחות".AddPaddingAndSign(30);
                    acountManage[i][7] = curCl.Address.AddPaddingAndSign(98);
                    acountManage[i][8] = "ישראל".AddPaddingAndSign(30);
                    acountManage[i][9] = "IL";
                    acountManage[i][10] = "".AddPaddingAndSign(15);

                    double amStart = GetClientAccountStateForDate(curCl, startDate);
                    double debPer = GetClientDebForPeriod(curCl, startDate, endDate);
                    double chaPer = GetClientPayForPeriod(curCl, startDate, endDate);

                    if (amStart >= 0)
                    {
                        acountManage[i][11] = amStart.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        acountManage[i][11] = amStart.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (debPer >= 0)
                    {
                        acountManage[i][12] = debPer.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        acountManage[i][12] = debPer.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    if (chaPer >= 0)
                    {
                        acountManage[i][13] = chaPer.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "+");
                    }
                    else
                    {
                        acountManage[i][13] = chaPer.ToString("N2")
                            .RemoveEntries()
                            .AddPaddingAndSign(15, "0", "-");
                    }

                    acountManage[i][14] = "0000";
                    acountManage[i][15] = "";
                    if (curCl.Fax?.Length == 9)
                    {
                        acountManage[i][15] = curCl.Fax;
                    }
                    else
                    {
                        while (acountManage[i][15].Length < 9)
                            acountManage[i][15] = "0" + acountManage[i][15];
                    }
                    acountManage[i][16] = "       ";
                    acountManage[i][17] = "+00000000000000";
                    acountManage[i][18] = "   ";
                    acountManage[i][19] = "                ";
                    runningNumber++;
                    i++;
                }
            }
            //
            //    "M100"
            if (itemsCount != 0)
            {
                itemsLongList = new string[itemsCount][];
                int i = 0;

                if (dealsList != null)
                {
                    foreach (var curDeal in dealsList)
                    {
                        foreach (var curItem in curDeal.DocumentItems)
                        {

                            itemsLongList[i] = new string[15];

                            itemsLongList[i][0] = "M100";
                            itemsLongList[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsLongList[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsLongList[i][3] = "".AddPaddingAndSign(20);
                            itemsLongList[i][4] = "                    ";
                            itemsLongList[i][5] = curItem.ID.ToString().AddPaddingAndSign(20);

                            if (curItem.Name.Length > 50)
                            {
                                itemsLongList[i][6] = curItem.Name.Substring(0, 49)
                                    .AddPaddingAndSign(50);
                            }
                            else
                            {
                                itemsLongList[i][6] = curItem.Name.AddPaddingAndSign(50);
                            }

                            itemsLongList[i][7] = "".AddPaddingAndSign(40);
                            itemsLongList[i][8] = "יחידה".AddPaddingAndSign(20);
                            itemsLongList[i][9] = "+00000000000";
                            itemsLongList[i][10] = "+00000000000";
                            itemsLongList[i][11] = "+00000000000";
                            itemsLongList[i][12] = "0000000000";
                            itemsLongList[i][13] = "0000000000";
                            itemsLongList[i][14] = "".AddPaddingAndSign(50);
                            runningNumber++;
                            i++;
                        }

                    }

                }

                if (deliveryList != null)
                {
                    foreach (var curDelivery in deliveryList)
                    {
                        var delList = curDelivery.DocumentItems;

                        foreach (var curItem in delList)
                        {

                            itemsLongList[i] = new string[15];
                            itemsLongList[i][0] = "M100";
                            itemsLongList[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsLongList[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsLongList[i][3] = "".AddPaddingAndSign(20);
                            itemsLongList[i][4] = "                    ";
                            itemsLongList[i][5] = curItem.ID.ToString().AddPaddingAndSign(20);
                            if (curItem.Name.Length > 50)
                            {
                                itemsLongList[i][6] =
                                    curItem.Name.Substring(0, 49).AddPaddingAndSign(50);
                            }
                            else
                            {
                                itemsLongList[i][6] = curItem.Name.AddPaddingAndSign(50);
                            }
                            itemsLongList[i][7] = "".AddPaddingAndSign(40);
                            itemsLongList[i][8] = "יחידה".AddPaddingAndSign(20);
                            itemsLongList[i][9] = "+00000000000";
                            itemsLongList[i][10] = "+00000000000";
                            itemsLongList[i][11] = "+00000000000";
                            itemsLongList[i][12] = "0000000000";
                            itemsLongList[i][13] = "0000000000";
                            itemsLongList[i][14] = "".AddPaddingAndSign(50);
                            runningNumber++;
                            i++;
                        }

                    }
                }

                if (taxList != null)
                {
                    foreach (var curTax in taxList)
                    {
                        foreach (var curItem in curTax.DocumentItems)
                        {
                            itemsLongList[i] = new string[15];
                            itemsLongList[i][0] = "M100";
                            itemsLongList[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsLongList[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsLongList[i][3] = "".AddPaddingAndSign(20);
                            itemsLongList[i][4] = "                    ";
                            itemsLongList[i][5] = curItem.ID.ToString().AddPaddingAndSign(20);
                            if (curItem.Name.Length > 50)
                            {
                                itemsLongList[i][6] = curItem.Name.Substring(0, 49)
                                    .AddPaddingAndSign(50);
                            }
                            else
                            {
                                itemsLongList[i][6] = curItem.Name.AddPaddingAndSign(50);
                            }
                            itemsLongList[i][7] = "".AddPaddingAndSign(40);
                            itemsLongList[i][8] = "יחידה".AddPaddingAndSign(20);
                            itemsLongList[i][9] = "+00000000000";
                            itemsLongList[i][10] = "+00000000000";
                            itemsLongList[i][11] = "+00000000000";
                            itemsLongList[i][12] = "0000000000";
                            itemsLongList[i][13] = "0000000000";
                            itemsLongList[i][14] = "".AddPaddingAndSign(50);
                            runningNumber++;
                            i++;
                        }

                    }
                }
                // ----- חשבונית מס זיכוי  C100-330
                //
                // --- חשבונית מס זיכוי     D110-330
                if (taxCreditList != null)
                {
                    foreach (var curTax in taxCreditList)
                    {
                        foreach (var curItem in curTax.DocumentItems)
                        {
                            itemsLongList[i] = new string[15];
                            itemsLongList[i][0] = "M100";
                            itemsLongList[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsLongList[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsLongList[i][3] = "".AddPaddingAndSign(20);
                            itemsLongList[i][4] = "                    ";
                            itemsLongList[i][5] = curItem.ID.ToString().AddPaddingAndSign(20);
                            if (curItem.Name.Length > 50)
                            {
                                itemsLongList[i][6] = curItem.Name.Substring(0, 49)
                                    .AddPaddingAndSign(50);
                            }
                            else
                            {
                                itemsLongList[i][6] = curItem.Name.AddPaddingAndSign(50);
                            }
                            itemsLongList[i][7] = "".AddPaddingAndSign(40);
                            itemsLongList[i][8] = "יחידה".AddPaddingAndSign(20);
                            itemsLongList[i][9] = "+00000000000";
                            itemsLongList[i][10] = "+00000000000";
                            itemsLongList[i][11] = "+00000000000";
                            itemsLongList[i][12] = "0000000000";
                            itemsLongList[i][13] = "0000000000";
                            itemsLongList[i][14] = "".AddPaddingAndSign(50);
                            runningNumber++;
                            i++;
                        }

                    }
                }

                if (taxReceiptList != null)
                {
                    foreach (var curTaxRec in taxReceiptList)
                    {
                        foreach (var curItem in curTaxRec.DocumentItems)
                        {
                            itemsLongList[i] = new string[15];
                            itemsLongList[i][0] = "M100";
                            itemsLongList[i][1] = runningNumber.ToString().AddPaddingAndSign(9, "0");
                            itemsLongList[i][2] = curUser.UniqueID.AddPaddingAndSign(9, "0");
                            itemsLongList[i][3] = "".AddPaddingAndSign(20);
                            itemsLongList[i][4] = "                    ";
                            itemsLongList[i][5] = curItem.ID.ToString().AddPaddingAndSign(20);
                            if (curItem.Name?.Length > 50)
                            {
                                itemsLongList[i][6] = curItem.Name.Substring(0, 49).AddPaddingAndSign(50);
                            }
                            else
                            {
                                itemsLongList[i][6] = curItem.Name.AddPaddingAndSign(50);
                            }
                            itemsLongList[i][7] = "".AddPaddingAndSign(40);
                            itemsLongList[i][8] = "יחידה".AddPaddingAndSign(20);
                            itemsLongList[i][9] = "+00000000000";
                            itemsLongList[i][10] = "+00000000000";
                            itemsLongList[i][11] = "+00000000000";
                            itemsLongList[i][12] = "0000000000";
                            itemsLongList[i][13] = "0000000000";
                            itemsLongList[i][14] = "".AddPaddingAndSign(50);
                            runningNumber++;
                            i++;
                        }

                    }
                }

            }
            int mkRecordCounter = 9;
            string curBusiNum = curUser.UniqueID;
            // CUR YEAR,MONTH,DAY,HOUR IN FORMAT.
            string curYear = DateTime.Now.Year.ToString().Substring(2);
            string curMonth = DateTime.Now.Month.ToString();
            if (curMonth.Length == 1)
                curMonth = "0" + curMonth;
            string curDay = DateTime.Now.Day.ToString();
            if (curDay.Length == 1)
            {
                curDay = "0" + curDay;
            }

            string curHour = DateTime.Now.Hour.ToString();
            if (curHour.Length == 1)
            {
                curHour = "0" + curHour;
            }

            string curMinute = DateTime.Now.Minute.ToString();
            if (curMinute.Length == 1)
            {
                curMinute = "0" + curMinute;
            }

            string[] mkOpen = new string[6];
            string[] mkClose = new string[10];

            runningNumber++;


            // OPEN FIELD.
            mkOpen[0] = "A100";
            mkOpen[1] = "000000001";
            mkOpen[2] = curUser.UniqueID;
            mkOpen[3] = curRandNumber.ToString().AddPaddingAndSign(15, "0");
            mkOpen[4] = "&OF1.31&";
            mkOpen[5] = "".AddPaddingAndSign(50);

            // C100 FIELDS.

            // CLOSING FIELD.
            mkClose[0] = "Z900";
            mkClose[1] = (runningNumber - 1).ToString().AddPaddingAndSign(9, "0");
            mkClose[2] = curUser.UniqueID;
            mkClose[3] = curRandNumber.ToString().AddPaddingAndSign(15, "0");
            mkClose[4] = "&OF1.31&";
            mkClose[5] = (runningNumber - 1).ToString().AddPaddingAndSign(15, "0");
            mkClose[6] = "".AddPaddingAndSign(50);

            // Case dir doesn't exists , create dir.
            string path = HttpContext.Current.Server.MapPath("~/OPENFRMT/" + curUser.ID);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string mkPath =
                HttpContext.Current.Server.MapPath("~/OPENFRMT/" + curUser.ID + "/BKMVDATAUTF.txt");

            //bkmvDataUtf
            //
            if (File.Exists(mkPath))
            {
                File.Delete(mkPath);
            }

            FileStream fs = File.Create(mkPath);
            fs.Close();

            FileStream mkStream = new FileStream(mkPath, FileMode.Open, FileAccess.ReadWrite);


            TextWriter twMk = new StreamWriter(mkStream, System.Text.Encoding.UTF8);

            for (int i = 0; i < 6; i++)
            {
                twMk.Write(mkOpen[i]);
            }
            twMk.WriteLine();
            //
            // --- חשבונית/חשבונית עסקה     C100-300
            if (dealsDet != null)
            {
                for (int i = 0; i < dealsCount; i++)
                {
                    for (int j = 0; j < 29; j++)
                    {
                        twMk.Write(dealsDet[i][j]);
                    }
                    twMk.WriteLine();
                }
            }
            //
            //------תעודת משלוח             C100-200 
            if (deliveryDet != null)
            {
                for (int i = 0; i < deliveryCount; i++)
                {
                    for (int j = 0; j < 29; j++)
                    {
                        twMk.Write(deliveryDet[i][j]);
                    }
                    twMk.WriteLine();
                }
            }
            //
            // ------- חשבונית-מס           C100-305 
            // ---- חשבונית מס / קבלה       D120-320
            if (taxDet != null)
            {
                for (int i = 0; i < taxCount; i++)
                {
                    for (int j = 0; j < 29; j++)
                    {
                        twMk.Write(taxDet[i][j]);
                    }
                    twMk.WriteLine();
                }
            }
            //
            // ----- חשבונית מס זיכוי       C100-330 
            // --- חשבונית מס זיכוי         D110-330
            if (taxCreditList != null)
            {
                for (int i = 0; i < taxCreditCount; i++)
                {
                    for (int j = 0; j < 29; j++)
                    {
                        twMk.Write(taxCreditDet[i][j]);
                    }
                    twMk.WriteLine();
                }
            }
            //
            // ----- חשבונית מס / קבלה      C100-320
            // ---- חשבונית מס / קבלה       D120-320
            if (taxReceiptDet != null)
            {
                for (int i = 0; i < taxReceiptCount; i++)
                {
                    for (int j = 0; j < 29; j++)
                    {
                        twMk.Write(taxReceiptDet[i][j]);
                    }
                    twMk.WriteLine();
                }
            }
            //
            // ----- קבלה               C100-400
            if (recDet != null)
            {
                for (int i = 0; i < recCount; i++)
                {
                    for (int j = 0; j < 29; j++)
                    {
                        twMk.Write(recDet[i][j]);
                    }
                    twMk.WriteLine();
                }
            }

            //
            //                  -----     M100     ----
            if (itemsDet != null)
            {
                for (int i = 0; i < itemsCount; i++)
                {
                    for (int j = 0; j < 24; j++)
                    {
                        twMk.Write(itemsDet[i][j]);
                    }
                    twMk.WriteLine();
                }
            }
            //
            // ---- חשבונית מס / קבלה   D120-320
            if (itemsTaxRecDet != null)
            {
                for (int i = 0; i < itemsTaxRecDet.Length; i++)
                {

                    if (itemsTaxRecDet[i] == null)
                    {
                        continue;
                    }

                    for (int j = 0; j < 16; j++)
                    {
                        twMk.Write(itemsTaxRecDet[i][j]);
                    }
                    twMk.WriteLine();
                }
            }
            //
            // ----- קבלה   C100-400
            // ---- קבלה    D120-400
            if (itemsRecDet != null)
            {
                for (int i = 0; i < itemsRecCount; i++)
                {
                    for (int j = 0; j < 16; j++)
                    {
                        twMk.Write(itemsRecDet[i][j]);
                    }
                    twMk.WriteLine();
                }
            }
            //
            // ---- הזמנה               B110-100
            if (clList != null)
            {
                for (int i = 0; i < clCount; i++)
                {
                    for (int j = 0; j < 20; j++)
                    {
                        twMk.Write(acountManage[i][j]);
                    }
                    twMk.WriteLine();
                }
            }

            if (itemsLongList != null)
            {
                for (int i = 0; i < itemsCount; i++)
                {
                    for (int j = 0; j < 15; j++)
                    {

                        twMk.Write(itemsLongList[i][j]);
                    }
                    twMk.WriteLine();
                }
            }

            for (int i = 0; i < 7; i++)
            {
                twMk.Write(mkClose[i]);
            }
            twMk.WriteLine();

            twMk.Close();


            StreamReader srUtf = new StreamReader(mkPath);
            string mkContent = srUtf.ReadToEnd();
            srUtf.Close();

            string mkPathAnsi =
                HttpContext.Current.Server.MapPath("~/OPENFRMT/" + curUser.ID + "/BKMVDATA.txt");

            //bkmvData
            //
            if (File.Exists(mkPathAnsi))
            {
                File.Delete(mkPathAnsi);
            }

            FileStream fsAnsi = File.Create(mkPathAnsi);
            fsAnsi.Close();

            StreamWriter twMkAnsi =
                new StreamWriter(mkPathAnsi, false, System.Text.Encoding.GetEncoding(1255));

            twMkAnsi.Write(mkContent);
            twMkAnsi.Close();

            // SET MK RECORDS NUMBER.
            int digits = mkRecordCounter.ToString().Length;
            digits = 15 - digits;
            if (digits > 0)
            {
                while (digits > 0)
                {
                    digits--;
                }
            }


            // SET INI OPEN FIELD
            string[] iniOpen = new string[33];
            iniOpen[0] = "A000";
            iniOpen[1] = "     ";
            iniOpen[2] = (runningNumber - 2).ToString().AddPaddingAndSign(15, "0");
            iniOpen[3] = curBusiNum.AddPaddingAndSign(9, "0");
            iniOpen[4] = curRandNumber.ToString().AddPaddingAndSign(15, "0");
            iniOpen[5] = "&OF1.31&";
            iniOpen[6] = "00186701";
            iniOpen[7] = "גיזברית".AddPaddingAndSign(20);
            iniOpen[8] = "מהדורה ראשונה".AddPaddingAndSign(20);
            iniOpen[9] = "512529462";
            iniOpen[10] = "ויקרא בע''מ".AddPaddingAndSign(20);
            iniOpen[11] = "2";
            iniOpen[12] = "E:\\OPENFORMAT\\" + curBusiNum + "." + curYear + "\\" + curMonth + curDay + curHour +
                          curMinute;
            sumUpForm[0] = "E:\\OPENFORMAT\\" + curBusiNum + "." + curYear + "\\" + curMonth + curDay + curHour +
                           curMinute;

            if (iniOpen[12].Length > 50)
                iniOpen[12] = iniOpen[12].Substring(0, 50);

            while (iniOpen[12].Length < 50)
            {
                iniOpen[12] = " " + iniOpen[12];
            }
            iniOpen[13] = "0";
            iniOpen[14] = "0";
            iniOpen[15] = "000000000";
            iniOpen[16] = "000000000";
            iniOpen[17] = "".AddPaddingAndSign(10);
            iniOpen[18] = curUser.Name.AddPaddingAndSign(50);
            iniOpen[19] = curUser.Address.AddPaddingAndSign(60);
            iniOpen[20] = curUser.City.AddPaddingAndSign(30);
            iniOpen[21] = curUser.Zip.AddPaddingAndSign(8);
            iniOpen[22] = "0000";



            string iniStYear = startDate.Year.ToString();
            string iniStMonth = startDate.Month.ToString();
            string iniStDay = startDate.Day.ToString();

            string iniEndYear;
            string iniEndMonth;
            string iniEndDay;

            DateTime endMax = DateTime.Now;
            if (endDate > endMax)
            {
                iniEndYear = endMax.Year.ToString();
                iniEndMonth = endMax.Month.ToString();
                iniEndDay = endMax.Day.ToString();
            }
            else
            {
                iniEndYear = endDate.Year.ToString();
                iniEndMonth = endDate.Month.ToString();
                iniEndDay = endDate.Day.ToString();
            }

            if (iniStMonth.Length == 1)
                iniStMonth = "0" + iniStMonth;
            if (iniStDay.Length == 1)
                iniStDay = "0" + iniStDay;
            if (iniEndMonth.Length == 1)
                iniEndMonth = "0" + iniEndMonth;
            if (iniEndDay.Length == 1)
                iniEndDay = "0" + iniEndDay;



            //
            // SET INI FIELD
            //
            iniOpen[23] = iniStYear + iniStMonth + iniStDay;
            iniOpen[24] = iniEndYear + iniEndMonth + iniEndDay;

            string[] sumUpFormSec = new string[15];

            sumUpFormSec[1] = iniStDay + iniStMonth + iniStYear;

            sumUpFormSec[2] = iniEndDay + iniEndMonth + iniEndYear;

            sumUpFormSec[3] = taxCount.ToString();

            sumUpFormSec[4] =
                GetAllTaxFormsByUserAndDates(organizationId, startDate, endDate).Any(t => t.Total >= 0)
                    ? GetAllTaxFormsByUserAndDates(organizationId, startDate, endDate)
                        .Where(t => t.Total >= 0)
                        .Sum(t => t.TotalwithoutTax)
                        .ToString("N2")
                    : "0.00";

            sumUpFormSec[5] = recCount.ToString();

            sumUpFormSec[6] = GetTotRecSumForUserByDate(curUser, startDate, endDate).ToString("N2");

            sumUpFormSec[7] = dealsCount.ToString();

            sumUpFormSec[8] = GetAllDealFormsByUserAndDates(organizationId, startDate, endDate).Any()
                ? GetAllDealFormsByUserAndDates(organizationId, startDate, endDate)
                    .Sum(d => d.TotalwithoutTax)
                    .ToString("N2")
                : "0.00";

            sumUpFormSec[9] = deliveryCount.ToString();

            sumUpFormSec[10] =
                GetTotDeliverySumForUserByDate(curUser, startDate, endDate).ToString("N2");

            sumUpFormSec[11] = taxReceiptCount.ToString();

            sumUpFormSec[12] = taxReceiptList.Any()
                ? taxReceiptList
                    .Sum(tr => tr.TotalwithoutTax)
                    .ToString("N2")
                : "0.00";

            sumUpFormSec[13] = taxCreditCount.ToString();

            sumUpFormSec[14] = taxCreditList.Any()
                ? taxCreditList
                    .Sum(t => t.TotalwithoutTax)
                    .ToString("N2")
                : "0.00";

            var taxesCount = taxRecieptRecCount + itemsRecCount;

            var allCounts = sumForPdfC100;


            sumUpForm[1] = iniStDay + iniStMonth + iniStYear;
            sumUpForm[2] = iniEndDay + iniEndMonth + iniEndYear;
            sumUpForm[3] = "1";
            sumUpForm[4] = clCount.ToString();
            sumUpForm[5] = itemsCount.ToString();
            sumUpForm[6] = allCounts.ToString();
            sumUpForm[7] = itemsCount.ToString();
            sumUpForm[8] = taxesCount.ToString();
            sumUpForm[10] = "1";



            //Obsolete

            HttpServerUtility curServer = HttpContext.Current.Server;


            //chkInspection
            //
            //chkInspection1
            //


            iniOpen[25] = DateTime.Now.Year + curMonth + curDay;
            iniOpen[26] = curHour + curMinute;

            iniOpen[27] = "0";
            iniOpen[28] = "1";
            iniOpen[29] = "winzip".AddPaddingAndSign(20);
            iniOpen[30] = "ILS";

            switch (otherBranches)
            {
                case false:
                    iniOpen[31] = "0";
                    break;
                default:
                    iniOpen[31] = "1";
                    break;
            }

            iniOpen[32] = "".AddPaddingAndSign(46);

            string iniPath = HttpContext.Current.Server.MapPath("~/OPENFRMT/" + curUser.ID + "/INIUTF.txt");

            //iniContentUtf
            //
            if (File.Exists(iniPath))
            {
                File.Delete(iniPath);
            }

            fs = File.Create(iniPath);
            fs.Close();

            FileStream iniStream = new FileStream(iniPath, FileMode.Open, FileAccess.ReadWrite);

            TextWriter tw = new StreamWriter(iniStream, System.Text.Encoding.UTF8);

            // SET INI FIELD
            for (int i = 0; i < 33; i++)
            {
                tw.Write(iniOpen[i]);
            }
            tw.WriteLine();

            iniDet[3] = "B100000000000000000";

            string sumClients = clCount.ToString().AddPaddingAndSign(15, "0");
            iniDet[4] = "B110" + sumClients;
            iniDet[5] = "M100" + sumItems;

            for (int i = 0; i < 6; i++)
            {
                tw.Write(iniDet[i]);
                tw.WriteLine();
            }

            tw.Close();

            StreamReader srIniUtf = new StreamReader(iniPath);
            string iniContent = srIniUtf.ReadToEnd();
            srIniUtf.Close();

            string iniPathAnsi = HttpContext.Current.Server.MapPath("~/OPENFRMT/" + curUser.ID + "/INI.txt");

            //iniContentAnsi

            if (File.Exists(iniPathAnsi))
            {
                File.Delete(iniPathAnsi);
            }

            FileStream fsIniAnsi = File.Create(iniPathAnsi);
            fsIniAnsi.Close();

            StreamWriter iniAnsi =
                new StreamWriter(iniPathAnsi, false, System.Text.Encoding.GetEncoding(1255));
            iniAnsi.Write(iniContent);
            iniAnsi.Close();


            var result = new List<ContentBase>()
            {
new TextContent
            {
                Name = "INI.txt",
                Description = "INI.txt",
                ExtraData = "/OPENFRMT/" + curUser.ID + "/INI.txt"
            },
new TextContent
            {
                Name = "INIUTF.txt",
                Description = "INIUTF.txt",
                ExtraData = "/OPENFRMT/" + curUser.ID + "/INIUTF.txt"
            },
new TextContent
            {
                Name = "BKMVDATA.txt",
                ExtraData = "/OPENFRMT/" + curUser.ID + "/BKMVDATA.txt",
                Description = "BKMVDATA.txt"
            },
new TextContent
            {
                Name = "BKMVDATAUTF.txt",
                ExtraData = "/OPENFRMT/" + curUser.ID + "/BKMVDATAUTF.txt",
                Description = "BKMVDATAUTF.txt"
            },
PrintHelper.CreatePdfInspection(curServer, curUser, sumUpForm),
PrintHelper.CreatePdfInspectionNumbers(curServer, curUser, sumUpFormSec)
            };

            return result;
        }

        private double GetClientAccountStateForDate(Client curCl, DateTime startDate)
        {

            var totals = _documentService.GetUserDocuments()
                .Where(x => x.ClientID == curCl.ID)
                .Where(x => x.DocumentType == DocumentType.Receipt || x.DocumentType == DocumentType.TaxInvoice)
                .Where(x => x.Status == DocumentStatusType.Printed || x.Status == DocumentStatusType.Send)
                .Where(x => x.ActualCreationDate <= startDate)
                .Select(x => x.Total)
                .ToArray();

            return totals.Sum();
        }

        private double GetClientDebForPeriod(Client curCl, DateTime startDate, DateTime endDate)
        {
            var totals = _documentService.GetUserDocuments()
                .Where(x => x.ClientID == curCl.ID)
                .Where(x => x.DocumentType == DocumentType.TaxInvoice)
                .Where(x => x.Status == DocumentStatusType.Printed || x.Status == DocumentStatusType.Send)
                .Where(x => x.ActualCreationDate >= startDate && x.ActualCreationDate <= endDate)
                .Select(x => x.Total)
                .ToArray();

            return totals.Sum();
        }

        private double GetClientPayForPeriod(Client curCl, DateTime startDate, DateTime endDate)
        {
            var totals = _documentService.GetUserDocuments()
                .Where(x => x.ClientID == curCl.ID)
                .Where(x => x.DocumentType == DocumentType.Receipt)
                .Where(x => x.Status == DocumentStatusType.Printed || x.Status == DocumentStatusType.Send)
                .Where(x => x.ActualCreationDate >= startDate && x.ActualCreationDate <= endDate)
                .Select(x => x.Total)
                .ToArray();

            return totals.Sum();
        }

        private double GetTotDeliverySumForUserByDate(Organization curUser, DateTime startDate, DateTime endDate)
        {
            var totals = _documentService.GetUserDocuments(curUser.ID)
                .Where(x => x.DocumentType == DocumentType.Waybill)
                .Where(x => x.Status == DocumentStatusType.Printed || x.Status == DocumentStatusType.Send)
                .Where(x => x.ActualCreationDate >= startDate && x.ActualCreationDate <= endDate)
                .Select(x => x.Total)
                .ToArray();

            return totals.Sum();
        }

        private double GetTotRecSumForUserByDate(Organization curUser, DateTime startDate, DateTime endDate)
        {
            var totals = _documentService.GetUserDocuments(curUser.ID)
                .Where(x => x.DocumentType == DocumentType.Receipt)
                .Where(x => x.Status == DocumentStatusType.Printed || x.Status == DocumentStatusType.Send)
                .Where(x => x.ActualCreationDate >= startDate && x.ActualCreationDate <= endDate)
                .Select(x => x.Total)
                .ToArray();

            return totals.Sum();
        }

        private Document[] GetAllReceiptsByUserAndDates(int userId, DateTime startDate, DateTime endDate)
        {
            return GetDocuments(DocumentType.Receipt, startDate, endDate, userId).ToArray();
        }

        private Document[] GetAllTaxReceiptFormsByUserAndDates(int userId, DateTime startDate, DateTime endDate)
        {
            return GetDocuments(DocumentType.TaxInvoiceReceipt, startDate, endDate, userId).ToArray();
        }

        private Document[] GetAllDeliveryFormsByUserAndDates(int userId, DateTime startDate, DateTime endDate)
        {
            return GetDocuments(DocumentType.Waybill, startDate, endDate, userId).ToArray();
        }

        private Document[] GetAllDealFormsByUserAndDates(int userId, DateTime startDate, DateTime endDate)
        {
            return GetDocuments(DocumentType.PreliminaryInvoice, startDate, endDate, userId).ToArray();
        }


        private Document[] GetAllTaxFormsByUserAndDates(int userId, DateTime startDate, DateTime endDate)
        {
            return GetDocuments(DocumentType.TaxInvoice, startDate, endDate, userId).ToArray();
        }

        private static string GetPaymentType(int id)
        {
            switch (id)
            {
                case 1: //Check
                    return "2";
                case 2:
                    return "4"; //2   Bank Transfer
                case 3:
                    return "1"; //3   Cash Payment
                case 4:
                    return "3"; //4   Payment by credit card
                case 5:
                    return "9"; //5   Withholding tax
            }

            throw new ArgumentException();
        }
    }

}
