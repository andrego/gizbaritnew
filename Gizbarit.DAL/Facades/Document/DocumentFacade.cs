﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Gizbarit.DAL.Common;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Providers.Sign;
using Gizbarit.DAL.Providers.Store;
using Gizbarit.DAL.Resources.Mail;
using Gizbarit.DAL.Services.Content;
using Gizbarit.DAL.Services.Document;
using Gizbarit.Utils.Email;

namespace Gizbarit.DAL.Facades.Document
{
    public class DocumentFacade : IDocumentFacade
    {
        private static readonly string SignFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Content\Certificates");
        
        private readonly IDocumentService _documentService;
        private readonly IDocumentProviderFactory _providerFactory;
        private readonly IContentService _contentService;
        private readonly IMailService _mailService;
        private readonly ISignProvider _signProvider;

        public DocumentFacade(
            IDocumentService documentService,
            IContentService contentService,
            IMailService mailService,
            IDocumentProviderFactory providerFactory,
            ISignProvider signProvider)
        {
            _documentService = documentService;
            _contentService = contentService;
            _mailService = mailService;
            _providerFactory = providerFactory;
            _signProvider = signProvider;
        }

        public ContentBase GetForPrint(Guid documentId, bool isCopyRequsted, bool isSpecReport = false)
        {
            var document = GetDocument(documentId);

            var content = this.GetForPrint(document, isCopyRequsted, isSpecReport);

            return content;
        }

        public ContentBase GetForPrint(Entities.Documents.Document document, bool isCopyRequsted, bool isSpecReport = false)
        {
            var provider = _providerFactory.Create(document);
            var args = new DocumentProviderArgs()
            {
                StampType = document.IsOriginalGenerated() || isCopyRequsted ? DocumentStampType.Copy : DocumentStampType.Original,
                IsCombinedReport = isSpecReport
            };

            var pdfContent = provider.Create(document, args);

           return pdfContent;
        }

        public ContentBase GetForPrint(Order order, string cultureCode = "he")
        {
            order.AssertNotNull(nameof(order));

            var factoryArgs = new ProviderFactoryArgs()
            {
                CultureInfo = new CultureInfo(cultureCode)
            };
            var args = new DocumentProviderArgs()
            {
                StampType = DocumentStampType.None,
            };
            var provider = _providerFactory.Create(order, factoryArgs);
            var pdfContent = provider.Create(order, args);

            return pdfContent;
        }

        public ContentBase GetForPrint(StoreStockInfo stockInfo)
        {
            stockInfo.AssertNotNull(nameof(stockInfo));
            var args = new DocumentProviderArgs()
            {
                StampType = DocumentStampType.None,
            };
            var provider = _providerFactory.Create(stockInfo);
            var pdfContent = provider.Create(stockInfo, args);

            return pdfContent;
        }

        public void SendByMail(Guid documentId, string customText, bool isCopyRequsted)
        {
            var document = GetDocument(documentId);

            this.SendByMail(document, customText, isCopyRequsted);
        }

        public void SendByMail(Entities.Documents.Document document, string customText, bool isCopyRequsted)
        {
            var organization = document.Organization;

            var provider = _providerFactory.Create(document);
            var args = new DocumentProviderArgs()
            {
                StampType = GetStampType(document, isCopyRequsted),
                IsDigitalySigned = document.IsSignRequred() && !isCopyRequsted
            };

            var pdfContent = provider.Create(document, args);
            var certificate = GetCertificate(document.Organization);
            
            if (document.IsSignRequred() && 
               (args.StampType == DocumentStampType.Original || args.StampType == DocumentStampType.OriginalCopy))
            {
                pdfContent = _signProvider.Sign(pdfContent, certificate);
            }

            if (document.IsOriginalNotGenerated())
            {
                _contentService.Create(pdfContent, true);
            }
            var docType = TemplateResource.ResourceManager.GetString(document.DocumentType.ToString()) + " " + document.Number;

            if (!document.Organization.IsVatCharge && document.DocumentType == DocumentType.TaxInvoiceReceipt)
            {
                docType = $"{pdfContent.Description} ";
            }
            var docName = docType  + ".pdf";
            var message = new MailMessage(organization.Email, document.Email ?? document.Client.Email);
            var attachment = new Attachment(pdfContent.GetStream(), docName, pdfContent.Type);

            message.IsBodyHtml = true;
            message.Subject = $"{pdfContent.Description} {TemplateResource.From} {document.Organization.Name}";
            message.Body = GetMessageText(document, pdfContent, customText);
            message.Attachments.Add(attachment);
            _mailService.Send(message);
        }

        public void SendByMail(Order order, string cultureCode = "he")
        {
            var organization = order.Supplier.Organization;
            var pdfContent = GetForPrint(order, cultureCode);

            var message = new MailMessage(organization.Email, order.Supplier.Email);
            var attachment = new Attachment(pdfContent.GetStream(), pdfContent.Name, pdfContent.Type);

            message.IsBodyHtml = true;

            message.Subject = 
                $"{pdfContent.Description} {TemplateResource.From} {organization.Name}";

            message.Body = 
                cultureCode == "he" 
                    ? $"{TemplateResource.OrderBodyHe} {organization.Name}" 
                    : $"{TemplateResource.OrderBodyEn} {organization.NameEng}";

            message.Attachments.Add(attachment);
            _mailService.Send(message);
        }

        public void SendByMail(StoreStockInfo stockInfo)
        {
            var organization = stockInfo.Organization;
            var pdfContent = GetForPrint(stockInfo);

            var message = new MailMessage(organization.Email, stockInfo.Email);
            var attachment = new Attachment(pdfContent.GetStream(), pdfContent.Name, pdfContent.Type);

            message.IsBodyHtml = true;
            message.Subject = $"{pdfContent.Description} {TemplateResource.From} {organization.Name}";
            message.Body = $"{TemplateResource.StoreStockInfoBody} {organization.Name}"; 
            message.Attachments.Add(attachment);
            _mailService.Send(message);
        }

        #region Provate methods
        private static string GetMessageText(Entities.Documents.Document document,  ContentBase pdf, string customText)
        {
            var template = TemplateResource.DefaultMailTemplate;
            var builder = new StringBuilder(template);
            var docType = TemplateResource.ResourceManager.GetString(document.DocumentType.ToString());
            if (!document.Organization.IsVatCharge && document.DocumentType == DocumentType.TaxInvoiceReceipt)
            {
                docType = $"{pdf.Description} ";
            }

            builder
                .Replace("{recipient}", document.Client.Name)
                .Replace("{custom-text}", customText)
                .Replace("{documentName}", docType)
                .Replace("{sender}", document.Organization.Name);

            return builder.ToString();
        }

        private DocumentStampType GetStampType(Entities.Documents.Document document, bool isCopyRequested)
        {
            if (isCopyRequested)
            {
                return DocumentStampType.Copy;
            }

            return document.IsOriginalGenerated() ? DocumentStampType.OriginalCopy : DocumentStampType.Original;
        }

        private CertificateContent GetCertificate(Organization organization)
        {
            var certificate = _contentService
                .GetAll()
                .OfType<CertificateContent>()
                .Where(x => x.OrganizationId == organization.ID)
                .FirstOrDefault(x => x.Expired <= DateTime.UtcNow);

            return certificate ?? GetCertificate("gizbarit.v3.pfx", "odessa1958");
        }

        private Entities.Documents.Document GetDocument(Guid id)
        {
            var document =
                _documentService.GetUserDocuments()
                    .Include(d => d.Payments)
                    .Include(d => d.Organization)
                    .First(d => d.ID == id);

            return document;
        }
        
        public static CertificateContent GetCertificate(string fileName, string password)
        {
            var certPath = Path.Combine(SignFolder, fileName);
            var cert = new CertificateContent()
            {
                Name = fileName,
                Password = password
            };


            if (!File.Exists(certPath))
            {
                cert.Name = "gizbarit.v3.pfx";
                cert.Password = "odessa1958";
                certPath = Path.Combine(SignFolder, cert.Name);
            }

            cert.Data = File.ReadAllBytes(certPath);

            return cert;
        }

        #endregion
    }
}
