﻿using System;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Providers.Store;

namespace Gizbarit.DAL.Facades.Document
{
    public interface IDocumentFacade
    {
        ContentBase GetForPrint(Entities.Documents.Document document, bool isCopyRequsted, bool isSpecReport = false);
        ContentBase GetForPrint(Guid documentId, bool isCopyRequsted, bool isSpecReport = false);

        ContentBase GetForPrint(Order order, string cultureCode = "he");
        ContentBase GetForPrint(StoreStockInfo stockInfo);

        void SendByMail(Guid documentId, string customText, bool isCopyRequsted);
        void SendByMail(Entities.Documents.Document document, string customText, bool isCopyRequsted);

        void SendByMail(Order order, string cultureCode = "he");
        void SendByMail(StoreStockInfo stockInfo);
    }
}