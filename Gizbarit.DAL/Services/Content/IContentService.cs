﻿using System;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Content
{
    public interface IContentService : IBaseService<ContentBase, Guid>
    {
    }
}
