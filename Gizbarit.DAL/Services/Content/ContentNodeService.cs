﻿using System;
using System.Data.Entity;
using System.Linq;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Content
{

    public class ContentNodeService : BaseService<ContentNode, Guid>, IContentNodeService
    {
        public override ContentNode Get(Guid id)
        {
            return GetAll()
                .Include(x=>x.Parent)
                .FirstOrDefault(type => (object)type.ID == (object)id);
        }

        public ContentNodeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
