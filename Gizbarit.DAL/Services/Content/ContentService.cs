﻿using System;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Content
{

    public class ContentService : BaseService<ContentBase, Guid>, IContentService
    {
        public ContentService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }


    public class PdfRepository : BaseService<PdfContent, Guid> //, IContentService
    {
        public PdfRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
    }

    public class CertificateRepository : BaseService<CertificateContent, Guid> //, IContentService
    {
        public CertificateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
    }

    public class ImageRepository : BaseService<ImageContent, Guid> //, IContentService
    {
        public ImageRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
    }
}
