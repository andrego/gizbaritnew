﻿using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Gizbarit.DAL.Services.Content
{
    public interface IContentNodeService: IBaseService<ContentNode, Guid>
    {
    }

    public static class ContentNodeServiceExtensions
    {
        public static bool Exists(this IContentNodeService context, string name, string cultureCode)
        {
            return context.GetAll().Any(x => x.Name == name && x.CultureCode == cultureCode);
        }

        public static ContentNode Get(this IContentNodeService context, string name, string cultureCode)
        {
            return context
                .GetAll()
                .FirstOrDefault(x => x.Name == name && x.CultureCode == cultureCode);
        }

        public static ICollection<ContentNode> Fetch(this IContentNodeService context, Guid? parentId)
        {
            var query = context.GetAll().Include(x => x.Childs);

            if (parentId.HasValue)
            {
                query = query.Where(x => x.ParentId == parentId);
            }

            return query.ToArray();
        }
    }
}
