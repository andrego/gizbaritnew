﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services
{
    public class BaseService<TEntity, TPrimaryId> : IBaseService<TEntity, TPrimaryId>
        where TEntity : class, IEntity<TPrimaryId>
    {
        protected readonly IUnitOfWork UnitOfWork;
        public BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public void Commit(bool saveChanges = false)
        {
            if (saveChanges)
                UnitOfWork.Commit();
        }

        public virtual TEntity Get(TPrimaryId id)
        {
            return GetAll().FirstOrDefault(type => (object)type.ID == (object)id);
        }

        public virtual TEntity Create(TEntity newItem, bool shouldBeCommited = false)
        {
            var idProperty = newItem.GetType().GetProperty("ID");
            if (idProperty != null && idProperty.PropertyType == typeof (Guid))
            {
                newItem.SetProperty("ID", Guid.NewGuid());
            }
            UnitOfWork.Add(newItem);
            Commit(shouldBeCommited);
            return newItem;
        }

        public virtual void Remove(TEntity removeItem, bool shouldBeCommited = false)
        {
            if (removeItem != null)
            {
                UnitOfWork.Remove(removeItem);
                Commit(shouldBeCommited);
            }
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return GetList<TEntity>();
        }

        protected virtual IQueryable<T> GetList<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            var list = GetList<T>();
            if (predicate != null)
            {
                return list.Where(predicate);
            }
            return list;
        }

        protected virtual IQueryable<T> GetList<T, TKey>(Expression<Func<T, bool>> predicate,
            Expression<Func<T, TKey>> orderBy) where T : class
        {
            return GetList(predicate).OrderBy(orderBy);
        }

        protected virtual IQueryable<T> GetList<T, TKey>(Expression<Func<T, TKey>> orderBy) where T : class
        {
            return GetList<T>().OrderBy(orderBy);
        }

        protected virtual IQueryable<T> GetList<T>() where T : class
        {
            return UnitOfWork.Get<T>();
        }

        public void Update(TEntity item, bool sholdBeCommited)
        {
            throw new NotImplementedException();
        }
    }
}
