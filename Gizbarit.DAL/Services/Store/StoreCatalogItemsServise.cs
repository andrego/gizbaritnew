﻿using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Store
{
    public class StoreCatalogItemsServise:BaseService<StoreCatalogItem, int>, IStoreCatalogItemsService
    {
        public StoreCatalogItemsServise(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
