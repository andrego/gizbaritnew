﻿using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Store
{
    public class StoreOrderDetailsService:BaseService<OrderDetail, int>, IStoreOrderDetailsService
    {
        public StoreOrderDetailsService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
