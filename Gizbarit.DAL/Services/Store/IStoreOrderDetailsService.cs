﻿using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Store
{
    public interface IStoreOrderDetailsService : IBaseService<OrderDetail, int>
    {
    }
}
