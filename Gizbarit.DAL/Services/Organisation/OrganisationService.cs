﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Interfaces;
using Gizbarit.Identity.Providers;
using Gizbarit.Utils;
using Microsoft.AspNet.Identity;

namespace Gizbarit.DAL.Services.Organisation
{
    public class OrganisationService : BaseService<Organization, int>, IOrganisationService
    {
        public OrganisationService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }


        public IEnumerable<BusinessType> GetBusinessTypes()
        {
            return UnitOfWork.Get<BusinessType>().ToList();
        }

        public void UpdateLastAccessedInfo(Organization org)
        {
            org.ViewDate = DateTime.UtcNow;
            UnitOfWork.Commit();
        }

        public bool IsUniqueBusinessId(string businessId)
        {
            return !(UnitOfWork.Get<Organization>().Any(o => o.UniqueID == businessId));
        }

        public bool IsUniqueEmail(string email)
        {
            return !(UnitOfWork.Get<Organization>().Any(o => o.Email == email));
        }

        public DateTime GetAdvancesPayDay(DateTime initialDate, int? orgId = null)
        {
            if (orgId == null)
            {
                orgId = HttpContext.Current.User.Identity.GetOrgId();
            }
            var organisation = UnitOfWork.Get<Organization>().FirstOrDefault(o => o.ID == orgId);
            if (organisation == null)
                throw new NullReferenceException($"Organisation ({orgId}) not found!");

            var vatPayDateDate =
                 initialDate.AddMonths(organisation.AdvancesAccountingMethod == 1
                    ? 1 // per month: add 1 month
                    : (initialDate.Month % 2 == 0 ?
                        1 // even month: add 1 month
                        : 2 // odd month: add 2 month
                        ));

            return new DateTime(vatPayDateDate.Year, vatPayDateDate.Month, 15);
        }

        public IEnumerable<CatalogItem> GetCatalogItems(int? orgId = null,bool isActive = true)
        {
            if (orgId == null)
            {
                orgId = HttpContext.Current.User.Identity.GetOrgId();
            }
            return UnitOfWork.Get<CatalogItem>().Where(c => c.OrganizationId == orgId.Value && c.Active == isActive).ToList();
        }

        public Organization GetCurrent()
        {
            var identity = HttpContext.Current.User.Identity as CustomIdentity;
            if(identity != null)
            {
                return Get(identity.ID);
            }

            var orgId = HttpContext.Current.User.Identity.GetUserId();
            return Get(Convert.ToInt32(orgId));
        }

        public Organization GetByUniqueId(string id)
        {
           return GetAll().FirstOrDefault(o=>o.UniqueID == id);
        }
    }
}
