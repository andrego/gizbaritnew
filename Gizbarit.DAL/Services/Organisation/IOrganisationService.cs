﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Organisation
{
    public interface IOrganisationService : IBaseService<Organization, int>
    {
        IEnumerable<BusinessType> GetBusinessTypes();
        void UpdateLastAccessedInfo(Organization org);
        bool IsUniqueBusinessId(string businessId);
        bool IsUniqueEmail(string email);
        DateTime GetAdvancesPayDay(DateTime initialDate, int? orgId = null);
        IEnumerable<CatalogItem> GetCatalogItems(int? orgId = null, bool isActive = true);
        Organization GetCurrent();
        Organization GetByUniqueId(string id);

    }


    public static class OrganisationServiceExtensions
    {
        public static Organization Login(this IOrganisationService service, string businessNumber, string password)
        {
            var org = service.GetAll()
                             .FirstOrDefault(o => o.UniqueID == businessNumber && o.Password == password && o.Status != OrganizationStatus.Deleted);

            return org;
        }

        public static async Task<IEnumerable<Organization>> GetByStatusAsync(this IOrganisationService service, OrganizationStatus status)
        {
            var org = await service.GetAll()
                .Where(o => o.Status == status)
                .ToArrayAsync();

            return org;
        }


        public static IEnumerable<Organization> GetByStatus(this IOrganisationService service, OrganizationStatus status)
        {
            var org = service.GetAll()   
                .Where(o => o.Status == status)
                .ToArray();

            return org;
        }

    }
}

