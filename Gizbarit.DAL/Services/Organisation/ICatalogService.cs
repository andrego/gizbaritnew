﻿using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Organisation
{
    public interface ICatalogService:IBaseService<CatalogItem, int>
    {
    }
}
