﻿using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Organisation
{
    public class CatalogService:BaseService<CatalogItem, int>, ICatalogService
    {
        public CatalogService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
