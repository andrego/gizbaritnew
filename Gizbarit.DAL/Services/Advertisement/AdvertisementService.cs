﻿using System;
using System.Linq;
using Gizbarit.DAL.Entities.Advertisement;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services
{
    public class AdvertisementService:BaseService<Advertisement, int>,IAdvertisementService
    {
        public AdvertisementService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }


        public IQueryable<Advertisement> GetActive()
        {
            var today = DateTime.Now.Date;
            return
                GetAll()
                    .Where(
                        a =>
                            (a.Organization.TrialPeriod &&
                             a.Organization.TrialPeriodExpirationDate >= today) ||
                            (!a.Organization.TrialPeriod &&
                             a.Organization.PaidPeriodExpirationDate >= today));
        }
    }

    public interface IAdvertisementService : IBaseService<Advertisement, int>
    {
        IQueryable<Advertisement> GetActive();
    }

}
