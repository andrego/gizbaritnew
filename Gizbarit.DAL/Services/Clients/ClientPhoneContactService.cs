﻿using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Clients
{
    public class ClientPhoneContactService:BaseService<ClientPhoneContact, int>, IClientPhoneContactService
    {
        public ClientPhoneContactService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            
        }
    }
}