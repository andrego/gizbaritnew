﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Clients
{
    public interface IClientPhoneContactService:IBaseService<ClientPhoneContact, int>
    {
    }
}
