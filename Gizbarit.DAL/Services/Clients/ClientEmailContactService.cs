﻿
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Clients
{
   public class ClientEmailContactService:BaseService<ClientEmailContact, int>, IClientEmailContactService
   {
       public ClientEmailContactService(IUnitOfWork unitOfWork) : base(unitOfWork)
       {
           
       }
    }
}
