﻿using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Clients
{
    public interface IClientService :IBaseService<Client, int>
    {
    }
}
