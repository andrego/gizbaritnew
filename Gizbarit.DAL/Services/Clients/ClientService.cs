﻿using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Clients
{
    public class ClientService:BaseService<Client, int>,IClientService
    {
        public ClientService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
