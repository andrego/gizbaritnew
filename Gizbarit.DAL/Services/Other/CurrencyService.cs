﻿using System;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Other
{
    public class CurrencyService:BaseService<Currency, String>, ICurrencyService
    {
        public CurrencyService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
