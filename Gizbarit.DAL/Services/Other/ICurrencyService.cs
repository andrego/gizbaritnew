﻿using System;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Other
{
    public interface ICurrencyService:IBaseService<Currency, String>
    {
    }
}
