﻿using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Supliers
{
    public class SupplierInvoiceService:BaseService<SupplierInvoice, int>, ISupplierInvoiceService
    {
        public SupplierInvoiceService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
