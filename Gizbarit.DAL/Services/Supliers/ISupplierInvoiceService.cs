﻿using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Supliers
{
    public interface ISupplierInvoiceService:IBaseService<SupplierInvoice, int>
    {
         
    }
}