﻿using System;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Bank
{
    public interface ITransactionService:IBaseService<BankTransaction, Guid>
    {
        //new void Commit(bool saveChanges = false);

        void UpdateAutoVatTransaction(int defaultBankId, DateTime vatDate, double amount);
        void UpdateAutoAdvanceTransaction(int defaultBankId, DateTime vatDate, double amount);
    }
}
