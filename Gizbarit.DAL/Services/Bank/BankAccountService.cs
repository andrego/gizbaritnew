﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Interfaces;
using Gizbarit.DAL.Models;

namespace Gizbarit.DAL.Services.Bank
{
    public class BankAccountService : BaseService<BankAccount, int>, IBankAccountService
    {
        public BankAccountService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IQueryable<BankAccount> GetUserAccounts(int orgId)
        {
            return UnitOfWork.Get<BankAccount>().Where(ba => ba.OrganizationID == orgId);
        }

        public async Task<double> SetYearlyBalance(int year, int accountId, double initialBalance = 0) //TODO refactor
        {
            var prevBalance =
                UnitOfWork.Get<BankAccountInitialBalanceYearly>()
                    .Where(b => b.BankAccountId == accountId)
                    .OrderByDescending(b => b.Year)
                    .FirstOrDefault();

            double prevBalanceValue = 0, curBalance = 0;
            int currentYearValue = 0;
            if (prevBalance != null)
            {
                prevBalanceValue = prevBalance.InitialBalance;
                currentYearValue = prevBalance.Year;
            }
            else
            {
                var firstTransaction = UnitOfWork.Get<BankTransaction>()
                    .Where(b => b.BankAccount.ID == accountId)
                    .OrderBy(b => b.DateInserted)
                    .FirstOrDefault();

                if (initialBalance == 0)
                {
                    var account = UnitOfWork.Get<BankAccount>().First(ba => ba.ID == accountId);
                    initialBalance = account.InitialBalance;
                }
                if (firstTransaction != null)
                {
                    currentYearValue = firstTransaction.DateInserted.Year;
                }
                else
                {
                    currentYearValue = year - 1;
                }
                prevBalanceValue = initialBalance;
            }
            if (currentYearValue == year)
            {
                if (prevBalance != null)
                {
                    prevBalance.InitialBalance = initialBalance;
                }
                else
                {
                    UnitOfWork.Add(new BankAccountInitialBalanceYearly()
                    {
                        InitialBalance = prevBalanceValue,
                        BankAccountId = accountId,
                        Year = currentYearValue
                    });
                }
            }
            else
            {
                while (currentYearValue < year)
                {
                    // get balance of next year start date. ( last year balance + sum of transactions in following year )
                    curBalance = prevBalanceValue +
                                 await GetTransactionsDiffByDates(accountId, new DateTime(currentYearValue, 01, 01),
                                     new DateTime(currentYearValue, 12, 31));

                    UnitOfWork.Add(new BankAccountInitialBalanceYearly()
                    {
                        InitialBalance = curBalance,
                        BankAccountId = accountId,
                        Year = ++currentYearValue
                    });
                    UnitOfWork.Commit();
                    prevBalanceValue = curBalance;

                }

            }
           
            UnitOfWork.Commit();
            return prevBalanceValue;


            //else
            //{
            //    var transAction =
            //        UnitOfWork.Get<BankTransaction>()
            //            .Where(bt => bt.BankId == accountId)
            //            .OrderBy(bt => bt.DateInserted)
            //            .FirstOrDefault();
            //    if (transAction == null)    // set new initialBalnace for current year
            //    {
            //        UnitOfWork.Add(new BankAccountInitialBalanceYearly()
            //        {
            //            InitialBalance = initialBalance,
            //            BankAccountId = accountId,
            //            Year = year
            //        });
            //        UnitOfWork.Commit();
            //        return initialBalance;
            //    }
            //    var currentYearValue = transAction.DateInserted.Year;
            //    UnitOfWork.Add(new BankAccountInitialBalanceYearly()
            //    {
            //        InitialBalance = initialBalance,
            //        BankAccountId = accountId,
            //        Year = currentYearValue
            //    });
            //    UnitOfWork.Commit();

            //    prevBalance = initialBalance;

            //    while (currentYearValue < year)
            //    {
            //        // get balance of next year start date. ( last year balance + sum of transactions in following year )
            //        curBalance = prevBalance +
            //                     await GetTransactionsDiffByDates(accountId, new DateTime(currentYearValue, 01, 01),
            //                         new DateTime(currentYearValue, 12, 31));

            //        UnitOfWork.Add(new BankAccountInitialBalanceYearly()
            //        {
            //            InitialBalance = curBalance,
            //            BankAccountId = accountId,
            //            Year = currentYearValue++
            //        });
            //        UnitOfWork.Commit();
            //        prevBalance = curBalance;
            //    }
            //    return prevBalance;
            //}
        }

        public BankAccount GetDefaultAccount(int orgId)
        {
            return GetUserAccounts(orgId).FirstOrDefault(b => b.Active && !b.IsDeleted) ??
                   GetUserAccounts(orgId).FirstOrDefault(b => !b.IsDeleted);
        }

        public IQueryable<BankTransaction> GetTransactions(int accountId)
        {
            return
                UnitOfWork.Get<BankTransaction>()
                    .Where(bt => bt.BankId == accountId)
                    .OrderByDescending(bt => bt.DateInserted);
        }

        public async Task<double> GetBalance(int accountId, DateTime date)
        {
            double balance = 0;

            var yearlyBalance =
                UnitOfWork.Get<BankAccountInitialBalanceYearly>()
                    .FirstOrDefault(b => b.BankAccountId == accountId && b.Year == date.Year);

            var initialBalance = yearlyBalance == null ? 0 : yearlyBalance.InitialBalance;

            balance = initialBalance + await GetTransactionsDiffByDates(accountId, new DateTime(date.Year, 1, 1), date);
            return balance;
        }

        public async Task<double> GetBalance(int accountId, DateTime date, string currencyName)
        {
            double balance = 0;

            balance = await GetTransactionsDiffByDates(accountId, new DateTime(date.Year, 1, 1), date, currencyName);
            return balance;
        }

        public async Task<AccountException> GetLimitExceedInfo(int accountId)
        {
            var account = UnitOfWork.Get<BankAccount>()
                    .FirstOrDefault(d => d.ID == accountId);


            var distinctDays =
                UnitOfWork.Get<BankTransaction>()
                .Where(d=>d.OrganizationId == account.OrganizationID)
                    .Where(d=>d.DateInserted >= DateTime.Now)
                    .OrderBy(d=>d.DateInserted)
                    .Select(d=>d.DateInserted)
                    .Distinct()
                    .ToList();

            var accountLimit = account == null ? 0 : account.Limit;

            foreach (var day in distinctDays)
            {
                var bal = await GetBalance(accountId, day);
                if (bal < -accountLimit)
                    return new AccountException {Date = day, DueValue = bal};
            }

            return null;
        
        }
        public async Task UpdateYearlyBalance(int accountId)
        {
            var prevBalances =
                UnitOfWork.Get<BankAccountInitialBalanceYearly>().Where(a=>a.BankAccountId == accountId).ToList();
                    
            if (prevBalances.Any())
            {
                foreach (var prev in prevBalances)
                {
                    UnitOfWork.Remove(prev);
                }
                UnitOfWork.Commit();
               
            }
            await SetYearlyBalance(DateTime.UtcNow.Year, accountId);
        }

        public async Task<double> GetTransactionsDiffByDates(int accountId, DateTime? startDate, DateTime endDate, string currencyName="ILS")
        {
            var creditTransactions =
                UnitOfWork.Get<BankTransaction>()
                    .Where(
                        bt =>
                            bt.BankId == accountId && bt.Credit &&
                            bt.DateInserted < endDate)
                            .Where(bt=> startDate == null || bt.DateInserted > startDate)
                            .Where(bt=> bt.Currency.ID == currencyName);

            var debitTransactions =
                UnitOfWork.Get<BankTransaction>()
                    .Where(
                        bt =>
                            bt.BankId == accountId && !bt.Credit &&
                            bt.DateInserted < endDate)
                            .Where(bt => startDate == null || bt.DateInserted > startDate)
                            .Where(bt => bt.Currency.ID == currencyName);

            double credit = creditTransactions.Any() ? await creditTransactions.SumAsync(c => c.Amount) : 0;
            double debit = debitTransactions.Any() ? await debitTransactions.SumAsync(c => c.Amount) : 0;

            return (credit - debit);
        }
    }
}
