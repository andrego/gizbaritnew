﻿using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Bank
{
    public interface IAdvanceService:IBaseService<Advance, int>
    {
    }
}
