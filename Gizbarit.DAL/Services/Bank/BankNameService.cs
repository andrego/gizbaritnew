﻿using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Bank
{
    public class BankNameService:BaseService<BankName, int>, IBankNameService
    {
        public BankNameService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
