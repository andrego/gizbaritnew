﻿using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Bank
{
    public class AdvanceService:BaseService<Advance, int>, IAdvanceService
    {
        public AdvanceService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
