﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Interfaces;
using Gizbarit.DAL.Models;

namespace Gizbarit.DAL.Services.Bank
{
    public interface IBankAccountService:IBaseService<BankAccount, int>
    {

        IQueryable<BankAccount> GetUserAccounts(int orgId);
        Task<double> SetYearlyBalance(int year, int accountId, double initialBalance);

        BankAccount GetDefaultAccount(int orgId);

        IQueryable<BankTransaction> GetTransactions(int accountId);

        Task<double> GetBalance(int accountId, DateTime date);

        Task<AccountException> GetLimitExceedInfo(int accountId);

        Task UpdateYearlyBalance(int accountId);

        Task<double> GetTransactionsDiffByDates(int accountId, DateTime? startDate, DateTime endDate,
            string currencyName = "ILS");
        
    }
}
