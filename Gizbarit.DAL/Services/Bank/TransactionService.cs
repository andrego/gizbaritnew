﻿using System;
using System.Linq;
using System.Web;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Interfaces;
using Gizbarit.Utils;

namespace Gizbarit.DAL.Services.Bank
{
    public class TransactionService : BaseService<BankTransaction, Guid>, ITransactionService
    {
        private const string VatMagicString = "מע\"מ|@|VAT|@|Предоплата НДС";
        private const string IncomeTaxMagicString = "מס הכנסה|@|Income Tax|@|Предоплата под. налога";

        public TransactionService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public void UpdateAutoVatTransaction(int defaultBankId, DateTime vatDate, double amount)
        {
            UpdateAdvanceTransaction(true, defaultBankId, vatDate, amount);
        }

        public void UpdateAutoAdvanceTransaction(int defaultBankId, DateTime vatDate, double amount)
        {
            UpdateAdvanceTransaction(false, defaultBankId, vatDate, amount);
        }

        private void UpdateAdvanceTransaction(bool vat, int defaultBankId, DateTime vatDate, double amount)
        {
            
            var orgId = HttpContext.Current.User.Identity.GetOrgId();
            var operation = GetAll().FirstOrDefault(t =>
                   t.OrganizationId == orgId && t.BankId == defaultBankId &&
                   t.Details == (vat ? VatMagicString : IncomeTaxMagicString)
                   && t.DateInserted == vatDate);
            if (operation != null)
            {
                operation.Amount += Math.Round(amount, MidpointRounding.AwayFromZero);
            }
            else
            {
                Create(new BankTransaction
                {
                    ID = Guid.NewGuid(),
                    Amount = Math.Round(amount, MidpointRounding.AwayFromZero),
                    BankId = defaultBankId,
                    OrganizationId = orgId,
                    Credit = false,
                    DateInserted = vatDate,
                    Details = (vat ? VatMagicString : IncomeTaxMagicString),
                    DateUpdated = vatDate,
                    CurrencyId = "ILS"
                    
                });
            }

            Commit(true);
        }
    }
}
