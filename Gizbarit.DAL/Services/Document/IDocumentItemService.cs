﻿using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Document
{
    public interface IDocumentItemService:IBaseService<DocumentItem, int>
    {
    }
}
