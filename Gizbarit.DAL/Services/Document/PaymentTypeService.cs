﻿using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Document
{
    public class PaymentTypeService:BaseService<PaymentType, byte>, IPaymentTypeService
    {
        public PaymentTypeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
