﻿using System.Linq;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Document
{
    public class DocumentNumberingService:BaseService<DocumentNumbering, int>, IDocumentNumberingService
    {
        public DocumentNumberingService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public DocumentNumbering GetByOrganizaionId(int orgId)
        {
            return this.GetAll().First(x => x.OrganizationId == orgId);
        }
    }
}
