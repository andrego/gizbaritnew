﻿using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Document
{
    public class PaymentService:BaseService<Payment, int>, IPaymentService
    {
        public PaymentService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
