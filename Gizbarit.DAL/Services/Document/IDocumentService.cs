﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Document
{
    public interface IDocumentService : IBaseService<Entities.Documents.Document, Guid>
    {
        long GetLastDocumentNumber( DocumentType type);
        Entities.Documents.Document GetReferenceDocumentByRefId(Guid id);
        IQueryable<DocumentReference> GetReferenceDocumentById(Guid id);
        Entities.Documents.Document GetAutoCreatedPreliminaryInvoice(int clientId, DateTime date);

        double GetDocumentReceiptsSum(Guid id);
        double GetDocumentPrepaymentSum(Guid id);
        double GetInvoiceTotalPrepaymentSum(Guid id);
        IQueryable<Entities.Documents.Document> GetUserDocuments(int? organizationId = null, bool isExport = false);
        void ChangeDocumentPaidStatus(Guid id, bool isPaid);

        IEnumerable<Entities.Documents.Document> Fetch(DocumentSpecifiation spec);

       IQueryable<Entities.Documents.Document> GetAll(bool isExport);
    }
}
