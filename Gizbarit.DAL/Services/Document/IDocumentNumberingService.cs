﻿using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Document
{
    public interface IDocumentNumberingService:IBaseService<DocumentNumbering, int>
    {
        DocumentNumbering GetByOrganizaionId(int orgId);
    }
}
