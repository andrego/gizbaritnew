﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Interfaces;
using Gizbarit.Utils;
using Microsoft.AspNet.Identity;

namespace Gizbarit.DAL.Services.Document
{
    public class DocumentService : BaseService<Entities.Documents.Document, Guid>, IDocumentService
    {
        public DocumentService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public new void Update(Entities.Documents.Document document, bool sholdBeCommited)
        {
            var currentDocument = Get(document.ID); 
          
            if (currentDocument != null)
            {
                currentDocument.Subject = document.Subject;
                currentDocument.Client = document.Client;
                currentDocument.TotalwithoutTax = document.TotalwithoutTax;
                currentDocument.Total = document.Total;
                currentDocument.IsTaxIncluded = document.IsTaxIncluded;
                currentDocument.Invoice = document.Invoice;
                currentDocument.Currency = document.Currency;
                currentDocument.Deposit = document.Deposit;
                currentDocument.PriceOffer = document.PriceOffer;
                currentDocument.Waybill = document.Waybill;
                currentDocument.Receipt = document.Receipt;
                currentDocument.PreliminaryInvoice = document.PreliminaryInvoice;
                currentDocument.InvoiceReceipt = document.InvoiceReceipt;
                currentDocument.DocumentItems = document.DocumentItems;
                currentDocument.IsRounded = document.IsRounded;

                if (HttpContext.Current.User.Identity.GetOrgType() == Enums.BusinessType.Exemption)
                {
                    currentDocument.IsSmallBusiness = true;
                }
                Commit(sholdBeCommited);
            }

        }


        public long GetLastDocumentNumber(DocumentType type)
        {
            var orgId = HttpContext.Current.User.Identity.GetOrgId();
            switch (type)
            {
                case DocumentType.PriceOffer:
                    {
                        var document = UnitOfWork.Get<Entities.Documents.Document>()
                            .Where(d => d.DocumentType == type && d.OrganizationID == orgId)
                            .OrderByDescending(po => po.Number).FirstOrDefault();

                        var number = document == null
                            ? UnitOfWork.Get<DocumentNumbering>()
                                .FirstOrDefault(n => n.OrganizationId == orgId)
                                .InvoiceQuote
                            : document.Number;
                        return ++number;
                    }
                case DocumentType.Waybill:
                    {
                        var document = UnitOfWork.Get<Entities.Documents.Document>()
                            .Where(d => d.DocumentType == type && d.OrganizationID == orgId)
                            .OrderByDescending(po => po.Number).FirstOrDefault();

                        var number = document == null
                            ? UnitOfWork.Get<DocumentNumbering>()
                                .FirstOrDefault(n => n.OrganizationId == orgId)
                                .InvoiceShip
                            : document.Number;
                        return ++number;
                    }
                case DocumentType.PreliminaryInvoice:
                    {
                        var document = UnitOfWork.Get<Entities.Documents.Document>()
                        .Where(d => d.DocumentType == type && d.OrganizationID == orgId)
                        .OrderByDescending(po => po.Number).FirstOrDefault();

                        var number = document == null
                            ? UnitOfWork.Get<DocumentNumbering>()
                                .FirstOrDefault(n => n.OrganizationId == orgId)
                                .ProformaInvoice
                            : document.Number;
                        return ++number;
                    }
                case DocumentType.TaxInvoice:
                    {
                        var document = UnitOfWork.Get<Entities.Documents.Document>()
                       .Where(d => d.DocumentType == type && d.OrganizationID == orgId)
                       .OrderByDescending(po => po.Number).FirstOrDefault();

                        var number = document == null
                            ? UnitOfWork.Get<DocumentNumbering>()
                                .FirstOrDefault(n => n.OrganizationId == orgId)
                                .Invoice
                            : document.Number;
                        return ++number;
                    }
                case DocumentType.Receipt:
                    {
                        var document = UnitOfWork.Get<Entities.Documents.Document>()
                        .Where(d => d.DocumentType == type && d.OrganizationID == orgId)
                        .OrderByDescending(po => po.Number).FirstOrDefault();

                        var number = document == null
                            ? UnitOfWork.Get<DocumentNumbering>()
                                .FirstOrDefault(n => n.OrganizationId == orgId)
                                .Receipt
                            : document.Number;
                        return ++number;
                    }
                case DocumentType.TaxInvoiceReceipt:
                    {
                        var document = UnitOfWork.Get<Entities.Documents.Document>()
                        .Where(d => d.DocumentType == type && d.OrganizationID == orgId)
                        .OrderByDescending(po => po.Number).FirstOrDefault();

                        var number = document == null
                            ? UnitOfWork.Get<DocumentNumbering>()
                                .FirstOrDefault(n => n.OrganizationId == orgId)
                                .InvoiceReceipt
                            : document.Number;
                        return ++number;
                    }
                default:
                    return 1;

            }
        }

        public Entities.Documents.Document GetReferenceDocumentByRefId(Guid id)
        {
            var refId = UnitOfWork.Get<DocumentReference>().FirstOrDefault(d => d.ReferenceDocumentId == id);
            if (refId == null)
                return null;
            return UnitOfWork.Get<Entities.Documents.Document>().FirstOrDefault(d => d.ID == refId.BaseDocumentId && d.DocumentType != DocumentType.Receipt);
        }

        public IQueryable<DocumentReference> GetReferenceDocumentById(Guid id)
        {
            return UnitOfWork.Get<DocumentReference>().Where(d => d.BaseDocumentId == id);
        }

        public Entities.Documents.Document GetAutoCreatedPreliminaryInvoice(int clientId, DateTime date)
        {
            return GetUserDocuments()
                .FirstOrDefault(d => d.DocumentType == DocumentType.PreliminaryInvoice &&
                                          d.ClientID == clientId &&
                                          d.DateCreated.Month == date.Month &&
                                          d.DateCreated.Year == date.Year &&
                                          d.PreliminaryInvoice.IsAutoCreated);
        }

        public double GetDocumentReceiptsSum(Guid id)
        {
            var result =
                UnitOfWork.Get<DocumentReference>()
                    .Where(d => d.ReferenceDocumentId == id && d.Type == DocumentType.PreliminaryInvoice && d.BaseDocument.DocumentType == DocumentType.Receipt)
                    .Select(d => d.BaseDocument)
                    .Sum(s=> (double?)s.Total) ?? 0;
            result += UnitOfWork.Get<Receipt>().Where(r => r.ReferenceDocument == id ).Select(r => r.Document).Sum(s=>(double?)s.Total) ?? 0;
            result += UnitOfWork.Get<Payment>().Where(p => p.DocumentID == id).Sum(p=>(double?)p.Amount) ?? 0;
            result += UnitOfWork.Get<DocumentReference>()
                    .Where(d => d.ReferenceDocumentId == id && d.Type == DocumentType.PreliminaryInvoice && d.CreditAmount != null)
                    .Sum(s => s.CreditAmount) ?? 0;
            return result;
        }

        public double GetDocumentPrepaymentSum(Guid id)
        {
            return UnitOfWork.Get<Receipt>().Where(r => r.ReferenceDocument == id).Select(r => r.Document).Sum(s => (double?)s.Total) ?? 0;
        }

        public double GetInvoiceTotalPrepaymentSum(Guid id)
        {
            var invoice = UnitOfWork.Get<Entities.Documents.Document>().FirstOrDefault(i => i.ID == id && i.DocumentType == DocumentType.TaxInvoice);
            if (invoice == null)
                return 0;

            double prepaymentSum = 0;
            var relatedDocuments = invoice.DocumentReferences.Select(r => r.BaseDocument).ToList();
            foreach (var relatedDocument in relatedDocuments)
            {
                prepaymentSum += GetDocumentPrepaymentSum(relatedDocument.ID);
            }
            return prepaymentSum;
        }

        public IQueryable<Entities.Documents.Document> GetUserDocuments(int? organizationId = null, bool isExport = false)
        {
            var orgId = organizationId ?? HttpContext.Current.User.Identity.GetOrgId();
            
            return GetAll(isExport).Where(d=>d.OrganizationID == orgId); 
        }

        public void ChangeDocumentPaidStatus(Guid id, bool isPaid)
        {
            var document = Get(id);
            if (document != null)
            {
                document.IsPaid = isPaid;
                Commit(true);
            }
        }

        public IEnumerable<Entities.Documents.Document> Fetch(DocumentSpecifiation spec)
        {
            var query = this.GetAll();

            query = query
                .Where(x => x.OrganizationID == spec.OrganizationId)
                .Where(x => x.DocumentType == spec.DocumentType)
                .OrderByDescending(x=>x.Number);

            return query;
        }

        public override void Remove(Entities.Documents.Document removeItem, bool shouldBeCommited = false)
        {
            var isRemoved = false;

            try
            {
                base.Remove(removeItem, shouldBeCommited);
                base.Commit(true);

                isRemoved = true;

                Trace.WriteLine($"Deleted document {removeItem.ID}");
            }
            catch (Exception e)
            {
                Trace.WriteLine($"Delete document {removeItem.ID} failed. {e}");
            }

            if (isRemoved)
            {
                return;
            }

            // Try mark document as deleted

            try
            {
                removeItem.Status = DocumentStatusType.Deleted;
                removeItem.Number = long.MinValue;

                this.Update(removeItem, true);
                Trace.WriteLine($"Deleted document {removeItem.ID}");
            }
            catch (Exception e)
            {
                Trace.WriteLine($"Mark as deleted document {removeItem.ID} failed. {e}");
                throw;
            }
        }
        
        public override IQueryable<Entities.Documents.Document> GetAll()
        {
            return GetAll(false);
        }

        public  IQueryable<Entities.Documents.Document> GetAll(bool isExport)
        {

            return base.GetAll(). Where(m => m.IsExport == isExport);
        }

        public override Entities.Documents.Document Get(Guid id)
        { 
            return base.GetAll().FirstOrDefault(m => m.ID == id);
        }
    }
}
