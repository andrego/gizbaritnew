﻿using Gizbarit.DAL.Entities.Documents;

namespace Gizbarit.DAL.Services.Document
{
    public class DocumentSpecifiation
    {
        public int? OrganizationId { get; set; }

        public DocumentType DocumentType { get; set; } = DocumentType.Receipt;

        public int PageIndex { get; set; } = 0;

        public int PageSize { get; set; } = 20;
    }
}