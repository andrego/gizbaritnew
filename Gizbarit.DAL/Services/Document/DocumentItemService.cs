﻿using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Document
{
    public class DocumentItemService:BaseService<DocumentItem, int >, IDocumentItemService
    {
        public DocumentItemService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
