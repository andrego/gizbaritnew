﻿using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Services.Document
{
    public interface IPaymentService:IBaseService<Payment, int>
    {
    }
}
