﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Payment;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;

namespace Gizbarit.DAL.Utils
{
    public class Getter
    {
        public static IList<Gizbarit.DAL.Models.Documents.Related.DocumentCatalogItemViewModel> GetCatalogItems(IOrganisationService organisationService)
        {
            return organisationService.GetCatalogItems().Select(c => c.ToCatalogItemViewModel()).ToList();
        }

        public static IList<PaymentTypeViewModel> GetPaymentTypes(IPaymentTypeService paymentTypeService)
        {
            return new List<PaymentTypeViewModel>
            {
                new PaymentTypeViewModel {Id = 1, Name = Translations.Documents.Check},
                new PaymentTypeViewModel {Id = 2, Name = Translations.Documents.BankTransfer},
                new PaymentTypeViewModel {Id = 3, Name = Translations.Documents.CashPayment},
                new PaymentTypeViewModel {Id = 4, Name = Translations.Documents.CreditCardPayment},
                new PaymentTypeViewModel {Id = 5, Name = Translations.Documents.WithholdingTax}
            };
        }

        public static IList<PaymentViewModel> GetPayments(IPaymentService paymentService, Guid documentId)
        {
            return
                paymentService.GetAll().Where(p => p.DocumentID == documentId).ToList().Select(x => new PaymentViewModel
                {
                    idPayment = x.ID,
                    PaymentType = x.PaymentType,
                    BranchName = x.BranchName,
                    BankName = x.BankName,
                    CheckNumber = x.PaymentNumber,
                    Sum = x.Amount,
                    DateOperation = x.Date,
                    CreditCardType = x.CreditCardType,
                    AccountNumber = x.AccountNumber
                }).ToList();
        } 

        public static int GetPaymentDueDays(Document d)
        {
            return d.PaymentDueDate == null
                ? 0
                : d.PaymentDueDate.Value.Subtract(DocumentUtils.GetDocumentPayDate(d.DateCreated, 0)).Days;
        }
    }
}