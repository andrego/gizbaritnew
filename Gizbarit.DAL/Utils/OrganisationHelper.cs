﻿using System;
using Gizbarit.DAL.Entities.Organization;

namespace Gizbarit.DAL.Utils
{
    public static class OrganisationHelper
    {
        public static DateTime GetVatPayDay(this Organization organisation, DateTime initialDate)
        {
            if (organisation == null)
                throw new NullReferenceException("Organisation is null");

            var vatPayDateDate =
                 initialDate.AddMonths(organisation.AccountingMethod == 1
                    ? 1 // per month: add 1 month
                    : (initialDate.Month % 2 == 0 ?
                        1 // even month: add 1 month
                        : 2 // odd month: add 2 month
                        ));

            return new DateTime(vatPayDateDate.Year, vatPayDateDate.Month, 15);

        }

        public static DateTime GetAdvancesPayDay(this Organization organisation, DateTime initialDate)
        {
            if (organisation == null)
                throw new NullReferenceException("Organisation is null");

            var vatPayDateDate =
                 initialDate.AddMonths(organisation.AdvancesAccountingMethod == 1
                    ? 1 // per month: add 1 month
                    : (initialDate.Month % 2 == 0 ?
                        1 // even month: add 1 month
                        : 2 // odd month: add 2 month
                        ));

            return new DateTime(vatPayDateDate.Year, vatPayDateDate.Month, 15);
        }
    }
}