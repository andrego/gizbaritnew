﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.Mvc;
//using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Interfaces;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Models.Organisation;
using Gizbarit.DAL.Models.Payment;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.Utils;

namespace Gizbarit.DAL.Utils
{
    public class DocumentUtils
    {
        private readonly IOrganisationService _organizationService;
        private readonly IDocumentService _documentService;
        private readonly ICatalogService _catalogService;
        private readonly IDocumentItemService _documentItemService;
        private readonly ITransactionService _transactionService;
        private readonly IAdvanceService _advanceService;
        private readonly IBankAccountService _bankAccountService;
        private readonly IPaymentService _paymentService;

        private readonly ICurrencyService _currencyService;

        public DocumentUtils(IOrganisationService organizationService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService, IAdvanceService advanceService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, ICatalogService catalogService,
            ICurrencyService currencyService)
        {
            _organizationService = organizationService;
            _documentService = documentService;
            _documentItemService = documentItemService;
            _transactionService = transactionService;
            _advanceService = advanceService;
            _bankAccountService = bankAccountService;
            _paymentService = paymentService;
            _catalogService = catalogService;

            _currencyService = currencyService;

        }

        public void AddClient(ClientViewModel model)
        {
            var client = _organizationService.GetCurrent().Clients.FirstOrDefault(c => c.UniqueId == model.UniqueId && c.Active);
            if (client == null) 
            {
                var organisation = _organizationService.GetCurrent();
                var newClient = new Client
                {
                    DateCreated = DateTime.UtcNow,
                    Name = model.Name,
                    UniqueId = model.UniqueId,
                    Address = model.Address,
                   // Phone = model.Phone,
                   // Email = model.Email,
                    RetainerAmount = model.RetainerAmount,
                    Active = true
                };
                newClient.ClientEmailContacts.Add(new ClientEmailContact() {Email = model.Email, IsActive = true});
                newClient.ClientPhoneContacts.Add(new ClientPhoneContact() {Phone = model.Phone, IsActive = true});

                organisation.Clients.Add(newClient);
                _organizationService.Commit(true);
                model.Id = newClient.ID;
            }
            else 
            {
                if (client.Email == null)
                    client.Email = model.Email;
                if (client.Phone == null)
                    client.Phone = model.Phone;
                if (client.Address == null)
                    client.Address = model.Address;
                _organizationService.Commit(true);
                model.Id = client.ID;
            }
        }

        public bool VerifyDocumentCreationDate(DocumentType type,
            DateTime dateCreated)
        {

            var lastDocument =
                _documentService.GetUserDocuments()
                    .OrderByDescending(d => d.Number)
                    .FirstOrDefault(d => d.DocumentType == type);
            if (lastDocument != null)
            {
                return lastDocument.DateCreated.Date <= dateCreated.Date;
            }
            return true;
        }

        public static DateTime GetDocumentPayDate(DateTime date, int daysCount)
        {
            var maxDayInMonth = DateTime.DaysInMonth(date.Year, date.Month);
            var maxDayDate = new DateTime(date.Year, date.Month,maxDayInMonth);
            return maxDayDate.AddDays(daysCount);
        }

        public BaseDocument InitBaseDocument(DocumentType type)
        {
            var organisation = _organizationService.GetCurrent();
            return new BaseDocument
            {
                DateCreated = DateTime.Now,
                Number = _documentService.GetLastDocumentNumber(type),
                TaxAmount = organisation.TaxRate,
                BusinessInfo = organisation.ToBaseAccountShort(),
                IsSmallBusiness = HttpContext.Current.User.Identity.GetOrgType() == Enums.BusinessType.Exemption,
                DocumentType = type
            };
        }


        public static void FilterModelState(ModelStateDictionary modelState, string[] keysToNotValidate)
        {
            #region Modify ModelState
            List<string> keysTobeRemoved = new List<string>();

            if (keysToNotValidate != null && keysToNotValidate.Any())
            {
                foreach (var key in keysToNotValidate)
                {
                    keysTobeRemoved.AddRange(
                        modelState.Where(x => String.Compare(key, 0, x.Key, 0, key.Length) == 0)
                            .Select(x => x.Key)
                            .ToList());
                }
            }
            foreach (var item in keysTobeRemoved)
            {
                modelState.Remove(item);
            }
            #endregion
        }

        public bool ValidateProductCode(IEnumerable<IHasCodeAndNullableId> products)
        {
            var codes = products.Where(p => p.ID.HasValue && p.ID == -1).Select(p => p.Code).ToList();
            return _catalogService.GetAll().Any(c => codes.Contains(c.Code));
        }

        public bool ValidateProductCode(IEnumerable<IHasCodeAndId> products)
        {
            var codes = products.Where(p => p.ID == -1).Select(p => p.Code).ToList();
            return _catalogService.GetAll().Any(c => codes.Contains(c.Code));
        }

        public void CleanDocumentItems(Document document)
        {
            var documentItems = document.DocumentItems.ToList();
            foreach (var item in documentItems)
            {
                _documentItemService.Remove(item, true);
            }
            document.DocumentItems.Clear();
        }

        public void CleanDocumentPayments(Document document)
        {
            var documentPayments = document.Payments.ToList();
            foreach (var payment in documentPayments)
            {
                _paymentService.Remove(payment, true);
            }
            document.Payments.Clear();
        }

        public void AddDocumentReferences(Document document, Guid? destination, DocumentType sourceType,
            DocumentType destinationType)
        {
           
            var refDocument = _documentService.GetAll(document.IsExport).FirstOrDefault(d => d.ID == destination);
          
            if (refDocument != null && refDocument.DocumentType == destinationType)
            {
                document.DocumentReferences.Add(new DocumentReference
                {
                    BaseDocumentId = refDocument.ID,
                    //ReferenceDocumentId = refDocument.ID,
                    Type = destinationType

                });
                if (sourceType == DocumentType.TaxInvoiceReceipt && document.InvoiceReceipt != null)
                    document.InvoiceReceipt.Paid += refDocument.PreliminaryInvoice.Paid;

                if (sourceType == DocumentType.TaxInvoice && document.Invoice != null)
                    document.Invoice.Paid += refDocument.PreliminaryInvoice.Paid;

                refDocument.DocumentReferences.Add(new DocumentReference
                {
                    BaseDocumentId = document.ID,
                    Type = sourceType
                });
            }
        }

        public void AddDocumentItems(Document document, IEnumerable<Gizbarit.DAL.Models.Documents.Related.DocumentCatalogItemViewModel> includedItems)
        {
            var organisation = _organizationService.GetCurrent();
            foreach (var item in includedItems)
            {
                if (item.ID.HasValue && item.ID != -1)
                {
                    item.ImagePath = organisation.CatalogItems.FirstOrDefault(c => c.ID == item.ID)?.ImagePath;
                    item.ImagePath = item.ImagePath == "plus_ic.png" ? null : item.ImagePath;
                }

                document.DocumentItems.Add(item.ToDocumentItem(document));
                if (item.DocumentId.HasValue && item.DocumentId != document.ID)
                {
                    _documentService.ChangeDocumentPaidStatus(item.DocumentId.Value, true);
                }
            }
        }

        public void AdDocumentPayments(PaymentViewModel payment, Guid docId, int clientId)
        {
            _paymentService.Create(new Payment
            {
                Amount = payment.Sum,
                DocumentID = docId,
                PaymentType = payment.PaymentType,
                Date = payment.DateOperation,
                BankName = payment.BankName,
                CreditCardType = payment.CreditCardType,
                BranchName = payment.BranchName,
                AccountNumber = payment.AccountNumber,
                PayerID = clientId,
                PaymentNumber = payment.CheckNumber
            });
        }

        public void AddTransaction(int orgId, PaymentViewModel payment, int bankAccoutId, long documentNumber, string clientName)
        {
            _transactionService.Create(new BankTransaction
            {
                ID = Guid.NewGuid(),
                OrganizationId = orgId,
                Amount = payment.Sum,
                BankId = bankAccoutId,
                Credit = true,
                DateInserted = payment.DateOperation,
                Details = String.Format("{2} #{0} ({1})", documentNumber, clientName, Translations.Bank.PaymentForInvoiceLabel),
                DateUpdated = payment.DateOperation,
                CurrencyId = "ILS"
            }, true);
        }

        public void ProcessDocumentBanking(InvoiceReceipt document)
        {
            var org = _organizationService.GetCurrent();
            AddTaxesOnDocumentCreate(document.Document.ToBaseDocument(org), document.Document.Number, DocumentType.TaxInvoiceReceipt);

            var bankAccount = _bankAccountService.GetDefaultAccount(HttpContext.Current.User.Identity.GetOrgId());

            foreach (var payment in document.Document.Payments.Where(p => p.PaymentType == 1 || p.PaymentType == 2))
            {
                AddTransaction(HttpContext.Current.User.Identity.GetOrgId(), payment.ToPaymentViewModel(), bankAccount.ID,
                         document.Document.Number, document.Document.Client.Name);
            }

        }

        public void ProcessDocumentBanking(Receipt document)
        {
            var refDocument = _documentService.Get(document.ReferenceDocument.Value);

            //var documentType = refDocument.DocumentType == DocumentType.PreliminaryInvoice
            //    ? PaymentType.Prepayment
            //    : PaymentType.Payment;
            //ProcessPayments(document.Document.ToReceiptViewModel(), document, documentType);
        }



        public void AddTaxesOnDocumentCreate(BaseDocument document, long taxInvoiceNumber,
          DocumentType documentType)
        {
            var org = _organizationService.GetCurrent();


            double conversionRate = 1;
            if (document.IsExport)
            {
                var firstOrDefault = _currencyService.Get("ILS")
                    .CurrenciesRates.FirstOrDefault(m => m.Symbol == document.Currency);

                conversionRate = (firstOrDefault != null) ? firstOrDefault.ConversionRate : 1;
            }
            _advanceService.Create(new Advance
            {
                OrganizationId = org.ID,
                DateCreated = document.DateCreated,
                Amount =  Math.Round(document.PriceWithTax * conversionRate, 2),
                TaxAmount = Math.Round(document.Tax, MidpointRounding.AwayFromZero),
                Credit = true,
                AdvanceAmount = Math.Round(document.TotalPrice * (org.AdvancesRate / 100) * conversionRate),
                DateAction = document.DateCreated,
                Description = document.Subject + " - " + document.ClientInfo.Name,
                InvoiceNumber = (int?)taxInvoiceNumber,
                InvoiceId = document.Id,
                ForDocumentType = documentType
            }, true);

            var defaultBank = _bankAccountService.GetDefaultAccount(org.ID);
            if (defaultBank != null)
            {
                UpdateAdvanceTransaction(org, document, defaultBank.ID);

                if (org.BusinessTypeId != (int)Enums.BusinessType.Exemption)
                {
                    UpdateVatTransaction(org, document, defaultBank.ID);
                }
            }
            else
            {
                //TODO log error
            }
        }

        public void ProcessToPreliminaryInvoice(Document document)
        {
            double newItemsSumm = 0;
            double newItemsWoTax = 0;

            var thisMonthPreliminaryInvoice =
                _documentService.GetAutoCreatedPreliminaryInvoice(document.ClientID,
                        document.DateCreated);

            var isNewDocument = (thisMonthPreliminaryInvoice == null);
            if (!isNewDocument && thisMonthPreliminaryInvoice.IsTaxIncluded != document.IsTaxIncluded)
            {
                return;
            }

            if (isNewDocument)
            {
                var prelimiNaryInvoce = _documentService.GetAll()
                    .AsNoTracking()
                    .FirstOrDefault(d => d.ID == document.ID);
                prelimiNaryInvoce.DocumentType = DocumentType.PreliminaryInvoice;
                thisMonthPreliminaryInvoice = prelimiNaryInvoce;
                thisMonthPreliminaryInvoice.ID = Guid.NewGuid();
                //thisMonthPreliminaryInvoice.OrganizationID = OrgId;
                thisMonthPreliminaryInvoice.PreliminaryInvoice = new PreliminaryInvoice
                {
                    IsAutoCreated = true,
                };
                thisMonthPreliminaryInvoice.Number =
                    _documentService.GetLastDocumentNumber(DocumentType.PreliminaryInvoice);
                thisMonthPreliminaryInvoice.Total = 0;
                thisMonthPreliminaryInvoice.TotalWithoutRounding = 0;
                thisMonthPreliminaryInvoice.TotalwithoutTax = 0;
                thisMonthPreliminaryInvoice.TotalTaxAmount = 0;
            }
            else
            {
                var oldItems = thisMonthPreliminaryInvoice.DocumentItems.Where(d => d.WaybillID == document.ID).ToList();
                foreach (var item in oldItems)
                {
                    item.WaybillID = null;
                    thisMonthPreliminaryInvoice.Total -= item.Total + item.TotalTax;
                    thisMonthPreliminaryInvoice.TotalWithoutRounding -= item.Total + item.TotalTax;
                    thisMonthPreliminaryInvoice.TotalwithoutTax -= item.Total;
                    thisMonthPreliminaryInvoice.TotalTaxAmount -= item.TotalTax;
                    _documentItemService.Remove(item);
                }
            }

            foreach (var item in document.DocumentItems)
            {
                var documentItem = new DocumentItem(item) { WaybillID = document.ID };

                newItemsSumm += item.Total + documentItem.TotalTax;
                newItemsWoTax += item.Total;
                thisMonthPreliminaryInvoice.DocumentItems.Add(documentItem);
            }

            thisMonthPreliminaryInvoice.Total += newItemsSumm;
            thisMonthPreliminaryInvoice.TotalWithoutRounding += newItemsSumm;
            thisMonthPreliminaryInvoice.TotalwithoutTax += newItemsWoTax;
            thisMonthPreliminaryInvoice.TotalTaxAmount += newItemsSumm - newItemsWoTax;

            var distinctCatalogItems =
            document.DocumentReferences
                .GroupBy(i => i.ReferenceDocumentId)
                .Select(g => g.First());

            foreach (var item in distinctCatalogItems)
            {
                //var documentRef = item.ToDocumentReference(DocumentType.PriceOffer);

                if (!thisMonthPreliminaryInvoice.DocumentReferences.Contains(item))
                    thisMonthPreliminaryInvoice.DocumentReferences.Add(item);
            }

            if (isNewDocument)
            {
                _documentService.Create(thisMonthPreliminaryInvoice, true);
            }
            else
            {
                _documentService.Update(thisMonthPreliminaryInvoice, true);
            }

        }

        private void UpdateAdvanceTransaction(Organization org, BaseDocument document, int defaultBankId)
        {
            var advancesPayDate = org.GetAdvancesPayDay(document.DateCreated);
            if (document.IsExport)
            {
                var firstOrDefault = _currencyService.Get("ILS")
                    .CurrenciesRates.FirstOrDefault(m => m.Symbol == document.Currency);

                var conversionRate = (firstOrDefault != null) ? firstOrDefault.ConversionRate : 1;
                _transactionService.UpdateAutoAdvanceTransaction(defaultBankId, advancesPayDate,
                    (document.TotalPrice * conversionRate)* (org.AdvancesRate / 100));

            }
            else
            {
                 _transactionService.UpdateAutoAdvanceTransaction(defaultBankId, advancesPayDate,
                     document.TotalPrice * (org.AdvancesRate / 100));
            }
            // _transactionService.UpdateAutoAdvanceTransaction(defaultBankId, advancesPayDate,
            //     document.TotalPrice * (org.AdvancesRate / 100));
        }

        private void UpdateVatTransaction(Organization org, BaseDocument document, int defaultBankId)
        {
            var vatPayDateDate = org.GetVatPayDay(document.DateCreated);
            _transactionService.UpdateAutoVatTransaction(defaultBankId, vatPayDateDate, document.Tax);
        }
    }
}