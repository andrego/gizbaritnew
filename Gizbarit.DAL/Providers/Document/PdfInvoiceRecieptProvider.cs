﻿using System.IO;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Resource = Gizbarit.DAL.Resources.Pdf.PdfInvoiceRecieptResource;


namespace Gizbarit.DAL.Providers.Document
{
    public class PdfInvoiceRecieptProvider : PdfProviderBase, IDocumentProvider<Entities.Documents.Document>
    {
        public ContentBase Create(Entities.Documents.Document entity, DocumentProviderArgs args)
        {
            var pdfDocument = new iTextSharp.text.Document(PageSize.A4);
            var outputStream = new MemoryStream();
            var stream = PdfWriter.GetInstance(pdfDocument, outputStream);

            pdfDocument.Open();
            pdfDocument.NewPage();

            var docName = entity.Organization.BusinessTypeId == 2 ? $"{Resource.InvoiceTitle} " : $"{Resource.InvoiceRecieptTitle} ";
            var refundDocName = entity.Organization.BusinessTypeId == 2 ? $"{Resource.InvoiceRefund} " : $"{Resource.InvoiceRecieptRefund} ";

            docName = entity.IsRefund() ? $"{refundDocName} {Resource.Number}' {entity.Number}" : $"{docName} {Resource.Number}' {entity.Number}";


            CreateHeader(pdfDocument, entity,  docName, args);
            CreateDescription(pdfDocument, entity);
            CreateItemTable(pdfDocument, entity);
            CreateSummaryTable(pdfDocument, entity);
            CreatePaymentTable(pdfDocument, entity);
            AddInternalCommentRow(pdfDocument, entity);
            CreateSignatureRow(pdfDocument, entity);
            AddingFooter(stream);
            
            pdfDocument.Close();

            var content = new PdfContent()
            {
                Name =$"{entity.DocumentType}-{entity.Number}.pdf",
                Data = outputStream.ToArray(),
                OrganizationId = entity.OrganizationID,
                Description = docName,
                DocumentId = entity.ID
            };

            content.Data = AddPageLayer(content.Data, !args.IsCombinedReport);

            return content;
        }
    }
}