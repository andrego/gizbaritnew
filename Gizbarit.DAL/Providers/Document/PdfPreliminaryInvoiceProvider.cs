﻿using System.IO;
using Gizbarit.DAL.Entities.Content;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Resource = Gizbarit.DAL.Resources.Pdf.PdfPreliminaryInvoiceResource;

namespace Gizbarit.DAL.Providers.Document
{
    public class PdfPreliminaryInvoiceProvider : PdfProviderBase, IDocumentProvider<Entities.Documents.Document>
    {
        public ContentBase Create(Entities.Documents.Document entity, DocumentProviderArgs args)
        {
            var pdfDocument = new iTextSharp.text.Document(PageSize.A4);
            var outputStream = new MemoryStream();
            var stream = PdfWriter.GetInstance(pdfDocument, outputStream);
            var inv = entity.PreliminaryInvoice;

            pdfDocument.Open();
            pdfDocument.NewPage();

            args.StampType = DocumentStampType.None;

            CreateHeader(pdfDocument, entity, $"{Resource.Title}' {inv.Document.Number}", args);
            CreateDescription(pdfDocument, entity);
            CreateItemTable(pdfDocument, entity);
            CreateSummaryTable(pdfDocument, entity);
            AddInternalCommentRow(pdfDocument, entity);
            CreateSignatureRow(pdfDocument, entity);
            CreateDueDayPaid(pdfDocument, entity);
            AddingFooter(stream);

            pdfDocument.Close();

            // TODO: Check it
            var content = new PdfContent()
            {
                Name = $"{entity.DocumentType}-{entity.Number}.pdf",
                Data = outputStream.ToArray()
            };

            content.Data = AddPageLayer(content.Data, !args.IsCombinedReport);

            return content;
        }

    }
}
