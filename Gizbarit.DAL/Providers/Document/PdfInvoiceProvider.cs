﻿using System.IO;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Resource = Gizbarit.DAL.Resources.Pdf.PdfInvoiceResource;


namespace Gizbarit.DAL.Providers.Document
{
    public class PdfInvoiceProvider : PdfProviderBase, IDocumentProvider<Entities.Documents.Document>
    {
        public ContentBase Create(Entities.Documents.Document entity, DocumentProviderArgs args)
        {

            var pdfDocument = new iTextSharp.text.Document(PageSize.A4);
            var outputStream = new MemoryStream();
            var stream = PdfWriter.GetInstance(pdfDocument, outputStream);
            var docName = entity.IsRefund() ? $"{Resource.Title}' {Resource.Refund} {Resource.Number}' {entity.Number}" : $"{Resource.Title} {Resource.Number}' {entity.Number}";

            pdfDocument.Open();
            pdfDocument.NewPage();

            CreateHeader(pdfDocument, entity, docName, args);
            CreateDescription(pdfDocument, entity);
            CreateItemTable(pdfDocument, entity);
            CreateSummaryTable(pdfDocument, entity);
            AddInternalCommentRow(pdfDocument, entity);
            CreateSignatureRow(pdfDocument, entity);
            AddingDueToRow(pdfDocument, entity);
            AddingFooter(stream);

            pdfDocument.Close();

            var content = new PdfContent()
            {
                Name = $"{entity.DocumentType}-{entity.Number}.pdf",
                Description = docName,
                Data = outputStream.ToArray()
            };

            content.Data = AddPageLayer(content.Data, !args.IsCombinedReport);

            return content;
        }


    }

}