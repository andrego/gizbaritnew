﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Providers.Store;
using Gizbarit.Utils;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Resource = Gizbarit.DAL.Resources.Pdf.PdfBaseResource;




namespace Gizbarit.DAL.Providers.Document
{
    public abstract class PdfProviderBase //: IDocumentProvider
    {
        protected readonly Font _arialFont;
        protected readonly Font _gishaFont;
        protected readonly Font _arialFontBold;
        protected readonly Font _orgFont;
        protected readonly Font _gishaFontBold;
        protected readonly ImageContent _digitalySignedLogo;
  
  //      public abstract ContentBase Create(Entities.Documents.Document document, DocumentProviderArgs args);

        protected PdfProviderBase()
        {
                _arialFont = DocumentProviderExtension.GetFont("ARIALUNI.TTF");
                _arialFontBold = DocumentProviderExtension.GetFont("GISHABD.TTF");
                _orgFont = DocumentProviderExtension.GetFont("ARIALUNI.TTF", 12);
                _gishaFont = DocumentProviderExtension.GetFont("GISHABD.TTF", 12);
                _gishaFontBold = DocumentProviderExtension.GetFont("GISHABD.TTF", 15);
                _digitalySignedLogo = DocumentProviderExtension.GetLogo("GARYWORD.png");
        }


        protected void CreateHeader(iTextSharp.text.Document document, Entities.Documents.Document entityDocument, string documentKind, DocumentProviderArgs args)
        {
            var docType = PdfTableExtentions.GetPdfType(args);

            var widths = new[] { 3f, 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);

            var logo = DocumentProviderExtension.GetLogo(entityDocument);

            // Images
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;

            if (args.IsDigitalySigned)
            {
                table.AddLogoImage(logo);
                table.AddSignLogoImage(_digitalySignedLogo);
            }
            else
            {
                table.AddLogoImage(logo);
                table.AddTextCell(string.Empty, _orgFont);
            }

            //// Company details
            var ent = entityDocument;

            table.AddCell(HeaderCell(ent.Organization));

            if (ent.Organization.NameEng != null)
            {
                table.AddCell(HeaderEngCell(ent.Organization, docType));
            }
            else
            {
                table.AddTextCell(docType, _gishaFontBold, 1, 3, Element.ALIGN_CENTER);
            }


            document.Add(table);

            widths = new[] {3f};
            table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.SetWidths(widths);


            table.AddTextCell(documentKind, _gishaFontBold, widths.Length, 3, Element.ALIGN_CENTER);

            table.AddTextCell(ent.DateCreated.Date.ToString("dd/MM/yyyy"), _orgFont, 2, 3, Element.ALIGN_RIGHT);
            table.AddTextCell($"{Resource.To}:", _gishaFont);
            table.AddTextCell(ent.Client.Name, _gishaFontBold, 2);
            table.AddTextCell($"{Resource.BusNo}. " + ent.Client.UniqueId, _orgFont, 2);
            table.AddTextCell(ent.Client.Email, _orgFont, 2);
            table.AddTextCell(ent.Client.Address, _orgFont, 2);
            table.AddTextCell($"{Resource.BusPhone}' " + ent.Client.Phone, _orgFont);
            table.AddTextCell(string.Empty, _gishaFont);
            document.Add(table);
        }

        protected void CreateDescription(iTextSharp.text.Document document, Entities.Documents.Document entityDocument)
        {


            var widths = new[] { 25f, 5f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 0.4f,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);

            if (entityDocument.DocumentType != DocumentType.Receipt)
            {
                table.AddTextCell($"{Resource.Desc}: ", _gishaFont);
                table.AddTextCell(entityDocument.Subject, _orgFont);
            }
            else
            {
                string docType = string.Empty;

                if (entityDocument.ExternalComments != null)
                {
                    docType = entityDocument.ExternalComments;
                }
                else
                {
                    var docReference = entityDocument.DocumentReferences.FirstOrDefault()?.BaseDocument;

                    if (docReference != null)
                    {
                        docType = Resource.ResourceManager.GetString(docReference.DocumentType.ToString());
                        docType = docType +" "+ docReference?.Number;
                    }
                }

                table.AddTextCell($"{Resource.RecieptOf}: ", _gishaFont); 
                table.AddTextCell($"{docType}", _gishaFont);
            }
            document.Add(table);
        }

        protected void CreateItemTable(iTextSharp.text.Document document, Entities.Documents.Document entityDocument)
        {
            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };

            var widths = new[] { 2f, 2f, 2f, 2f, 4f, 2f, 1f };
            var itemCatalogs = entityDocument.DocumentItems;
            var lineNum = 1;
            var paymentHeaderResources = PdfTableExtentions.PaymentHeaderResource;
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 0.5f,
                SpacingAfter = 0.4f,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };

            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;
            table.DefaultCell.BackgroundColor = BaseColor.GRAY;

            foreach (var headerItem in paymentHeaderResources.Values)
            {
                table.AddHeaderTextCell(headerItem, _gishaFont);
            }
            foreach (var item in itemCatalogs)
            {
                var imageData = DocumentProviderExtension.GetCatalogImage(item.ImagePath);

                table.AddBoxTextCell(lineNum++, _arialFont);
                table.AddBoxTextCell(item.Code, _arialFont);

                if (item.ImagePath == null && imageData != null)
                {
                    table.AddBoxTextCell(item.Name, _arialFont);
                }
                else
                {
                    table.AddCatalogImage(imageData, item.Name, _arialFont);
                }

                table.AddBoxTextCell(item.Price.ToString("N2", num), _arialFont);
                table.AddBoxTextCell(item.Quantity, _arialFont);
                table.AddBoxTextCell($"{item.Discount}%", _arialFont);
                var discountedPrice = item.Price * item.Quantity * (1 - item.Discount / 100);
                if (discountedPrice != null)
                {
                    table.AddBoxTextCell(discountedPrice.Value.ToString("N2", num), _arialFont);
                }
            }
            document.Add(table);
        }

        protected void CreateSummaryTable(iTextSharp.text.Document document, Entities.Documents.Document entityDocument)
        {
            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };

            var widths = new[] { 2f, 4f, 9f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 0.3f,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;
            var totalwithoutTax = entityDocument.TotalwithoutTax;
            var taxPercentage = entityDocument.TaxPercentage;
            var totalTaxAmount = entityDocument.TotalTaxAmount;
            var totalWithoutRounding = entityDocument.TotalWithoutRounding;
            var total = entityDocument.Total;
            var rounded = total - totalWithoutRounding;

            switch (entityDocument.Organization.BusinessTypeId)
            {
                case 1:
                    {
                        table.AddTextCell(string.Empty, _arialFontBold);
                        table.AddBoxTextCell($"{Resource.TotalWithoutTax}", _arialFontBold);
                        table.AddBoxTextCell(totalwithoutTax.ToString("N2", num), _arialFontBold);
                        table.AddTextCell(string.Empty, _arialFontBold);
                        table.AddBoxTextCell($"{Resource.Tax}" + taxPercentage + "% ", _arialFontBold);
                        table.AddBoxTextCell(totalTaxAmount.ToString("N2", num), _arialFontBold);
                        table.AddTextCell(string.Empty, _arialFontBold);
                        table.AddBoxTextCell($"{Resource.TotalWithoutRounding}", _arialFontBold);
                        table.AddBoxTextCell(totalWithoutRounding.ToString("N2", num), _arialFontBold);
                        break;
                    }
                case 2:
                    {
                        table.AddTextCell(string.Empty, _arialFontBold);
                        table.AddBoxTextCell($"{Resource.TotalWithoutTax}", _arialFontBold);
                        table.AddBoxTextCell(totalwithoutTax.ToString("N2", num), _arialFontBold);
                        break;
                    }
            }
            if (entityDocument.IsRounded)
            {
                table.AddTextCell(string.Empty, _arialFontBold);
                table.AddBoxTextCell($"{Resource.Round}", _arialFontBold);
                table.AddBoxTextCell(rounded.ToString("N2", num), _arialFontBold);
                table.AddTextCell(string.Empty, _arialFontBold);
                table.AddBoxTextCell($"{Resource.TotalRounded}", _arialFontBold);
                table.AddBoxTextCell(total.ToString("N2", num), _arialFontBold);
            }
            document.Add(table);
        }

        protected void CreatePaymentTable(iTextSharp.text.Document document, Entities.Documents.Document entityDocument)
        {
            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };

            var widths = new[] { 25f, 5f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 0.5f,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.AddTextCell($"{Resource.PaymentInfo}: ", _gishaFont);
            table.AddTextCell(string.Empty, _arialFont);
            document.Add(table);

            widths = new[] { 5f, 5f, 5f, 5f, 5f, 5f, 7f };
            table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 0.5f,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            var paymentHeaderItems = PdfTableExtentions.PaymentItemsResource;
            foreach (var headerItem in paymentHeaderItems.Values)
            {
                table.AddHeaderTextCell(headerItem, _gishaFont);
            }
            var dictionary = PdfTableExtentions.PaymentTypeResource;
            var itemPayments = entityDocument.Payments;
            foreach (var item in itemPayments)
            {
                var payment = dictionary[item.PaymentType];
                var itemDate = item.Date.ToString("dd/MM/yyyy");

                table.AddBoxTextCell(payment, _arialFont);
                table.AddBoxTextCell(item.AccountNumber, _arialFont);
                table.AddBoxTextCell(item.BranchName, _arialFont);
                table.AddBoxTextCell(item.BankName, _arialFont);
                table.AddBoxTextCell(item.PaymentNumber, _arialFont);
                table.AddBoxTextCell(item.Amount.ToString("N2", num), _arialFont);
                table.AddBoxTextCell(itemDate, _arialFont);
            }
            document.Add(table);
        }

        protected void AddInternalCommentRow(iTextSharp.text.Document document, Entities.Documents.Document entityDocument)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 50,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.SetWidths(widths);
            if (entityDocument.InternalComments != null)
                table.AddTextCell($"הערות:    " + entityDocument.InternalComments, _gishaFont, 2);
            document.Add(table);
        }

        protected void CreateSignatureRow(iTextSharp.text.Document document, Entities.Documents.Document entityDocument = null)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 50,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.SetWidths(widths);

            table.AddTextCell($"{ Resource.BusSign}: _______________________", _gishaFontBold);
            document.Add(table);
        }
        protected void AddingDueToRow(iTextSharp.text.Document document, Entities.Documents.Document entityDocument)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 50,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            if (entityDocument.PaymentDueDate != null)
            {
                table.AddTextCell(
                    $"{Resource.PaymentDueDate}: " + entityDocument.PaymentDueDate.Value.ToString("dd/MM/yyyy"),
                    _gishaFont);
            }
            document.Add(table);
        }
        protected void CreateWayBillSign(iTextSharp.text.Document document, Entities.Documents.Document entityDocument)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.AddTextCell($"{ Resource.ReciptSign}: _____________________", _gishaFont);
            document.Add(table);

            widths = new[] { 3f, 3f };
            table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.AddTextCell($"{Resource.Date}: ______________________", _gishaFont);
            table.AddTextCell($"{Resource.Hour}: ______________________", _gishaFont, widths.Length, 3, Element.ALIGN_CENTER);
            document.Add(table);
        }

        protected void CreateDueDaysRow(iTextSharp.text.Document document, Entities.Documents.Document entityDocument)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 50,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };

            if (entityDocument.PaymentDueDate.HasValue)
            {
                var days = (entityDocument.PaymentDueDate.Value - entityDocument.DateCreated).Days;
                table.DefaultCell.NoWrap = false;
                table.SetWidths(widths);
                table.AddTextCell($"{Resource.Proposition} - " + (days) + $" {Resource.Days}. ", _gishaFont, 2);
            }
            document.Add(table);
        }

        protected void CreateDueDayPaid(iTextSharp.text.Document document, Entities.Documents.Document entityDocument)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 50,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            if (entityDocument.PaymentDueDate != null)
                table.AddTextCell($"{Resource.PaymentDueDate}:    {entityDocument.PaymentDueDate.Value:dd/MM/yyyy}" , _gishaFontBold, 2);
            document.Add(table);
        }

        protected void AddingFooter(PdfWriter stream)
        {
            var footer = new Paragraph($"{Resource.FooterPdfString}", _arialFont)
            {
                Alignment = Element.ALIGN_CENTER
            };
            var footerTbl = new PdfPTable(1)
            {
                TotalWidth = 450,
                HorizontalAlignment = Element.ALIGN_RIGHT,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            footerTbl.DefaultCell.NoWrap = false;
            var cell = new PdfPCell(footer)
            {
                Border = 0,
                //PaddingLeft = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                HorizontalAlignment = Element.ALIGN_CENTER,
                NoWrap = false
            };
            footerTbl.AddCell(cell);
            footerTbl.WriteSelectedRows(0, -1, 70, 15, stream.DirectContent);
        }

        protected byte[] AddPageLayer(byte[] bytes, bool isPageNumsNeeded = true)
        {
            using (var stream = new MemoryStream())
            {
                var reader = new PdfReader(bytes);
                var document = new PdfDocument();

                using (var stamper = new PdfStamper(reader, stream))
                {
                    var pages = reader.NumberOfPages;
                    for (var i = 1; i <= pages; i++)
                    {
                        var stampContent = stamper.GetOverContent(i);
                        var rectangle = reader.GetPageSize(i);
                        rectangle.Left += document.LeftMargin / 2;
                        rectangle.Right -= document.RightMargin / 2;
                        rectangle.Top -= document.TopMargin / 2;
                        rectangle.Bottom += document.BottomMargin / 2 - 4f;

                        if(isPageNumsNeeded)
                        ColumnText.ShowTextAligned
                            (
                            stamper.GetUnderContent(i), 
                            Element.ALIGN_RIGHT,
                            new Phrase($"{Resource.CurrentPage} " + (i) + $" {Resource.TotalPages}- " + pages, _arialFont), 
                            568f, 15f, 0, 
                            PdfWriter.RUN_DIRECTION_RTL,0
                            );

                        rectangle.Border = Rectangle.BOX;
                        stampContent.SetColorStroke(BaseColor.BLACK);
                        stampContent.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                        stampContent.Rectangle(rectangle);
                        stampContent.Stroke();
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }

        protected byte[] AddDigitalLogo(byte[] bytes)
        {
            using (var stream = new MemoryStream())
            {
                var reader = new PdfReader(bytes);

                using (var stamper = new PdfStamper(reader, stream))
                {
                    var pdfContentByte = stamper.GetOverContent(1);

                    var image = Image.GetInstance(_digitalySignedLogo.Data);
                    image.SetAbsolutePosition(225, 670);
                    image.ScalePercent(15);
                    pdfContentByte.AddImage(image);
                    stamper.Close();
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }

        private PdfPCell HeaderCell(Organization org)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            var hebHeader = new PdfPCell(table)
            {
                HorizontalAlignment = Element.ALIGN_RIGHT,
                Border = 0,
                NoWrap = false,
                BackgroundColor = BaseColor.WHITE
            };


            // Company details
            var businessType = org.BusinessType;
            var registerType = org.RegisterType;
            string orgType = null;

            if (businessType.ID == 1 )
            {
                switch (registerType)
                {
                    case Enums.RegisterType.Company:
                        {
                            orgType = $"{Resource.Company}";
                            break;
                        }

                    case Enums.RegisterType.Private:
                        {
                            orgType = $"{Resource.PrivateWithTax}";
                            break;
                        }
                    case Enums.RegisterType.Partnership:
                        {
                            orgType = $"{Resource.Partnership}";
                            break;
                        }

                    case Enums.RegisterType.NonProfit:
                        {
                            orgType = $"{Resource.NonProfit}";
                            break;
                        }
                    default:
                        {
                            orgType = $"{Resource.Company}";
                            break;
                        }
                }
            }
            else
            {
                switch (registerType)
                {
               
                    case Enums.RegisterType.Private:
                        {
                            orgType = $"{Resource.PrivateNoTax}";
                            break;
                        }
                    case Enums.RegisterType.NonProfit:
                        {
                            orgType = $"{Resource.NonProfit}";
                            break;
                        }
                    default:
                        {
                           orgType = $"{Resource.PrivateNoTax}";
                            break;
                        }
                }
            }

            table.AddTextCell(org.Name, _gishaFontBold);
            table.AddTextCell(string.Empty, _gishaFont);
            table.AddTextCell(org.Address, _gishaFont);
            table.AddTextCell(string.Empty, _gishaFont);

            if (org.Fax != null)
            {
                table.AddTextCell($"{Resource.BusPhone} " + org.Phone1 + $", {Resource.BusFax} " + org.Fax, _orgFont, 2);
            }
            else
            {
                table.AddTextCell($"{Resource.BusPhone} " + org.Phone1, _orgFont);
            }
            table.AddTextCell(string.Empty, _orgFont);
            
            table.AddCell(org.Email);

            table.AddTextCell(string.Empty, _gishaFont);

            table.AddTextCell($"{orgType}. {org.UniqueID}", _orgFont, 2);

            table.AddTextCell(string.Empty, _gishaFont);
            table.AddTextCell(string.Empty, _gishaFont);


            return hebHeader;
        }

        private PdfPCell HeaderEngCell(Organization org, string docType)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_LTR
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            var engHeader = new PdfPCell(table)
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                NoWrap = false,
                BackgroundColor = BaseColor.WHITE
            };


            // Company details
            table.AddTextCell(org.NameEng, _gishaFontBold, 1, 1);
            table.AddTextCell(string.Empty, _gishaFont, 1, 1);
            table.AddTextCell(org.AddressEng, _gishaFont, 1, 1);
            table.AddTextCell(string.Empty, _gishaFont, 1, 1);

            if (org.Fax != null)
            {
                table.AddTextCell($"{Resource.BusPhoneEng}. " + org.Phone1 + $", {Resource.BusFaxEng}. " + org.Fax, _orgFont, 1, 1);
            }
            else
            {
                table.AddTextCell($"{Resource.BusPhoneEng}. " + org.Phone1, _orgFont, 1, 1);
            }
            table.AddTextCell(string.Empty, _orgFont, 1, 1);
            table.AddTextCell(org.Email, _orgFont, 1, 1);

            table.AddTextCell(string.Empty, _gishaFont, 1, 1);

            table.AddTextCell($"{Resource.BusNoEng}. {org.UniqueID}", _orgFont, 1, 1);

            table.AddTextCell(string.Empty, _gishaFont, 1, 1);
            table.AddTextCell(string.Empty, _gishaFont, 1, 1);
            //ORIG or COPY
            table.AddTextCell(docType, _gishaFontBold, 1, 3, Element.ALIGN_CENTER);

            return engHeader;
        }

        public static byte[] ConcatAndAddContent(List<byte[]> pdfByteContent)
        {

            using (var ms = new MemoryStream())
            {
                using (var doc = new iTextSharp.text.Document())
                {
                    using (var copy = new PdfSmartCopy(doc, ms))
                    {
                        doc.Open();

                        //Loop through each byte array
                        foreach (var p in pdfByteContent)
                        {

                            //Create a PdfReader bound to that byte array
                            using (var reader = new PdfReader(p))
                            {

                                //Add the entire document instead of page-by-page
                                copy.AddDocument(reader);
                            }
                        }
                    }
                    doc.Close();
                }

                //Return just before disposing
                return ms.ToArray();
            }
        }

        protected void CreateOrderHeader(iTextSharp.text.Document document, Entities.Store.Order entityDocument, string documentKind, DocumentProviderArgs args)
        {
            var docType = PdfTableExtentions.GetPdfType(args);

            var widths = new[] { 3f, 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 40,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);

            var entityDoc = entityDocument.Supplier.Organization.Documents.FirstOrDefault();

            var logo = DocumentProviderExtension.GetLogo(entityDoc);

            // Images
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;

            if (args.IsDigitalySigned)
            {
                table.AddLogoImage(logo);
                table.AddSignLogoImage(_digitalySignedLogo);
            }
            else
            {
                table.AddLogoImage(logo);
                table.AddTextCell(string.Empty, _orgFont);
            }

            //// Company details
            var ent = entityDocument;

            table.AddCell(HeaderCell(ent.Supplier.Organization));

            if (ent.Supplier.Organization.NameEng != null)
            {
                table.AddCell(HeaderEngCell(ent.Supplier.Organization, docType));
            }
            else
            {
                table.AddTextCell(docType, _gishaFontBold, 1, 3, Element.ALIGN_CENTER);
            }


            document.Add(table);

            widths = new[] { 3f };
            table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 40,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.SetWidths(widths);


            table.AddTextCell(documentKind, _gishaFontBold, widths.Length, 3, Element.ALIGN_CENTER);

            table.AddTextCell(ent.Date.ToString("dd/MM/yyyy"), _orgFont, 2, 3, Element.ALIGN_RIGHT);
            table.AddTextCell($"{Resource.To}:", _gishaFont);
            table.AddTextCell(ent.Supplier.Name, _gishaFontBold, 2);
            table.AddTextCell($"{Resource.BusNo}. " + ent.SupplierId, _orgFont, 2);
            table.AddTextCell(ent.Supplier.Email, _orgFont, 2);
            table.AddTextCell(ent.Supplier.Address, _orgFont, 2);
            table.AddTextCell($"{Resource.BusPhone}' " + ent.Supplier.Phone, _orgFont);
            table.AddTextCell(string.Empty, _gishaFont);
            document.Add(table);
        }

        protected void CreateOrderHeaderEng(iTextSharp.text.Document document, Entities.Store.Order entityDocument, string documentKind, DocumentProviderArgs args)
        {
            var docType = PdfTableExtentions.GetPdfType(args);

            var widths = new[] { 3f, 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 40,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);

            var org = entityDocument.Supplier.Organization;

            var logo = DocumentProviderExtension.GetLogo(org.PathToLogo);

            // Images
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;

            if (args.IsDigitalySigned)
            {
                table.AddLogoImage(logo);
                table.AddSignLogoImage(_digitalySignedLogo);
            }
            else
            {
                table.AddLogoImage(logo);
                table.AddTextCell(string.Empty, _orgFont);
            }

            //// Company details
            var ent = entityDocument;

            table.AddCell(HeaderCell(ent.Supplier.Organization));

            if (ent.Supplier.Organization.NameEng != null)
            {
                table.AddCell(HeaderEngCell(ent.Supplier.Organization, docType));
            }
            else
            {
                table.AddTextCell(docType, _gishaFontBold, 1, 3, Element.ALIGN_CENTER);
            }


            document.Add(table);

            widths = new[] { 3f };
            table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 40,
                RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                HorizontalAlignment = PdfWriter.RUN_DIRECTION_LTR
            };
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.SetWidths(widths);


            table.AddTextCell(documentKind, _gishaFontBold, widths.Length, 3, Element.ALIGN_CENTER);

            table.AddTextCell(ent.Date.ToString("dd/MM/yyyy"), _orgFont, 2, 1, Element.ALIGN_RIGHT);
            table.AddTextCell($"To:", _gishaFont, 2, 1);
            table.AddTextCell(ent.Supplier.Name, _gishaFontBold, 2, 1);
            table.AddTextCell($"Bus.No. :" + ent.Supplier.ID, _orgFont, 2, 1);
            table.AddTextCell(ent.Supplier.Email, _orgFont, 2, 1);
            table.AddTextCell(ent.Supplier.Address, _orgFont, 2, 1);
            table.AddTextCell($"Phone: " + ent.Supplier.Phone, _orgFont, 2, 1);
            table.AddTextCell(string.Empty, _gishaFont);

            //נא לאשר את ההזמנה
            document.Add(table);
        }

        protected void CreateOrderItemTable(iTextSharp.text.Document document, Entities.Store.Order entityDocument)
        {
            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };

            var widths = new[] { 2f, 2f, 4f, 1f };
            var itemCatalogs = entityDocument.OrderDetails;
            var lineNum = 1;
            var orderHeaderResources = PdfTableExtentions.OrderHeaderResource;
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 0.5f,
                SpacingAfter = 50,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };

            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;
            table.DefaultCell.BackgroundColor = BaseColor.GRAY;

            foreach (var headerItem in orderHeaderResources.Values)
            {
                table.AddHeaderTextCell(headerItem, _gishaFont);
            }
            foreach (var item in itemCatalogs)
            {

                table.AddBoxTextCell(lineNum++, _arialFont);
                table.AddBoxTextCell(item.CatalogItem.Name, _arialFont);
                table.AddBoxTextCell(item.CatalogItem.Code, _arialFont);
                table.AddBoxTextCell(item.Quantity, _arialFont);
            }
            document.Add(table);
        }

        protected void OrderConformationString(iTextSharp.text.Document document)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 40,
                RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                HorizontalAlignment = PdfWriter.RUN_DIRECTION_LTR
            };
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.SetWidths(widths);

            table.AddTextCell(@"נא לאשר את ההזמנה", _gishaFontBold, widths.Length);
            
            document.Add(table);
        }

        protected void EnglishOrderConformationString(iTextSharp.text.Document document)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 40,
                RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                HorizontalAlignment = PdfWriter.RUN_DIRECTION_LTR
            };
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.SetWidths(widths);

            table.AddTextCell(@"Please, send us order confirmation", _gishaFontBold, widths.Length, 3, Element.ALIGN_RIGHT);

            document.Add(table);
        }

        protected void CreateEnglishOrderItemTable(iTextSharp.text.Document document, Entities.Store.Order entityDocument)
        {
            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };

            var widths = new[] { 1f, 4f, 2f, 2f };
            var itemCatalogs = entityDocument.OrderDetails;
            var lineNum = 1;
            var orderHeaderResources = PdfTableExtentions.OrderHeaderResourceEng;
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 0.5f,
                SpacingAfter = 50,
                RunDirection = PdfWriter.RUN_DIRECTION_LTR
            };

            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;
            table.DefaultCell.BackgroundColor = BaseColor.GRAY;

            foreach (var headerItem in orderHeaderResources.Values)
            {
                table.AddHeaderTextCell(headerItem, _gishaFont, 1, 2, Element.ALIGN_CENTER);
            }
            foreach (var item in itemCatalogs)
            {
                table.AddBoxTextCell(lineNum++, _arialFont, 1, 1, 1);
                table.AddBoxTextCell(item.CatalogItem.Name, _arialFont, 1, 1, 1);
                table.AddBoxTextCell(item.CatalogItem.Code, _arialFont, 1, 1, 1);
                table.AddBoxTextCell(item.Quantity, _arialFont,1,1,1);
            }
            document.Add(table);
        }

        protected void CreateStockHeader(iTextSharp.text.Document document, StoreStockInfo entityDocument, string documentKind, DocumentProviderArgs args)
        {
            var docType = PdfTableExtentions.GetPdfType(args);

            var widths = new[] { 3f, 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 20,
                SpacingAfter = 40,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);


            var entityDoc = entityDocument.Organization.Documents.FirstOrDefault();

            var logo = DocumentProviderExtension.GetLogo(entityDoc);

            // Images
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;

            if (args.IsDigitalySigned)
            {
                table.AddLogoImage(logo);
                table.AddSignLogoImage(_digitalySignedLogo);
            }
            else
            {
                table.AddLogoImage(logo);
                table.AddTextCell(string.Empty, _orgFont);
            }

            //// Company details
            var ent = entityDocument;

            table.AddCell(HeaderCell(ent.Organization));

            if (ent.Organization.NameEng != null)
            {
                table.AddCell(HeaderEngCell(ent.Organization, docType));
            }
            else
            {
                table.AddTextCell(docType, _gishaFontBold, 1, 3, Element.ALIGN_CENTER);
            }

            document.Add(table);

            widths = new[] { 3f };
            table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.SetWidths(widths);

            documentKind += DateTime.Today.ToString("dd/MM/yyyy");
            table.AddTextCell(documentKind, _gishaFontBold, widths.Length, 3, Element.ALIGN_CENTER);

            document.Add(table);

        }

        protected void CreateStockItemTable(iTextSharp.text.Document document, StoreStockInfo entityDocument)
        {
            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };

            var widths = new[] { 2f, 2f, 2f, 2f, 4f, 1f };
            var lineNum = 1;
            var stockHeaderResources = PdfTableExtentions.StockItemsResource;

            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 0.5f,
                SpacingAfter = 0.4f,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };

            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;
            table.DefaultCell.BackgroundColor = BaseColor.GRAY;

            foreach (var headerItem in stockHeaderResources.Values)
            {
                table.AddHeaderTextCell(headerItem, _gishaFont);
            }

            var catalogItemsGroups = entityDocument.Invoices
                .OrderByDescending(c => c.Date)
                .SelectMany(i => i.Items)
                .GroupBy(c => c.CatalogItemId);
            double? totalSum = 0;

            foreach (var catalogItemsGroup in catalogItemsGroups)
            {
                var item = catalogItemsGroup.First();
                var quantity = catalogItemsGroup.Sum(x => x.Quantity);

                var usedQuantity = item.UsedQuantity ?? 0;
                if (item.UsedQuantity == null)
                {
                    usedQuantity =
                        item.StoreInvoice.Organization.Documents
                            .Where(
                                d =>
                                    d.DocumentType == DocumentType.Waybill ||
                                    d.DocumentType == DocumentType.TaxInvoice ||
                                    d.DocumentType == DocumentType.TaxInvoiceReceipt)
                                    .Where(d => d.IsOriginalGenerated())
                            .Sum(
                                d => d.DocumentItems.Where(i => i.Code == item.CatalogItem.Code).Sum(cc => cc.Quantity));
                }

                var totalItemSum = item.CatalogItem.Price * (quantity - usedQuantity);
                table.AddBoxTextCell(lineNum++, _arialFont);
                table.AddBoxTextCell(item.CatalogItem.Name, _arialFont);
                table.AddBoxTextCell(item.CatalogItem.Code, _arialFont);
                table.AddBoxTextCell(quantity - usedQuantity, _arialFont);
                table.AddBoxTextCell(item.CatalogItem.Price, _arialFont);
                table.AddBoxTextCell(totalItemSum, _arialFont);
                totalSum += totalItemSum;
            }
            document.Add(table);
            CreateStockItemSummaryTable(document, totalSum);
        }

        protected void CreateStockItemSummaryTable(iTextSharp.text.Document document, double? totalSum)
        {
            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };

            var widths = new[] { 2f, 4f, 7f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 0.3f,
                SpacingAfter = 40,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;

            table.AddTextCell(string.Empty, _arialFontBold);
            table.AddBoxTextCell($"{Resource.TotalPrice}", _arialFontBold);
            table.AddBoxTextCell(totalSum?.ToString("N2", num), _arialFontBold);

            document.Add(table);
        }


    }
}
