﻿using System;
using System.Globalization;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Interfaces;
using Gizbarit.DAL.Providers.Store;

namespace Gizbarit.DAL.Providers.Document
{

    public class ProviderFactoryArgs
    {
        public CultureInfo CultureInfo { get; set; } = new CultureInfo("he");
    }

    public interface IDocumentProviderFactory
    {
  //      IDocumentProvider<T> Create<T>(DocumentType documentType, bool isVatCharge) where T : class;

        IDocumentProvider<T> Create<T>(T context, ProviderFactoryArgs args = null) where T : class;
    }

    /// <summary>
    /// Pdf Provider factory
    /// </summary>
    public class PdfProviderFactory : IDocumentProviderFactory
    {
        public IDocumentProvider<T> Create<T>(T context, ProviderFactoryArgs args = null) where T : class
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var document = context as Entities.Documents.Document;
            args = args ?? new ProviderFactoryArgs();

            if (document != null)
            {
                return Create(document) as IDocumentProvider<T>;
            }

            var storeStockInfo = context as StoreStockInfo;
            if (storeStockInfo != null)
            {
                return Create(storeStockInfo) as IDocumentProvider<T>;
            }

            var order = context as Order;
            if (order != null)
            {
                return Create(order, args) as IDocumentProvider<T>;
            }


            throw new NotSupportedException($"{typeof(T)} not supported.");
        }

        private static IDocumentProvider<Order> Create(Order order, ProviderFactoryArgs args)
        {
            return args.CultureInfo.TwoLetterISOLanguageName == "he" ? 
                    new StoreOrderPdfProvider() : 
                    new StoreOrderPdfProviderEn() as IDocumentProvider<Order>;
        }

        private static IDocumentProvider<Order> CreateEng(Order order)
        {
            return new StoreOrderPdfProviderEn();
        }

        private static IDocumentProvider<StoreStockInfo> Create(StoreStockInfo storeStockInfo)
        {
            return new StoreStockPdfProvider();
        }

        public IDocumentProvider<T> Create<T>() where T : class, IEntity
        {
            throw new NotImplementedException();
        }

        private static IDocumentProvider<Entities.Documents.Document> Create(Entities.Documents.Document context)
        {
            var documentType = context.DocumentType;
            var isVatCharge = context.Organization.IsVatCharge;

            switch (documentType)
            {
                case DocumentType.PriceOffer:
                    return new PdfPriceOfferProvider();

                case DocumentType.Waybill:
                    return new PdfWaybillProvider();

                case DocumentType.PreliminaryInvoice:
                    return new PdfPreliminaryInvoiceProvider();

                case DocumentType.TaxInvoice:
                    return new PdfInvoiceProvider();

                case DocumentType.Receipt:
                    return isVatCharge
                        ? new PdfRecieptProvider() as IDocumentProvider<Entities.Documents.Document>
                        : new PdfInvoiceRecieptProvider();

                case DocumentType.TaxInvoiceReceipt:
                    return new PdfInvoiceRecieptProvider();

                default:
                    throw new NotSupportedException(documentType.ToString());

            }

        }
    }
}
