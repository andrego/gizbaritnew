﻿using System;
using System.Collections.Generic;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Entities.Payment;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Resource = Gizbarit.DAL.Resources.Pdf.PdfBaseResource;


namespace Gizbarit.DAL.Providers.Document
{
    public static class PdfTableExtentions
    {
        public static void AddTextCell(this PdfPTable table, object data, Font font, int colSpan = 1,
            int direction = PdfWriter.RUN_DIRECTION_RTL, int align = Element.ALIGN_LEFT)
        {
            var text = data?.ToString() ?? string.Empty;
            var phrase = new Phrase(text, font);
            var cell = new PdfPCell(phrase)
            {
                HorizontalAlignment = align,
                RunDirection = direction,
                Border = 0,
                NoWrap = false,
                BackgroundColor = BaseColor.WHITE
            };

            table.AddCell(cell);
        }

        public static void AddBoxTextCell(this PdfPTable table, object data, Font font, int colSpan = 1,
            int direction = PdfWriter.RUN_DIRECTION_RTL, int align = Element.ALIGN_LEFT, BaseColor backGround = null)
        {
            backGround = backGround ?? BaseColor.WHITE;

            var text = data?.ToString() ?? string.Empty;
            var phrase = new Phrase(text, font);
            var cell = new PdfPCell(phrase)
            {
                HorizontalAlignment = align,
                RunDirection = direction,
                Border = 15,
                NoWrap = false,
                BackgroundColor = backGround
            };

            table.AddCell(cell);
        }

        public static void AddHeaderTextCell(this PdfPTable table, object data, Font font, int colSpan = 1,
            int direction = PdfWriter.RUN_DIRECTION_RTL, int align = Element.ALIGN_LEFT)
        {
            var text = data?.ToString() ?? string.Empty;
            var phrase = new Phrase(text, font);
            var cell = new PdfPCell(phrase)
            {
                HorizontalAlignment = align,
                RunDirection = direction,
                Border = 15,
                NoWrap = false,
                BackgroundColor = BaseColor.LIGHT_GRAY
            };

            table.AddCell(cell);
        }

        public static void AddLogoImage(this PdfPTable pdfTable, ContentBase image)
        {

            var widths = new[] {60f, 40f};
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 95,
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;

            var externalTableCell = new PdfPCell()
            {
                Colspan = 1,
                Border = 0,
                NoWrap = false,
                HorizontalAlignment = Element.ALIGN_LEFT
            };
            table.DefaultCell.PaddingLeft = 40f;
            table.AddCell(string.Empty);

            if (image.Data != null)
            {

                var logo = Image.GetInstance(image.Data);
                logo.ScalePercent(100);

                var imageCell = new PdfPCell(logo, true)
                {
                    Colspan = 1,
                    Border = 0,
                    NoWrap = false,
                    HorizontalAlignment = Element.ALIGN_LEFT
                };
                table.AddCell(imageCell);
            }
            else
            {
                table.AddCell(string.Empty);
            }

            externalTableCell.AddElement(table);

            pdfTable.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTable.AddCell(externalTableCell);

        }

        public static void AddCatalogImage(this PdfPTable pdfTable, ContentBase image, string nameItemText, Font font)
        {
            if (image.Data != null)
            {
                var widths = new[] {20f, 80f};
                var innerTable = new PdfPTable(widths)
                {
                    WidthPercentage = 100,
                };

                var externalTableCell = new PdfPCell()
                {
                    Colspan = 1,
                    Border = Rectangle.BOTTOM_BORDER,
                    NoWrap = false,
                    HorizontalAlignment = Element.ALIGN_LEFT
                };

                var catalogItemImage = Image.GetInstance(image.Data);

                var imageCell = new PdfPCell(catalogItemImage, true)
                {
                    Colspan = 1,
                    Border = Rectangle.NO_BORDER,
                    NoWrap = false,
                    HorizontalAlignment = Element.ALIGN_LEFT
                };
                catalogItemImage?.ScalePercent(100);


                innerTable.DefaultCell.Border = Rectangle.NO_BORDER;
                innerTable.DefaultCell.NoWrap = false;
                innerTable.SetWidths(widths);
                innerTable.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                innerTable.DefaultCell.PaddingLeft = 40f;
                innerTable.AddCell(imageCell);
                innerTable.AddTextCell(nameItemText, font);

                externalTableCell.AddElement(innerTable);

                pdfTable.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                pdfTable.AddCell(externalTableCell);
            }
        }


        public static void AddSignLogoImage(this PdfPTable pdfTable, ContentBase image)
        {

            var widths = new[] { 60f, 40f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 98,
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);

            var externalTableCell = new PdfPCell()
            {
                Colspan = 1,
                Border = 0,
                NoWrap = false,
            };
            table.DefaultCell.PaddingLeft = 40f;

            if (image.Data != null)
            {

                var logo = Image.GetInstance(image.Data);
                logo.ScalePercent(100);

                var imageCell = new PdfPCell(logo, true)
                {
                    Colspan = 1,
                    Border = 0,
                    NoWrap = false,
                };
                table.AddCell(imageCell);
            }
            else
            {
                table.AddCell(string.Empty);
            }
            table.AddCell(string.Empty);

            externalTableCell.AddElement(table);

            pdfTable.AddCell(externalTableCell);

        }


        public static void AddParagraph(this iTextSharp.text.Document doc, object data, Font font)
        {
            throw new NotImplementedException();
        }

        public static void AddOrganization(this iTextSharp.text.Document doc, Organization org, Font font)
        {
            throw new NotImplementedException();
        }

        public static void AddClient(this iTextSharp.text.Document doc, Client org, Font font)
        {
            throw new NotImplementedException();
        }

        public static void AddItems(this iTextSharp.text.Document doc, IEnumerable<DocumentItem> org, Font font)
        {
            throw new NotImplementedException();
        }

        public static void AddItems(this iTextSharp.text.Document doc, IEnumerable<Payment> org, Font font)
        {
            throw new NotImplementedException();
        }

        public static readonly Dictionary<byte, string> PaymentTypeResource = new Dictionary<byte, string>
        {
            {1, $"{Resource.Check}"},
            {2, $"{Resource.BankTransfer}"},
            {3, $"{Resource.Cash}"},
            {4, $"{Resource.CreditCard}"},
            {5, $"{Resource.WithholdingTax}"}
        };

        public static readonly Dictionary<byte, string> PaymentHeaderResource = new Dictionary<byte, string>
        {
            {1, $"{Resource.ItemNum}'"},
            {2, $"{Resource.ItemCode}"},
            {3, $"{Resource.ItemDescription}"},
            {4, $"{Resource.ItemPrice}"},
            {5, $"{Resource.Quantity}"},
            {6, $"{Resource.Discount}"},
            {7, $"{Resource.TotalPrice}"}
        };

        public static readonly Dictionary<byte, string> PaymentItemsResource = new Dictionary<byte, string>
        {
            {1, $"{Resource.PaymentType}"},
            {2, $"{Resource.AccountNo}"},
            {3, $"{Resource.Branch}"},
            {4, $"{Resource.Bank}"},
            {5, $"{Resource.PaymentNo}"},
            {6, $"{Resource.PaymentSum}"},
            {7, $"{Resource.Date}"}
        };

        public static readonly Dictionary<byte, string> OrderHeaderResource = new Dictionary<byte, string>
        {
            {1, $"{Resource.CatalogItemNum}'"},
            {2, $"{Resource.CatalogItemDescribtion}"},
            {3, $"{Resource.CatalogItemCode}"},
            {4, $"{Resource.CatalogItemNeededQuantity}"}
        };

        public static readonly Dictionary<byte, string> OrderHeaderResourceEng = new Dictionary<byte, string>
        {
            {1, $"{Resource.CatalogItemNumEng}"},
            {2, $"{Resource.CatalogItemCodeEng}"},
            {3, $"{Resource.CatalogItemDescribtionEng}"},
            {4, $"{Resource.CatalogItemNeededQuantityEng}"}
        };

        public static readonly Dictionary<byte, string> StockItemsResource = new Dictionary<byte, string>
        {
            {1, $"{Resource.CatalogItemNum}"},
            {2, $"{Resource.CatalogItemDescribtion}"},
            {3, $"{Resource.CatalogItemCode}"},
            {4, $"{Resource.CatalogItemQuantity}"},
            {5, $"{Resource.CatalogItemPriceNetto} ב-₪"},
            {6, $"{Resource.CatalogItemPrice} ב-₪"}
        };

        public static string GetPdfType(DocumentProviderArgs args)
        {
            var docType = string.Empty;

            switch (args.StampType)
            {
                case
                    DocumentStampType.Original:
                {
                    docType = $"{Resource.OriginalDoc}";
                    break;
                }
                case
                    DocumentStampType.Copy:
                {
                    docType = $"{Resource.CopyDoc}";
                    break;
                }
                case DocumentStampType.OriginalCopy:
                    docType = $"{Resource.OriginalCopyDoc}";
                    break;
            }
            return docType;
        }
    }
}