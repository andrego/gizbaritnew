﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gizbarit.DAL.Entities.Content;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Resource = Gizbarit.DAL.Resources.Pdf.PdfWaybillResource;


namespace Gizbarit.DAL.Providers.Document
{
    public class PdfWaybillProvider :  PdfProviderBase, IDocumentProvider<Entities.Documents.Document>

    {
        public ContentBase Create(Entities.Documents.Document entity, DocumentProviderArgs args)
        {
            var originalArgs = new DocumentProviderArgs()
            {
                StampType = DocumentStampType.Original
            };
            ContentBase origPdf, copyPdf;
            List<byte[]> pdfContentList;
            var copyArgs = new DocumentProviderArgs()
            {
                StampType = DocumentStampType.Copy
            };
            //var docStatus = entity.Waybill.Document.Status;

            if (args.StampType == DocumentStampType.Original)
            {
                origPdf = CreateInternal(entity, originalArgs);
                copyPdf = CreateInternal(entity, copyArgs);

                pdfContentList = new List<byte[]>
                {
                    origPdf.Data,
                    copyPdf.Data
                };
            }
            else
            {
                copyPdf = CreateInternal(entity, copyArgs);

                pdfContentList = new List<byte[]>
                {
                    copyPdf.Data
                };
            }


            var outputStream = ConcatAndAddContent(pdfContentList);
            var content = new PdfContent()
            {
                Name = Guid.NewGuid().ToString(),
                Data = outputStream.ToArray()
            };

            return content;
        }

        private ContentBase CreateInternal(Entities.Documents.Document entity, DocumentProviderArgs args)
        {
            var pdfDocument = new iTextSharp.text.Document(PageSize.A4);
            var outputStream = new MemoryStream();
            var stream = PdfWriter.GetInstance(pdfDocument, outputStream);
            var inv = entity.Waybill;

            pdfDocument.Open();
            pdfDocument.NewPage();

            CreateHeader(pdfDocument, entity, $"{Resource.Title}' " + inv.Document.Number, args);
            CreateDescription(pdfDocument, entity);
            CreateItemTable(pdfDocument, entity);
            CreateSummaryTable(pdfDocument, entity);
            AddInternalCommentRow(pdfDocument, entity);
            CreateSignatureRow(pdfDocument, entity);
            CreateWayBillSign(pdfDocument, entity);
            AddingFooter(stream);

            pdfDocument.Close();

            // TODO: Check it
            var content = new PdfContent()
            {
                Name = Guid.NewGuid().ToString(),
                Data = outputStream.ToArray()
            };

            content.Data = AddPageLayer(content.Data, !args.IsCombinedReport);

            return content;
        }

        
    }
}
