﻿using System;
using System.IO;
using System.Runtime.Caching;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Providers.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Org.BouncyCastle.Crypto.Generators;

namespace Gizbarit.DAL.Providers.Document
{
    public static class DocumentProviderExtension
    {
        private static MemoryCache Cache = MemoryCache.Default;
        private static readonly IStorage Storage = StorageFactory.Create();

        private const string LogoFolder = "logos";
        private const string FontFolder = "fonts";
        private const string CatalogFolder = "catalog";

        public static ImageContent GetLogo(Entities.Documents.Document doc)
        {
            return GetLogo(doc?.Organization?.PathToLogo);
        }

        public static ImageContent GetLogo(string fileName)
        {

            var cacheKey = $"Logo-{fileName}";

            if (Cache.Contains(cacheKey))
            {
                return Cache[cacheKey] as ImageContent;
            }


            var logo = new ImageContent()
            {
                Name = fileName
            };

            if (string.IsNullOrWhiteSpace(fileName))
            {
                return logo;
            }

            var logoPath = Path.Combine(LogoFolder, fileName);

            if (Storage.Exists(logoPath))
            {
                logo.Data = Storage.ReadAllBytes(logoPath);
            }

            Cache[cacheKey] = logo;

            return logo;
        }

        public static ImageContent GetCatalogImage(string fileName)
        {
            var catalogImage = new ImageContent()
            {
                Name = fileName
            };

            if (string.IsNullOrWhiteSpace(fileName))
            {
                return catalogImage;
            }

            var catalogImagePath = Path.Combine(CatalogFolder, fileName);

            if (Storage.Exists(catalogImagePath))
            {
                catalogImage.Data = Storage.ReadAllBytes(catalogImagePath);
            }

            return catalogImage;
        }

        public static Font GetFont(string fontName, float size = 10, int fontWeight = Font.NORMAL, BaseColor color = null)
        {
            var cacheKey = $"Font-{fontName}";

            if (Cache.Contains(cacheKey))
            {
                return Cache[cacheKey] as Font;
            }

            color = color ?? BaseColor.BLACK;

            var fontPath = Path.Combine(FontFolder, fontName);
            var bytes = Storage.ReadAllBytes(fontPath);
            var fontBase = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, true, bytes, null);
            var font = new Font(fontBase, size, fontWeight, color);

            Cache[cacheKey] = font;

            return font;
        }
    }
}
