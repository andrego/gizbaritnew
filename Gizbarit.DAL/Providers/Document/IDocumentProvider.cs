﻿using Gizbarit.DAL.Entities.Content;

namespace Gizbarit.DAL.Providers.Document
{


    public interface IDocumentProvider<T> where T: class
    {
        ContentBase Create(T context, DocumentProviderArgs args);
    }

    public class DocumentProviderArgs
    {
        public DocumentStampType StampType { get; set; } = DocumentStampType.None;
        public ContentBase DigitalSignLogo { get; set; }

        public bool IsDigitalySigned { get; set; }
        public bool IsCombinedReport { get; set; } = false;
    }

    public enum DocumentStampType
    {
        None,
        Original,
        OriginalCopy,
        Copy
    }
}