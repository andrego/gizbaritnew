﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Gizbarit.DAL.Entities.Content;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

namespace Gizbarit.DAL.Providers.Sign
{
    public class PdfSignProvider : ISignProvider
    {
        public ContentBase Sign(ContentBase document, CertificateContent certificate)
        {
            var stream = new MemoryStream();
            var reader = new PdfReader(document.Data);
            var stamper = PdfStamper.CreateSignature(reader, stream, '\0');
            var stamp = stamper.SignatureAppearance;
            var cert = new X509Certificate2(certificate.Data, certificate.Password, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet);
            var certParser = new X509CertificateParser();
            var pk = DotNetUtilities.GetKeyPair(cert.PrivateKey).Private;
            var result = new PdfContent
            {
                DocumentId = document.DocumentId,
                Description = document.Description,
                Name = document.Name,
                OrganizationId = document.OrganizationId
            };

            IExternalSignature es = new PrivateKeySignature(pk, "SHA256");
            var chain = new[]
            {
                certParser.ReadCertificate(cert.RawData)
            };

            //TODO: Make full details
            stamp.Reason = "חתימה אלקטרונית  --  מערכת ניהול עסקים גיזברית";
            stamp.Location = "gizbarit.biz, gizbarit.co.il";
            stamp.SignatureCreator = "gizbarit.biz, gizbarit.co.il";

            stamp.CertificationLevel = 3;
            stamp.SignDate = DateTime.Now;
            

            // Sign the PDF file and finish up.
            MakeSignature.SignDetached(stamp, es, chain, null, null, null, 0, CryptoStandard.CMS);
            stamper.Close();


            result.Data = stream.ToArray();

            return result;
        }
    }
}
