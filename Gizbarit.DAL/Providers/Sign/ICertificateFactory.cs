﻿using Gizbarit.DAL.Entities.Content;

namespace Gizbarit.DAL.Providers.Sign
{
    public interface ICertificateFactory
    {
        CertificateContent Create(Entities.Organization.Organization organization, CertificateContent caCertificate);
    }
}