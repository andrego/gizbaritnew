﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Organization;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

namespace Gizbarit.DAL.Providers.Sign
{
    public class BouncyCertificateFactory : ICertificateFactory
    {
        public CertificateContent Create(Organization organization, CertificateContent caCertificate)
        {
            //const string password = "odessa1958";
            var issuerCertificate = new X509Certificate2();
            issuerCertificate.Import(caCertificate.Data, caCertificate.Password, X509KeyStorageFlags.PersistKeySet);
            var issuerCert = DotNetUtilities.FromX509Certificate(issuerCertificate);

            var kpgen = new RsaKeyPairGenerator();
            kpgen.Init(new KeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), 2048));
            var kp = kpgen.GenerateKeyPair();

            var gen = new X509V3CertificateGenerator();
            var subjectName = new X509Name($"CN= {organization.Name} , " +
                                           $"O= {organization.UniqueID} {organization.FirstName} {organization.LastName} , " +
                                           $"E= {organization.Email} , C= IL");
            var issuerName = issuerCert?.SubjectDN;
            var serialNo = BigInteger.ProbablePrime(120, new Random());

            gen.SetSignatureAlgorithm(issuerCert?.SigAlgName.Replace("-", string.Empty));
            gen.SetSerialNumber(serialNo);
            gen.SetSubjectDN(subjectName);
            gen.SetSubjectUniqueID(new bool[] { });
            gen.SetIssuerDN(issuerName);
            gen.SetIssuerUniqueID(new bool[] { });
            gen.SetNotAfter(DateTime.Now.AddYears(1));
            gen.SetNotBefore(DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)));
            gen.SetPublicKey(kp.Public);

            //gen.CopyAndAddExtension(X509Extensions.AuthorityKeyIdentifier, false, issuerCert);
            gen.AddExtension(X509Extensions.AuthorityKeyIdentifier.Id,
                false,
                new AuthorityKeyIdentifier(
                    SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(kp.Public),
                    new GeneralNames(new GeneralName(issuerCert?.IssuerDN)),
                    serialNo));
            gen.CopyAndAddExtension(X509Extensions.AuthorityInfoAccess.Id, false, issuerCert);
            gen.CopyAndAddExtension(X509Extensions.BasicConstraints.Id, false, issuerCert);
            gen.CopyAndAddExtension(X509Extensions.KeyUsage.Id, false, issuerCert);
            gen.CopyAndAddExtension(X509Extensions.ExtendedKeyUsage.Id, false, issuerCert);
            gen.CopyAndAddExtension(X509Extensions.SubjectKeyIdentifier.Id, false, issuerCert);
            //gen.CopyAndAddExtension(X509Extensions.SubjectAlternativeName.Id, true, issuerCert);
            var newCert = gen.Generate(kp.Private);
            newCert.CheckValidity();
            newCert.Verify(kp.Public);

            // Convert BouncyCastle X509 Certificate to .NET's X509Certificate
            var cert = DotNetUtilities.ToX509Certificate(newCert);
            var certBytes = cert.Export(X509ContentType.Pkcs12, "password");

            // Convert X509Certificate to X509Certificate2
            var cert2 = new X509Certificate2(certBytes, "password");

            // Convert BouncyCastle Private Key to RSA
            var rsaPriv = DotNetUtilities.ToRSA(kp.Private as RsaPrivateCrtKeyParameters);

            // Setup RSACryptoServiceProvider with "KeyContainerName" set
            var csp = new CspParameters
            {
                KeyContainerName = "KeyContainer"
            };

            var rsaPrivate = new RSACryptoServiceProvider(csp);

            // Import private key from BouncyCastle's rsa
            rsaPrivate.ImportParameters(rsaPriv.ExportParameters(true));

            // Set private key on our X509Certificate2
            cert2.PrivateKey = rsaPrivate;

            var exportCertContent = new CertificateContent
            {
                Data = cert2.Export(X509ContentType.Pkcs12, "password"),
                Password = "password",
                Expired = DateTime.Now.AddYears(1)
            };
            return exportCertContent;
        }
    }
}
