﻿using Gizbarit.DAL.Entities.Content;

namespace Gizbarit.DAL.Providers.Sign
{
    public interface ISignProvider
    {
         ContentBase Sign(ContentBase document, CertificateContent certificate);
    }
}
