﻿using Gizbarit.DAL.Entities.Content;

namespace Gizbarit.DAL.Providers.Report
{
    class NullReportProvider : IReportProvider
    {
        public ContentBase Create(ReportProviderArgs args)
        {
            return new PdfContent()
            {
                Name = "EmptyReport.pdf",
                Data = new byte[1000]
            };
        }
    }
}
