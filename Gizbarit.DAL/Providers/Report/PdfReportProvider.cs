﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Gizbarit.DAL.Common;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Resources.Mail;
using Gizbarit.DAL.Resources.Pdf;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Resource = Gizbarit.DAL.Resources.Pdf.PdfBaseResource;

namespace Gizbarit.DAL.Providers.Report
{
    class PdfReportProvider : IReportProvider
    {
        protected readonly Font _arialFont;
        protected readonly Font _gishaFont;
        protected readonly Font _arialFontBold;
        protected readonly Font _orgFont;
        protected readonly Font _gishaFontBold;

        protected internal PdfReportProvider()
        {
            _arialFont = DocumentProviderExtension.GetFont("ARIALUNI.TTF");
            _arialFontBold = DocumentProviderExtension.GetFont("GISHABD.TTF");
            _orgFont = DocumentProviderExtension.GetFont("ARIALUNI.TTF", 12);
            _gishaFont = DocumentProviderExtension.GetFont("GISHABD.TTF");
            _gishaFontBold = DocumentProviderExtension.GetFont("GISHABD.TTF", 15);
        }

        public ContentBase Create(ReportProviderArgs args)
        {

            var pdfDocument = new iTextSharp.text.Document(PageSize.A4);
            var outputStream = new MemoryStream();
            var stream = PdfWriter.GetInstance(pdfDocument, outputStream);

            pdfDocument.Open();
            pdfDocument.NewPage();

            CreateHeader(pdfDocument,args);
            if (args.Organization.BusinessTypeId == 1)
            {
                if (args.Invoices.Count > 0 && args.Invoices.IsNotEmpty())
                {
                    AddTableDescription(pdfDocument, Resource.Invoices);
                    CreateInvoiceTable(pdfDocument, args);
                }

                if (args.Reciepts.Count > 0 && args.Reciepts.IsNotEmpty())
                {
                    AddTableDescription(pdfDocument, Resource.Reciepts);
                    CreateRecieptsTable(pdfDocument, args);
                }

                if (args.InvoiceReciepts.Count > 0 && args.InvoiceReciepts.IsNotEmpty())
                {
                    AddTableDescription(pdfDocument, Resource.InvoiceReciepts);
                    CreateInvoiceRecieptsTable(pdfDocument, args);
                }
            }

            if (args.Organization.BusinessTypeId == 2)
            {
                {
                    AddTableDescription(pdfDocument, Resource.Reciepts);
                    CreateFreeTaxRecieptsTable(pdfDocument, args);
                }
            }

           AddingFooter(stream);

            pdfDocument.Close();

            var content = new PdfContent()
            {
                Name = $"{args.Organization.Name} - {TemplateResource.DescriptionText} {args.FromDate:dd.MM.yyyy} - {args.ToDate:dd.MM.yyyy}.pdf", 
                Data = outputStream.ToArray()
            };

            content.Data = AddPageLayer(content.Data);
            return content;
        }


        private void CreateFreeTaxRecieptsTable(iTextSharp.text.Document pdfDocument, ReportProviderArgs args)
        {
            var dictionary = PdfTableExtentions.PaymentTypeResource;

            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };
            double sumAmount = 0, sumTotal = 0;

            var widths = new[] { 2f, 2f, 2f, 2f, 2f, 2f, 2f, 1f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10f,
                SpacingAfter = 20f,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;
            table.DefaultCell.BackgroundColor = BaseColor.GRAY;

            foreach (var value in FreeTaxRecieptHeaderResource.Values)
            {
                table.AddHeaderTextCell(value, _gishaFont, 1, 3, Element.ALIGN_CENTER);
            }
            foreach (var invoiceReciept in args.InvoiceReciepts)
            {
                foreach (var payment in invoiceReciept.Payments)
                {
                    var payType = payment.PaymentType != 0 ? dictionary[payment.PaymentType] : $"{Resource.ExceptionType}";

                    table.AddBoxTextCell(invoiceReciept.Number.ToString(), _arialFont, 1, 3, Element.ALIGN_CENTER);
                    table.AddBoxTextCell(invoiceReciept.ActualCreationDate.ToString("dd/MM/yyyy"), _arialFont, 1, 3, Element.ALIGN_CENTER);
                    table.AddBoxTextCell(invoiceReciept.Client.Name, _arialFont, 1, 3, Element.ALIGN_CENTER);
                    table.AddBoxTextCell(invoiceReciept.Subject, _arialFont, 1, 3, Element.ALIGN_CENTER);
                    table.AddBoxTextCell(invoiceReciept.Total.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER);
                    sumTotal += invoiceReciept.Total;
                    table.AddBoxTextCell(payType, _arialFont, 1, 3, Element.ALIGN_CENTER);
                    table.AddBoxTextCell(payment.Date.ToString("dd/MM/yyyy"), _arialFont, 1, 3, Element.ALIGN_CENTER);
                    table.AddBoxTextCell(payment.Amount.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER);
                    sumAmount += payment.Amount;
                }
            }

            table.AddBoxTextCell($"{Resource.TotalPrice}", _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(sumTotal.ToString("N2", num), _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(sumAmount.ToString("N2", num), _gishaFont);

            pdfDocument.Add(table);
        }


        private void CreateInvoiceRecieptsTable(iTextSharp.text.Document pdfDocument, ReportProviderArgs args)
        {
            var dictionary = PdfTableExtentions.PaymentTypeResource;

            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };
            double sumAmount = 0, sumTotalwithoutTax = 0, sumTotalTaxAmount = 0, sumTotal = 0;

            var widths = new[] { 2f, 2f, 2f, 2f, 2f, 2f, 2f, 2f, 2f, 1f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10f,
                SpacingAfter = 20f,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;
            table.DefaultCell.BackgroundColor = BaseColor.GRAY;

            foreach (var value in InvoiceRecieptHeaderResource.Values)
            {
                table.AddHeaderTextCell(value, _gishaFont, 1, 3, Element.ALIGN_CENTER);
            }

            var i = 0;
            foreach (var invoiceReciept in args.InvoiceReciepts)
            {
                var backGround = i % 2 == 0 ? BaseColor.WHITE : BaseColor.LIGHT_GRAY;

                foreach (var payment in invoiceReciept.Payments)
                {
                    var payType = payment.PaymentType != 0 ? dictionary[payment.PaymentType] : $"{Resource.ExceptionType}";

                    table.AddBoxTextCell(invoiceReciept.Number.ToString(), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(invoiceReciept.ActualCreationDate.ToString("dd/MM/yyyy"), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(invoiceReciept.Client.Name, _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(invoiceReciept.Subject, _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(invoiceReciept.TotalwithoutTax.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    sumTotalwithoutTax += invoiceReciept.TotalwithoutTax;
                    table.AddBoxTextCell(invoiceReciept.TotalTaxAmount.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    sumTotalTaxAmount += invoiceReciept.TotalTaxAmount;
                    table.AddBoxTextCell(invoiceReciept.Total.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    sumTotal += invoiceReciept.Total;
                    table.AddBoxTextCell(payType, _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(payment.Date.ToString("dd/MM/yyyy"), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(payment.Amount.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    sumAmount += payment.Amount;
                }
                i++;
            }

            table.AddBoxTextCell($"{Resource.TotalPrice}", _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(sumTotalwithoutTax.ToString("N2", num), _gishaFont);
            table.AddBoxTextCell(sumTotalTaxAmount.ToString("N2", num), _gishaFont);
            table.AddBoxTextCell(sumTotal.ToString("N2", num), _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(sumAmount.ToString("N2", num), _gishaFont);

            pdfDocument.Add(table);
        }


        private void CreateRecieptsTable(iTextSharp.text.Document pdfDocument, ReportProviderArgs args)
        {
            var dictionary = PdfTableExtentions.PaymentTypeResource;

            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };
            double sumAmount = 0;
            var widths = new[] { 2f, 2f, 2f, 2f, 2f, 2f, 2f, 1f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10f,
                SpacingAfter = 20f,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;
            table.DefaultCell.BackgroundColor = BaseColor.GRAY;

            foreach (var value in RecieptHeaderResource.Values)
            {
                table.AddHeaderTextCell(value, _gishaFont, 1, 3, Element.ALIGN_CENTER);
            }

            var i = 0;
            foreach (var reciept in args.Reciepts)
            {
                var invoice = reciept.Invoice?.Document;
                var preInvoice = reciept.PreliminaryInvoice?.Document;
                var backGround = i % 2 == 0 ? BaseColor.WHITE : BaseColor.LIGHT_GRAY;
                foreach (var payment in reciept.Payments)
                {
                    var payType = payment.PaymentType != 0 ? dictionary[payment.PaymentType] : $"{Resource.ExceptionType}";

                    table.AddBoxTextCell(reciept.Number.ToString(), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(invoice?.Number.ToString(), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(reciept.ActualCreationDate.ToString("dd/MM/yyyy"), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(reciept.Client.Name, _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(preInvoice?.Number, _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(payType, _arialFont, 1, 3,Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(payment.Date.ToString("dd/MM/yyyy"), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    table.AddBoxTextCell(payment.Amount.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER, backGround);
                    
                    sumAmount += payment.Amount;
                }

                i++;
            }

            table.AddBoxTextCell($"{Resource.TotalPrice}", _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(sumAmount.ToString("N2", num), _gishaFont);

            pdfDocument.Add(table);
        }


        private void CreateInvoiceTable(iTextSharp.text.Document pdfDocument, ReportProviderArgs args)
        {
            var num = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = ","
            };
            double sumTotalwithoutTax = 0, sumTotalTaxAmount = 0, sumTotal = 0;
            var widths = new[] { 2f, 2f, 2f, 3f, 3f, 2f, 1f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10f,
                SpacingAfter = 20f,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.DefaultCell.Border = 4;
            table.DefaultCell.BackgroundColor = BaseColor.GRAY;

            foreach (var value in InvoiceHeaderResource.Values)
            {
                table.AddHeaderTextCell(value, _gishaFont, 1, 3, Element.ALIGN_CENTER);
            }
            foreach (var invoice in args.Invoices)
            {
                table.AddBoxTextCell(invoice.Number.ToString(), _arialFont, 1, 3, Element.ALIGN_CENTER);
                table.AddBoxTextCell(invoice.ActualCreationDate.ToString("dd/MM/yyyy"), _arialFont, 1, 3, Element.ALIGN_CENTER);
                table.AddBoxTextCell(invoice.Client.Name, _arialFont, 1, 3, Element.ALIGN_CENTER);
                table.AddBoxTextCell(invoice.Subject, _arialFont, 1, 3, Element.ALIGN_CENTER);
                table.AddBoxTextCell(invoice.TotalwithoutTax.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER);
                sumTotalwithoutTax += invoice.TotalwithoutTax;
                table.AddBoxTextCell(invoice.TotalTaxAmount.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER);
                sumTotalTaxAmount += invoice.TotalTaxAmount;
                table.AddBoxTextCell(invoice.Total.ToString("N2", num), _arialFont, 1, 3, Element.ALIGN_CENTER);
                sumTotal += invoice.Total;
            }

            table.AddBoxTextCell($"{Resource.TotalPrice}", _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(string.Empty, _gishaFont);
            table.AddBoxTextCell(sumTotalwithoutTax.ToString("N2", num), _gishaFont);
            table.AddBoxTextCell(sumTotalTaxAmount.ToString("N2", num), _gishaFont);
            table.AddBoxTextCell(sumTotal.ToString("N2", num), _gishaFont);

            pdfDocument.Add(table);
        }


        private void CreateHeader(iTextSharp.text.Document pdfDocument, ReportProviderArgs args)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 20,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);

            table.AddTextCell($"{Resource.DocumentFor}: ", _gishaFont);
            table.AddTextCell(args.Organization.Name, _gishaFontBold);
            table.AddTextCell($"{Resource.Periods}: {args.FromDate:dd/MM/yyyy} - {args.ToDate:dd/MM/yyyy}", _gishaFont);

            pdfDocument.Add(table);
        }

        private void AddTableDescription(iTextSharp.text.Document pdfDocument, string tabName)
        {
            var widths = new[] { 3f };
            var table = new PdfPTable(widths)
            {
                WidthPercentage = 100,
                SpacingBefore = 10,
                SpacingAfter = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            table.DefaultCell.Border = 0;
            table.DefaultCell.NoWrap = false;
            table.SetWidths(widths);
            table.AddTextCell($"{tabName}: ", _gishaFontBold);
            pdfDocument.Add(table);
        }

        private byte[] AddPageLayer(byte[] bytes)
        {
            using (var stream = new MemoryStream())
            {
                var reader = new PdfReader(bytes);
                var document = new PdfDocument();

                using (var stamper = new PdfStamper(reader, stream))
                {
                    var pages = reader.NumberOfPages;
                    for (var i = 1; i <= pages; i++)
                    {
                        var stampContent = stamper.GetOverContent(i);
                        var rectangle = reader.GetPageSize(i);
                        rectangle.Left += document.LeftMargin / 2;
                        rectangle.Right -= document.RightMargin / 2;
                        rectangle.Top -= document.TopMargin / 2;
                        rectangle.Bottom += document.BottomMargin / 2 - 4f;

                        ColumnText.ShowTextAligned
                        (
                            stamper.GetUnderContent(i),
                            Element.ALIGN_RIGHT,
                            new Phrase(
                                $"{PdfBaseResource.CurrentPage} {i} {PdfBaseResource.TotalPages}- {pages} ",
                                _arialFont),
                            568f, 15f, 0,
                            PdfWriter.RUN_DIRECTION_RTL, 0
                        );

                        rectangle.Border = Rectangle.BOX;
                        stampContent.SetColorStroke(BaseColor.BLACK);
                        stampContent.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                        stampContent.Rectangle(rectangle);
                        stampContent.Stroke();
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }



        private void AddingFooter(PdfWriter stream)
        {
            var footer = new Paragraph($"{PdfBaseResource.FooterPdfString}", _arialFont)
            {
                Alignment = Element.ALIGN_CENTER
            };
            var footerTbl = new PdfPTable(1)
            {
                TotalWidth = 450,
                HorizontalAlignment = Element.ALIGN_RIGHT,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            footerTbl.DefaultCell.NoWrap = false;
            var cell = new PdfPCell(footer)
            {
                Border = 0,
                //PaddingLeft = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                HorizontalAlignment = Element.ALIGN_CENTER,
                NoWrap = false
            };
            footerTbl.AddCell(cell);
            footerTbl.WriteSelectedRows(0, -1, 70, 15, stream.DirectContent);
        }


        private static readonly Dictionary<byte, string> InvoiceHeaderResource = new Dictionary<byte, string>
        {
            {1, $"{Resource.ItemNum}'"},
            {2, $"{Resource.Date}"},
            {3, $"{Resource.ClientName}"},
            {4, $"{Resource.Description}"},
            {5, $"{Resource.WithoutTax}"},
            {6, $"{Resource.Tax}"},
            {7, $"{Resource.WithTax}"}
        };

        private static readonly Dictionary<byte, string> RecieptHeaderResource = new Dictionary<byte, string>
        {
            {1, $"{Resource.ItemNum}'"},
            {2, $"{Resource.ByTaxInvoice}"},
            {3, $"{Resource.Date}"},
            {4, $"{Resource.ClientName}"},
            {5, $"{Resource.ByPreInvoice}"},
            {6, $"{Resource.PaymentType}"},
            {7, $"{Resource.PaymentDate}"},
            {8, $"{Resource.PaymentSum}"}
        };

        private static readonly Dictionary<byte, string> InvoiceRecieptHeaderResource = new Dictionary<byte, string>
        {
            {1 , $"{Resource.ItemNum}'"},
            {2 , $"{Resource.Date}"},
            {3 , $"{Resource.ClientName}"},
            {4 , $"{Resource.Description}"},
            {5 , $"{Resource.WithoutTax}"},
            {6 , $"{Resource.Tax}"},
            {7 , $"{Resource.WithTax}"},
            {8 , $"{Resource.PaymentType}"},
            {9 , $"{Resource.PaymentDate}"},
            {10, $"{Resource.PaymentSum}"}
        };

        private static readonly Dictionary<byte, string> FreeTaxRecieptHeaderResource = new Dictionary<byte, string>
        {
            {1 , $"{Resource.ItemNum}'"},
            {2 , $"{Resource.Date}"},
            {3 , $"{Resource.ClientName}"},
            {4 , $"{Resource.Description}"},
            {5 , $"{Resource.WithTax}"},
            {6 , $"{Resource.PaymentType}"},
            {7 , $"{Resource.PaymentDate}"},
            {8 , $"{Resource.PaymentSum}"}
        };

    }
}
