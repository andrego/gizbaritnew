﻿using System;
using System.Collections.Generic;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Organization;

namespace Gizbarit.DAL.Providers.Report
{
    public class ReportProviderArgs
    {
        public Organization Organization { get; set; }

        public DateTime FromDate { get; set; } = DateTime.UtcNow.Date.AddMonths(-1);

        public DateTime ToDate { get; set; } = DateTime.UtcNow.Date;

        // With Tax
        public ICollection<Entities.Documents.Document> Invoices { get; set; } = new List<Entities.Documents.Document>();
        
        // With Tax
        public ICollection<Entities.Documents.Document> Reciepts { get; set; } = new List<Entities.Documents.Document>();
        
        // All
        public ICollection<Entities.Documents.Document> InvoiceReciepts { get; set; } = new List<Entities.Documents.Document>();

    }

    public interface IReportProvider
    {
        ContentBase Create(ReportProviderArgs args);
    }
}
