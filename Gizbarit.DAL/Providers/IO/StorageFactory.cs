﻿using System.IO;
using System.Web;
using Gizbarit.DAL.Common;

namespace Gizbarit.DAL.Providers.IO
{
    public static class StorageFactory
    {
        public static readonly string BasePath = @"C:\_Storage"; //VirtualPathUtility.ToAbsolute("~/Content");

        public static IStorage Create(string folder = null, string basePath = null)
        {
            basePath = basePath.IsEmpty() ? BasePath : basePath;
            var uri = folder.IsEmpty() ? Path.Combine(basePath) : Path.Combine(basePath, folder);
            var storage = new AzureBlobStorage();//new FileSystemStorage(uri);

            return storage;
        }
    }
}
