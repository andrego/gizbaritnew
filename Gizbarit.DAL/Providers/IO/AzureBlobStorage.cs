﻿using System.IO;
using Gizbarit.DAL.Common;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Gizbarit.DAL.Providers.IO
{
    public class AzureBlobStorage : IStorage
    {
        public bool Exists(string path)
        {
            path.AssertNotEmpty(nameof(path));

            var container = CreateContainer(path);
            var blobName = Path.GetFileName(path);
            var isExists = container.GetBlockBlobReference(blobName).Exists();

            return isExists;
        }

        public void Write(string path, Stream stream)
        {
            path.AssertNotEmpty(nameof(path));
            stream.AssertNotNull(nameof(stream));

            var container = CreateContainer(path);
            var blobName = Path.GetFileName(path);
            var blobBlock = container.GetBlockBlobReference(blobName);
            
            if (stream.CanSeek)
            {
                stream.Seek(0, SeekOrigin.Begin);
            }

            blobBlock.UploadFromStream(stream);
        }

        public Stream Read(string path)
        {
            path.AssertNotEmpty(nameof(path));

            var container = CreateContainer(path);
            var blobName = Path.GetFileName(path);

            var blobBlock = container.GetBlockBlobReference(blobName);

            return blobBlock.OpenRead();
        }

        public void Delete(string path)
        {
            path.AssertNotEmpty(nameof(path));

            var container = CreateContainer(path);
            var blobName = Path.GetFileName(path);

            var blobBlock = container.GetBlockBlobReference(blobName);
            try
            {
                blobBlock.Delete();
            }
            catch (StorageException)
            {
            }


        }

        private static CloudBlobContainer CreateContainer(string path)
        {
            // Retrieve storage account from connection string.
            var storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            var blobClient = storageAccount.CreateCloudBlobClient();
            var containerName = Path.GetDirectoryName(path)?.ToLower();

            // Retrieve reference to a previously created container.
            var container = blobClient.GetContainerReference(containerName);

            container.CreateIfNotExists();

            return container;
        }
    }
}
