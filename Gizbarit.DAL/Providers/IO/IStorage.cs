﻿using System;
using System.IO;
using System.Text;
using Gizbarit.DAL.Entities.Content;

namespace Gizbarit.DAL.Providers.IO
{
    public interface IStorage
    {
        bool Exists(string path);
        void Write(string path, Stream stream);
        Stream Read(string path);
        void Delete(string path);
    }

    public static class StorageExtensions
    {
        public static void WriteAllBytes(this IStorage storage, string path, byte[] bytes)
        {
            if (storage == null)
            {
                throw new ArgumentNullException(nameof(storage));
            }

            if (bytes == null)
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            var stream = new MemoryStream(bytes);

            storage.Write(path, stream);
        }

        public static void WriteAllText(this IStorage storage, string path, string text)
        {
            if (storage == null)
            {
                throw new ArgumentNullException(nameof(storage));
            }

            text = text ?? string.Empty;
           
            var byteArray = Encoding.UTF8.GetBytes(text);
            var stream = new MemoryStream(byteArray);

            storage.Write(path, stream);
        }

        public static void WriteContent(this IStorage storage, ContentBase content)
        {
            if (content == null)
            {
                throw new ArgumentNullException(nameof(content));
            }

            storage.WriteAllBytes(content.Name, content.Data);
        }

        public static byte[] ReadAllBytes(this IStorage storage, string path)
        {
            if (storage == null)
            {
                throw new ArgumentNullException(nameof(storage));
            }

            var stream = storage.Read(path);
            using(var ms = new MemoryStream())
            {
                stream.CopyTo(ms);

                return ms.ToArray();
            }
        }

        public static string ReadAllText(this IStorage storage, string path)
        {
            if (storage == null)
            {
                throw new ArgumentNullException(nameof(storage));
            }

            var stream = storage.Read(path);
            var reader = new StreamReader(stream);

            return reader.ReadToEnd();
        }

        public static ContentBase ReadContent(this IStorage storage, string path)
        {
            if (storage == null)
            {
                throw new ArgumentNullException(nameof(storage));
            }
            var content = new ContentBase()
            {
                Name = path,
                Data = storage.ReadAllBytes(path)
            };

            return content;
        }
    }
}
