﻿using System;
using System.IO;

namespace Gizbarit.DAL.Providers.IO
{
    public class FileSystemStorage : IStorage
    {
        private readonly string _basePath;

        public FileSystemStorage(string basePath)
        {
            if (string.IsNullOrWhiteSpace(basePath))
            {
                throw new ArgumentNullException(basePath);
            }

            if (!Directory.Exists(basePath))
            {
                Directory.CreateDirectory(basePath);
            }

            this._basePath = basePath;
        }

        public bool Exists(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException(path);
            }

            var uri = Path.Combine(_basePath, path);

            var isExists = File.Exists(uri);

            return isExists;
        }

        public void Write(string path, Stream stream)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException(path);
            }

            var uri = Path.Combine(_basePath, path);

            using (var outputStream = File.Create(uri))
            {
                stream.CopyTo(outputStream);
            }
        }

        public Stream Read(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException(path);
            }

            var uri = Path.Combine(_basePath, path);

            return File.OpenRead(uri);
        }

        public void Delete(string path)
        {
            if (this.Exists(path))
            {
                File.Delete(path);
            }
        }
    }
}
