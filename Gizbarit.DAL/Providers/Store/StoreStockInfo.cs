﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Entities.Store;

namespace Gizbarit.DAL.Providers.Store
{
    public class StoreStockInfo
    {
        public Organization Organization { get; set; }

        public ICollection<StoreInvoice> Invoices { get; set; }

        [Required(ErrorMessage = "*")]
        [EmailAddress]
        public string Email { get; set; }

        [EmailAddress]
        public string BccEmail { get; set; }
    }
}