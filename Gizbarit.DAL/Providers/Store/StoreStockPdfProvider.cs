﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Providers.Document;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Gizbarit.DAL.Providers.Store
{
    public class StoreStockPdfProvider : PdfProviderBase, IDocumentProvider<StoreStockInfo>
    {
        public ContentBase Create(StoreStockInfo context, DocumentProviderArgs args)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            args = args ?? new DocumentProviderArgs()
            {
                StampType = DocumentStampType.None
            };

            var docName = $"ספירת מלאי";
            var entity = context;

            var pdfDocument = new iTextSharp.text.Document(PageSize.A4);
            var outputStream = new MemoryStream();
            var stream = PdfWriter.GetInstance(pdfDocument, outputStream);
            
            pdfDocument.Open();
            pdfDocument.NewPage();

            args.StampType = DocumentStampType.None;

            CreateStockHeader(pdfDocument, entity , $"ספירת מלאי ל- ", args);
            CreateStockItemTable(pdfDocument, entity);
            CreateSignatureRow(pdfDocument);
            AddingFooter(stream);

            pdfDocument.Close();

            var content = new PdfContent()
            {
                Name = $"{entity}.pdf",
                Description = docName,
                Data = outputStream.ToArray(),
                OrganizationId = entity.Organization.ID,
                //DocumentId = entity
            };

            content.Data = AddPageLayer(content.Data, !args.IsCombinedReport);

            return content;
        }

    }
}