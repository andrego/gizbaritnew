﻿using System;
using System.IO;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Providers.Document;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Gizbarit.DAL.Providers.Store
{
    public class StoreOrderPdfProviderEn : PdfProviderBase, IDocumentProvider<Order>
    {
        public ContentBase Create(Order context, DocumentProviderArgs args = null)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            args = args ?? new DocumentProviderArgs()
            {
                StampType = DocumentStampType.None
            };

            var entity = context;
            var org = entity.Supplier.Organization;

            var pdfDocument = new iTextSharp.text.Document(PageSize.A4);
            var outputStream = new MemoryStream();
            var stream = PdfWriter.GetInstance(pdfDocument, outputStream);
            var documentId = entity.Number;

            pdfDocument.Open();
            pdfDocument.NewPage();

            args.StampType = DocumentStampType.None;

            CreateOrderHeaderEng(pdfDocument, entity, $"Delivery order {documentId}", args);
            CreateEnglishOrderItemTable(pdfDocument, entity);
            EnglishOrderConformationString(pdfDocument);
            //CreateSignatureRow(pdfDocument);

            AddingFooter(stream);
            pdfDocument.Close();



            var content = new PdfContent()
            {
                Name = $"order No {documentId}.pdf",
                //Description = entity,
                Data = outputStream.ToArray(),
                OrganizationId = entity.SupplierId,
                // DocumentId = documentId
            };

            content.Data = AddPageLayer(content.Data, !args.IsCombinedReport);



            return content;
        }
    }
}
