﻿using System;
using System.IO;
using System.Linq;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Services.Organisation;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Gizbarit.DAL.Providers.Store
{
    public class StoreOrderPdfProvider : PdfProviderBase, IDocumentProvider<Order>
    {
        public ContentBase Create(Order context, DocumentProviderArgs args = null)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            args = args ?? new DocumentProviderArgs()
            {
                StampType = DocumentStampType.None
            };

            var entity = context;

            var pdfDocument = new iTextSharp.text.Document(PageSize.A4);
            var outputStream = new MemoryStream();
            var stream = PdfWriter.GetInstance(pdfDocument, outputStream);
            var documentId = entity.Number;

            pdfDocument.Open();
            pdfDocument.NewPage();

            args.StampType = DocumentStampType.None;

            //CreateOrdeHeader(pdfDocument, entity, $"הזמנת הספקה מספר {entity?.Number}", args);
            CreateOrderHeader(pdfDocument, entity, $"הזמנת הספקה {documentId}", args);
            CreateOrderItemTable(pdfDocument, entity);
            OrderConformationString(pdfDocument);

            AddingFooter(stream);

            pdfDocument.Close();


            var content = new PdfContent()
            {
                Name = $"order No {documentId}.pdf",
                //Description = entity,
                Data = outputStream.ToArray(),
                OrganizationId = entity.SupplierId,
               // DocumentId = documentId
            };

            content.Data = AddPageLayer(content.Data, !args.IsCombinedReport);



            return content;
        }
    }
}