using System;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Documents;

namespace Gizbarit.DAL.Entities
{
    public partial class Deposit
    {
        public Guid ID { get; set; }

        public long Number { get; set; }

        public int BankAccount { get; set; }

        public virtual BankAccount BankAccount1 { get; set; }

        public virtual Document Document { get; set; }
    }
}
