using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    [Table("LeadCellNumber")]
    public partial class LeadCellNumber
    {
        public int Id { get; set; }

        [Required]
        [StringLength(3)]
        public string CountryCode { get; set; }

        [Required]
        [StringLength(3)]
        public string AreaCode { get; set; }

        [Required]
        [StringLength(10)]
        public string Mobile { get; set; }

        public int SmsCode { get; set; }

        public int? OrganizationId { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
