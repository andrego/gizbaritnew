﻿using System;
using System.Collections.Generic;
using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Store
{
    public class StoreInvoice : IEntity
    {
        public StoreInvoice()
        {
            Items = new HashSet<StoreCatalogItem>();
        }
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public long Number { get; set; }
        public string Description { get; set; }
        public StoreInvoiceType Type { get; set; }
        public int SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual Organization.Organization Organization { get; set; }
        public virtual ICollection<StoreCatalogItem> Items { get; set; } 
    }
    public enum StoreInvoiceType
    {
        Invoice,
        Waybill
    }
}
