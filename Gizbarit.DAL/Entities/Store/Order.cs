﻿using System;
using System.Collections.Generic;
using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Store
{
    public class Order : IEntity
    {
        public Order()
        {
            OrderDetails = new List<OrderDetail>();
        }
        public int ID { get; set; }
        public int Number { get; set; }
        public DateTime Date { get; set; }

        public int SupplierId { get; set; }
        public bool WasProcessed { get; set; }
        public DocumentStatusType Status { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        
        public virtual Supplier Supplier { get; set; }
    }

    public class OrderDetail : IEntity
    {
        public int ID { get; set; }
        public int CatalogItemId { get; set; }
        public double Quantity { get; set; }
        public int OrderId { get; set; }

        public virtual CatalogItem CatalogItem { get; set; }
        public virtual Order Order { get; set; }
    }
}
