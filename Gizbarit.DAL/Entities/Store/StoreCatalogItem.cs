﻿using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Store
{
    public class StoreCatalogItem : IEntity
    {
        public int ID { get; set; }
        public double Quantity { get; set; }
        public double? UsedQuantity { get; set; }
        public int CatalogItemId { get; set; }
        public int StoreInvoiceId { get; set; }
        public virtual CatalogItem CatalogItem { get; set; }
        public virtual StoreInvoice StoreInvoice { get; set; }
    }
}
