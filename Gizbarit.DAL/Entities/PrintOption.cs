using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    public partial class PrintOption
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrgId { get; set; }

        public string Comments { get; set; }

        public bool? CodeCollum { get; set; }

        public bool? ItemDiscountCullom { get; set; }

        public bool? DiscountTypeCullom { get; set; }

        public bool? RoundAmount { get; set; }

        public string HtmlText { get; set; }

        public bool? BankDetails { get; set; }

        public int? DocumentTemplate { get; set; }

        [StringLength(150)]
        public string Drawing { get; set; }

        public int? TemplateColor { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
