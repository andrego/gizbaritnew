using System;

namespace Gizbarit.DAL.Entities
{
    public partial class Log
    {
        public int Id { get; set; }

        public int? Type { get; set; }

        public string Source { get; set; }
        public string Message { get; set; }

        public DateTime? Date { get; set; }

        public int? OrgId { get; set; }
    }
}
