namespace Gizbarit.DAL.Entities
{
    public partial class Page
    {
        public int ID { get; set; }

        public string Url { get; set; }

        public string Name { get; set; }

        public bool? IsActive { get; set; }
    }
}
