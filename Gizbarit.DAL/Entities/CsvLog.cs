using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    [Table("CsvLog")]
    public partial class CsvLog
    {
        public int ID { get; set; }

        public DateTime Date { get; set; }

        public int RowID { get; set; }

        [Required]
        [StringLength(100)]
        public string CustomerName { get; set; }

        [Required]
        public string Subject { get; set; }

        public bool? SuccefullClearing { get; set; }

        public bool? DocumentCreated { get; set; }

        [Required]
        [StringLength(200)]
        public string Message { get; set; }

        public int OrganizationID { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
