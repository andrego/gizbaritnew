using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Catalog
{
    public partial class CatalogItem:IEntity
    {
        public int ID { get; set; }

        public int OrganizationId { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [Required]
        public string Name { get; set; }

        public double? Price { get; set; }
        public double SupplierPrice { get; set; }
        public double SellingPrice { get; set; }
        public double? PriceAfterTax { get; set; }

        public double? VatPercentage { get; set; }

        public bool Active { get; set; }
        public string ImagePath { get; set; }
        public virtual ICollection<StoreCatalogItem> StoreCatalogItems { get; set; }
        public virtual ICollection<OrderDetail> StoreOrders { get; set; }



        public virtual Organization.Organization Organization { get; set; }
    }
}
