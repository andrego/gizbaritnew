using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Entities
{
    public partial class CreditCompany
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public long ExternalNumber { get; set; }

        public int OrganizationID { get; set; }

        public bool? Active { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
