using Gizbarit.DAL.Entities.Bank;

namespace Gizbarit.DAL.Entities
{
    public partial class Permission
    {
        public int ID { get; set; }

        public int OrganizationID { get; set; }

        public bool? Invoice { get; set; }

        public bool? Receipt { get; set; }

        public bool? InvoiceCredit { get; set; }

        public bool? InvoiceReceipt { get; set; }

        public bool? ProformaInvoice { get; set; }

        public bool? Deposit { get; set; }

        public bool? CheckReturn { get; set; }

        public bool? PriceQuote { get; set; }

        public bool? PurchaseOrder { get; set; }

        public bool? ShippingInvoice { get; set; }

        public int? BranchID { get; set; }

        public virtual Branch Branch { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
