using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Gizbarit.DAL.Entities.Organization;

namespace Gizbarit.DAL.Entities
{
    public partial class ClearingCompany
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClearingCompany()
        {
            OrganizationsClearingAccounts = new HashSet<OrganizationsClearingAccount>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        
        public virtual ICollection<OrganizationsClearingAccount> OrganizationsClearingAccounts { get; set; }
    }
}
