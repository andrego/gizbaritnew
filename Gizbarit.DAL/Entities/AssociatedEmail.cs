using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Entities
{
    public partial class AssociatedEmail
    {
        public int ID { get; set; }

        public int OrganizationID { get; set; }

        [Required]
        [StringLength(50)]
        public string Mail { get; set; }

        public bool Active { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
