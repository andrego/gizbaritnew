using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    public partial class ExpirationUpdateReason
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ExpirationUpdateReason()
        {
            ExpirationDateHistories = new HashSet<ExpirationDateHistory>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        
        public virtual ICollection<ExpirationDateHistory> ExpirationDateHistories { get; set; }
    }
}
