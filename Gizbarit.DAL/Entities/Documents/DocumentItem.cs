using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Documents
{
    public partial class DocumentItem:IEntity
    {

        public DocumentItem()
        {
            
        }

        public DocumentItem(DocumentItem item)
        {
            DocumentID = item.DocumentID;
            WaybillID = item.WaybillID;
            Name = item.Name;
            Price = item.Price;
            Quantity = item.Quantity;
            TotalwithoutTax = item.TotalwithoutTax;
            TaxPercentage = item.TaxPercentage;
            Total = item.Total;
            InternalComments = item.InternalComments;
            ExternalComments = item.ExternalComments;
            CatalogID = item.CatalogID;
            Discount = item.Discount;
            TotalTax = item.TotalTax;
            Code = item.Code;
        }

        public int ID { get; set; }

        public Guid DocumentID { get; set; }

        [ForeignKey("Waybill")]
        public Guid? WaybillID { get; set; }

        [Required]
        [StringLength(3000)]
        public string Name { get; set; }

        public double Price { get; set; }

        public double Quantity { get; set; }

        public double TotalwithoutTax { get; set; }

        public double TaxPercentage { get; set; }

        public double Total { get; set; }

        public string InternalComments { get; set; }

        public string ExternalComments { get; set; }

        public int? CatalogID { get; set; }

        public double? Discount { get; set; }
        public string ImagePath { get; set; }

        public int? ClientCategory { get; set; }

        public double TotalTax { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        public virtual Document Document { get; set; }
        public virtual Waybill Waybill { get; set; }
    }
}
