using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Documents
{
    public partial class PriceOffer
    {
        [ForeignKey("Document")]
        public Guid ID { get; set; }

        public virtual Document Document { get; set; }
    }
}
