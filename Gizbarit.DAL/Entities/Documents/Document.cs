using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Interfaces;
using Gizbarit.DAL.Models.CustomerPayment;

namespace Gizbarit.DAL.Entities.Documents
{
    public partial class Document : IEntity<Guid>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Document()
        {
            BankTransactions = new HashSet<BankTransaction>();
            DocumentItems = new HashSet<DocumentItem>();
            DocumentReferences = new HashSet<DocumentReference>();
            Payments = new HashSet<Payment.Payment>();
            Payments1 = new HashSet<Payment.Payment>();

            ActualCreationDate = DateTime.UtcNow;
            Status = DocumentStatusType.Initial;
            TaxPercentage = 17;
        }

        public Guid ID { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        [StringLength(4000)]
        public string Subject { get; set; }

        public string ExternalComments { get; set; }

        public string InternalComments { get; set; }

        [Required]
        [StringLength(50)]
        public string UserIP { get; set; }

        public int ClientID { get; set; }

        public double TotalwithoutTax { get; set; }

        public double Total { get; set; }
        public double TotalWithoutRounding { get; set; }

        [StringLength(50)]
        public string OrgUniqueIdentifier { get; set; }

        public double ConversionRate { get; set; }

        public int OrganizationID { get; set; }

        public double TaxPercentage { get; set; }

        public double TotalTaxAmount { get; set; }

        public DocumentStatusType Status { get; set; }
        public bool IsPaid { get; set; }

        public double Discount { get; set; }

        public bool? DiscountBeforeTax { get; set; }

        public bool IsPaymentProcessed { get; set; }

        public DateTime? PaymentDueDate { get; set; }

        public int? GeneralClientId { get; set; }

        public short? Language { get; set; }

        public DateTime ActualCreationDate { get; set; }

        public double? TotalCredit { get; set; }

        public bool IsTaxIncluded { get; set; }
        public bool IsRounded { get; set; }
        public bool IsSmallBusiness { get; set; }

        public long Number { get; set; }

        public string CurrencyId { get; set; }
        public DocumentType DocumentType { get; set; }
        
        public bool IsExport { get; set; }
        public DeliveryTermsType DeliveryTermsType { get; set; }
        public TermsOfPaymentType TermsOfPaymentType { get; set; }
        public string TimeOfDelivery { get; set; }
        public string PointOfDelivery { get; set; }
        public string ExportAditionInfo { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public virtual ICollection<BankTransaction> BankTransactions { get; set; }

        public virtual Client Client { get; set; }
            

        public virtual Currency Currency { get; set; }

        public virtual Deposit Deposit { get; set; }

        
        public virtual ICollection<DocumentItem> DocumentItems { get; set; }

        
        public virtual ICollection<DocumentReference> DocumentReferences { get; set; }
        public ICollection<DocumentItem> WaybillItems { get; set; }

        public virtual GeneralClient GeneralClient { get; set; }

        public virtual Organization.Organization Organization { get; set; }

        public virtual PriceOffer PriceOffer { get; set; }

        
        public virtual InvoiceReceipt InvoiceReceipt { get; set; }

        
        public virtual Invoice Invoice { get; set; }
        
        public virtual ICollection<Payment.Payment> Payments { get; set; }

        public virtual ICollection<Payment.Payment> Payments1 { get; set; }

        public virtual PreliminaryInvoice PreliminaryInvoice { get; set; }

        public virtual Receipt Receipt { get; set; }
        public virtual Waybill Waybill { get; set; }

       
        public void Update(Document document)
        {
            Subject = document.Subject;
            ClientID = document.ClientID;
            TotalwithoutTax = document.TotalwithoutTax;
            TotalWithoutRounding = document.TotalWithoutRounding;
            Total = document.Total;
            TotalTaxAmount = document.TotalTaxAmount;
            IsTaxIncluded = document.IsTaxIncluded;
            IsRounded = document.IsRounded;
            PaymentDueDate = document.PaymentDueDate;
            InternalComments = document.InternalComments;

            Email = document.Email;
        }
    }

    

    public enum DocumentType
    {
        PriceOffer,
        Waybill,
        PreliminaryInvoice,
        TaxInvoice,
        Receipt,
        TaxInvoiceReceipt
    }

    public enum DocumentStatusType
    {
        Initial, //0
        Printed, //1
        Saved, //2

        [Obsolete]
        Paid = 2, //2

        Send, //3
        InUse, //4
        Deleted = 30
    }

    #region ForExport

    public enum DeliveryTermsType
    {
        
        EXW,
        FOB,
        FAS,
        FCA,
        CPT,
        CIF,
        CFR,
        CIP,
        DAT,
        DAF,
        DDP
    }

    public enum TermsOfPaymentType
    {
        AdvancePayment,
        LC,
        DocumentaryCollection,
        OpenAccount
    }
    #endregion

}
