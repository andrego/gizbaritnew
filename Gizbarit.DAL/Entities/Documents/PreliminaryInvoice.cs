using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Documents
{
    public partial class PreliminaryInvoice
    {
        [ForeignKey("Document")]
        public Guid ID { get; set; }

        public double Paid { get; set; }
        public bool IsAutoCreated { get; set; }

        public virtual Document Document { get; set; }
    }
}
