using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Documents
{
    public partial class InvoiceReceipt
    {
        [Key]
        [ForeignKey("Document")]
        public Guid ID { get; set; }

        public double Paid { get; set; }

        public double Deduction { get; set; }

        public virtual Document Document { get; set; }
    }
}
