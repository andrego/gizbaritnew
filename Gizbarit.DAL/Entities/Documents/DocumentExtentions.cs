﻿using System;

namespace Gizbarit.DAL.Entities.Documents
{
    public static class DocumentExtensions
    {
        public static bool IsOriginalGenerated(this Document doc)
        {
            if (doc == null)
            {
                throw new ArgumentNullException(nameof(doc));
            }

            return doc.Status == DocumentStatusType.Printed || doc.Status == DocumentStatusType.Send;
        }

        public static bool IsOriginalNotGenerated(this Document doc)
        {
            return !doc.IsOriginalGenerated();
        }

        public static bool IsSignRequred(this Document doc)
        {
            switch (doc.DocumentType)
            {
                case DocumentType.TaxInvoice:
                case DocumentType.Receipt:
                case DocumentType.TaxInvoiceReceipt:
                    return true;
                    
                default:
                    return false;
            }
        }

        public static bool IsSignNotRequred(this Document doc)
        {
            return !IsSignRequred(doc);
        }

        public static bool IsNew(this Document doc)
        {
            return doc.ID == Guid.Empty;
        }

        public static bool IsRefund(this Document doc)
        {
            return doc.Total < 0;
        }
    }
}
