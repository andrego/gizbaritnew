using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Documents
{
    public partial class Receipt
    {
        [ForeignKey("Document")]
        public Guid ID { get; set; }

        public Guid? ReferenceDocument { get; set; }

        public double Deduction { get; set; }

        public virtual Document Document { get; set; }
    }
}
