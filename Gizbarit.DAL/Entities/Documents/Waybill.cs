using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Documents
{
    public partial class Waybill
    {
        public Waybill()
        {
            IncludedDocumenItems = new HashSet<DocumentItem>();
        }
        [ForeignKey("Document")]
        public Guid ID { get; set; }

        public TimeSpan? DepartureTime { get; set; }

        public ICollection<DocumentItem> IncludedDocumenItems { get; set; } 
        public virtual Document Document { get; set; }
    }
}
