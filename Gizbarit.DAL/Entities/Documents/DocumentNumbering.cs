using System.ComponentModel.DataAnnotations.Schema;
using Gizbarit.DAL.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Entities.Documents
{
    [Table("DocumentNumbering")]
    public partial class DocumentNumbering :IEntity
    {
        public int ID { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long Invoice { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long Receipt { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long InvoiceReceipt { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long InvoiceCredit { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long ProformaInvoice { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long InvoiceOrder { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long InvoiceQuote { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long InvoiceShip { get; set; }

        public long Register { get; set; }

        public long TaxCredit { get; set; }

        public long Income { get; set; }

        public long ExemptIncome { get; set; }

        public long Deal { get; set; }

        public long GeneralCustomer { get; set; }

        public long Deposit { get; set; }

        public string AccountNumber { get; set; }

        public string BankName { get; set; }

        public string BranchName { get; set; }

        public string AccountOwner { get; set; }

        public string BIC { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }
       
        public virtual Organization.Organization Organization { get; set; }
    }
}
