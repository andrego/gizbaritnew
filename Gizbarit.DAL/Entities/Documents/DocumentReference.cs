using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Documents
{
    public partial class DocumentReference
    {
        [Key]
        [Column(Order = 0)]
        public Guid BaseDocumentId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid ReferenceDocumentId { get; set; }

        public DocumentType Type { get; set; }

        public double? CreditAmount { get; set; }

        public virtual Document BaseDocument { get; set; }
        public virtual Document ReferenceDocument { get; set; }
    }
}
