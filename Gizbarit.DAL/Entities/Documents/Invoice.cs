using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Documents
{
    public partial class Invoice
    {
        [ForeignKey("Document")]
        public Guid ID { get; set; }

        public double Paid { get; set; }

        public virtual Document Document { get; set; }
    }
}
