using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    public partial class UserInquiry
    {
        public int ID { get; set; }

        public int? UserID { get; set; }

        public short InquiryStatus { get; set; }

        public short UserApprover { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateCreated { get; set; }

        public int? LeadID { get; set; }

        public virtual InquiryStatus InquiryStatus1 { get; set; }
    }
}
