using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    public partial class Credit
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public int Quota { get; set; }

        public DateTime BuyDate { get; set; }

        public double Price { get; set; }

        public int OrganizationId { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
