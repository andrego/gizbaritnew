﻿using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Entities
{
    public class Person
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(256)]
        public string LastName { get; set; }

        [Required]
        [StringLength(256)]
        public string Position { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string FullName => $"{this.FirstName} {this.LastName}".Trim();
    }
}
