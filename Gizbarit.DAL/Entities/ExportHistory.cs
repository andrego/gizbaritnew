using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    [Table("ExportHistory")]
    public partial class ExportHistory
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrgID { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime ExportDate { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? ExportEndDate { get; set; }
    }
}
