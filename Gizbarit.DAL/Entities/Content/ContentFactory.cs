﻿using System;

namespace Gizbarit.DAL.Entities.Content
{
    public class ContentFactory
    {
        public static ImageContent CreateSignature(string name, byte[] data)
        {
            return new ImageContent
            {
                Name = name,
                Data = data,
                Category = ContentCategory.Signature
            };
        }

        public static ImageContent CreateStamp(string name, byte[] data)
        {
            return new ImageContent
            {
                Name = name,
                Data = data,

                Category = ContentCategory.Stamp
            };
        }

        public static CertificateContent CreateCertificate(string name, byte[] data, DateTime expired)
        {
            return new CertificateContent
            {
                Name = name,
                Data = data,
                Category = ContentCategory.Stamp
            };
        }

    }
}