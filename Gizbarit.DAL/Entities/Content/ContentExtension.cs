﻿using System;
using System.IO;

namespace Gizbarit.DAL.Entities.Content
{
    public static class ContentExtension
    {
        public static Stream GetStream(this ContentBase content)
        {
            if (content?.Data == null)
            {
                throw new ArgumentException("Can't' create stream. Content is empty");
            }

            return new MemoryStream(content.Data);
        }
    }
}