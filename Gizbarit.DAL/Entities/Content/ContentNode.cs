﻿using Gizbarit.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Gizbarit.DAL.Entities.Content
{
    public enum ContentNodeStatus
    {
        Active = 1,
        Disabled,
        Deleted
    }

    public class ContentNode : IEntity<Guid>
    {
        [Key]
        public Guid ID { get; set; } = Guid.NewGuid();

        [Required]
        [StringLength(256)]
        [Index("IX_NAME_CULTURE_Parent", Order = 1, IsUnique = true)]
        public string Name { get; set; }

        [Required]
        [Index("IX_NAME_CULTURE_Parent", Order = 2, IsUnique = true)]
        [StringLength(256)]
        public string CultureCode { get; set; } = "he";

        //[Required]
        [StringLength(1024)]
        public string Url { get; set; }

        [Required]
        [StringLength(256)]
        public string Title { get; set; }

        [StringLength(1024)]
        public string Description { get; set; }

        [AllowHtml]
        public string Content { get; set; }

        [Index("IX_NAME_CULTURE_Parent", Order = 3, IsUnique = true)]
        public Guid? ParentId { get; set; }
        
        public ContentNode Parent { get; set; }

        public virtual ICollection<ContentNode> Childs { get; set; } = new List<ContentNode>();

        [Required]
        public int Index { get; set; } = 10;
        public ContentNodeStatus Status { get; set; } = ContentNodeStatus.Active;

        public DateTime Updated { get; set; } = DateTime.UtcNow;

        [NotMapped]
        public bool Selected { get; set; }
    }
}
