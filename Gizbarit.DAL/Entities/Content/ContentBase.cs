﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Mime;
using Gizbarit.DAL.Interfaces;
using Newtonsoft.Json;

namespace Gizbarit.DAL.Entities.Content
{
    [Flags]
    public enum ContentCategory
    {
        General = 1,
        Text = 2, 
        Image = 4,
        Document = 8,
        Certificate = 16 + Text,
        Stamp = 32 + Image,
        Signature = 64 + Image,
        Pdf = 128 + Document,
        Word = 256 + Document,
        Exel = 512 + Document,
        Template = 1024
    }

    public class ContentBase : IEntity<Guid>
    {
        [Key]
        public Guid ID { get; set; } = Guid.NewGuid();

        [Index("IX_OrgId_DocId", Order = 1)]
        public int? OrganizationId { get; set; }

        [Index("IX_OrgId_DocId", Order = 2)]
        public Guid? DocumentId { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        public ContentCategory Category { get; set; }

        [Required]
        [StringLength(128)]
        public string Type { get; set; }

        [Required]
        public DateTime Created { get; set; } = DateTime.UtcNow;

        [Required]
        public DateTime Expired { get; set; } = DateTime.MaxValue;

        public string Description { get; set; }

        public byte[] Data { get; set; }

        public string ExtraData { get; set; }

        protected T GetExtraData<T>() where T : class, new()
        {
            return this.ExtraData == null ? new T() : JsonConvert.DeserializeObject<T>(this.ExtraData);
        }
    }

    public class CertificateContent : ContentBase
    {
        public CertificateContent()
        {
            this.Category = ContentCategory.Certificate;
            this.Type = MediaTypeNames.Text.Plain;
        }

        [NotMapped]
        public string Password
        {
            get { return this.ExtraData; }
            set { this.ExtraData = value; }
        }
    }

    public class ImageContent : ContentBase
    {
     public ImageContent()
        {
            this.Category = ContentCategory.Image;
            this.Type = MediaTypeNames.Image.Jpeg;
        }
    }

    public class PdfContent : ContentBase
    {
        public PdfContent()
        {
            this.Category = ContentCategory.Pdf;
            this.Type = MediaTypeNames.Application.Pdf;
        }
    }

    public class TextContent : ContentBase
    {
        public TextContent()
        {
            this.Category = ContentCategory.Text;
            this.Type = MediaTypeNames.Text.Plain;
        }
    }
}
