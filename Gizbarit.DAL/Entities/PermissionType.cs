using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Entities
{
    public partial class PermissionType
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
