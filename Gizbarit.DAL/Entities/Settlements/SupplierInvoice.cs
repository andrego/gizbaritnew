﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Settlements
{
    public class SupplierInvoice:IEntity
    {
        public SupplierInvoice()
        {
            IsConfirmed = false;
            Receipts = new HashSet<SupplierReceipt>();
        }
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public long Number { get; set; }
        public double Amount { get; set; }
        public bool IsConfirmed { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }

        public int OrganizationID { get; set; }
        public int SupplierID { get; set; }
        public virtual Organization.Organization Organization { get; set; }
        public virtual Supplier Supplier { get; set; }

        public ICollection<SupplierReceipt> Receipts { get; set; }
    }
}
