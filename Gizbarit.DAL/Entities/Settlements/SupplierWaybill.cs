﻿using System;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Settlements
{
    public class SupplierWaybill : IEntity
    {
        public int ID { get; set; }

        [StringLength(400)]
        public string SupplyDescription { get; set; }
        public long Number { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public int OrganizationID { get; set; }
        public int SupplierID { get; set; }
        
        public virtual Organization.Organization Organization { get; set; }
        public virtual Supplier Supplier { get; set; }
        
    }
}
