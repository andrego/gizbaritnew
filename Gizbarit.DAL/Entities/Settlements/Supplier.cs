﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Settlements
{
    public class Supplier:IEntity
    {
        public Supplier()
        {
            Invoices = new HashSet<SupplierInvoice>();
            Waybills = new HashSet<SupplierWaybill>();
            Receipts = new HashSet<SupplierReceipt>();
            StoreInvoices = new HashSet<StoreInvoice>();
        }
        public int ID { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [Required]
        [StringLength(20)]
        public string Phone { get; set; }

        [Required]
        [StringLength(400)]
        public string Address { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public int OrganizationID { get; set; }

        public virtual Organization.Organization Organization { get; set; }

        public virtual ICollection<SupplierInvoice> Invoices { get; set; }
        public virtual ICollection<SupplierWaybill> Waybills { get; set; }

        public virtual ICollection<SupplierReceipt> Receipts { get; set; } 
        public virtual ICollection<StoreInvoice> StoreInvoices { get; set; } 

        public virtual ICollection<Order> Orders { get; set; } 


    }
}
