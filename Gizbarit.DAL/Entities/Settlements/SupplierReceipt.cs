﻿using System;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Settlements
{
    public class SupplierReceipt:IEntity
    {
        public int ID { get; set; }
        public double Amount { get; set; }
        public DateTime PaymentDate { get; set; }
        [MaxLength(50)]
        public string Number { get; set; }

        public int OrganizationID { get; set; }
        public int SupplierID { get; set; }
        public int SupplierInvoiceID { get; set; }
        public virtual Organization.Organization Organization { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual SupplierInvoice SupplierInvoice { get; set; }
    }
}
