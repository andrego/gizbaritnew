using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Organization
{
    [Table("OrganizationPayPal")]
    public partial class OrganizationPayPal
    {
        public int Id { get; set; }

        public int? OrganizationId { get; set; }

        public int? DocumentType { get; set; }

        public string EmailPayPal { get; set; }

        public bool? IsAuto { get; set; }

        public string ApiSignature { get; set; }

        public string ApiPassword { get; set; }

        public string ApiEmail { get; set; }
    }
}
