using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Interfaces;
using Gizbarit.DAL.Models.CustomerPayment;

namespace Gizbarit.DAL.Entities.Organization
{
    public partial class Client:IEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            BalanceAdjusts = new HashSet<BalanceAdjust>();
            Documents = new HashSet<Document>();
            Retainers = new HashSet<Retainer>();
            Payments = new HashSet<Payment.Payment>();

            ClientEmailContacts = new HashSet<ClientEmailContact>();
            ClientPhoneContacts = new HashSet<ClientPhoneContact>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int OrganizationID { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(50)]
        public string UniqueId { get; set; }

        public int PaymentTerms { get; set; }

        public long? ExternalNumber { get; set; }

        [StringLength(50)]
        
        public string Email
        {
            get
            {
                var clientEmailContact = ClientEmailContacts.FirstOrDefault(m => m.IsActive);
                if (clientEmailContact != null)
                    return clientEmailContact.Email;

                return null;// or return String.Empty; ??
            }
            set
            {
                var clientEmail = ClientEmailContacts.FirstOrDefault(m => m.IsActive);
                if (clientEmail != null)
                    clientEmail.Email = value;
               

            }
        }

        [StringLength(50)]

        public string Phone
        {
            get
            {
                var clientPhoneContact = ClientPhoneContacts.FirstOrDefault(m => m.IsActive);
                if (clientPhoneContact != null)
                    return clientPhoneContact.Phone;

                return null; //or return String.Empty; ?????
            }
            set
            {
                var clientPhone = ClientPhoneContacts.FirstOrDefault(m => m.IsActive);
                if (clientPhone != null)
                    clientPhone.Phone = value;
             
            }
        }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string Zip { get; set; }

        [StringLength(300)]
        public string Address { get; set; }

        [StringLength(50)]
        public string Cell { get; set; }

        public bool Active { get; set; }

        public bool Retainer { get; set; }

        public double? RetainerAmount { get; set; }

        [StringLength(50)]
        public string RetainerTitle { get; set; }

        public bool? HasBeenExported { get; set; }

        public string Guid { get; set; }

        
        public virtual ICollection<BalanceAdjust> BalanceAdjusts { get; set; }

        public virtual Entities.Organization.Organization Organization { get; set; }

        
        public virtual ICollection<Document> Documents { get; set; }

        
        public virtual ICollection<Retainer> Retainers { get; set; }

        public virtual ICollection<Payment.Payment> Payments { get; set; }

        public virtual ICollection<ClientEmailContact> ClientEmailContacts { get; set; } 

        public virtual ICollection<ClientPhoneContact> ClientPhoneContacts { get; set; } 

   
    }
}
