﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Organization
{
   public class ClientPhoneContact:IEntity
    {
        public int ID { get; set; }
        public string Phone { get; set; }
        public bool IsActive { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}
