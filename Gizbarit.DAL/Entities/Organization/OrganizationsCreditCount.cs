using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Organization
{
    [Table("OrganizationsCreditCount")]
    public partial class OrganizationsCreditCount
    {
        public int ID { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public int Credits { get; set; }

        public int OrganizationID { get; set; }

        public virtual Organization Organization { get; set; }
    }
}
