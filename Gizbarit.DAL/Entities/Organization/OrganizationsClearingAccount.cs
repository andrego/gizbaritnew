using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Organization
{
    public partial class OrganizationsClearingAccount
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrganizationID { get; set; }

        public short CompanyType { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        [StringLength(50)]
        public string Terminal { get; set; }

        public virtual ClearingCompany ClearingCompany { get; set; }

        public virtual Organization Organization { get; set; }
    }
}
