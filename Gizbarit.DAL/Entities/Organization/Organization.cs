using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Gizbarit.DAL.Entities.Advertisement;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Interfaces;
using Gizbarit.Utils;

namespace Gizbarit.DAL.Entities.Organization
{
    public enum OrganizationStatus
    {
        Active = 10,
        LicenseExpired = 20,
        Deleted = 30
    }

    public partial class Organization : IEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Organization()
        {
            Advances = new HashSet<Advance>();
            AssociatedEmails = new HashSet<AssociatedEmail>();
            BalanceAdjusts = new HashSet<BalanceAdjust>();
            BankAccounts = new HashSet<BankAccount>();
            BankTransactions = new HashSet<BankTransaction>();
            Clients = new HashSet<Client>();
            CreditCompanies = new HashSet<CreditCompany>();
            Credits = new HashSet<Credit>();
            CsvLogs = new HashSet<CsvLog>();
            Documents = new HashSet<Document>();
            ExpirationDateHistories = new HashSet<ExpirationDateHistory>();
            OrganizationsCreditCounts = new HashSet<OrganizationsCreditCount>();
            Permissions = new HashSet<Permission>();
            Retainers = new HashSet<Retainer>();
            DocumentNumberings = new HashSet<DocumentNumbering>();
            CatalogItems = new HashSet<CatalogItem>();
            DateCreated = DateTime.UtcNow;
            PaidPeriodExpirationDate = DateCreated.AddMonths(13); //TODO is it correct?
            TrialPeriodExpirationDate = DateCreated.AddMonths(1); //TODO is it correct?
            TrialPeriod = true; //TODO is it correct?
            Suppliers = new HashSet<Supplier>();
            SupplierInvoices = new HashSet<SupplierInvoice>();
            StoreInvoices = new HashSet<StoreInvoice>();

            Advertisements = new HashSet<Advertisement.Advertisement>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string NameEng { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        [Required]
        [StringLength(50)]
        public string UniqueID { get; set; }

        [Required]
        public string Address { get; set; }

        public string AddressEng { get; set; }

        [Required]
        [StringLength(50)]
        public string Phone1 { get; set; }

        [StringLength(50)]
        public string Phone2 { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateCreated { get; set; }

        [Column(TypeName = "image")]
        public byte[] Logo { get; set; }

        public int Limit { get; set; }

        public int BundleId { get; set; }

        public int Credit { get; set; }


        [StringLength(50)]
        public string City { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [StringLength(50)]
        public string Subheading { get; set; }

        [StringLength(50)]
        public string Website { get; set; }

        public double TaxRate { get; set; }

        public short AccountingMethod { get; set; }

        [StringLength(50)]
        public string ExternalAccountingEmail { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PaidPeriodExpirationDate { get; set; }


        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime TrialPeriodExpirationDate { get; set; }

        public bool TrialPeriod { get; set; }

        public bool ApiActive { get; set; }

        public bool? AutoPayment { get; set; }

        public int? OccupationTypeID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ViewDate { get; set; }

        [Column(TypeName = "image")]
        public byte[] Signature { get; set; }

        public string PathToLogo { get; set; }

        public double AdvancesRate { get; set; }

        public short AdvancesAccountingMethod { get; set; }
        public Enums.RegisterType RegisterType { get; set; }


        [NotMapped]
        public string BookKeepingEmail { get; set; }

        public virtual ICollection<Advance> Advances { get; set; }


        public virtual ICollection<AssociatedEmail> AssociatedEmails { get; set; }


        public virtual ICollection<BalanceAdjust> BalanceAdjusts { get; set; }


        public virtual ICollection<BankAccount> BankAccounts { get; set; }


        public virtual ICollection<BankTransaction> BankTransactions { get; set; }
        public virtual ICollection<Supplier> Suppliers { get; set; }
        public virtual ICollection<SupplierInvoice> SupplierInvoices { get; set; }
        public virtual ICollection<SupplierWaybill> SupplierWaybills { get; set; }
        public virtual ICollection<SupplierReceipt> SupplierReceipts { get; set; }

        public virtual Bundle Bundle { get; set; }

        public int BusinessTypeId { get; set; }
        public virtual BusinessType BusinessType { get; set; }


        public virtual ICollection<Client> Clients { get; set; }


        public virtual ICollection<CreditCompany> CreditCompanies { get; set; }
        public virtual ICollection<StoreInvoice> StoreInvoices { get; set; }


        public virtual ICollection<Credit> Credits { get; set; }


        public virtual ICollection<CsvLog> CsvLogs { get; set; }


        public virtual ICollection<DocumentNumbering> DocumentNumberings { get; set; }


        public virtual ICollection<Document> Documents { get; set; }


        public virtual ICollection<ExpirationDateHistory> ExpirationDateHistories { get; set; }

        public virtual OrganizationsClearingAccount OrganizationsClearingAccount { get; set; }

        public virtual OrganizationsCreditCard OrganizationsCreditCard { get; set; }


        public virtual ICollection<OrganizationsCreditCount> OrganizationsCreditCounts { get; set; }


        public virtual ICollection<Permission> Permissions { get; set; }

        public virtual PrintOption PrintOption { get; set; }


        public virtual ICollection<Retainer> Retainers { get; set; }

        public virtual ICollection<CatalogItem> CatalogItems { get; set; }

        public virtual ICollection<Advertisement.Advertisement> Advertisements { get; set; }

        public virtual OrganizationInfo OrganizationInfo { get; set; }

        public bool IsVatCharge => this.BusinessTypeId == 1;

        public bool IsVatNotCharge => this.BusinessTypeId == 2;

        public int GetGizbaritPrice()
        {
            if (this.IsVatNotCharge)
                return 100;

            switch (this.RegisterType)
            {
                case Enums.RegisterType.Private:
                case Enums.RegisterType.Partnership:
                    return 300;
                case Enums.RegisterType.Company:
                    return 400;
                default:
                    return 200;
            }
        }

        [DefaultValue(OrganizationStatus.Active)]
        public OrganizationStatus Status { get; set; } = OrganizationStatus.Active;
    }


}
