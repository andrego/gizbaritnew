using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Organization
{
    public partial class OrganizationsCreditCard
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrganizationID { get; set; }

        [Required]
        [StringLength(50)]
        public string ExpirationDate { get; set; }

        [Required]
        [StringLength(50)]
        public string Token { get; set; }

        [Required]
        [StringLength(50)]
        public string OwnerID { get; set; }

        public virtual Organization Organization { get; set; }
    }
}
