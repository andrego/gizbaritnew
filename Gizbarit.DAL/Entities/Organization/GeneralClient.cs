using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Documents;

namespace Gizbarit.DAL.Entities.Organization
{
    public partial class GeneralClient
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GeneralClient()
        {
            Documents = new HashSet<Document>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Identifier { get; set; }

        
        public virtual ICollection<Document> Documents { get; set; }
    }
}
