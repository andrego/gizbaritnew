﻿using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Advertisement
{
    public class Advertisement : IEntity
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public string PathToLogo { get; set; }
        public string Title { get; set; }
        public AdvertiseLanguages Language { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }


    public enum AdvertiseLanguages
    {
        EN,
        RU,
        HE
    }
}
