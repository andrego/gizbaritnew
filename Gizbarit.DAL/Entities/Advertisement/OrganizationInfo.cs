﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Advertisement
{
   public partial class OrganizationInfo:IEntity
    {
        
       [Key]
       [ForeignKey("Organization")]
       public int ID { get; set; }
       
        public string Site { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string LinkedIn { get; set; }

        
 
       public virtual Organization.Organization Organization { get; set; }
    }
}
