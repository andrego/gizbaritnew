using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    [Table("ExpirationDateHistory")]
    public partial class ExpirationDateHistory
    {
        public int ID { get; set; }

        public short UpdateReason { get; set; }

        public short UserApprover { get; set; }

        public DateTime UpdateDate { get; set; }

        public DateTime NewDate { get; set; }

        public DateTime OldDate { get; set; }

        public int OrganizationID { get; set; }

        [StringLength(50)]
        public string Comment { get; set; }

        public virtual ExpirationUpdateReason ExpirationUpdateReason { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
