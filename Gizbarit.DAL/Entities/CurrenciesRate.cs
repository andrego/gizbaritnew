using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    [Table("CurrenciesRate")]
    public partial class CurrenciesRate
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string Symbol { get; set; }

        public double ConversionRate { get; set; }

      //  [Key]
      //  [Column(Order = 1)]
        public DateTime Date { get; set; }

        public virtual Currency Currency { get; set; }
    }
}
