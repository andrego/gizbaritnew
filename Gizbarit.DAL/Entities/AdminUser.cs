using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Entities
{
    public partial class AdminUser
    {
        public int ID { get; set; }

        [Required]
        [StringLength(25)]
        public string UserName { get; set; }

        [Required]
        [StringLength(25)]
        public string Password { get; set; }

        [StringLength(50)]
        public string Email { get; set; }
    }
}
