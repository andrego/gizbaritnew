using System;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Bank
{
    public partial class Advance:IEntity
    {
        public int ID { get; set; }

        public int OrganizationId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateAction { get; set; }

        [Required]
        public string Description { get; set; }

        public int? InvoiceNumber { get; set; }
        public Guid? InvoiceId { get; set; }
        public DocumentType? ForDocumentType { get; set; }

        public int? SupplierInvoiceNumber { get; set; }

        public Guid? BankTransactionId { get; set; }

        public double Amount { get; set; }

        public bool Credit { get; set; }

        public double? AdvanceAmount { get; set; }

        public double? TaxAmount { get; set; }

        public bool Deleted { get; set; }

        public virtual BankTransaction BankTransaction { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
