using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Entities.Bank
{
    public partial class Branch
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Branch()
        {
            Permissions = new HashSet<Permission>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int OrganizationID { get; set; }

        public string Description { get; set; }

        public bool? Enabled { get; set; }

        public bool? IsDeafult { get; set; }

        
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
