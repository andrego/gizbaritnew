﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Bank
{
    public class BankName:IEntity
    {
        public int ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public virtual ICollection<BankAccount> BankAccounts { get; set; } 
        
    }
}
