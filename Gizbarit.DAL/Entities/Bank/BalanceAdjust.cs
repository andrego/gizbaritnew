using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Gizbarit.DAL.Entities.Organization;

namespace Gizbarit.DAL.Entities.Bank
{
    [Table("BalanceAdjust")]
    public partial class BalanceAdjust
    {
        public int ID { get; set; }

        public int OrganizationID { get; set; }

        public int ClientID { get; set; }

        public DateTime BalanceDate { get; set; }

        public double Amount { get; set; }

        [Required]
        [StringLength(100)]
        public string Details { get; set; }

        public bool BalanceType { get; set; }

        public virtual Client Client { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
