using System;
using System.Collections.Generic;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Bank
{
    public partial class BankTransaction:IEntity<Guid>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BankTransaction()
        {
            Advances = new HashSet<Advance>();
        }

        public Guid ID { get; set; }

        public DateTime DateInserted { get; set; }

        public DateTime DateUpdated { get; set; }

        public int BankId { get; set; }

        public int OrganizationId { get; set; }

        public Guid? DocumentId { get; set; }

        public bool Credit { get; set; }

        public string Details { get; set; }

        public double Amount { get; set; }

        public string CurrencyId { get; set; }
        public virtual Currency Currency { get; set; }

        public virtual ICollection<Advance> Advances { get; set; }

        public virtual BankAccount BankAccount { get; set; }

        public virtual Document Document { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
