using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities.Bank
{
    [Table("BankAccountInitialBalanceYearly")]
    public partial class BankAccountInitialBalanceYearly
    {
        public int ID { get; set; }

        public int BankAccountId { get; set; }

        public double InitialBalance { get; set; }

        public int Year { get; set; }

        public virtual BankAccount BankAccount { get; set; }
    }
}
