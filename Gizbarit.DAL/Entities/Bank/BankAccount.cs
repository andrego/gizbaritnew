using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Bank
{
    public partial class BankAccount : IEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BankAccount()
        {
            BankAccountInitialBalanceYearlies = new HashSet<BankAccountInitialBalanceYearly>();
            BankTransactions = new HashSet<BankTransaction>();
            Deposits = new HashSet<Deposit>();
        }

        public int ID { get; set; }

        [StringLength(50)]
        public string BranchName { get; set; }

        [Required]
        [StringLength(50)]
        public string AccountNumber { get; set; }

        public long ExternalNumber { get; set; }

        public int OrganizationID { get; set; }

        public bool Active { get; set; }

        public double InitialBalance { get; set; }

        public double Limit { get; set; }

        public bool IsDeleted { get; set; }

        public int BankNameId { get; set; }
        public virtual BankName BankName { get; set; }

        
        public virtual ICollection<BankAccountInitialBalanceYearly> BankAccountInitialBalanceYearlies { get; set; }

        public virtual Organization.Organization Organization { get; set; }

        
        public virtual ICollection<BankTransaction> BankTransactions { get; set; }

        
        public virtual ICollection<Deposit> Deposits { get; set; }
    }
}
