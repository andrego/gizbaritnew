using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities
{
    public partial class Currency:IEntity<String>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Currency()
        {
            CurrenciesRates = new HashSet<CurrenciesRate>();
            Documents = new HashSet<Document>();
        }

        [Key]
        [StringLength(3)]
        public string ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(20)]
        public string Country { get; set; }

        [StringLength(5)]
        public string Sign { get; set; }

        
        public virtual ICollection<CurrenciesRate> CurrenciesRates { get; set; }

        public virtual ICollection<BankTransaction> BankTransactions { get; set; } 
        
        public virtual ICollection<Document> Documents { get; set; }
    }
}
