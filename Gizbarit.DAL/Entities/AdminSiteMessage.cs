using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Entities
{
    public partial class AdminSiteMessage
    {
        public int ID { get; set; }

        [Required]
        public string Text { get; set; }

        public int PageId { get; set; }

        public bool? Active { get; set; }
    }
}
