using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    public partial class CategoryDrawing
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DrawingCategoryID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(150)]
        public string DrawingName { get; set; }
    }
}
