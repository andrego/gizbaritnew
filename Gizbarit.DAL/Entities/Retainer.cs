using System;
using System.ComponentModel.DataAnnotations.Schema;
using Gizbarit.DAL.Entities.Organization;

namespace Gizbarit.DAL.Entities
{
    public partial class Retainer
    {
        public int ID { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public int ClientID { get; set; }

        public int OrganizationID { get; set; }

        public virtual Client Client { get; set; }

        public virtual Organization.Organization Organization { get; set; }
    }
}
