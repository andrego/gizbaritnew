namespace Gizbarit.DAL.Entities
{
    public partial class Country
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Name_Hebrew { get; set; }
    }
}
