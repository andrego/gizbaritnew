using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gizbarit.DAL.Entities
{
    public partial class Bundle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Bundle()
        {
            Organizations = new HashSet<Organization.Organization>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public string Descritption { get; set; }

        public double Price { get; set; }

        public int Quota { get; set; }

        public int Months { get; set; }

        public short Type { get; set; }

        public bool Public { get; set; }

        public virtual BundleType BundleType { get; set; }

        
        public virtual ICollection<Organization.Organization> Organizations { get; set; }
    }
}
