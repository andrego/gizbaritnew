using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL.Entities.Payment
{
    public partial class Payment : IEntity<int>
    {
        public int ID { get; set; }

        public Guid DocumentID { get; set; }

        public byte PaymentType { get; set; }

        public double Amount { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        [StringLength(50)]
        public string BankName { get; set; }

        [StringLength(50)]
        public string AccountNumber { get; set; }

        [StringLength(50)]
        public string PaymentNumber { get; set; }

        public int? PayerID { get; set; }

        [StringLength(5)]
        public string ExpirationDate { get; set; }

        public int? NumberOfPayments { get; set; }

        public int? CreditCardType { get; set; }

        [StringLength(50)]
        public string BranchName { get; set; }

        public Guid? DepositID { get; set; }

        public bool? IsInvoiceIssued { get; set; }

        public virtual Document Document { get; set; }

        public virtual Document Document1 { get; set; }
        public virtual Client Payer { get; set; }
    }
}
