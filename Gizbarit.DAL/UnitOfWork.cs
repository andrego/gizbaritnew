﻿using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using Gizbarit.DAL.Interfaces;
using NLog;

namespace Gizbarit.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GizbratDataContext _dataContext = new GizbratDataContext();
        protected readonly Logger _logger = LogManager.GetCurrentClassLogger();


        public UnitOfWork(GizbratDataContext dataContext)
        {
            _dataContext = dataContext;
            _dataContext.Configuration.LazyLoadingEnabled = true;
        }

        #region Implementation of IUnitOfWork

        /// <summary>
        /// Return a IQueryable of all data objects of the specified type.
        /// </summary>
        public virtual IQueryable<T> Get<T>()
            where T : class
        {
            return _dataContext.Set<T>();
        }

        /// <summary>
        /// Marks object as added in the unit of work.
        /// </summary>
        public void Add<T>(T entity)
            where T : class
        {
            _dataContext.Set<T>().Add(entity);
        }
        /// <summary>
        /// Marks object as removed in the unit of work.
        /// </summary>
        public void Remove<T>(T entity)
            where T : class
        {
            _dataContext.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Commits current changes.
        /// </summary>
        public void Commit()
        {
            try
            {
                _dataContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _logger.Error("Exception in EFUnitOfWork -> Commit. Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                    }
                }
                throw;
            }
            //catch (Exception ex)
            //{
            //    throw;
            //}
        }

        /// <summary>
        /// Return a IQueryable of all data objects of the specified type.
        /// </summary>
        public virtual IEnumerable<T> SqlQuery<T>(string sql, params object[] parameters)
            where T : class
        {
            return _dataContext.Database.SqlQuery<T>(sql, parameters).ToList();
        }

        #endregion

        #region Implementation of IDisposable

        /// <summary>
        /// Cleans up external resources.
        /// </summary>
        public void Dispose()
        {
            _dataContext.Dispose();
        }

       

        #endregion

    }
}
