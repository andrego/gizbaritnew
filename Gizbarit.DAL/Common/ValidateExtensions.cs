﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gizbarit.DAL.Common
{
    public static class ValidateExtensions
    {
        public static bool IsEmpty(this string context)
        {
            return string.IsNullOrWhiteSpace(context);
        }

        public static bool IsNotEmpty(this string context)
        {
            return !string.IsNullOrWhiteSpace(context);
        }

        public static bool IsEmpty<T>(this IEnumerable<T> context)
        {
            return context == null || !context.Any();
        }

        public static bool IsNotEmpty<T>(this IEnumerable<T> context)
        {
            return context != null && context.Any();
        }

        public static bool IsNull(this object context)
        {
            return context == null;
        }

        public static bool IsNotNull(this object context)
        {
            return context != null;
        }

        public static T AssertNotNull<T>(this T context, string name = "context")
        {
            if (context == null)
            {
                throw new ArgumentNullException(name);
            }

            return context;
        }

        public static string AssertNotEmpty(this string context, string name = "context")
        {
            if (context.IsEmpty())
            {
                throw new ArgumentNullException(name);
            }

            return context;
        }

        public static IEnumerable<T> AssertNotEmpty<T>(this IEnumerable<T> context, string name = "context")
        {
            var assertNotEmpty = context as T[] ?? context.ToArray();
            if (context == null || assertNotEmpty.Any())
            {
                throw new ArgumentNullException(name);
            }

            return assertNotEmpty;
        }
    }
}
