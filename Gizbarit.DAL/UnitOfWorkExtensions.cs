﻿using System.Reflection;

namespace Gizbarit.DAL
{
    public static class UnitOfWorkExtensions
    {
        public static void SetProperty(this object obj, string propertyName, object value)
        {
            PropertyInfo prop = obj.GetType().GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance);
            if (null != prop && prop.CanWrite)
            {
                prop.SetValue(obj, value, null);
            }
        }
    }
}
