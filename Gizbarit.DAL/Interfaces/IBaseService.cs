﻿using System.Linq;

namespace Gizbarit.DAL.Interfaces
{
    public interface IBaseService<T, TKey> where T : class
    {
        T Get(TKey id);
        T Create(T newItem, bool shouldBeCommited = false);
        void Update(T item, bool sholdBeCommited = false);
        IQueryable<T> GetAll();
        void Remove(T removeItem, bool shouldBeCommited = false);
        void Commit(bool saveChanges = false);
    }
}
