﻿using System.ComponentModel.DataAnnotations;

namespace Gizbarit.DAL.Interfaces
{
    public interface IEntity : IEntity<int>
    {
        new int ID { get; set; }
    }

    public interface IEntity<T>
    {
        [Key]
        T ID { get; set; }
    }
}
