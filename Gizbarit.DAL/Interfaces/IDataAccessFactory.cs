﻿namespace Gizbarit.DAL.Interfaces
{
    public interface IDataAccessFactory
    {
        IUnitOfWork CreateUnitOfWork();

        GizbratDataContext GetDbContext();
    }
}
