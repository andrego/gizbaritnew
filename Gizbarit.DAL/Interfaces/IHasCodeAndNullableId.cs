﻿namespace Gizbarit.DAL.Interfaces
{
    public interface IHasCodeAndNullableId:IHasCode
    {
        int? ID { get; set; }
    }

    public interface IHasCodeAndId: IHasCode
    {
        int ID { get; set; }
    }
    public interface IHasCode
    {
        string Code { get; set; }
    }
}
