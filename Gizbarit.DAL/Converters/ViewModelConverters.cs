﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Models.Account;
using Gizbarit.DAL.Models.Account.Bank;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Models.Organisation;
using Gizbarit.DAL.Models.Payment;
using Gizbarit.DAL.Models.Settlements;
using Gizbarit.DAL.Models.Store;
using Gizbarit.Utils;

namespace Gizbarit.DAL.Converters
{
    public static class ViewModelConverters
    {
        public static BankAccount ToBankAccountEntity(this BankAccountViewModel ba)
        {
            return new BankAccount
            {
                IsDeleted = false,
                AccountNumber = ba.AccountNo.ToString(),
                BranchName = ba.Branch,
                ExternalNumber = 0,
                BankNameId = (short) ba.BankId,
                Active = ba.IsActive == 1,
                InitialBalance = ba.AccountBalance,
                Limit = ba.Limit
            };
        }

        public static Organization ToOrganizationEntity(this BaseAccountProfile account)
        {
            var organisation = new Organization
            {
                //Entity.Value = this.Value
                Password = account.Password,
                Email = account.Email,
                FirstName = account.FirstName,
                LastName = account.LastName,
                Phone1 = account.Phone,
                Fax = account.Fax,
                City = account.City,
                Address = account.Street,
                Name = account.BusinessName,
                UniqueID = account.BusinessId,
                Zip = account.Zip,
                AdvancesRate = account.IncomeTax,
                AdvancesAccountingMethod = (short) account.TaxPeriodId,
                AccountingMethod = (short) account.VatPeriodId,
                BusinessTypeId = (byte) account.BusinesTypeId,
                TaxRate = account.BusinesTypeId == 2 ? 0 : 17,
                RegisterType = (Enums.RegisterType) account.RegisterTypeId,
                NameEng = account.BusinessNameEng,
                AddressEng = account.StreetEng,
            };

            var imageName = $"{account.TempLogoId}.{account.LogoExtension}";
            if (File.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Logos/"), imageName)))
            {
                organisation.PathToLogo = imageName;
            }
            return organisation;
        }

        public static DocumentNumbering ToDocumentNumbering(this InitialDocumentNumbers initialDocumentNumbers, bool isSmallBusines = false)
        {
            return new DocumentNumbering()
            {
                Invoice = initialDocumentNumbers.InvoiceNo,
                InvoiceQuote = initialDocumentNumbers.InvoiceQouteNo,
                InvoiceShip = initialDocumentNumbers.InvoiceShipNo,
                ProformaInvoice = initialDocumentNumbers.ProformaInvoiceNo,
                InvoiceReceipt = !isSmallBusines? initialDocumentNumbers.InvoiceReceiptNo: initialDocumentNumbers.ReceiptNo,
                Receipt = !isSmallBusines ? initialDocumentNumbers.ReceiptNo : initialDocumentNumbers.InvoiceReceiptNo,
            };
        }

        public static BaseAccountProfile ToBaseAccountProfile(this Organization currentOrganisationData)
        {
            return new BaseAccountProfile
            {
                Id = currentOrganisationData.ID,
                FirstName = currentOrganisationData.FirstName,
                LastName = currentOrganisationData.LastName,
                City = currentOrganisationData.City,
                Street = currentOrganisationData.Address,
                Email = currentOrganisationData.Email,
                EmailValidation = currentOrganisationData.Email,
                Password = currentOrganisationData.Password,
                PasswordValidation = currentOrganisationData.Password,
                Zip = currentOrganisationData.Zip,
                Fax = currentOrganisationData.Fax,
                Phone = currentOrganisationData.Phone1,
                BusinessName = currentOrganisationData.Name,
                BusinessId = currentOrganisationData.UniqueID,
                BusinessIdValidation = currentOrganisationData.UniqueID,
                PathToLogo = currentOrganisationData.PathToLogo,
                VatPeriodId = currentOrganisationData.AccountingMethod,
                IncomeTax = Convert.ToInt32(currentOrganisationData.AdvancesRate),
                TaxPeriodId = currentOrganisationData.AdvancesAccountingMethod,
                BusinesTypeId = currentOrganisationData.BusinessTypeId,
                RegisterTypeId = (int)currentOrganisationData.RegisterType,
                BusinessNameEng = currentOrganisationData.NameEng,
                StreetEng = currentOrganisationData.AddressEng,
                RegisterTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = Translations.Register.Company, Value = ((int)Enums.RegisterType.Company).ToString()},
                    new SelectListItem {Text = Translations.Register.Private, Value = ((int)Enums.RegisterType.Private).ToString()},
                    new SelectListItem {Text = Translations.Register.Partnership, Value = ((int)Enums.RegisterType.Partnership).ToString()},
                    new SelectListItem {Text = Translations.Register.NonProfit, Value = ((int)Enums.RegisterType.NonProfit).ToString()},

                }
        };
        }

        public static BaseAccountShort ToBaseAccountShort(this Organization currentOrganisationData)
        {
            return new BaseAccountShort
            {
                Address = currentOrganisationData.Address,
                Email = currentOrganisationData.Email,
                Fax = currentOrganisationData.Fax,
                Phone = currentOrganisationData.Phone1,
                BusinessId = currentOrganisationData.UniqueID,
                PathToLogo = currentOrganisationData.PathToLogo,
                BusinesName = currentOrganisationData.Name,
                RegisterType = currentOrganisationData.RegisterType,
                AddressEng = currentOrganisationData.AddressEng,
                BusinesNameEng = currentOrganisationData.NameEng,
                BookKeppengEmail = currentOrganisationData.BookKeepingEmail
            };
        }

        public static LoginUserVerifyResult ToLoginUserVerifyResult(this Organization org)
        {
            return new LoginUserVerifyResult
            {
                ID = org.ID,
                BusinessType = org.BusinessTypeId,
                FirstName = org.FirstName,
                LastName = org.LastName,
                AccountingMethod = org.AccountingMethod,
                Email = org.Email,
                ExpirationDate = org.PaidPeriodExpirationDate,
                Quota = org.Bundle.Quota,
                RegisterType = org.RegisterType
            };
        }

        public static BankAccountMiniViewModel ToAccountMiniViewModel(this BankAccount ac)
        {
            return new BankAccountMiniViewModel
            {
                Id = ac.ID,
                AccountNumber = ac.AccountNumber,
                BankName = ac.BankName.Name,
                IsDefault = ac.Active,
                Limit = Math.Round(ac.Limit, 2)
            };
        }

        public static ClientViewModel ToClientViewModel(this Client model)
        {
            return new ClientViewModel()
            {
                Id = model.ID,
                Name = model.Name,
                Address = model.Address,
                Email = model.Email,
                City = model.City,
                UniqueId = model.UniqueId,
                Fax = model.Fax,
                Phone = model.Phone,

                AdditionEmails = model.ClientEmailContacts.Select(m => m.Email),
                AdditionPhones = model.ClientPhoneContacts.Select(m => m.Phone)
            };
        }

        public static SupplierViewModel ToSupplierViewModel(this Supplier s)
        {
            return new SupplierViewModel
            {
                Id = s.ID,
                Name = s.Name,
                Email = s.Email,
                Address = s.Address,
                Phone = s.Phone
            };
        }

        public static SupplierReceiptViewModel ToSupplierReceiptViewModel(this SupplierReceipt r)
        {
            return new SupplierReceiptViewModel
            {
                Id = r.ID,
                Number = r.Number,
                Date = r.PaymentDate,
                Amount = r.Amount
            };
        }

        public static SupplierInvoiceViewModel ToSupplierInvoiceViewModel(this SupplierInvoice i)
        {
            return new SupplierInvoiceViewModel
            {
                Id = i.ID,
                Number = i.Number,
                Date = i.Date,
                Total = i.Amount,
                IsConfirmed = i.IsConfirmed
            };
        }

        public static Models.Documents.Related.DocumentCatalogItemViewModel ToCatalogItemViewModel(this CatalogItem c)
        {
            return new Models.Documents.Related.DocumentCatalogItemViewModel
            {
                ID = c.ID,
                Name = c.Name,
                Code = c.Code,
                Price = c.Price ?? 0,
                ImagePath = c.ImagePath,
                SupplierPrice = c.SupplierPrice
            };
        }

        public static PaymentTypeViewModel ToPaymentTypeViewModel(this PaymentType t)
        {
            return new PaymentTypeViewModel {Id = t.ID, Name = t.Name};
        }

        public static IEnumerable<PaymentViewModel> ToPayments(this IEnumerable<Payment> payments)
        {
            return payments.Select(x => new PaymentViewModel
            {
                idPayment = x.ID,
                PaymentType = x.PaymentType,
                BranchName = x.BranchName,
                BankName = x.BankName,
                Sum = x.Amount,
                DateOperation = x.Date,
                CreditCardType = x.CreditCardType,
                AccountNumber = x.AccountNumber,
                CheckNumber = x.PaymentNumber,
            });
        }

        public static BaseDocument ToBaseDocument(this Document document, Organization organization)
        {
             return  new BaseDocument
            {
                DateCreated = document.DateCreated,
                Number = document.Number,
                PaymentDueDate = document.PaymentDueDate,
                OrganizationId = organization.ID,
                PriceWithTax = document.Total,
                TotalWithoutRounding = document.TotalWithoutRounding,
                TaxAmount = document.TaxPercentage,
                TotalPrice = document.TotalwithoutTax,
                Tax= document.TotalTaxAmount,
                InUse = document.IsPaid,
                BusinessInfo = organization.ToBaseAccountShort(),
                Id = document.ID,
                DocumentType = document.DocumentType,
                IsTaxIncluded = document.IsTaxIncluded,
                Subject = document.Subject,
                ClientInfo = document.Client.ToClientViewModel(),
                IsRounded = document.IsRounded,
                IsSmallBusiness = document.IsSmallBusiness,
                Status = document.Status,

                IsExport = document.IsExport,
                DeliveryTerms = document.DeliveryTermsType,
                TermsOfPayment = document.TermsOfPaymentType,
                Currency = document.Currency.ID,
                PointOfDelivery = document.PointOfDelivery,
                ExportAditionInfo = document.ExportAditionInfo



               
            };
           
        }

        public static PaymentViewModel ToPaymentViewModel(this Payment p)
        {
            return new PaymentViewModel
            {
                AccountNumber = p.AccountNumber,
                DateOperation = p.Date,
                Sum = p.Amount,
            };
        }

        public static StoreCatalogItemViewModel ToViewModel(this StoreCatalogItem item)
        {
            if (item == null)
                return new StoreCatalogItemViewModel();

            if (item.UsedQuantity == null)
            {
                item.UsedQuantity =
                    item.StoreInvoice.Organization.Documents
                        .Where(
                            d =>
                                d.DocumentType == DocumentType.Waybill ||
                                d.DocumentType == DocumentType.TaxInvoice ||
                                d.DocumentType == DocumentType.TaxInvoiceReceipt)
                                .Where(d=>d.IsOriginalGenerated())
                        .Sum(
                            d => d.DocumentItems.Where(i => i.Code == item.CatalogItem.Code).Sum(cc=>cc.Quantity));
            }
            return new StoreCatalogItemViewModel
            {
                Id = item.ID,
                Code = item.CatalogItem.Code,
                Name = item.CatalogItem.Name,
                SupplierPrice = item.CatalogItem.SupplierPrice,
                Price = item.CatalogItem.SellingPrice,
                Quantity = item.Quantity,
                UsedQuantity = item.UsedQuantity,
                Supplier = item.StoreInvoice.Supplier.Name,
                SupplierId = item.StoreInvoice.SupplierId
            };
        }
    }
}