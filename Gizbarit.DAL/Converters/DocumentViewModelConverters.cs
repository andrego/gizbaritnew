﻿using System;
using System.Linq;
using System.Web;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Models.Documents.Related;
using Gizbarit.DAL.Models.Settlements;
using Gizbarit.DAL.Services.Document;
using Gizbarit.Utils;
using DocumentUtils = Gizbarit.DAL.Utils.DocumentUtils;

namespace Gizbarit.DAL.Converters
{
    public static class DocumentViewModelConverters //TODO refactor using inheritance
    {
        public static Document ToDocument(this PriceOfferViewModel model, Currency currency)
        {
            var ip = HttpContext.Current.Request.UserHostAddress;
            var document = new Document()
            {
                ID = model.BaseDocument.Id == default(Guid) ? Guid.NewGuid() : model.BaseDocument.Id,
                ClientID = model.BaseDocument.ClientInfo.Id,
                Total = model.BaseDocument.PriceWithTax,
                TotalWithoutRounding = model.BaseDocument.TotalWithoutRounding,
                TotalwithoutTax = model.BaseDocument.TotalPrice,
                //TotalTaxAmount = model.BaseDocument.PriceWithTax - model.BaseDocument.TotalPrice,
                TotalTaxAmount = model.BaseDocument.Tax,
                Subject = model.BaseDocument.Subject,
                TaxPercentage = model.BaseDocument.TaxAmount,
                UserIP = ip,
                PaymentDueDate = model.BaseDocument.DateCreated.AddDays(model.PaymentDueDateDays ?? 0),
                DocumentType = DocumentType.PriceOffer,
                ActualCreationDate = DateTime.UtcNow,
                DateCreated = model.BaseDocument.DateCreated,
                Currency = currency,
                IsTaxIncluded = model.BaseDocument.IsTaxIncluded,
                InternalComments = model.OfferExtras,
                IsRounded = model.BaseDocument.IsRounded,

                IsExport = model.BaseDocument.IsExport,
                DeliveryTermsType = model.BaseDocument.DeliveryTerms,
                TermsOfPaymentType = model.BaseDocument.TermsOfPayment,
                PointOfDelivery = model.BaseDocument.PointOfDelivery,
                ExportAditionInfo = model.BaseDocument.ExportAditionInfo,
                TimeOfDelivery = model.BaseDocument.TimeOfDelivery,

                Email = model.BaseDocument.ClientInfo.Email,
                Phone = model.BaseDocument.ClientInfo.Phone
            };
            return document;
        }

        public static Document ToDocument(this WayBillViewModel model, Currency currency)
        {
            var ip = HttpContext.Current.Request.UserHostAddress;
            var document = new Document()
            {
                ID = model.BaseDocument.Id == default(Guid) ? Guid.NewGuid() : model.BaseDocument.Id,
                ClientID = model.BaseDocument.ClientInfo.Id,
                Total = model.BaseDocument.PriceWithTax,
                TotalWithoutRounding = model.BaseDocument.TotalWithoutRounding,
                TotalwithoutTax = model.BaseDocument.TotalPrice,
                //TotalTaxAmount = model.BaseDocument.PriceWithTax - model.BaseDocument.TotalPrice,
                TotalTaxAmount = model.BaseDocument.Tax,
                Subject = model.BaseDocument.Subject,
                TaxPercentage = model.BaseDocument.TaxAmount,
                UserIP = ip,
                DocumentType = DocumentType.Waybill,
                ActualCreationDate = DateTime.UtcNow,
                DateCreated = model.BaseDocument.DateCreated,
                Currency = currency,
                IsTaxIncluded = model.BaseDocument.IsTaxIncluded,
                IsRounded = model.BaseDocument.IsRounded,

                Email = model.BaseDocument.ClientInfo.Email,
                Phone = model.BaseDocument.ClientInfo.Phone

            };
            return document;
        }

        public static Document ToPreliminaryInvoiceDocument(this WayBillViewModel model, Currency currency)
        {
            var document = model.ToDocument(currency);
            document.DocumentType = DocumentType.PreliminaryInvoice;
            return document;
        }

        public static Document ToDocument(this TaxInvoiceViewModel model, Currency currency)
        {
            var ip = HttpContext.Current.Request.UserHostAddress;
            var document = new Document()
            {
                ID = model.BaseDocument.Id == default(Guid) ? Guid.NewGuid() : model.BaseDocument.Id,
                ClientID = model.BaseDocument.ClientInfo.Id,
                Total = model.BaseDocument.PriceWithTax,
                TotalWithoutRounding = model.BaseDocument.TotalWithoutRounding,
                TotalwithoutTax = model.BaseDocument.TotalPrice,
                //TotalTaxAmount = model.BaseDocument.PriceWithTax - model.BaseDocument.TotalPrice,
                TotalTaxAmount = model.BaseDocument.Tax,
                Subject = model.BaseDocument.Subject,
                TaxPercentage = model.BaseDocument.TaxAmount,
                UserIP = ip,
                DocumentType = DocumentType.TaxInvoice,
                ActualCreationDate = DateTime.UtcNow,
                DateCreated = model.BaseDocument.DateCreated,
                Currency = currency,
                InternalComments = model.OfferExtras,
                IsTaxIncluded = model.BaseDocument.IsTaxIncluded,
                PaymentDueDate = DocumentUtils.GetDocumentPayDate(model.BaseDocument.DateCreated, model.PaymentDueDateDays ?? 0),
                IsRounded = model.BaseDocument.IsRounded,

                IsExport = model.BaseDocument.IsExport,
                DeliveryTermsType = model.BaseDocument.DeliveryTerms,
                TermsOfPaymentType = model.BaseDocument.TermsOfPayment,
                PointOfDelivery = model.BaseDocument.PointOfDelivery,
                ExportAditionInfo = model.BaseDocument.ExportAditionInfo,
                TimeOfDelivery = model.BaseDocument.TimeOfDelivery,

                Email = model.BaseDocument.ClientInfo.Email,
                Phone = model.BaseDocument.ClientInfo.Phone
        };

          return document;
        }

        public static Document ToDocument(this PreliminaryInvoiceViewModel model, Currency currency)
        {
            var ip = HttpContext.Current.Request.UserHostAddress;
            var document = new Document()
            {
                ID = model.BaseDocument.Id == default(Guid) ? Guid.NewGuid() : model.BaseDocument.Id,
                ClientID = model.BaseDocument.ClientInfo.Id,
                Total = model.BaseDocument.PriceWithTax,
                TotalWithoutRounding = model.BaseDocument.TotalWithoutRounding,
                TotalwithoutTax = model.BaseDocument.TotalPrice,
                //TotalTaxAmount = model.BaseDocument.PriceWithTax - model.BaseDocument.TotalPrice,
                TotalTaxAmount = model.BaseDocument.Tax,
                Subject = model.BaseDocument.Subject,
                TaxPercentage = model.BaseDocument.TaxAmount,
                UserIP = ip,
                DocumentType = DocumentType.PreliminaryInvoice,
                ActualCreationDate = DateTime.UtcNow,
                DateCreated = model.BaseDocument.DateCreated,
                PaymentDueDate = DocumentUtils.GetDocumentPayDate(model.BaseDocument.DateCreated, model.PaymentDueDateDays ?? 0),
                Currency = currency,
                InternalComments = model.OfferExtras,
                IsTaxIncluded = model.BaseDocument.IsTaxIncluded,
                IsRounded = model.BaseDocument.IsRounded,

                IsExport = model.BaseDocument.IsExport,
                DeliveryTermsType = model.BaseDocument.DeliveryTerms,
                TermsOfPaymentType = model.BaseDocument.TermsOfPayment,
                PointOfDelivery = model.BaseDocument.PointOfDelivery,
                ExportAditionInfo = model.BaseDocument.ExportAditionInfo,
                TimeOfDelivery = model.BaseDocument.TimeOfDelivery,

                Email = model.BaseDocument.ClientInfo.Email,
                Phone = model.BaseDocument.ClientInfo.Phone
            };
            return document;
        }

        public static Document ToDocument(this ReceiptViewModel model, Currency currency)
        {
            var ip = HttpContext.Current.Request.UserHostAddress;
            var orgId = HttpContext.Current.User.Identity.GetOrgId();
            var document = new Document()
            {
                //ID = model.BaseDocument.Id == default(Guid) ? Guid.NewGuid() : model.BaseDocument.Id,
                ID = model.BaseDocument.Id == default(Guid) ? Guid.Empty : model.BaseDocument.Id,
                ClientID = model.BaseDocument.ClientInfo.Id,
                UserIP = ip,
                DocumentType = DocumentType.Receipt,
                ActualCreationDate = DateTime.UtcNow,
                DateCreated = model.BaseDocument.DateCreated,
                Currency = currency,
                OrganizationID = orgId,
                Total = model.PaymentsTotal,

                Email = model.BaseDocument.ClientInfo.Email,
                Phone = model.BaseDocument.ClientInfo.Phone
            };
            return document;
        }

        public static Document ToDocument(this InvoiceReceiptViewModel model, Currency currency, string ip)
        {
            var document = new Document()
            {
                ID = model.BaseDocument.Id == default(Guid) ? Guid.NewGuid() : model.BaseDocument.Id,
                ClientID = model.BaseDocument.ClientInfo.Id,
                Total = model.BaseDocument.PriceWithTax,
                TotalWithoutRounding = model.BaseDocument.TotalWithoutRounding,
                TotalwithoutTax = model.BaseDocument.TotalPrice,
                //TotalTaxAmount = model.BaseDocument.PriceWithTax - model.BaseDocument.TotalPrice,
                TotalTaxAmount = model.BaseDocument.Tax,
                Subject = model.BaseDocument.Subject,
                TaxPercentage = model.BaseDocument.TaxAmount,
                UserIP = ip,
                DocumentType = DocumentType.TaxInvoiceReceipt,
                ActualCreationDate = DateTime.UtcNow,
                DateCreated = model.BaseDocument.DateCreated,
                InternalComments = model.OfferExtras,
                PaymentDueDate = DocumentUtils.GetDocumentPayDate(model.BaseDocument.DateCreated, model.PaymentDueDateDays ?? 0),
                Currency = currency,
                IsTaxIncluded = model.BaseDocument.IsTaxIncluded,
                IsRounded = model.BaseDocument.IsRounded,

                Email = model.BaseDocument.ClientInfo.Email,
                Phone = model.BaseDocument.ClientInfo.Phone,
                TimeOfDelivery = model.BaseDocument.TimeOfDelivery
            };
            return document;
        }

        public static DocumentItem ToDocumentItem(this DocumentCatalogItemViewModel model, Document document)
        {
            return new DocumentItem
            {
                DocumentID = document.ID,
                WaybillID = null,
                CatalogID = model.ID == -1 ? null : model.ID,
                Name = model.Name,
                Price = model.Price,
                Quantity = model.Quantity,
                TotalTax =
                    Math.Round((model.CalculateItemPrice() * document.TaxPercentage)/100, 2),
                TaxPercentage = document.TaxPercentage,
                Discount = model.Discount ?? 0,
                Code = model.Code,
                ImagePath = model.ImagePath,
                Total = model.CalculateItemPrice()
            };
        }

        public static double CalculateItemPrice(this DocumentCatalogItemViewModel model)
        {
            return model.Price*model.Quantity -
                   model.Price*model.Quantity*
                   (model.Discount.HasValue && model.Discount != 0 ? 1/model.Discount.Value : 0);
        }

        public static DocumentReference ToDocumentReference(this DocumentCatalogItemViewModel model, DocumentType type,
            double credit = 0)
        {
            return new DocumentReference
            {
                ReferenceDocumentId = model.DocumentId ?? Guid.Empty,
                Type = type,
                CreditAmount = credit
            };
        }

        public static DocumentCatalogItemViewModel ToDocumentItemViewModel(this DocumentItem model)
        {
            return new DocumentCatalogItemViewModel
            {
                ID = model.CatalogID,
                Name = model.Name,
                DocumentId = model.DocumentID,
                Price = model.Price,
                Quantity = model.Quantity,
                Discount = model.Discount ?? 0,
                Code = model.Code,
                ImagePath = model.ImagePath,

                Currency = model.Document.Currency.ID,
                DeliveryTermsOfPayments = model.Document.DeliveryTermsType.ToString(),
                TermsOfPaymentType = model.Document.TermsOfPaymentType.ToString(),

                PointOfDelivery = model.Document.PointOfDelivery,
                ExportAditionInfo = model.Document.ExportAditionInfo,
                TimeOfDelivery = model.Document.TimeOfDelivery

            };
        }

        public static SettlementsWithCustommerViewModel ToSettlementsViewModel(this Document d,
            IDocumentService documentService)
        {
            var result = new SettlementsWithCustommerViewModel
            {
                InvoiceType = d.DocumentType,
                PreliminaryInvoice = d.ID,
                PreliminaryInvoiceNumber = d.DocumentType == DocumentType.TaxInvoice ||
                                           d.DocumentType == DocumentType.TaxInvoiceReceipt
                    ? 0
                    : (long) d.Number,
                Invoice = d.DocumentType == DocumentType.TaxInvoice ||
                          d.DocumentType == DocumentType.TaxInvoiceReceipt
                    ? d.ID
                    : Guid.Empty,
                InvoiceNumber = d.DocumentType == DocumentType.TaxInvoice ||
                                d.DocumentType == DocumentType.TaxInvoiceReceipt
                    ? d.Number
                    : 0,
                InvoiceTotal = d.DocumentType == DocumentType.TaxInvoice ||
                               d.DocumentType == DocumentType.TaxInvoiceReceipt
                    ? d.Total
                    : 0,
                PreliminaryInvoiceCreationDate = d.DateCreated,
                ClientName = d.Client.Name,
                Subject = d.Subject,
                TotalWithVat = d.Total,
                PaymentDate = d.PaymentDueDate ?? d.DateCreated,

                Currency = d.Currency.ID
            };

            if (d.DocumentType == DocumentType.PreliminaryInvoice)
            {
                result.ReceiptTotal = (d.IsExport)
                    ? d.Payments.Sum(m => m.Amount)
                    : documentService.GetDocumentPrepaymentSum(d.ID);
            }
            else
            {
                result.ReceiptTotal = 0;
            }

            //result.ReceiptTotal = d.DocumentType == DocumentType.PreliminaryInvoice
            //    ? documentService.GetDocumentPrepaymentSum(d.ID)
            //    : 0;
            result.InvoiceTotalReceiptSum = documentService.GetInvoiceTotalPrepaymentSum(d.ID);
            //result.TotalPaid = result.ReceiptTotal ?? 0;


            if (d.IsExport && result.InvoiceType == DocumentType.TaxInvoice) // need check if TotalPaid right after customerPayment's and in settlement table !!!!!!
                    {
                        result.TotalPaid = d.Payments.Sum(p => (double?)p.Amount) ?? 0;
                        //break;
                    }
            switch (result.InvoiceType)
            {
                case DocumentType.TaxInvoiceReceipt:
                    result.TotalPaid = d.Payments.Sum(p => (double?) p.Amount) ?? 0;
                    break;
                case DocumentType.TaxInvoice:
                case DocumentType.PreliminaryInvoice:
                    
                    //result.ReceiptTotal = d.DocumentType == DocumentType.PreliminaryInvoice
                    //       ? documentService.GetDocumentPrepaymentSum(d.ID)
                    //       : 0;
                    //result.TotalPaid = documentService.GetDocumentReceiptsSum(d.ID);
                    var invoiceDocument =
                        documentService.GetReferenceDocumentByRefId(result.PreliminaryInvoice);

                    if (invoiceDocument == null)
                        break;
                    result.Invoice = invoiceDocument.ID;
                    result.InvoiceType = invoiceDocument.DocumentType;
                    result.InvoiceNumber = invoiceDocument.Number;
                    result.InvoiceTotal = invoiceDocument.Total;

                    result.PaymentDate = invoiceDocument.PaymentDueDate ?? invoiceDocument.DateCreated;

                    var invoiceReceiptsSum = documentService.GetDocumentReceiptsSum(invoiceDocument.ID);

                    //second receipt
                    if (invoiceReceiptsSum > 0)
                    {
                        result.TotalPaid += invoiceReceiptsSum;
                    }

                    break;
            }
            result.InvoiceTotalReceiptSum = result.Invoice != Guid.Empty
                ? documentService.GetInvoiceTotalPrepaymentSum(result.Invoice)
                : 0;
            return result;
        }

        public static SettlementsWaybillViewModel ToWaybillViewModel(this Document d, IDocumentService documentService)
        {
            var model = new SettlementsWaybillViewModel
            {
                Number = d.Number,
                Date = d.DateCreated,
                Amount = d.Total,
                Subject = d.Subject,
                ClientName = d.Client.Name,
                WaybillId = d.ID
            };
            var preliminaryInvoice = documentService.GetAutoCreatedPreliminaryInvoice(d.ClientID, d.DateCreated);
            if (preliminaryInvoice != null)
            {
                model.PreliminaryInvoiceId = preliminaryInvoice.ID;
                model.PreliminaryInvoiceNumber = preliminaryInvoice.Number;
                model.PreliminaryInvoiceDate = preliminaryInvoice.DateCreated;
                model.TotalAmount = preliminaryInvoice.Total;
            }
            return model;
        }

        public static InvoiceReceiptViewModelContainer ToInvoiceReceiptViewModelContainer(this Document d)
        {
            return new InvoiceReceiptViewModelContainer
            {
                Id = d.ID,
                DateCreated = d.DateCreated,
                Total = d.TotalwithoutTax,
                ClientName = d.Client.Name,
                Number = d.Number,
                TotalWithVat = d.Total,
                VatAmount = d.TotalTaxAmount,
                Description = d.Subject,
                IsPrinted = d.Status == DocumentStatusType.Printed,
            };
        }

        public static ReceiptViewModel ToReceiptViewModel(this Document document)
        {
            return new ReceiptViewModel
            {
                BaseDocument =  new BaseDocument
                {
                    DateCreated = document.DateCreated,
                    Number = document.Number,
                    PaymentDueDate = document.PaymentDueDate,
                    PriceWithTax = document.Total,
                    TotalWithoutRounding = document.TotalWithoutRounding,
                    TaxAmount = document.TaxPercentage,
                    TotalPrice = document.TotalwithoutTax,
                    Tax = document.Total - document.TotalwithoutTax,
                    InUse = document.IsPaid,
                    Id = document.ID,
                    DocumentType = document.DocumentType,
                    IsTaxIncluded = document.IsTaxIncluded,
                    Subject = document.Subject,
                    ClientInfo = document.Client.ToClientViewModel(),
                    IsRounded = document.IsRounded,
                    IsSmallBusiness = document.IsSmallBusiness
                },
                Payments = document.Payments.Select(p=>p.ToPaymentViewModel()),
                OtherDescription = document.ExternalComments,
                PaymentsTotal = document.Payments.Sum(p=>p.Amount)
            };
        }

        public static ReceiptViewModelContainer ToReceiptViewModelContainer(this Document d)
        {
            if (d.DocumentType == DocumentType.TaxInvoiceReceipt)
            {
                return new ReceiptViewModelContainer
                {
                    Id = d.ID,
                    DateCreated = d.DateCreated,
                    Amount = d.Total,
                    ClientName = d.Client.Name,
                    Number = d.Number,
                    IsPrinted = d.Status == DocumentStatusType.Printed || d.Status == DocumentStatusType.Send
                };
            }
            return new ReceiptViewModelContainer
            {
                Id = d.ID,
                DateCreated = d.DateCreated,
                Amount = d.Total,
                ClientName = d.Client.Name,
                Deduction = d.Receipt.Deduction,
                Number = d.Number,
                ReferenceDocumentId = d.Receipt.ReferenceDocument,
                 IsPrinted = d.Status == DocumentStatusType.Printed || d.Status == DocumentStatusType.Send
            };
        }
    }
}