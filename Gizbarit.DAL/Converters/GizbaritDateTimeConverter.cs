﻿using System;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Gizbarit.API.Results
{
    public class GizbaritDateTimeConverter:DateTimeConverterBase
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((DateTime)value).ToString("dd.MM.yyyy"));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            DateTime parseDate;
            string[] datePaterns = new string[] { "dd.MM.yyyy", "dd.MM.yyyy hh.mm.ss" };

            foreach (var item in datePaterns)
            {

                if (DateTime.TryParseExact(reader.Value.ToString(), item, CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out parseDate))
                    return parseDate;
            }


            return DateTime.Parse(reader.Value.ToString());
        }
    }
}