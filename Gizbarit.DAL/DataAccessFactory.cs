﻿using System;
using Gizbarit.DAL.Interfaces;

namespace Gizbarit.DAL
{
    /// <summary>
    /// Implements IDataAccessFactory for working with EntityFramework DbContext.
    /// </summary>
    /// <typeparam name="T">Specific DbContext used to access the DB. Must have a constructor taking a connection string as the single parameter.</typeparam>
    public class DataAccessFactory<T> : IDataAccessFactory
        where T : GizbratDataContext
    {
        private readonly string _connectionString;

        public DataAccessFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Instantiates a new UnitOfWork.
        /// </summary>
        public IUnitOfWork CreateUnitOfWork()
        {
            var context = GetDbContext();
            return new UnitOfWork(context);
        }

        public GizbratDataContext GetDbContext()
        {
            var context = (GizbratDataContext)Activator.CreateInstance(typeof(T), _connectionString);
            return context;
        }
    }
}
