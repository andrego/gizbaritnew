﻿using System;
using System.Security.Principal;
using Gizbarit.Identity.Providers;

namespace Gizbarit.Web.Providers
{
    [Serializable]
    public class CustomPrincipal : IPrincipal
    {
        #region Implementation of IPrincipal

        /// <summary>
        /// Determines whether the current principal belongs to the specified role.
        /// </summary>
        /// <returns>
        /// true if the current principal is a member of the specified role; otherwise, false.
        /// </returns>
        /// <param name="role">The name of the role for which to check membership. </param>
        public bool IsInRole(string role)
        {
            var customIdentity = Identity as CustomIdentity;
            var userName = customIdentity == null ? Identity.Name : customIdentity.Email;

            var isInRole = System.Web.Security.Roles.Provider.IsUserInRole(userName, role);

            return isInRole || role.Equals(customIdentity?.UserRoleName);
        }

        /// <summary>
        /// Gets the identity of the current principal.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Security.Principal.IIdentity"/> object associated with the current principal.
        /// </returns>
        public IIdentity Identity { get; private set; }

        #endregion

        public CustomIdentity CustomIdentity { get { return (CustomIdentity)Identity; } set { Identity = value; } }

        public CustomPrincipal(CustomIdentity identity)
        {
            Identity = identity;
        }
    }
}
