﻿using System;
using System.Web.Security;

namespace Gizbarit.Web.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        private int _cacheTimeoutInMinutes = 30;
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            int val;
            if (!string.IsNullOrEmpty(config["cacheTimeoutInMinutes"]) && Int32.TryParse(config["cacheTimeoutInMinutes"].ToString(), out val))
                _cacheTimeoutInMinutes = val;


            base.Initialize(name, config);
        }
        public override bool IsUserInRole(string Email, string roleName)
        {
            bool outputResult = true;


            return outputResult;

        }
        public override string[] GetRolesForUser(string roleId)
        {
            string[] role = new string[] { };
            //using (HostViteDbEntities _db = new HostViteDbEntities())
            //{
            //    try
            //    {
            //        Role userRole = _db.Roles.Find(int.Parse(roleId));

            //        if (userRole != null)
            //        {
            //            role = new string[] { userRole.RoleName };

            //            if (userRole.NumberRole == 1)
            //            {
            //                UserProperty.User.Roles = NameRoles.Administrator;
            //            }
            //            else if (userRole.NumberRole == 2)
            //            {
            //                UserProperty.User.Roles = NameRoles.Manager;
            //            }
            //            else if (userRole.NumberRole == 3)
            //            {
            //                UserProperty.User.Roles = NameRoles.User;
            //            }
            //            else if (userRole.NumberRole == 4)
            //            {
            //                UserProperty.User.Roles = NameRoles.Host;
            //            }
            //        }
            //    }
            //    catch
            //    {
            //        role = new string[] { };
            //    }
            //}
            return role;
        }

        public override void CreateRole(string roleName)
        {
            //Role newRole = new Role() { RoleName = roleName };
            //HostViteDbEntities db = new HostViteDbEntities();
            //db.Roles.Add(newRole);
            //db.SaveChanges();
        }

        #region  ==========  ENTRY function  ==========
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }
        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }
        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }
        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
