﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Security;
using Gizbarit.DAL;
using Gizbarit.DAL.Common;
using Gizbarit.DAL.Entities;

namespace Gizbarit.Web.Providers
{
   public class DbRoleProvider: RoleProvider
   {
        private const string AdminRole = "admin";
        private readonly Lazy<IDictionary<string, AdminUser>> _adminUsersLazy = new Lazy<IDictionary<string, AdminUser>>(GetAdminUsers);
       private IDictionary<string, AdminUser> AdminUsers => _adminUsersLazy.Value;

        public override string ApplicationName { get; set; } = "/";

       public override void Initialize(string name, NameValueCollection config)
       {
           base.Initialize(name, config);
       }

       public override bool IsUserInRole(string username, string roleName)
        {
            if (username.IsEmpty() || roleName.IsEmpty())
            {
                return false;
            }

            var userInRole = AdminUsers.ContainsKey(username);

            return userInRole;
        }

        public override string[] GetAllRoles()
        {
            return new[] { AdminRole };
        }

        public override string[] GetRolesForUser(string username)
        {
            if (username.IsEmpty() || !AdminUsers.ContainsKey(username))
            {
                return new string[0];
            }

            return GetAllRoles();
        }

        public override bool RoleExists(string roleName)
        {
            return AdminRole.Equals(roleName, StringComparison.InvariantCultureIgnoreCase);
        }

        public override string[] GetUsersInRole(string roleName)
        {
            return AdminUsers.Values.Select(x => x.UserName).ToArray();
        }

        #region Not Implemented members

        public override void CreateRole(string roleName)
       {
           throw new NotImplementedException();
       }

       public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
       {
           throw new NotImplementedException();
       }

       public override void AddUsersToRoles(string[] usernames, string[] roleNames)
       {
           throw new NotImplementedException();
       }

       public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
       {
           throw new NotImplementedException();
       }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
       {
           throw new NotImplementedException();
       }
       

       #endregion

        private static Dictionary<string, AdminUser> GetAdminUsers()
        {
            using (var context = new GizbratDataContext())
            {
                var adminUsers = context.AdminUsers.ToArray();
                var userMapping = adminUsers.ToDictionary(x => x.UserName, v => v,
                    StringComparer.InvariantCultureIgnoreCase);

                return userMapping;
            }
        }

    }
}
