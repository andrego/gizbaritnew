﻿$(function () {
    $("#tabs").tabs();

    $.validator.methods.date = function (value, element) {
        return this.optional(element) || moment(value, "DD.MM.YYYY", true).isValid();
    }

    $(".datepicker").datepicker(
    {
        dateFormat: "dd.mm.yy"
    });
    $('#ui-datepicker-div').addClass('ll-skin-latoja');

    $(function () {
        $("table.slimtable").slimtable({
            colSettings: [
                { colNumber: 8, enableSort: false }],
            itemsPerPage: 20,
            ippList: [10, 20, 30, 50],
            text1: "",
            text2: "Loading..."
        });
    });

    $("#download").click(function (e) {
        var link = $(this);
        var params = $("form").serialize();

        link.attr("href", "/Report/Download?" + params);
    });
});