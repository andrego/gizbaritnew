﻿function hideShow(item) {
    var $el = $("#" + item);
    console.log($el.html);
    $el.toggle();
}

//for example element =  '#id .row'
function hideShowNew(element) {
    var $el = $(element);

    $el.toggle();
}

function getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
}


function closeSendSuccess() {
    $("#fullopaqueSendSuccess").hide();
    $(".popupSendSuccess").hide();
}

function AjaxFormOnSuccess(data, index, pathForRedirect) {
    window.location.href = pathForRedirect;//'/Clients/Index/';
}

function AjaxFormOnFailure(xhr, status, index) {

    var jsonData = JSON.parse(xhr.responseText);

    //alert(JSON.stringify(jsonData));
    var nameClass = "#form-" + index + " .ValidationMSGClass";
    $(nameClass).text("");

    for (var i = 0; i < jsonData.length; i++) {

        var idElement = "#form-" + index + " #p" + jsonData[i]["key"].toString() + "Error";

        var error = jsonData[i]["errors"].toString();
        $(idElement).text(error);
    }
}

function showDeleteDialog(id) {
    $('#deleteBtn').attr("data-id", id);
    $('#fullopaquePrintCopy').show();
    $('.popupPrintCopy').show();
}

function deleteBank(button) {
    var id = $(button).attr("data-id");
    if (typeof id !== "undefined") {
        window.location.href = "/Bank/Delete?accountId=" + id;
    }
}

function goToUrl(url) {
    location.href = url;
}

function Redirect(Contoller, Action, OtherArguments, isInNewTab) {
    //window.location.href = "/" + Contoller + "/" + Action + "/" + OtherArguments;
    //window.location.replace = "/" + Contoller + "/" + Action + "/" + OtherArguments;
    var url = "/" + Contoller + "/" + Action + "/" + OtherArguments;


    if (isInNewTab) {
        var win = window.open(url, '_blank');
        if (win) {

            win.focus();
        } else {
            alert('Error, when open ', url);
        }
    }
    else {
        window.location.href = url;
    }
}

$(document).ajaxStart(function () {
    $("#fullopaqueGeneral").show();
});

$(document).ajaxStop(function () {
    $("#fullopaqueGeneral").hide();
});

$(document).on('keyup', '.not-cyrillic', function (ev) {
    this.value = this.value.replace(/[а-я]+/i, "");
});

