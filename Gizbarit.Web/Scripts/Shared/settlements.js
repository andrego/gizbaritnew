﻿var year = new Date().getFullYear();
var month = -1;
var cache = {}, lastXhr;

$(document).ready(function (e) {
    $("#yearSelector").click(function () {
        $('#years').toggle();
        return false;
    });

    $('body').on('focus', "#supplierName", function () {
        $(this).autocomplete({
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response($.map(cache[term], function (v, i) {
                        return { value: v.Name, id: v.Id, address: v.Address, phone: v.Phone };
                    }));
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/Settlements/GetSupplierByName",
                    dataType: "json",
                    data: {
                        supplierName: request.term
                    },

                    success: function (data) {
                        cache[term] = data;
                        response($.map(data, function (v, i) {
                            return { value: v.Name, id: v.Id, address: v.Address, phone: v.Phone };
                        }));
                    }
                });
            },
            minLength: 0,
            delay: 400,

            select: function (event, ui) {

                $('#supplierId').val(ui.item.id);
                $('#supplierName').val(ui.item.value);
                $('#supplierAddress').val(ui.item.address);
                $('#supplierPhone').val(ui.item.phone);
                if (typeof supplierChange != "undefined") {
                    supplierChange(ui.item.id);
                }

            }

        })
        .focus(function () {
            $(this).autocomplete('search', $(this).val());
        });
    });

    $('body').on('focus', "#supplierName", function () {
        $(this).keyup(function () {
            if ($(this).val().length === 0) {
                $('#supplierId').val("");
                $('#supplierAddress').val("");
                $('#supplierPhone').val("");
                if (typeof supplierChange != "undefined") {
                    supplierChange(null);
                }
            }
        });
    });

    $('body').on('focus', ".DateOperationClass", function () {
        $(this).datepicker(
            {
                dateFormat: 'dd.mm.yy',
                defaultDate: new Date(year, month - 1, 1),
                changeMonth: false,
                changeYear: false,
                stepMonths: 0
            });
        $('#ui-datepicker-div').addClass('ll-skin-latoja');
    });

    function setYear(yearToSet) {
        year = yearToSet;
    }

});