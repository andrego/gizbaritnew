﻿var indexForSelectedCatalogItem = 0;
var indexForSelectedCatalogItem = 0;
var indexForSelectedPaymentItem = 0;

function itemInputChange(element) {
    var elementId = $(element).attr('id');
    if (elementId === 'CatalogItemAmount' || elementId === 'CatalogItemCode') {
        validateItemAmount(element);
    }

    CountItemPrice(element);
    CountTotalPrice();
}

function validateItemAmount(element) {
    var parentForm = $(element).closest('tr.add');
    var code = parentForm.find("#CatalogItemCode").val();
    var amount = parentForm.find("#CatalogItemAmount").val();
    if (code !== '' && amount !== '') {
        $.ajax({
            type: 'GET',
            url: '/Store/CatalogItemCount?code='+ code,
            success: function (response) {
                var availableAmount = parseFloat(response);
                if (isNaN(availableAmount)) {
                    $(element).removeClass('input-validation-error');
                    return;
                }
                if (availableAmount < amount) {
                    $(element).addClass('input-validation-error');
                    $(element).val('');
                    $("#fullopaque").show();
                    $(".popup").show();
                } else {
                    $(element).removeClass('input-validation-error');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }
}

function paymentInputChange() {
    countPaymentTotalPrice();
}

function deleteRounding() {
    $("#PriceWithTax").val($("#TotalWithoutRounding").val());
    $("#PriceWithTax").hide();
    $("#totalWithoutRoundingLabel").hide();
    $("#totalLabel1").show();
    $("#totalLabel2").hide();
}

function checkAddDeleteButtons() {
    
    var visibleRowCount = $(".add:not([data-hidden='true'])").length;
    var $lastRow = $(".add:not([data-hidden='true']").last();
    $lastRow.find("#deleteRow").remove();
    if ($lastRow.find("#addnew").length === 0) {
        $lastRow.find("#Button").append('<input class="blueboxesfullwidth" id="addnew" onclick="addCatalogRow(this);" style="background-color: #8dc63f; border-radius: 5px; cursor: pointer;" type="button" value="+">');
    }
    if (visibleRowCount !== 1) {
        $lastRow.find("#Button").prepend("<img src=\"/Css/images/delete.png\" class=\"cursor\" id=\"deleteRow\" height=\"20\" name=\"delete\" onclick=\"deleteCatalogRow(this)\">");
    }
}

function toFixed(num, fixed) {
    var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
    return num.toString().match(re)[0];
}

function countPaymentTotalPrice() {
    var priceList = document.querySelectorAll('#PaymentSum');

    var totalPrice = 0;

    for (var i = 0; i < priceList.length; i++) {
        totalPrice += parseFloat($(priceList[i]).val());
    }
    if (isNaN(totalPrice)) {
        totalPrice = 0;
    }

    $("#PaymentsTotal").val(totalPrice.toFixed(2));
}

function CountItemPrice(element) {
    var parentForm = $(element).closest('tr.add');

    var amount = parseFloat(parentForm.find("#CatalogItemAmount").val());
    var discount = parseFloat(parentForm.find("#CatalogItemDiscount").val());
    var price = parseFloat(parentForm.find("#CatalogItemPrice").val());

    if (isNaN(discount)) {
        discount = 0;
    }
    var itemTotalPrice = (amount * price * (1 - discount / 100)).toFixed(2);


    if (isNaN(itemTotalPrice))
        parentForm.find("#CatalogItemTotalPrice").val("");
    else
        parentForm.find("#CatalogItemTotalPrice").val(itemTotalPrice);

}

function CountTotalPrice() {
    var priceList = $(".catalog-table tbody").find('input[name=CatalogItemTotalPrice]');
    var totalwithoutTax = 0;
    var totalPrice = 0;

    for (var i = 0; i < priceList.length; i++) {
        totalwithoutTax += parseFloat($(priceList[i]).val());
    }

   

    if (isNaN(totalwithoutTax))
        return;

    $("#TotalPrice").val(totalwithoutTax.toFixed(2));

    var taxAmount = parseFloat($("#TaxAmount").val());
    if (isNaN(taxAmount))
        return;


    var tax = ((totalwithoutTax * taxAmount) / 100);

    $("#Tax").val(tax.toFixed(2));

    if ($("#IsTaxIncluded").is(":checked")) {
        totalPrice = totalwithoutTax;
        tax = ((totalwithoutTax / (taxAmount + 100)) * taxAmount);
        totalwithoutTax = totalwithoutTax - tax;
        if ($("#IsRounded").is(":checked")) {
            $("#IsRounded").prop("checked", false);
            $("#totalWithoutRoundingLabel").hide();
            $("#PriceWithTax").hide();
        }
        $("#TotalPrice").val(totalwithoutTax.toFixed(2));
        $("#Tax").val(tax.toFixed(2));
    }
    else {

        totalPrice = totalwithoutTax + tax;
    }
    $("#TotalWithoutRounding").val(totalPrice.toFixed(2));
    $("#PriceWithTax").val(totalPrice.toFixed(2));

    if (totalPrice < 0) {
        $("#creditLabel").show();
    } else {
        $("#creditLabel").hide();
    }
    onIsRoundedChange($("#IsRounded")[0]);
}

function submitEditForm(callback) {
    var form = $("#documentForm");

    var currencyElement = document.getElementById('BaseDocument_Currency');
    if (currencyElement != null) {
        currencyElement.removeAttribute('disabled');
        $(currencyElement).css('background-color', '#c7daf2');
    }
    
    var data = form.serialize(); 
    $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: data, 
        success: function (response) {
            $('.contentarea').html(response);
            init();
            if (callback && typeof (callback) === "function") {
                callback();
            }
            
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alertify.error(thrownError);
        }

    });
}

function CatalogItemSelect(catalog, imageUpdate) {
    var option = $(catalog).find(':selected');
    var parentForm = $(catalog).closest('tr.add');
    var $imageTr;
    if (option.val() === "-1") {
        parentForm.find("#CatalogItemDescriptionSpan").html("");
        parentForm.find("#CatalogItemDescription").val("").attr('readonly', false);
        parentForm.find("#CatalogItemPrice").val("").attr('readonly', false);
        parentForm.find("#CatalogId").val("");
        parentForm.find("#CatalogItemCode").val("");
        parentForm.find("#CatalogItemAmount").val("");
        parentForm.find("#CatalogItemDiscount").val("");
        $imageTr = parentForm.find("#image-tr");
        $imageTr.empty();
    } else {
        if (typeof imageUpdate === "undefined") {
            parentForm.find("#CatalogItemDescriptionSpan").html(option.data('name'));
            parentForm.find("#CatalogItemDescription").val(option.data('name')).attr('readonly', true);
            parentForm.find("#CatalogItemPrice").val(option.data('price')).attr('readonly', true);
            parentForm.find("#CatalogItemCode").val(option.data('code'));
        }
        parentForm.find("#CatalogItemAmount").change();

        $imageTr = parentForm.find("#image-tr");
        $imageTr.empty();
        if (option.data('image') !== "/Content/Catalog/plus_ic.png") {
            $imageTr.append('<img class="catalog-image" src="' + option.data('image') + '" />');
        }
       
        parentForm.find("#CatalogId").val(option.data('id'));
    }

    CountItemPrice(catalog);
    CountTotalPrice();

}

function closeDocumentError() {
    $("#fullopaque").toggle();
    $(".popup").toggle();
}

function addRoundingRow(btn) {
    var $itemPattern = $("#catalogRowPattern tbody tr").clone();
    var idItem = parseInt($(".add").last().attr("item-id"));

    indexForSelectedCatalogItem++;

    $itemPattern.attr('item-id', ++idItem);
    $itemPattern.addClass("add");
    $itemPattern.find("#CatalogItemDescription").attr('name', 'IncludedCatalogItems[' + idItem + '].Name').val("");
    $itemPattern.find("#CatalogItemDescription").css('height', '28px');
    $itemPattern.find("#CatalogItemDescription").autoResize({extraSpace : 0});
    $itemPattern.find("#CatalogItemPrice").attr('name', 'IncludedCatalogItems[' + idItem + '].Price').val("");
    $itemPattern.find("#CatalogId").attr('name', 'IncludedCatalogItems[' + idItem + '].ID').val("");
    $itemPattern.find("#CatalogItemAmount").attr('name', 'IncludedCatalogItems[' + idItem + '].Quantity').val("");
    $itemPattern.find("#ID").attr('name', 'IncludedCatalogItems[' + idItem + '].Id').val("");
    $itemPattern.find("#DocumentId").attr('name', 'IncludedCatalogItems[' + idItem + '].DocumentId').val("");
    $itemPattern.find("#CatalogItemDiscount").attr('name', 'IncludedCatalogItems[' + idItem + '].Discount').val("");
    $itemPattern.attr("data-hidden", false);
    $itemPattern.attr("id", "");

    if ($(btn).parent().find("#deleteRow").length === 0) {
        $(btn).parent().append($itemPattern.find("#deleteRow").clone());
    }

    $(btn).remove();

    $itemPattern.appendTo(".catalog-table tbody");
}

function addCatalogRow(btn) {
   
    if ($("#isReadonly").val() === "1")
        return;
    var $itemPattern = $("#catalogRowPattern tbody tr").clone();
    var indexes = [];
    $(".add").each(function() {
        var $context = $(this);
        indexes.push(+$context.attr("item-id"));
    });

    var lastIndex = getMaxOfArray(indexes);

   if ($("#IsRounded:checked")) {
        deleteRounding();
        $("#IsRounded").prop('checked', false);

        btn = $(".add").last().find("#addnew");
    }
    checkAddDeleteButtons();
   
    indexForSelectedCatalogItem++;

    $itemPattern.attr('item-id', ++lastIndex);
    $itemPattern.addClass("add");
    $itemPattern.find("#Index").val(lastIndex);
    $itemPattern.find("#CatalogItemDescription").attr('name', 'IncludedCatalogItems[' + lastIndex + '].Name').val("");
    $itemPattern.find("#CatalogItemDescription").attr('name', 'IncludedCatalogItems[' + lastIndex + '].Name').val("");
    $itemPattern.find("#CatalogItemDescription").css('height', '28px');
    $itemPattern.find("#CatalogItemDescription").autoResize();
    $itemPattern.find("#CatalogItemPrice").attr('name', 'IncludedCatalogItems[' + lastIndex + '].Price').val("");
    $itemPattern.find("#CatalogId").attr('name', 'IncludedCatalogItems[' + lastIndex + '].ID').val("");
    $itemPattern.find("#CatalogItemAmount").attr('name', 'IncludedCatalogItems[' + lastIndex + '].Quantity').val("");
    $itemPattern.find("#CatalogItemCode").attr('name', 'IncludedCatalogItems[' + lastIndex + '].Code').val("");
    $itemPattern.find("#ID").attr('name', 'IncludedCatalogItems[' + lastIndex + '].Id').val("");
    $itemPattern.find("#DocumentId").attr('name', 'IncludedCatalogItems[' + lastIndex + '].DocumentId').val("");
    $itemPattern.find("#CatalogItemDiscount").attr('name', 'IncludedCatalogItems[' + lastIndex + '].Discount').val("");
    $itemPattern.attr("data-hidden", false);
    $itemPattern.attr("id", "");

    if ($(btn).parent().find("#deleteRow").length === 0) {
        $(btn).parent().append($itemPattern.find("#deleteRow").clone());
    }
    
    $(btn).remove();

    $itemPattern.appendTo(".catalog-table tbody");
}

function addDealOfferItemsToCatalogItems(row, inheritPaymentDueDateDays) {

    var parent = $(row).closest('div.DealOfferRow');
    
    var elements = parent.find('div[name=CatalogItemOfDealOffer]');

    for (var i = 0; i < elements.length; i++) {
        //update check boxes for currency, DeliveryTermsOfPayments and TermsOfPaymentsType
        
        var currencyElement = document.getElementById('BaseDocument_Currency');
        if (currencyElement != null) {
                var currencyValue = $(elements[i]).find('[name="CatalogItemCurrency"]').val();
                    var selectedValue = currencyElement.options[currencyElement.selectedIndex].value;

                    if (currencyElement.hasAttribute('disabled') && selectedValue != currencyValue) {
                        alertify.error('You can not add document because it has different currency');
                        return;
                    }
                    currencyElement.value = currencyValue;
                    currencyElement.setAttribute('disabled', 'disabled');
                    $(currencyElement).css('background-color', 'gray');
            }

            var deliveryTermsOfPaymentElement = document.getElementById('BaseDocument_DeliveryTerms');
            if (deliveryTermsOfPaymentElement != null) {
                var addDocumentDeliveryTerms = $(elements[i]).find('[name="CatalogItemDeliveryTermsOfPayments"]').val();
                deliveryTermsOfPaymentElement.value = addDocumentDeliveryTerms;
            }

            var termsOfPaymentsElement = document.getElementById('BaseDocument_TermsOfPayment');
            if (termsOfPaymentsElement != null) {
                var addDocumentTermsOfPayment = $(elements[i]).find('[name="CatalogItemTermsOfPaymentType"]').val();
                termsOfPaymentsElement.value = addDocumentTermsOfPayment;
            }

            var pointOfDeliveryElement = document.getElementById('BaseDocument_PointOfDelivery');
            if (pointOfDeliveryElement != null) {
                var addDocumentPointOfDelivery = $(elements[i]).find('[name="CatalogItemPointOfDelivery"]').val();
                pointOfDeliveryElement.value = addDocumentPointOfDelivery;
            }

            var exportAditionInfoElement = document.getElementById('BaseDocument_ExportAditionInfo');
            if (exportAditionInfoElement != null) {
                var addDocumentAditionInfoElement = $(elements[i]).find('[name="CatalogItemExportAditionInfo"]').val();
                exportAditionInfoElement.value = addDocumentAditionInfoElement;
            }

            var exportTimeOfDeliveryElement = document.getElementById('BaseDocument_TimeOfDelivery');
            if (exportTimeOfDeliveryElement != null) {
                var addDocumentAditionTimeOfDelivery = $(elements[i]).find('[name="CatalogItemTimeOfDelivery"]').val();
                exportTimeOfDeliveryElement.value = addDocumentAditionTimeOfDelivery;
            }

        //end of updating boxes

        var taxIncluded = $(elements[i]).find("[name='TaxIncluded']").val();
        if (taxIncluded === 'true') {
            if (!$("#IsTaxIncluded").prop("checked") && $("#addnew:disabled").length) {
                $("#fullopaque").toggle();
                $(".popup").toggle();
                return;
            } else {
                $("#IsTaxIncluded").prop('checked', true);
            }
        } else {
            if ($("#IsTaxIncluded").prop("checked") && $("#addnew:disabled").$8length) {
                $("#fullopaque").toggle();
                $(".popup").toggle();
                return;
            } else {
                $("#IsTaxIncluded").prop('checked', false);
            }
        }

        var lastRowInCatalogItems = $("#addnew").closest('tr.add');
        if (lastRowInCatalogItems.find("#CatalogItemPrice").val() !== "") {
            $("#addnew").click();
            lastRowInCatalogItems = $("#addnew").closest('tr.add');
        }

        var price = $(elements[i]).find("[name='CatalogItemPrice']").val();
        lastRowInCatalogItems.find("#CatalogItemPrice").val(price);

        var catalogId = $(elements[i]).find("[name='CatalogId']").val();
        lastRowInCatalogItems.find("#CatalogId").val(catalogId);

        var catalogItemAmount = $(elements[i]).find("[name='CatalogItemAmount']").val();
        lastRowInCatalogItems.find("#CatalogItemAmount").val(catalogItemAmount);

        var catalogItemDiscount = $(elements[i]).find("[name='CatalogItemDiscount']").val();
        lastRowInCatalogItems.find("#CatalogItemDiscount").val(catalogItemDiscount);

        var catalogItemCode = $(elements[i]).find("[name='CatalogItemCode']").val();
        lastRowInCatalogItems.find("#CatalogItemCode").val(catalogItemCode);

        var catalogItemNameSelect = $(elements[i]).find("[name='CatalogItemNameSelect']").val();
        lastRowInCatalogItems.find(".CatalogItemNameSelect").val(catalogItemNameSelect);

        var catalogItemDescription = $(elements[i]).find("[name='CatalogItemDescription']").val();
        lastRowInCatalogItems.find("#CatalogItemDescription").val(catalogItemDescription);
        var catalogItemImage = $(elements[i]).find("[name='CatalogItemImage']").val();
        var $imageTr = lastRowInCatalogItems.find("#image-tr");
        $imageTr.empty();
        if (catalogItemImage !== "") {
            $imageTr.append('<img class="catalog-image" src="/Content/Catalog/' + catalogItemImage + '" />');
        }

        var catalogItemDocumentId = $(elements[i]).find("[name='CatalogItemDocumentId']").val();
        lastRowInCatalogItems.find("#DocumentId").val(catalogItemDocumentId);
        if (inheritPaymentDueDateDays) {
           var paymentDueDateDays = $(elements[i]).find("#DocPaymentDueDateDays").val();
            $("#PaymentDueDateDays").val(paymentDueDateDays); 
        }
        

        var extras = $(elements[i]).find("#DocExtras").val();
        $("#OfferExtras").val(extras);
        lastRowInCatalogItems.find("#CatalogItemAmount").change();
    }

    parent.find("#addnew").prop('disabled', true).css("background-color", "gray");

    parent.css("opacity", "0.4");
}

function deleteCatalogRow(btn) {
    if ($("#isReadonly").val() === "1")
        return;
    var $row = $(btn).parent().parent().parent();

    if ($row.has("#roundingId").length !== 0) {
        $("#IsRounded").prop('checked', false);
    }

    $row.remove();
    checkAddDeleteButtons();
    CountTotalPrice();

    var countsOfRows = document.querySelectorAll('.tablesorter .add').length;
    var currencyElement = document.getElementById('BaseDocument_Currency');

    if (countsOfRows < 2 && currencyElement != null) {
            currencyElement.removeAttribute('disabled');
            $(currencyElement).css('background-color','#c7daf2');
    }

}

function deletePaymentRow(row) {
    var currentRow = $(row).parent().parent().parent();

    currentRow.remove();

    var $lastVisibleRow = $(".PaymentRow:visible").last();
    var visibleRowCount = $(".PaymentRow:visible").length;
    if (visibleRowCount === 1) {
        $lastVisibleRow.find("#Button").replaceWith(
            "<div id=\"Button\" class=\"fivecolumns\" style=\"width: 60px;\">" +
            "<input class=\"blueboxesfullwidth\" id=\"addnewpayment\" onclick=\"addPaymentRow(this);\"" +
            " style=\"background-color: #8dc63f; border-radius: 5px; cursor: pointer;\" type=\"button\" value=\"+\">" +
            "</div>");
    } else {
        $lastVisibleRow.find("#Button").replaceWith(
             "<div id=\"Button\" class=\"fivecolumns\" style=\"width: 60px;\">" +
            "<img src=\"/Css/images/delete.png\" class=\"cursor\" id=\"deleteRow\" height=\"20\" name=\"delete\" onclick=\"deletePaymentRow(this)\">" +
            "<input class=\"blueboxesfullwidth\" id=\"addnewpayment\" onclick=\"addPaymentRow(this);\"" +
            " style=\"background-color: #8dc63f; border-radius: 5px; cursor: pointer;\" type=\"button\" value=\"+\">" + "</div>");
    }
}

function addPaymentRow(btn) {
    var $currentRow = $(btn).parent().parent().parent();
    var itemPattern = $("#paymentRowTemplate").html();
    var $parent = $("#PaymentsTable");
    var $lastRow = $("#PaymentsTable > .PaymentRow").last();

    var idItem = parseInt($lastRow.attr("item-id"));

    $parent.append(itemPattern);
    $lastRow = $("#PaymentsTable > .PaymentRow").last();
    $lastRow.attr('item-id', ++idItem);

    $lastRow.find("#Index").val(idItem);

    $lastRow.find("#PaymentType").attr('name', 'Payments[' + idItem + '].PaymentType').val("");

    $lastRow.find("#PaymentCheckNumber").attr('name', 'Payments[' + idItem + '].CheckNumber').val("");
    $lastRow.find("#PaymentAccountNumber").attr('name', 'Payments[' + idItem + '].AccountNumber').val("");
    $lastRow.find("#PaymentBranchName").attr('name', 'Payments[' + idItem + '].BranchName').val("");
    $lastRow.find("#PaymentBankName").attr('name', 'Payments[' + idItem + '].BankName').val("");
    $lastRow.find("#PaymentSum").attr('name', 'Payments[' + idItem + '].Sum').val("");
    $lastRow.find(".DateOperationClass").attr('name', 'Payments[' + idItem + '].DateOperation').val("");

    $lastRow.find("#Button").replaceWith(
            "<div id=\"Button\" class=\"fivecolumns\" style=\"width: 60px;\">" +
            "<img src=\"/Css/images/delete.png\" class=\"cursor\" id=\"deleteRow\" height=\"20\" name=\"delete\" onclick=\"deletePaymentRow(this)\">" +
            "<input class=\"blueboxesfullwidth\" id=\"addnewpayment\" onclick=\"addPaymentRow(this);\"" +
            " style=\"background-color: #8dc63f; border-radius: 5px; cursor: pointer;\" type=\"button\" value=\"+\">" +
            "</div>");

    $currentRow.find("#Button").replaceWith(
        "<div id=\"Button\" class=\"fivecolumns\" style=\"width: 60px;\">" +
        "<img src=\"/Css/images/delete.png\" class=\"cursor\" id=\"deleteRow\" " +
        "height=\"20\" name=\"delete\" onclick=\"deletePaymentRow(this)\">" +
        "</div>");
}

var indexForSelectedDocuments = 0;
var selectedDocumentRowPattern;


function checkPaid(element) {
    var $selectedElement = $(element).find("option:selected");
    var total = $selectedElement.data("total");
    var totalVat = $selectedElement.data("totalvat");
    var paid = $selectedElement.data("paid");
    var due = (totalVat - paid).toFixed(2);
    $("#total").text(total);
    $("#totalVat").text(totalVat);
    $("#paid").text(paid);
    $("#due").text(due);
    $("#tableHint").show();

}

function addDocumentRow(row) {
    if (indexForSelectedDocuments == 0) {
        selectedDocumentRowPattern = $("#SelectedDocumentsTable").html();
    }


    indexForSelectedDocuments++;
    $("#SelectedDocumentsTable").append(selectedDocumentRowPattern);
    $lastRow = $("#SelectedDocumentsTable > #DocumentRow").last();

    $lastRow.attr('item-id', indexForSelectedDocuments);



    $lastRow.find("#ListOfTaxItems").attr('name', 'SelectedDocumentItems[' + indexForSelectedDocuments + '].Id').val("");
    $lastRow.find("#ListOfDealFormItems").attr('name', 'SelectedDocumentItems[' + indexForSelectedDocuments + '].Id').val("");
    $lastRow.find("#InUse").attr('name', 'SelectedDocumentItems[' + indexForSelectedDocuments + '].InUse').val("1");

    $lastRow.find("#TaxInvoiceLabel").hide();
    $lastRow.find("#ListOfTaxItems").hide();
    $lastRow.find("#PrepaymentInvoiceLabel").hide();
    $lastRow.find("#ListOfDealFormItems").val("").hide();
    $lastRow.find("#OtherDescription").val("").hide();

    $(row).replaceWith("<img src=\"/Css/images/delete.png\" class=\"cursor\" height=\"20\" name=\"delete\" onclick=\"deleteRow(this)\">");
}


function ReceiptForItemSelect(selector) {
    var option = $(selector).find(':selected');
    var Row = $(selector).closest('#DocumentRow');

    switch (option.val()) {
        case "TaxInvoice":
            Row.find("#TaxInvoiceLabel").show();
            Row.find("#ListOfTaxItems").show();

            Row.find("#PrepaymentInvoiceLabel").hide();
            Row.find("#ListOfDealFormItems").val("").hide();
            Row.find("#OtherDescription").val("").hide();
            $("#tableHint").hide();
            break;
        case "PrepaymentInvoice":
            Row.find("#PrepaymentInvoiceLabel").show();
            Row.find("#ListOfDealFormItems").show();

            Row.find("#TaxInvoiceLabel").hide();
            Row.find("#ListOfTaxItems").val("").hide();
            Row.find("#OtherDescription").val("").hide();
            $("#tableHint").hide();
            break;
        case "other":
            Row.find("#OtherDescription").show();

            Row.find("#TaxInvoiceLabel").hide();
            Row.find("#ListOfTaxItems").val("").hide();
            Row.find("#PrepaymentInvoiceLabel").hide();
            Row.find("#ListOfDealFormItems").val("").hide();
            $("#tableHint").hide();
            break;
        default:
            break;
    }
}

function isNewDocument() {
    var id = $("input[name='BaseDocument.Id']").val();
    return (typeof id !== "undefined" && id!= null && id !== '00000000-0000-0000-0000-000000000000');
}


function checkForValidationErrors() {
    var ret = $(".validation-summary-errors").length;
    return !ret;
}


function deleteRow(row) {
    var currentRow = $(row).parent().parent().parent();
    currentRow.hide();
}

function showAfterPrintDialog() {
    setTimeout(function () {
        $("#fullopaquePrint").show();
        $(".popupPrint").show();
    }, 6000);
    
}

function hideSaveSuccessDialog() {
    $("#fullopaqueSaveSuccess").hide();
    $(".popupSaveSuccess").hide();
}

function showSaveSuccessDialog() {
    $("#fullopaqueSaveSuccess").show();
    $(".popupSaveSuccess").show();
}

function hideAfterPrintDialog() {
    $("#fullopaquePrint").hide();
    $(".popupPrint").hide();
}

function showSendCopyDialog() {
    $("#fullopaquePrintCopy").show();
    
    $(".popupPrintCopy").show();
   
}

function closeGeneralError() {
    $("#client_error").hide();
}

function showGeneralError() {
    $("#client_error").show();
}

function showCustomTextWindow(withcopy) {
    
}

function sendDocument(sender, withCopy) {
    var text = encodeURIComponent($(sender).parent().find(".customTextArea").val());
    if (withCopy) {
        $("#BaseDocument_Send").val("True");
    } else {
        $("#BaseDocument_SendCopy").val("False");
    }
    var context = $("#btn-send");
    location.href = context.data("url") + "?isCopyRequested=" + withCopy + '&customText='+text;
}

function parseBoolean(val) {
    if (typeof val === "undefined") {
        console.log("stringToBoolean Undefined Error");
        return false;
    }
    if (typeof val === "boolean") {
        return string;
    }

    val = val || "";

    switch (val.toLowerCase()) {
        case "true":
        case "yes":
        case "1":
            return true;

        case "false":
        case "no":
        case "0":
        case null:
            return false;
        default:
            return false;
    }
}

function onIsRoundedChange(elem) {
   
    if (elem.checked) {
        
        var total = parseFloat($("#TotalWithoutRounding").val());
        var rounded = Math.round(total);
       
        if (rounded === 0) {
            $(elem).prop("checked", false);
            return;
        }
        
        $("#PriceWithTax").val(rounded.toFixed(2));
        $("#PriceWithTax").show();
        $("#totalWithoutRoundingLabel").show();
        $("#totalLabel1").hide();
        $("#totalLabel2").show();
       
    } else {
        deleteRounding();
    }
}


function init() {
    autosize($('textarea'));
    var cache = {}, lastXhr;

    $('.add').each(function() {
        var $ctx = $(this);
        $ctx.find('#CatalogItemPrice').change();
    });

    $('#ClientInfo\\.Name').autocomplete({
        source: function (request, response) {
            var term = request.term;
          

            if (term in cache) {
                response($.map(cache[term], function (v, i) {
                    return { value: v.Name, id: v.Id, address: v.Address, number: v.UniqueId, email: v.Email, phone: v.Phone, additionEmails: v.AdditionEmails, additionPhones: v.AdditionPhones };
                }));
                return;
            }
            $.ajax({
                type: "POST",
                url: "/documents/GetClientsByName",
                dataType: "json",
                global:false,
                data: {
                    clientName: request.term
                },

                success: function (data) {
                    cache[term] = data;
                    response($.map(data, function (v, i) {
                       
                        return { value: v.Name, id: v.Id, address: v.Address, number: v.UniqueId, email: v.Email, phone: v.Phone, additionEmails: v.AdditionEmails, additionPhones: v.AdditionPhones };
                    }));
                }
            });
        },
        minLength: 0,
        delay: 1000,

        select: function (event, ui) { /// when checkbox with clients click
            closeGeneralError();
           
  

            var selectEmailElement = '<select class="blueboxeswidthitalics" id="ClientInfo.Email" name="BaseDocument.ClientInfo.Email" style="text-align-last: right"></select>';
            checkAndReplaceSelectContactField(ui.item.additionEmails, '#ClientInfo\\.Email', selectEmailElement);

            var selectPhoneElement = '<select class="blueboxeswidthitalics" id="ClientInfo.Phone" name="BaseDocument.ClientInfo.Phone" style="text-align-last: right;"></select> ';
            checkAndReplaceSelectContactField(ui.item.additionPhones, '#ClientInfo\\.Phone', selectPhoneElement);
            

            $('#ClientInfo\\.Id').val(ui.item.id);
            $('#ClientInfo\\.Name').val(ui.item.value);
            $('#ClientInfo\\.UniqueId').val(ui.item.number);
            //  $('#ClientInfo\\.Email').val(ui.item.email);
           
            $('#ClientInfo\\.Address').val(ui.item.address);
            // $('#ClientInfo\\.Phone').val(ui.item.phone);
          
            if (typeof clientChange != "undefined") {
                clientChange(ui.item.id);
            }


        }

    })
        .focus(function () {
            $(this).autocomplete('search', $(this).val());
        });

    $('#ClientInfo\\.Name').keyup(function () {
        if ($(this).val().length === 0) {
            $('#ClientInfo\\.Id').val("0");
            $('#ClientInfo\\.UniqueId').val("");
            //$('#ClientInfo\\.Email').val("");

            $('#ClientInfo\\.Address').val("");
            // $('#ClientInfo\\.Phone').val("");

            var inputEmail = '<input id="ClientInfo.Email" name="BaseDocument.ClientInfo.Email" class="blueboxeswidthitalics" type="text" placeholder="Email" value="">';
            $('#ClientInfo\\.Email').replaceWith(inputEmail);

            var inputPhone = '<input id="ClientInfo.Phone" name="BaseDocument.ClientInfo.Phone" class="blueboxeswidthitalics" type="text" placeholder="Phone" value="">';
            $('#ClientInfo\\.Phone').replaceWith(inputPhone);
            if (typeof clientChange != "undefined") {
                clientChange(null);
            }
        }
    });

    $("#ClientInfo\\.UniqueId").change(function () {
        var id = $("#ClientInfo\\.Id").val();
        var value = $(this).val();
        if (value.length > 0 && id == 0) {
            $.ajax({
                type: "GET",
                url: "/clients/VerifyExist?uniqueId=" + value,
                complete: function(xhr, statusText) {
                    switch (xhr.status) {
                    case 302:
                        showGeneralError();
                        break;
                    case 202:
                        closeGeneralError();
                        break;
                    default:
                        alert('Error');

                    }

                },
                success: function() {
                }
            });
        } else {
            closeGeneralError();
        }

    });

    if ($("#TaxAmount").val() === "0") {
        $("#zeroVAT").prop("checked", true);
        $("#IsTaxIncluded").prop('disabled', true);
    }

    $("#taxAmountSelect").change(function () {
        var value = $("#taxAmountSelect").val();
        $("#TaxAmount").val(value);
        $("#IsTaxIncluded").prop('checked', false);
        CountTotalPrice();
    });

    $("#IsRounded").change(function () {
        onIsRoundedChange(this);
    });

    $('body').on('focus', ".DateOperationClass", function () {
        $(this).datepicker(
            {
                dateFormat: 'dd.mm.yy'
            });
        $('#ui-datepicker-div').addClass('ll-skin-latoja');
    });

    $("#IsTaxIncluded").change(function () {
        CountTotalPrice();
    });


    /************************************************************************/
    //return full details or empty input boxes
    var getClientDetails = function (query) {
        $.post(
            "/Documents/GetClientDetailsByName",
            { name: query },
            function (data) {
                $("#clientdetails").html(data);
            });
    };

    $('#clientnamebox').autocomplete({
        source: function (request, response) {
            $.post(
                    "/Documents/GetClientsNamesByQuery",
                    { query: request.term },
                    function (data) {
                        response(data.value);
                    }
                );
        },
        select: function (event, ui) {
            getClientDetails(ui.item.value)
        }
    });
    //clean details on reselect 
    $('#clientnamebox').keydown(function () {
        $("#clientdetails").html('');
    });

    //onEnter
    $('#clientnamebox').keypress(function (e) {
        if (e.keyCode === 13) {
            getDetails($('#clientnamebox').val());
        }
    });

    /*********************************************************************************/
    var newLine = $("#cltalogitems").html();


    var getItemDetails = function (query) {
        $.post(
            "/Documents/GetItemByName",
            { name: query },
            function (data) {
                $("#CatalogItemName").val(data.value.Name);
                $("#CatalogItemDescription").val(data.value.Name);
                $("#CatalogItemPrice").val(data.value.Price);
                $("#cltalogitems").append(newLine);
            });
    };

    $('#CatalogItemName').autocomplete({
        source: function (request, response) {
            $.post(
                    "/Documents/GetCatalogItemsNames",
                    { query: request.term },
                    function (data) {
                        response(data.value);
                    }
                );
        },
        select: function (event, ui) {
            getItemDetails(ui.item.value);
        }
    });


    $("#btn-print").click(function (event) {
        event.preventDefault();
        event.stopPropagation();

        var pdf = null;
        var isSaveAllowed = parseBoolean($("#issaveallowed").val());

        if (!isSaveAllowed) {
            pdf = $("#btn-print").data("url");
            printJS(pdf);
        } else {
            submitEditForm(function () {
                if (checkForValidationErrors()) {
                    pdf = $("#btn-print").data("url");
                    printJS(pdf);
                    showAfterPrintDialog();
                }
            });
        }
    });

    

    $('.check_box').click(function () {
        $('.check_box').hide();
        $('.uncheck_box').show();
        $('.customTextArea').hide();
        $('.customTextArea').text('');
        
    });

    $('.uncheck_box').click(function () {
        $('.uncheck_box').hide();
        $('.check_box').show();
        $('.customTextArea').show();
        $('.customTextArea').attr('disabled', false);
    });

    $("#btn-send").click(function (event) {
        event.preventDefault();
        event.stopPropagation();

        var context = $(this);
        var isSaveAllowed = parseBoolean($("#issaveallowed").val());
        var isConfirmRequred = parseBoolean($("#isconfirmrequred").val());
        
        if (isSaveAllowed) {
           submitEditForm(function () {
               isConfirmRequred = parseBoolean($("#isconfirmrequred").val());
                if (checkForValidationErrors()) {
                    if (isConfirmRequred) {
                        showSendCopyDialog();
                    } else {
                        $("#fullopaquePrintCopy").show();
                        $(".popupCustomText").show();
                        return;
                    }
                }
            });

            return;
        }

        if (isConfirmRequred) {
            showSendCopyDialog();
        } else {
            $("#fullopaquePrintCopy").show();
            $(".popupCustomText").show();
            location.href = context.data("url") + "?isCopyRequested=false";
        }
    });

    $("#btn-save").click(function() {
        submitEditForm(function() {
            if (checkForValidationErrors()) {
                showSaveSuccessDialog();
            }
        });
    });

    $(".CatalogItemNameSelect").each(function() {
        var context = $(this);
        var $selectedItem = context.find(':selected');
        if ($selectedItem.val() !== "-1") {
            CatalogItemSelect(context, true);
        }
    });
}


function clickTable(element) {
    var grid = element.closest('table');
    var tbody = grid.querySelector('tbody');
    var rowsArray = [].slice.call(tbody.rows);

    rowsArray.sort(compareCatalogByDescription);

    grid.removeChild(tbody);

    for (var i = 0; i < rowsArray.length; i++) {
        tbody.appendChild(rowsArray[i]);
    }

    grid.appendChild(tbody);

    var btn = $(tbody).find('#addnew');
    $(btn).remove();
    
    checkAddDeleteButtons();

}

function compareCatalogByDescription(rowA, rowB) {
    var rowAElementTextArea = rowA.querySelector('#CatalogItemDescription');
    var rowBElementTextArea = rowB.querySelector('#CatalogItemDescription');
    return rowAElementTextArea.value > rowBElementTextArea.value;
}

function checkAndReplaceSelectContactField(contactArray, updateContactSelector, updateSelectElement) {
      if (contactArray.length === 1 && contactArray[0] == null) {
          $(updateContactSelector).val('');
          return;
      } 
          
      $(updateContactSelector).replaceWith(updateSelectElement);
      $(updateContactSelector).find('option').remove();

      for (var i = 0; i < contactArray.length; i++) {
          $(updateContactSelector).append('<option>' + contactArray[i] + '</option>');
      }

      
  }

function cancelSendCopyOfDocuments() {
    $('.popupCustomText').hide();
    $('#fullopaquePrintCopy').hide();
}

$(document).ready(function (e) {
    init();
});



