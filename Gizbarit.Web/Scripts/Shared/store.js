﻿function StoreCatalogItemSelect(catalog) {
    var option = $(catalog).find(':selected');
    var parentForm = $(catalog).closest('tr.add');
    if (option.val() === "-1") {
        parentForm.find("#name").val("").attr('readonly', false);
        parentForm.find("#code").val("").attr('readonly', false);
        parentForm.find("#price").val("");
    } else {
        parentForm.find("#name").val(option.data('name')).attr('readonly', true);
        parentForm.find("#code").val(option.data('code')).attr('readonly', true);
        parentForm.find("#price").val(option.data('price'));
    }
}