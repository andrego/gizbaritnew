﻿using System.Web.Mvc;

namespace Gizbarit.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminControllerBase : Controller
    {
    }
}