﻿using Gizbarit.DAL.Entities.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gizbarit.DAL.Common;
using Gizbarit.DAL.Services.Content;

namespace Gizbarit.Web.Areas.Admin.Controllers
{
    public class ContentNodeController : AdminControllerBase
    {
        private readonly IContentNodeService _contentNodeService;


        public ContentNodeController(IContentNodeService contentNodeService)
        {
            this._contentNodeService = contentNodeService;
        }
        // GET: Admin/ContentNode
        public ActionResult Index(Guid? id)
        {
            var model = _contentNodeService.Fetch(id);
            return View("Index", model);
        }

       
        // GET: Admin/ContentNode/Create
        public ActionResult Create(Guid? parentId)
        {
            var model = new ContentNode
            {
                ParentId = parentId
            };

            if (!parentId.HasValue)
            {
                return View("Edit", model);
            }


            var parent = _contentNodeService.Get(parentId.Value);

            model.CultureCode = parent.CultureCode;
            model.Name = $"{parent.Name}-{parent.Childs.Count}";
            model.Index = parent.Childs.Count * 10;

            return View("Edit", model);
        }

        // POST: Admin/ContentNode/Create
        [HttpPost]
        public ActionResult Create(ContentNode model)
        {
            if (!ModelState.IsValid)
            {
                return View("Edit", model);
            }

            try
            {
                var isExists = _contentNodeService.Exists(model.Name, model.CultureCode);
                // TODO: Add insert logic here
                if (isExists)
                {
                    ModelState.AddModelError("", $@"Page {model.Name}-{model.CultureCode} is exists");
                    return View("Edit", model);
                }

                if (model.Url.IsEmpty())
                {
                    model.Url = $"/{model.CultureCode}/{model.Name}";
                    if (model.ParentId.HasValue)
                    {
                        var parent = _contentNodeService.Get(model.ParentId.Value);
                        model.Url = $"{parent.Url}/{model.Name}".ToLower();
                    }
                }

                _contentNodeService.Create(model, true);

                return RedirectToAction("Edit", new { id = model.ID });
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("Edit", model);
            }
        }

        // GET: Admin/ContentNode/Edit/5
        public ActionResult Edit(Guid id)
        {
            var model = _contentNodeService.Get(id);

            return View("Edit", model);
        }

        // POST: Admin/ContentNode/Edit/5
        [HttpPost]
        public ActionResult Edit(ContentNode model)
        {
            if (!ModelState.IsValid)
            {
                return View("Edit", model);
            }

            try
            {
                var existed = _contentNodeService.Get(model.ID);

                if (model.Url.IsEmpty())
                {
                    model.Url = $"/{model.CultureCode}/{model.Name}".ToLower();
                    if (model.ParentId.HasValue)
                    {
                        var parent = _contentNodeService.Get(model.ParentId.Value);
                        model.Url = $"{parent.Url}/{model.Name}".ToLower();
                    }
                }

                existed.Name = model.Name;
                existed.CultureCode = model.CultureCode;
                existed.Title = model.Title;
                existed.Url = model.Url;
                existed.Index = model.Index;
                existed.Description = model.Description;
                existed.Content = model.Content;
                existed.Updated = DateTime.UtcNow;

                _contentNodeService.Commit(true);

                return View("Edit", existed);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("Edit", model);
            }
        }

        // GET: Admin/ContentNode/Delete/5
        public ActionResult Delete(Guid id)
        {

            try
            {
                var existed = _contentNodeService.Get(id);

                this._contentNodeService.Remove(existed);
                this._contentNodeService.Commit(true);

                return RedirectToAction("Index", "ContentNode");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("Edit");
            }
        }

        // POST: Admin/ContentNode/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
