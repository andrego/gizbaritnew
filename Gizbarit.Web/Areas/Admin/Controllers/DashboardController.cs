﻿using System.Web.Mvc;

namespace Gizbarit.Web.Areas.Admin.Controllers
{
    public class DashboardController : AdminControllerBase
    {
        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            return RedirectToAction("Index");
        }
    }
}