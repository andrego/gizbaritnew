﻿using System;
using System.Web.Mvc;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Web.Areas.Admin.Models;

namespace Gizbarit.Web.Areas.Admin.Controllers
{
    public class DocumentController : AdminControllerBase
    {
        private readonly IDocumentService _documentService;
        private readonly IOrganisationService _organisationService;
        private readonly IDocumentNumberingService _numberingService;

        public DocumentController(IDocumentService documentService, 
                                  IDocumentNumberingService numberingService,
                                  IOrganisationService organisationService)
        {
            _documentService = documentService;
            _numberingService = numberingService;
            _organisationService = organisationService;
        }

        // GET: Admin/Document
        public ActionResult Index(DocumentSpecifiation specifiation)
        {
            if (!specifiation.OrganizationId.HasValue)
            {
                return RedirectToAction("Index", "Organization", new {Area = "Admin"});
            }

            var orgId = specifiation.OrganizationId.Value;
            var invoiceSpec = new DocumentSpecifiation
            {
                OrganizationId = orgId,
                DocumentType = DocumentType.TaxInvoice
            };
            var receiptSpec = new DocumentSpecifiation
            {
                OrganizationId = orgId,
                DocumentType = DocumentType.Receipt
            };
            var invoiceReceiptSpec = new DocumentSpecifiation
            {
                OrganizationId = orgId,
                DocumentType = DocumentType.TaxInvoiceReceipt
            };

            var model = new OrganizationDocumentsViewModel
            {
                Organization = _organisationService.Get(orgId),
                InvocieReciepts = _documentService.Fetch(invoiceReceiptSpec),
                Invocies = _documentService.Fetch(invoiceSpec),
                Reciepts = _documentService.Fetch(receiptSpec),
                DocumentNumbering = _numberingService.GetByOrganizaionId(orgId)
            };


            return View("Index", model);
        }

        [HttpGet]
        public ActionResult Edit(Guid? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index");
            }

            var doc = _documentService.Get(id.Value);

            if(doc == null)
            {
                return RedirectToAction("Index");
            }


            return View("Edit", doc);
        }

        [HttpPost]
        public ActionResult Edit(Document model)
        {
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var doc = _documentService.Get(model.ID);

            doc.Status = model.Status;

            _documentService.Update(doc, true);

            return View("Edit", doc);
        }

        [HttpPost]
        public ActionResult UpdateNumbering(DocumentNumbering model)
        {
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var existed = _numberingService.GetByOrganizaionId(model.OrganizationId);

            existed.Invoice = model.Invoice;
            existed.Receipt = model.Receipt;
            existed.InvoiceReceipt = model.InvoiceReceipt;
            existed.InvoiceCredit = model.InvoiceCredit;
            existed.ProformaInvoice = model.ProformaInvoice;
            existed.InvoiceOrder = model.InvoiceOrder;
            existed.InvoiceQuote = model.InvoiceQuote;
            existed.InvoiceShip = model.InvoiceShip;


            _numberingService.Commit(true);

            var spec = new DocumentSpecifiation()
            {
                OrganizationId = existed.OrganizationId
            };

            return RedirectToAction("Index", spec);
        }

        [HttpGet]
        public ActionResult Delete(Guid? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index");
            }

            var document = _documentService.Get(id.Value);

            if (document != null)
            {
                _documentService.Remove(document, true);
            }

            return RedirectToAction("Index");
        }
    }
}