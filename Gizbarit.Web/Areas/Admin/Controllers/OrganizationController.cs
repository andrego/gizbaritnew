﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils;
using Gizbarit.Web.Areas.Admin.Models;
using Gizbarit.Web.Quartz.Jobs;
using Gizbarit.Web.Quartz.Services;
using Quartz;

namespace Gizbarit.Web.Areas.Admin.Controllers
{
    public class OrganizationController : AdminControllerBase
    {
        private readonly IOrganisationService _organisationService;
        private readonly IUserNotifyService _userNotifyService;
        public OrganizationController(IOrganisationService organisationService, IUserNotifyService userNotifyService)
        {
            _organisationService = organisationService;
            _userNotifyService = userNotifyService;
        
            

        }
        // GET: Admin/Organization
        public async Task<ActionResult> Index()
        {
            var model = await _organisationService.GetByStatusAsync(OrganizationStatus.Active);

            return View("Index", model);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index", "Organization");
            }

            var org = _organisationService.Get(id.Value);

            if (org == null)
            {
                return RedirectToAction("Index", "Organization");
            }
            
            var model = OrganizationViewModel.Create(org);

            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(OrganizationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Edit", model);
            }

            var org = _organisationService.Get(model.ID);

            org.UniqueID = model.UniqueID;
            org.Name = model.Name;
            org.Password = model.Password;
            org.PaidPeriodExpirationDate = model.PaidPeriodExpirationDate;
            org.TrialPeriodExpirationDate = model.TrialPeriodExpirationDate;
            org.TrialPeriod = model.TrialPeriod;
            
            _userNotifyService.RemoveJob(org);
            _userNotifyService.AddOrUpdateJob<SendNotificationJob>(org);
           
            _organisationService.Commit(true);

            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            var org = _organisationService.Get(id);

            org.Status = OrganizationStatus.Deleted;
            org.ApiActive = false;
            org.Name = $"{org.Name} [{org.UniqueID}]";
            org.TrialPeriodExpirationDate = DateTime.MinValue;
            org.PaidPeriodExpirationDate = DateTime.MinValue;
            org.ViewDate = DateTime.UtcNow;
            org.UniqueID = Guid.NewGuid().ToString();

            _organisationService.Commit(true);

            return RedirectToAction("Index");
        }
    }
}