﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Gizbarit.DAL;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Models;
using Gizbarit.Utils;

namespace Gizbarit.Web.Areas.Admin.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        // GET: Admin/Account
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }



        [HttpPost]
        public ActionResult Login(AdminLoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Login", model);
            }

            try
            {
                var adminUser = GetUser(model);
                var verifed = new LoginUserVerifyResult
                {
                    Email = model.UserName,
                    ID = adminUser.ID,
                    ExpirationDate = DateTime.MaxValue
                };
                verifed.Email = model.UserName;
                Gizbarit.Utils.Helper.SetAuthCookie(verifed, true);

                return RedirectToAction("Index", "Organization", routeValues: new { Area = "Admin" });
            }
            catch (Exception e)
            {
                return View("Login", model);
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
            }

            return Redirect("/Login");
        }


        private static AdminUser GetUser(AdminLoginViewModel model)
        {
            using (var context = new GizbratDataContext())
            {
                var user = context.AdminUsers.First(x => x.UserName == model.UserName && x.Password == model.Password);

                return user;
            }
        }
    }
}