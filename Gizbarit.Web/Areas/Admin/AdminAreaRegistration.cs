﻿using System.Web.Mvc;

namespace Gizbarit.Web.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Admin";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                defaults: new {controller = "Account", action = "Index", id = UrlParameter.Optional},
                namespaces: new string[] {"Gizbarit.Web.Areas.Admin.Controllers"}
            );
        }
    }
}