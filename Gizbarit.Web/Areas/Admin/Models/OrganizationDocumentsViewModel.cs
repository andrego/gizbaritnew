﻿using System.Collections.Generic;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Organization;

namespace Gizbarit.Web.Areas.Admin.Models
{
    public class OrganizationDocumentsViewModel
    {
        public Organization Organization { get; set; }

        public IEnumerable<Document> Reciepts { get; set; } = new List<Document>();

        public IEnumerable<Document> Invocies { get; set; } = new List<Document>();
        public IEnumerable<Document> InvocieReciepts { get; set; } = new List<Document>();

        public DocumentNumbering DocumentNumbering { get; set; } = new DocumentNumbering();

        public DocumentType SelectedType { get; set; } = DocumentType.Receipt;
    }
}