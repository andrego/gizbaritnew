﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Gizbarit.DAL.Entities.Organization;

namespace Gizbarit.Web.Areas.Admin.Models
{
    public class OrganizationViewModel
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string UniqueID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Paid period expiration")]
        public DateTime PaidPeriodExpirationDate { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Trial period expiration")]
        public DateTime TrialPeriodExpirationDate { get; set; }

        [DisplayName("Trial period")]
        public bool TrialPeriod { get; set; }

        private OrganizationStatus Status { get; set; } = OrganizationStatus.Active;


        public static OrganizationViewModel Create(Organization item)
        {
            if (item == null)
            {
                return new OrganizationViewModel();
            }

            var instance = new OrganizationViewModel()
            {
                ID = item.ID,
                UniqueID = item.UniqueID,
                Name = item.Name,
                Email = item.Email,
                PaidPeriodExpirationDate = item.PaidPeriodExpirationDate,
                TrialPeriodExpirationDate = item.TrialPeriodExpirationDate,
                TrialPeriod = item.TrialPeriod,
                Password = item.Password,
                Status = item.Status
            };

            return instance;
        }
    }
}