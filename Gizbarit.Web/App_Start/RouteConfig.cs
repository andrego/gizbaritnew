﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Gizbarit.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.RouteExistingFiles = true;

            routes.MapRoute(
                name: "Documents",
                url: "Documents",
                defaults: new { controller = "Documents", action = "PriceOffers", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Logos",
                url: "Content/Logos/{id}",
                defaults: new { controller = "Storage", action = "Logo" }
            );

            routes.MapRoute(
                name: "Catalog",
                url: "Content/Catalog/{id}",
                defaults: new { controller = "Storage", action = "Catalog" }
            );

            routes.MapRoute(
                name: "Video",
                url: "Home/Video/{id}",
                defaults: new { controller = "Home", action = "Video", id = UrlParameter.Optional },
                namespaces: new[] { "Gizbarit.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new []{ "Gizbarit.Web.Controllers" }
            );
        }
    }
}
