﻿using System.Web.Optimization;

namespace Gizbarit.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/excel").Include(
                "~/Scripts/xlsx.core.min.js",
                "~/Scripts/FileSaver.js",
                "~/Scripts/tableexport.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/general").Include(
            "~/Scripts/Shared/general.js",
            "~/Scripts/autoresize/autosize.js",
            "~/Scripts/jquery-ui-1.12.1.min.js",
            "~/Scripts/Shared/documents.js",
            "~/Scripts/alertifyjs/alertify.min.js"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/bundles/tablesorter").Include(
                "~/Content/tablesorter/style.css"));

            bundles.Add(new StyleBundle("~/Content/dropzonescss").Include(
                "~/Css/dropzone/basic.min.css",
                "~/Css/dropzone/dropzone.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/dropzonescripts").Include(
                "~/Scripts/dropzone/dropzone.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/tablesorter").Include(
                "~/Scripts/jquery.tablesorter.min.js"));



            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery.unobtrusive-ajax.min.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Scripts/moment.min.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/jquery-ui-1.12.1.min.js",
                "~/Scripts/respond.min.js",
                "~/Scripts/DataTables/media/js/jquery.dataTables.min.js",
                "~/Scripts/DataTables/media/js/dataTables.bootstrap.min.js",
                "~/Scripts/Summernote/summernote.min.js",
                "~/Scripts/Summernote/plugin/specialchars/summernote-ext-specialchars.js",
                "~/Scripts/Summernote/plugin/rtl/summernote-ext-rtl.js"
            //    "~/Scripts/Tinymce/tinymce.min.js"
                )

            );

            bundles.Add(new StyleBundle("~/bundles/admin-ltr").Include(
                "~/Content/bootstrap.min.css",
                "~/Css/Shared/jquery-ui.css",
                "~/Css/datepicker/latoja.datepicker.css",
                "~/Content/font-awesome.min.css",
                "~/Content/DataTables/media/css/dataTables.bootstrap.min.css",
                "~/Scripts/Summernote/summernote.css"
                ));

            bundles.Add(new StyleBundle("~/bundles/admin-rtl").Include(
               "~/Content/bootstrap.min.css",
               "~/Content/bootstrap-rtl.min.css",
               "~/Content/font-awesome.min.css",
               "~/Content/themes/base/datepicker.css",
               "~/Content/font-awesome.min.css",
               "~/Content/DataTables/media/css/dataTables.bootstrap.min.css",
               "~/Scripts/Summernote/summernote.css"
               ));

            bundles.Add(new StyleBundle("~/bundles/advert-style").Include(
                "~/Css/Advertisement/AdvShowStyle.css",
                "~/Css/Advertisement/IndexAdvertisementCreate.css"));

            bundles.Add(new StyleBundle("~/bundles/main").Include(
                "~/Css/Shared/style.css",
                "~/Css/Shared/add_style.css",
                "~/Css/Shared/jquery-ui.css",
                "~/Css/datepicker/latoja.datepicker.css",
                "~/Css/Advertisement/IndexAdvertisementCreate.css"));

            bundles.Add(new StyleBundle("~/bundles/main-rtl").Include(
                "~/Css/Shared/style_il.css"));
            bundles.Add(new StyleBundle("~/bundles/alertifyjs").Include(
                "~/Scripts/alertifyjs/css/alertify.min.css",
                "~/Scripts/alertifyjs/css/themes/default.min.css"));
            bundles.Add(new StyleBundle("~/bundles/alertifyjs-rtl").Include(
                "~/Scripts/alertifyjs/css/alertify-rtl.min.css",
                "~/Scripts/alertifyjs/css/themes/default-rtl.min.css"));
        }
    }
}
