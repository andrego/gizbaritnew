﻿using System.Configuration;
using System.Reflection;
using Autofac;
using Gizbarit.DAL;
using Gizbarit.DAL.Facades.Document;
using Gizbarit.DAL.Facades.Report;
using Gizbarit.DAL.Interfaces;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Providers.Sign;
using Gizbarit.DAL.Services.Content;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Store;
using Gizbarit.Utils.Email;
using Gizbarit.Web.Quartz;
using Module = Autofac.Module;

namespace Gizbarit.Web.Util
{
    public class AutofacModuleConfig : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterServices(builder);
            RegisterInfrastructureServices(builder);
        }


        private static void RegisterServices(ContainerBuilder builder)
        {

            string dbConnectionString =
#if DEBUG
                ConfigurationManager.ConnectionStrings["DebugConnection"].ConnectionString;
#else
            ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
#endif
            builder.RegisterType<DataAccessFactory<GizbratDataContext>>()
                .As<IDataAccessFactory>()
                .WithParameter("connectionString", dbConnectionString)
                .SingleInstance();
            builder.Register(c => c.Resolve<IDataAccessFactory>().CreateUnitOfWork())
                .As<IUnitOfWork>()
                .InstancePerRequest();
            builder.RegisterType<PdfProviderFactory>().As<IDocumentProviderFactory>().InstancePerRequest();
            builder.RegisterType<PdfSignProvider>().As<ISignProvider>().InstancePerRequest();

            builder.RegisterType<StoreCatalogItemsServise>().As<IStoreCatalogItemsService>().InstancePerRequest();

            builder.RegisterType<DocumentFacade>()
                .As<IDocumentFacade>()
                .UsingConstructor(typeof(IDocumentService), typeof(IContentService),
                    typeof(IMailService), typeof(IDocumentProviderFactory),
                    typeof(ISignProvider))
                .InstancePerRequest();

            builder.RegisterType<ReportFacade>()
                .As<IReportFacade>()
                .UsingConstructor(typeof(IOrganisationService), typeof(IDocumentService), typeof(IMailService), typeof(IDocumentFacade))
                .InstancePerRequest();

            var asm = Assembly.Load("Gizbarit.DAL");
            builder
                .RegisterAssemblyTypes(asm)
                .Where(
                    t => t.Name.EndsWith("Service") || t.Name.EndsWith("Factory") || t.Name.EndsWith("EntityExpression"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            var asmServices = Assembly.Load("Gizbarit.Utils");
            builder
                .RegisterAssemblyTypes(asmServices)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Service") && t.Name != "QuarzSchedulerService")
                .AsImplementedInterfaces()
                .InstancePerRequest();

        }

        private static void RegisterInfrastructureServices(ContainerBuilder builder)
        {
            builder.RegisterType<JobsProvider>().As<IJobsProvider>();
            builder.RegisterInstance(QuarzSchedulerService.Instatnce).As<IQuarzSchedulerService>();
        }

    }
}