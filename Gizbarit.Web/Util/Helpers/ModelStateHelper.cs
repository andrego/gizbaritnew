﻿using System.Text;
using System.Web.Mvc;

namespace Gizbarit.Utils
{
    public static class ModelStateHelper
    {
        public static string SerializeError(this ModelStateDictionary modelState)
        {
            var sb = new StringBuilder();
            foreach (ModelState state in modelState.Values)
            {
                for (int index = 0; index < state.Errors.Count; index++)
                {
                    ModelError error = state.Errors[index];
                    sb.AppendFormat("{0}", error.ErrorMessage);
                    if (index != state.Errors.Count-1)
                        sb.Append(", ");
                }
            }
            return sb.ToString();
        }
    }
}