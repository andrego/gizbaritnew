﻿using System.Web.Mvc;

namespace Gizbarit.Utils
{
    public static class RazorHelpers
    {
        public static MvcHtmlString MonthToString(this HtmlHelper html, int month)
        {
            string monthString = "";
            switch (month)
            {
                case 1:
                    monthString = Translations.General.January;
                    break;
                case 2:
                    monthString = Translations.General.February;
                    break;
                case 3:
                    monthString = Translations.General.March;
                    break;
                case 4:
                    monthString = Translations.General.April;
                    break;
                case 5:
                    monthString = Translations.General.May;
                    break;
                case 6:
                    monthString = Translations.General.June;
                    break;
                case 7:
                    monthString = Translations.General.July;
                    break;
                case 8:
                    monthString = Translations.General.August;
                    break;
                case 9:
                    monthString = Translations.General.September;
                    break;
                case 10:
                    monthString = Translations.General.October;
                    break;
                case 11:
                    monthString = Translations.General.November;
                    break;
                case 12:
                    monthString = Translations.General.December;
                    break;
                default:
                    monthString = Translations.General.All;
                    break;

            }
            return new MvcHtmlString(monthString);
        }
    }
}