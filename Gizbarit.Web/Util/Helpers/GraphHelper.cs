﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Gizbarit.DAL.Models.Graph;
using Gizbarit.DAL.Services.Bank;

namespace Gizbarit.Web.Util.Helpers
{
    public class GraphDataHelper
    {
        public static async Task<List<GraphDataElement>> ComputeDaysData(IBankAccountService bankAccountService, int accountId)
        {
            var timeSpan = DateTime.UtcNow.AddMonths(2) - DateTime.UtcNow;
            var startDate = DateTime.UtcNow;
            var endDate = startDate.AddMonths(2);
            var result = new List<GraphDataElement>();
            var balance = await bankAccountService.GetBalance(accountId,
                   startDate);

            var creditTransactions = await bankAccountService.GetTransactions(accountId).Where(
                t => t.DateInserted > startDate && t.DateInserted < endDate && t.Credit)
                .OrderBy(t => t.DateInserted)
                .ToListAsync();

            var debitTransaction = await bankAccountService.GetTransactions(accountId).Where(
                t => t.DateInserted > startDate && t.DateInserted < endDate && !t.Credit)
                .OrderBy(t => t.DateInserted)
                .ToListAsync();

            for (int i = 0; i < timeSpan.TotalDays; i++)
            {
                var currDate = DateTime.UtcNow.AddDays(i);

                var credits = creditTransactions.Where(
                    t =>
                        t.DateInserted.Month == currDate.Month && t.DateInserted.Year == currDate.Year &&
                        t.DateInserted.Day == currDate.Day).Sum(t => t.Amount);
                var debits = debitTransaction.Where(
                   t =>
                       t.DateInserted.Month == currDate.Month && t.DateInserted.Year == currDate.Year &&
                       t.DateInserted.Day == currDate.Day).Sum(t => t.Amount);
                if (credits != 0 || debits != 0)
                {
                    balance += credits - debits;
                    result.Add(new GraphDataElement
                    {
                        Date = currDate,
                        Amount = Math.Round(balance, 2)
                    });
                }
            }
            return result;
        }

        public static async Task<List<GraphDataElement>> ComputeMonthData(IBankAccountService bankAccountService, int accountId)
        {
            var result = new List<GraphDataElement>();
            var currdate = DateTime.UtcNow;

            for (int i = 0; i < 12; i++)
            {
                var currMonth = currdate.AddMonths(i);
                var balance = await bankAccountService.GetBalance(accountId,
                    new DateTime(currMonth.Year, currMonth.Month, DateTime.DaysInMonth(currMonth.Year, currMonth.Month)));
                result.Add(new GraphDataElement
                {
                    Amount = balance,
                    Date = currMonth
                });
            }
            return result;
        }
    }
}