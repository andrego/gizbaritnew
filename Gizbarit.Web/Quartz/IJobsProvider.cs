﻿using System.Collections.Generic;
using Quartz;

namespace Gizbarit.Web.Quartz
{
    public interface IJobsProvider
    {
        IEnumerable<KeyValuePair<IJobDetail, ITrigger>> GetJobs();
    }
}
