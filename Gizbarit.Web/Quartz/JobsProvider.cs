﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Web.Quartz.Jobs;
using Gizbarit.Web.Quartz.Services;
using Quartz;

namespace Gizbarit.Web.Quartz
{
    public class JobsProvider : IJobsProvider
    {
        private readonly IUserNotifyService _notifyService;
        private readonly IOrganisationService _organisationService;
        public JobsProvider(IUserNotifyService notifyService, IOrganisationService organisationService)
        {
            _notifyService = notifyService;
            _organisationService = organisationService;
        }
        public IEnumerable<KeyValuePair<IJobDetail, ITrigger>> GetJobs()
        {
            var organisations =
                _organisationService.GetByStatus(OrganizationStatus.Active);

            var userNotifyJobs =
                organisations.Select(org => _notifyService.GenerateJobs<SendNotificationJob>(org));

            var jobs = new List<KeyValuePair<IJobDetail, ITrigger>>();

            foreach (var userJobs in userNotifyJobs)
            {
                jobs.AddRange(userJobs);
            }

           jobs.Add(_notifyService.GenerateJobForUpdateCurrenciesRate<UpdateCurrenciesRateJob>(new JobKey("CurrencyRateUpdateJob") ));
            return jobs;
        }
    }
}