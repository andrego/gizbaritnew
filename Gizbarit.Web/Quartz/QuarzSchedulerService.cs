﻿using System;
using System.Text;
using Autofac;
using NLog;
using Quartz;
using Quartz.Impl.Matchers;

namespace Gizbarit.Web.Quartz
{
    public class QuarzSchedulerService:IQuarzSchedulerService, IQuarzSchedulerSingleton
    {
        private IScheduler _scheduler;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static QuarzSchedulerService _instatnce;

        public static QuarzSchedulerService Instatnce
        {
            get { return _instatnce ?? (_instatnce = new QuarzSchedulerService()); }
        }

        public void SetDiContainer(IContainer container)
        {
            if (container != null)
            {
                _scheduler = container.Resolve<IScheduler>();
                _scheduler.JobFactory = new AutofacJobFactory(container);
            }
        }

        public void AddJob(IJobDetail job, ITrigger trigger, bool replace = false)
        {
            if (replace)
            {
                if (_scheduler.CheckExists(job.Key))
                    DeleteJob(job.Key);
            }
            _logger.Info($"Job added {job.Key}");
            _scheduler.ScheduleJob(job, trigger);
        }

        public void DeleteJob(JobKey jobKey)
        {
            _scheduler.DeleteJob(jobKey);
        }

        public void Start(IJobsProvider startJobProvider = null)
        {
            if (startJobProvider != null)
            {
                foreach (var jobPair in startJobProvider.GetJobs())
                {
                    AddJob(jobPair.Key, jobPair.Value, true);
                }
            }
            _scheduler.Start();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public string List()
        {
            var sb = new StringBuilder();
            var jobGroups = _scheduler.GetJobGroupNames();
            foreach (string group in jobGroups)
            {
                var groupMatcher = GroupMatcher<JobKey>.GroupContains(group);
                var jobKeys = _scheduler.GetJobKeys(groupMatcher);
                foreach (var jobKey in jobKeys)
                {
                    var detail = _scheduler.GetJobDetail(jobKey);
                    var triggers = _scheduler.GetTriggersOfJob(jobKey);
                    foreach (ITrigger trigger in triggers)
                    {
                        sb.AppendLine("Group: "+group+"<br>");
                        sb.AppendLine("Name: "+jobKey.Name + "<br>");
                        sb.AppendLine("Description: "+detail.Description + "<br>");
                        sb.AppendLine("Trigger key name: "+trigger.Key.Name + "<br>");
                        sb.AppendLine("Trigger key group: " + trigger.Key.Group + "<br>");
                        sb.AppendLine("Trigger type: " + trigger.GetType().Name + "<br>");
                        sb.AppendLine("State: " + _scheduler.GetTriggerState(trigger.Key) + "<br>");
                        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();
                        if (nextFireTime.HasValue)
                        {
                            sb.AppendLine("Next fire: "+nextFireTime.Value.LocalDateTime + "<br>");
                        }

                        DateTimeOffset? previousFireTime = trigger.GetPreviousFireTimeUtc();
                        if (previousFireTime.HasValue)
                        {
                            sb.AppendLine("Prev fire: "+previousFireTime.Value.LocalDateTime + "<br>");
                        }
                    }
                    sb.AppendLine("<br>");
                }
            }
            return sb.ToString();
        }
    }
}