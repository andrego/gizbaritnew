﻿using System;
using System.Collections.Generic;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.Utils;
using Quartz;

namespace Gizbarit.Web.Quartz.Services
{
    public class UserNotifyService : IUserNotifyService
    {
        private readonly IQuarzSchedulerService _quarzSchedulerService;

        public UserNotifyService(IQuarzSchedulerService quarzSchedulerService)
        {
            _quarzSchedulerService = quarzSchedulerService;
        }

        public KeyValuePair<IJobDetail, ITrigger> GenerateJobForUpdateCurrenciesRate<T>(JobKey jobKey) where T : IJob
        {
            IJobDetail job = JobBuilder.Create<T>()
              .WithIdentity(jobKey)
              .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule(m => m.WithIntervalInHours(2).RepeatForever())
                .Build();

            return new KeyValuePair<IJobDetail, ITrigger>(job, trigger);
        }

        private KeyValuePair<IJobDetail, ITrigger> GenerateJob<T>(JobKey jobKey, DateTimeOffset startAt) where T : IJob
        {
            IJobDetail job = JobBuilder.Create<T>()
               .WithIdentity(jobKey)
               .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .StartAt(startAt)
                .Build();

            return new KeyValuePair<IJobDetail, ITrigger>(job, trigger);
        }

        public IEnumerable<KeyValuePair<IJobDetail, ITrigger>> GenerateJobs<T>(Organization organization) where T : IJob
        {
            var list = new List<KeyValuePair<IJobDetail, ITrigger>>();

            if (organization.TrialPeriod)
            {
                if (organization.TrialPeriodExpirationDate.AddDays(-7) > DateTime.Now.Date)
                    list.Add(GenerateJob<T>(GenerateTrialEndJobKey(organization.ID),
                        organization.TrialPeriodExpirationDate.AddDays(-7)));

                if (organization.TrialPeriodExpirationDate.AddDays(-3) > DateTime.Now.Date)
                    list.Add(GenerateJob<T>(GenerateTrialEnd3JobKey(organization.ID),
                   organization.TrialPeriodExpirationDate.AddDays(-3)));

                //list.Add(GenerateJob<T>(GenerateDisableTrialJobKey(organization.ID),
                //organization.TrialPeriodExpirationDate));
            }
            else
            {
                if (organization.PaidPeriodExpirationDate.AddDays(-7) > DateTime.Now.Date)
                    list.Add(GenerateJob<T>(GenerateExpirationEndJobKey(organization.ID),
                        organization.PaidPeriodExpirationDate.AddDays(-7)));

                if (organization.PaidPeriodExpirationDate.AddDays(-3) > DateTime.Now.Date)
                    list.Add(GenerateJob<T>(GenerateExpirationEnd3JobKey(organization.ID),
                        organization.PaidPeriodExpirationDate.AddDays(-3)));
            }
            return list;
        }

        public void AddOrUpdateJob<T>(Organization organization) where T : IJob
        {
            var jobs = GenerateJobs<T>(organization);
            foreach (var job in jobs)
            {
                _quarzSchedulerService.AddJob(job.Key, job.Value, true);
            }
        }

        public void RemoveJob(Organization organization)
        {
            if (!organization.TrialPeriod)
            {
                _quarzSchedulerService.DeleteJob(GenerateTrialEndJobKey(organization.ID));
                _quarzSchedulerService.DeleteJob(GenerateDisableTrialJobKey(organization.ID));
            }
            else
            {
                _quarzSchedulerService.DeleteJob(GenerateExpirationEndJobKey(organization.ID));
            }
        }

        private JobKey GenerateTrialEndJobKey(int id)
        {
            return new JobKey(id.ToString(), Constants.TrialEndJobGroupKey);
        }

        private JobKey GenerateTrialEnd3JobKey(int id)
        {
            return new JobKey(id.ToString(), Constants.TrialEndJobGroupKey3);
        }

        private JobKey GenerateExpirationEndJobKey(int id)
        {
            return new JobKey(id.ToString(), Constants.ExpirationJobGroupKey);
        }

        private JobKey GenerateExpirationEnd3JobKey(int id)
        {
            return new JobKey(id.ToString(), Constants.ExpirationJobGroupKey3);
        }

        private JobKey GenerateDisableTrialJobKey(int id)
        {
            return new JobKey(id.ToString(), Constants.DisableTrialModeGroupKey);
        }
    }
}