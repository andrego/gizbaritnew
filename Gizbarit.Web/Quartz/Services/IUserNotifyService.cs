﻿using System;
using System.Collections.Generic;
using Gizbarit.DAL.Entities.Organization;
using Quartz;

namespace Gizbarit.Web.Quartz.Services
{
    public interface IUserNotifyService
    {
        KeyValuePair<IJobDetail, ITrigger> GenerateJobForUpdateCurrenciesRate<T>(JobKey jobKey) where T : IJob;
 
         IEnumerable<KeyValuePair<IJobDetail, ITrigger>> GenerateJobs<T>(Organization organization) where T : IJob;
        void AddOrUpdateJob<T>(Organization organization) where T : IJob;
        void RemoveJob(Organization organization);
    }
}
