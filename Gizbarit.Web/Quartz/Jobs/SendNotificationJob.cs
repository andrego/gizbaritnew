﻿using System;
using System.Net.Mail;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils;
using Gizbarit.Utils.Email;
using Gizbarit.DAL.Resources.Mail;
using NLog;
using Quartz;

namespace Gizbarit.Web.Quartz.Jobs
{
    public class SendNotificationJob : IJob
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IMailService _mailService;
        private readonly IOrganisationService _organisationService;

        public SendNotificationJob(IMailService mailService, IOrganisationService organisationService)
        {
            _mailService = mailService;
            _organisationService = organisationService;
        }

        public void Execute(IJobExecutionContext context)
        {
            _logger.Info($"!!! Job for organisation {context.JobDetail.Key.Name}");
            var orgId = int.Parse(context.JobDetail.Key.Name);

            var organisation = _organisationService.Get(orgId);

            if (context.JobDetail.Key.Group == Constants.TrialEndJobGroupKey ||
                context.JobDetail.Key.Group == Constants.TrialEndJobGroupKey3)
            {
                _logger.Info($"Trial period end for organisation {organisation.UniqueID}");
                SendTrialEndEmail(organisation);
            }

            if (context.JobDetail.Key.Group == Constants.ExpirationJobGroupKey ||
                context.JobDetail.Key.Group == Constants.ExpirationJobGroupKey3)
            {
                _logger.Info($"Paid period end for organisation {organisation.UniqueID}");
                SendYearEndEmail(organisation);
            }

            if (context.JobDetail.Key.Group == Constants.DisableTrialModeGroupKey)
            {
                _logger.Info(
                    $"Disabled trial mode for [{organisation.UniqueID}]." +
                    $" TrialEndDate was at: {organisation.TrialPeriodExpirationDate.ToString("dd.MM.yyyy")}");
                DisableTrial(organisation);
            }
        }

        private void SendTrialEndEmail(Organization organization)
        {
            var message = new MailMessage(organization.Email, organization.Email)
            {
                Subject = TemplateResource.TrialEndHeader,
                Body = String.Format(TemplateResource.DefaultMailTemplate2,
                String.Format(TemplateResource.TrialEndBody,organization.TrialPeriodExpirationDate.ToString("dd.MM.yyyy"), organization.GetGizbaritPrice())),
                IsBodyHtml = true
            };

            _mailService.SendFromAdmin(message);
        }

        private void SendYearEndEmail(Organization organization)
        {
            var message = new MailMessage(organization.Email, organization.Email)
            {
                Subject = TemplateResource.YearEndHeader,
                Body = String.Format(TemplateResource.DefaultMailTemplate2,
                String.Format(TemplateResource.YearEndBody, organization.PaidPeriodExpirationDate.ToString("dd.MM.yyyy"), organization.GetGizbaritPrice())),
                IsBodyHtml = true
            };

            _mailService.SendFromAdmin(message);
        }

        private void DisableTrial(Organization organization)
        {
            organization.TrialPeriod = false;
            _organisationService.Commit(true);
        }
    }
}