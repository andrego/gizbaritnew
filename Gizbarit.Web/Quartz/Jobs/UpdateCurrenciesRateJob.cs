﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Models.CurrenciesRateDeserialize;
using Gizbarit.DAL.Services.Other;
using NLog;
using Quartz;

namespace Gizbarit.Web.Quartz.Jobs
{
    public class UpdateCurrenciesRateJob : IJob
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ICurrencyService _currencyService;

        private static object locker = new object();

        public UpdateCurrenciesRateJob(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }

        public void Execute(IJobExecutionContext context)
        {
            _logger.Info($"Job for update currencies rate {context.JobDetail.Key.Name}");
            UpdateCurrenciesRate();
        }


        private void UpdateCurrenciesRate()
        {
            try
            {

                XmlSerializer formatter = new XmlSerializer(typeof(Currencies));

                var data = LoadDocumentAsync("http://www.boi.org.il/currency.xml").Result;

                var deserializedCurrencies = (Currencies)formatter.Deserialize(new StringReader(data));
                var ils = _currencyService.Get("ILS");
                if (ils == null)
                    return;

                foreach (var item in deserializedCurrencies.Currency)
                {
                    var currency = _currencyService.Get(item.CurrencyCode);
                    if (currency == null)
                    {
                        currency = new Gizbarit.DAL.Entities.Currency()
                        {
                            ID = item.CurrencyCode,
                            Name = item.Name,
                            Country = item.Country
                        };
                        _currencyService.Create(currency, true);
                    }

                    var currencyRate =
                        ils.CurrenciesRates.FirstOrDefault(m => m.Symbol == item.CurrencyCode);
                    if (currencyRate == null)
                    {
                        ils.CurrenciesRates.Add(new CurrenciesRate()
                        {
                            ConversionRate = item.Rate,
                            Date = DateTime.Parse(deserializedCurrencies.LastUpdate),
                            Symbol = item.CurrencyCode,
                        });
                    }
                    else
                    {
                        currencyRate.ConversionRate = item.Rate;
                        currencyRate.Date = DateTime.Parse(deserializedCurrencies.LastUpdate);
                    }

                }
                _currencyService.Commit(true);

            }
            catch (Exception ex)
            {

                _logger.Error($"Error in updatecurrenciesrate: {ex.Message}");
            }
        }

        private async Task<string> LoadDocumentAsync(string loadUri)
        {
            using (WebClient client = new WebClient())
            {
                return await client.DownloadStringTaskAsync(new Uri(loadUri));
            }
        }
    }
}