﻿namespace Gizbarit.Web.Quartz
{
    public interface IQuarzSchedulerSingleton
    {
        void Start(IJobsProvider startJobProvider = null);
        void Stop();
    }
}
