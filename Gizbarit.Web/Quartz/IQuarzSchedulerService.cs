﻿using Quartz;

namespace Gizbarit.Web.Quartz
{
    public interface IQuarzSchedulerService
    {
        void AddJob(IJobDetail job, ITrigger trigger, bool replace = false);
        void DeleteJob(JobKey jobKey);
    }
}
