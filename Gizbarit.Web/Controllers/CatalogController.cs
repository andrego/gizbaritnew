﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Models.Organisation;
using Gizbarit.DAL.Providers.IO;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils;
using Microsoft.Ajax.Utilities;

namespace Gizbarit.Web.Controllers
{
    public class CatalogController : BaseController
    {
        private IStorage _storage = StorageFactory.Create();
        private readonly IOrganisationService _organisationService;
        private readonly ICatalogService _catalogService;

        public CatalogController(IOrganisationService organisationService, ICatalogService catalogService)
        {
            _organisationService = organisationService;
            _catalogService = catalogService;
        }


        // GET: Catalog
        public ActionResult Index()
        {
            var org = _organisationService.Get(User.Identity.GetOrgId());
            var items = org.CatalogItems.Select(c => new CatalogItemViewModel
            {
                ID = c.ID,
                Name = c.Name,
                Active = c.Active,
                Code = c.Code,
                Price = c.Price,
                PriceAfterTax = c.PriceAfterTax,
                VatPercentage = c.VatPercentage,
                ImagePath = c.ImagePath ?? "plus_ic.png"
            }).ToList();

            if (!items.Any())
            {
                items = new List<CatalogItemViewModel>
                {
                    new CatalogItemViewModel {ID = -1, ImagePath="plus_ic.png"}
                };
            }
            return View(items);
        }

        [HttpPost]
        public ActionResult Index(List<CatalogItemViewModel> items)
        {
            
            if (ModelIsValid(items))
            {
                var organization = _organisationService.Get(User.Identity.GetOrgId());
                foreach (var item in items)
                {
                    if (item.ID == -1)
                    {
                        organization.CatalogItems.Add(new CatalogItem
                        {
                            Name = item.Name,
                            Code = item.Code,
                            Active = item.Active,
                            Price = item.Price,
                            PriceAfterTax = item.PriceAfterTax,
                            VatPercentage = item.VatPercentage,
                            ImagePath = item.ImagePath
                        });
                        
                    }
                    else
                    {
                        var currentItem = organization.CatalogItems.FirstOrDefault(c => c.ID == item.ID);
                        if (currentItem == null)
                            return HttpNotFound(Translations.Errors.CatalogItemNotFound);
                        
                        currentItem.Name = item.Name;
                        currentItem.Code = item.Code;
                        currentItem.Price = item.Price;
                        currentItem.Active = item.Active;
                        currentItem.PriceAfterTax = item.PriceAfterTax;
                        currentItem.VatPercentage = item.VatPercentage;

                        if (!item.ImagePath.IsNullOrWhiteSpace())
                            currentItem.ImagePath = item.ImagePath;

                    }
                    _organisationService.Commit(true);
                }
                return RedirectToAction("Index");
            }
            return View(items);
        }


        [HttpPost]
        public ActionResult UploadImage()
        {
            string fileName;
            try
            {
                if (Request.Files.Count == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    fileName = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";
                    var savedFileName = Path.Combine(Server.MapPath("~/Content/Catalog/"), fileName);
                    file.SaveAs(savedFileName);
                    _storage.Write(Path.Combine("catalog", fileName), file.InputStream);
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return Json(new { Path = fileName });
        }

        public ActionResult Delete(int itemId)
        {
            if (itemId == -1)
                return RedirectToAction("Index");

            var organization = _organisationService.Get(User.Identity.GetOrgId());

            var item = organization.CatalogItems.FirstOrDefault(a => a.ID == itemId);
            if (item != null)
            {
                if(item.ImagePath != null)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/Catalog/"), item.ImagePath);
                    System.IO.File.Delete(path);
                    _storage.Delete(Path.Combine("catalog", item.ImagePath));
                }
                _catalogService.Remove(item,true);
                return RedirectToAction("Index");
            }
            return HttpNotFound("Item not found");
        }

        public bool ModelIsValid(List<CatalogItemViewModel> items)
        {
            if (ModelState.IsValid)
            {
                var org = _organisationService.GetCurrent();
                for (int index = 0; index < items.Count; index++)
                {
                    if(items[index].ID != -1)
                        continue;

                    var currentCode = items[index].Code;
                    if (_catalogService.GetAll().Any(c => c.Code == currentCode && c.OrganizationId == org.ID))
                    {
                        ModelState.AddModelError("["+index+"].Code", Translations.Errors.ProductCodeExists);
                    }
                }
                
            }
            return ModelState.IsValid;
        }
    }
}