﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Models.Account.Bank;
using Gizbarit.DAL.Models.Graph;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.Utils;
using Gizbarit.Web.Filters;
using Gizbarit.Web.Util.Helpers;

namespace Gizbarit.Web.Controllers
{
    public class BankController : BaseController
    {
        private readonly IBankAccountService _bankAccountService;
        private readonly IBankNameService _bankNameService;
        private readonly IOrganisationService _organisationService;
        private readonly ITransactionService _transactionService;
        private readonly IAdvanceService _advanceService;
        private readonly ICurrencyService _currencyService;

        public BankController(IOrganisationService organisationService, IBankAccountService bankAccountService,
            IBankNameService bankNameService, ITransactionService transactionService, IAdvanceService advanceService,
            ICurrencyService currencyService)
        {
            _bankAccountService = bankAccountService;
            _bankNameService = bankNameService;
            _organisationService = organisationService;
            _transactionService = transactionService;
            _advanceService = advanceService;
            _currencyService = currencyService;
        }

        // GET: Bank
        public ActionResult Accounts()
        {
            var bankAccounts =
                           _bankAccountService.GetUserAccounts(User.Identity.GetOrgId())
                           .Include(b => b.BankName)
                           .Where(a => !a.IsDeleted)
                           .ToList()
                           .Select(ba => new BankAccountViewModel()
                           {
                               Id = ba.ID,
                               Bank = _bankNameService.GetAll().Select(bn => new SelectListItem
                               {
                                   Value = bn.ID.ToString(),
                                   Text = bn.Name
                               }),
                               BankId = ba.BankNameId,
                               Limit = ba.Limit,
                               AccountBalance = ba.InitialBalance,
                               AccountNo = ba.AccountNumber,
                               Branch = ba.BranchName,
                               IsActive = ba.Active ? 1 : 0
                           }).ToList();

            if (!bankAccounts.Any())
            {
                bankAccounts = new List<BankAccountViewModel> { new BankAccountViewModel { Id = -1 } };
            }
            return View(bankAccounts);
        }

        [HttpPost]
        public ActionResult Accounts(List<BankAccountViewModel> bankAccounts)
        {
            if (ModelState.IsValid)
            {
                var organization = _organisationService.Get(User.Identity.GetOrgId());

                foreach (var account in bankAccounts)
                {
                    if (account.Id == -1)
                    {
                        var newAccount = new DAL.Entities.Bank.BankAccount
                        {
                            IsDeleted = false,
                            AccountNumber = account.AccountNo,
                            BranchName = account.Branch,
                            Limit = account.Limit,
                            BankNameId = account.BankId,
                            Active = account.IsActive == 1,
                            InitialBalance = account.AccountBalance
                        };
                        organization.BankAccounts.Add(newAccount);
                        _organisationService.Commit(true);
                        _bankAccountService.SetYearlyBalance(DateTime.Now.Year, newAccount.ID, newAccount.InitialBalance);
                    }
                    else
                    {
                        var currentAccount = organization.BankAccounts.FirstOrDefault(a => a.ID == account.Id);
                        if (currentAccount == null)
                            return HttpNotFound("Account not found");

                        currentAccount.AccountNumber = account.AccountNo;
                        currentAccount.BranchName = account.Branch;
                        currentAccount.BankNameId = account.BankId;
                        currentAccount.Active = account.IsActive == 1;
                        currentAccount.Limit = account.Limit;
                        currentAccount.IsDeleted = account.IsDeleted;

                        _organisationService.Commit(true);
                    }

                }
                return RedirectToAction("Accounts");
            }

            return View(bankAccounts);
        }

        public ActionResult Delete(int accountId)
        {
            if (accountId == -1)
                return RedirectToAction("Accounts");

            var organization = _organisationService.Get(User.Identity.GetOrgId());

            var account = organization.BankAccounts.FirstOrDefault(a => a.ID == accountId);
            if (account == null)
                return HttpNotFound("Account not found");
            if (organization.BankAccounts.Count(a => !a.IsDeleted) == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, Translations.Errors.AtLeastOne);
            }
            if (account.Active)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, Translations.Errors.CantDeleteActiveAccount);
            }

            account.IsDeleted = true;
                _organisationService.Commit(true);
                return RedirectToAction("Accounts");
            
        }

        public async Task<ActionResult> Transactions(int? accountId, int? month, int? year, bool showTaxesFirst = false, string currency = "ILS")
        {
            month = month ?? DateTime.UtcNow.Month;
            year = year ?? DateTime.UtcNow.Year;

            var account = accountId == null
                ? _bankAccountService.GetDefaultAccount(User.Identity.GetOrgId())
                : _bankAccountService.GetUserAccounts(User.Identity.GetOrgId()).FirstOrDefault(a => a.ID == accountId);

          
            if (account == null)
                return HttpNotFound();

            var filterYear = year;
            var filterMonth = month;
            if (filterMonth == 0)
            {
                filterMonth = 12;
                filterYear--;
            }

            var model = await GetBankPageModel(account, year.Value, month.Value, filterYear.Value, filterMonth.Value, currency);

            ViewBag.Year = year;
            ViewBag.Month = month;
            ViewBag.CurrentAccount = account.ToAccountMiniViewModel();
            ViewBag.ShowTaxesFirst = showTaxesFirst;
            ViewBag.AccountId = account.ID;

            

            model.Currency = currency;
            if (currency != "ILS")
            {
                
                ViewBag.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();
            }


            return View(model);
        }

        private async Task<BankPageModel> GetBankPageModel(BankAccount account, int year, int month, int filterYear, int filterMonth, string currency)
        {
            
            return new BankPageModel
            {
                Transactions = await _bankAccountService.GetTransactions(account.ID).Where(
                   t => t.DateInserted.Month == month && t.DateInserted.Year == year &&   t.Currency.ID == currency)
                   .OrderBy(t => t.DateInserted)
                   .Select(t => new TransactionViewModel
                   {
                       Id = t.ID,
                       DateOperation = t.DateInserted,
                       Description = t.Details,
                       Amount = t.Amount,
                       Credit = t.Credit,
                       AccountId = t.BankId,
                       Currency = t.Currency.ID
                       
                   }).ToListAsync(),

               MonthInitialBalance = (currency == null || currency == "ILS")? await _bankAccountService.GetBalance(account.ID,
                  new DateTime((int)filterYear, (int)filterMonth, 1)):
                await _bankAccountService.GetTransactionsDiffByDates(account.ID,
                                new DateTime((int)filterYear, 1, 1), new DateTime((int)filterYear, (int)filterMonth, 1), currency ),

                Accounts = _bankAccountService.GetUserAccounts(User.Identity.GetOrgId())
                          .Include(b => b.BankName)
                          .Where(a => !a.IsDeleted)
                          .ToList()
                          .Select(ba => new BankAccountMiniViewModel()
                          {
                              Id = ba.ID,
                              BankName = ba.BankName.Name,
                              AccountNumber = ba.AccountNumber,
                              IsDefault = ba.Active,
                              Limit = ba.Limit
                          }).ToList(),
            };
        }

        public ActionResult GetGraphData(int? accountId)
        {
            var account = accountId == null
                ? _bankAccountService.GetDefaultAccount(User.Identity.GetOrgId())
                : _bankAccountService.GetUserAccounts(User.Identity.GetOrgId()).FirstOrDefault(a => a.ID == accountId);

            if (account == null)
                return HttpNotFound();

            var model = new BankGraphDataModel
            {
                GraphMonthData = AsyncHelpers.RunSync<List<GraphDataElement>>(() => GraphDataHelper.ComputeMonthData(_bankAccountService, account.ID)),
                GraphDaysData = AsyncHelpers.RunSync<List<GraphDataElement>>(() => GraphDataHelper.ComputeDaysData(_bankAccountService, account.ID)),
                Limit = account.Limit
            };
            return PartialView("_GraphPartialView", model);
        }

        //[ValidateAjax]
        [HttpPost]
        public ActionResult AddTransaction(EditTransactionViewModel model)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            
            if (ModelState.IsValid)
            {
                var org = _organisationService.Get(User.Identity.GetOrgId());


                if (model.DateOperation < org.DateCreated)
                {

                    return Json(serializer.Serialize(new { Success = false, Message = Translations.Bank.OperationError }));
                }

                var account =
                       _bankAccountService.GetUserAccounts(User.Identity.GetOrgId())
                           .FirstOrDefault(a => a.ID == model.AccountId);
                if (account == null)
                    return Json(serializer.Serialize(new { Success = false, Message = Translations.Bank.Bank404 }));
                model.Currency = model.Currency.Trim();
                var currency = _currencyService.Get(model.Currency);
                if (model.DateEnd == null)
                {
                    
                    account.BankTransactions.Add(new BankTransaction
                    {
                        Amount = model.Amount,
                        Details = model.Description,
                        Credit = model.Credit,
                        DateInserted = model.DateOperation.SetCurrentTime(),
                        DateUpdated = model.DateOperation.SetCurrentTime(),
                        OrganizationId = User.Identity.GetOrgId(),
                        ID = Guid.NewGuid(),
                        Currency = currency
                       
                    });
                    _bankAccountService.UpdateYearlyBalance(account.ID);
                }
                else
                {
                    var monthDiff = MonthDiff((DateTime)model.DateEnd, model.DateOperation);
                    if (monthDiff > 0)
                    {
                        var firstOperationDate = model.DateOperation;
                        for (int i = 0; i <= monthDiff; i++)
                        {
                            var date = firstOperationDate.AddMonths(i);
                            account.BankTransactions.Add(new BankTransaction
                            {
                                Amount = model.Amount,
                                Details = model.Description,
                                Credit = model.Credit,
                                DateInserted = date.SetCurrentTime(),
                                DateUpdated = date.SetCurrentTime(),
                                OrganizationId = User.Identity.GetOrgId(),
                                ID = Guid.NewGuid(),
                                Currency = currency
                            });
                        }
                        _bankAccountService.UpdateYearlyBalance(account.ID);
                    }
                }
                _bankAccountService.Commit(true);
                return Json(serializer.Serialize(new { Success = true, Message = "" }));
            }
            return Json(serializer.Serialize(new { Success = false, Message = ViewData.ModelState.SerializeError() }));

        }

        [ValidateAjax]
        [HttpPost]
        public ActionResult Transactions(TransactionViewModel model)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var account =
                      _bankAccountService.GetUserAccounts(User.Identity.GetOrgId())
                          .FirstOrDefault(a => a.ID == model.AccountId);
            if (account == null)
                return Json(serializer.Serialize(new { Success = false, Message = Translations.Errors.BankAccountNotFound }));

            var transaction = _transactionService.GetAll().FirstOrDefault(t => t.ID == model.Id && t.BankAccount.ID == account.ID);
            if (transaction == null)
                return Json(serializer.Serialize(new { Success = false, Message = Translations.Errors.TransactionNotFound }));

            transaction.Amount = model.Amount;
            if (transaction.Details != "מס הכנסה|@|Income Tax|@|Предоплата под. налога" &&
                transaction.Details != "מע\"מ|@|VAT|@|Предоплата НДС")
            {
                transaction.DateInserted = transaction.DateUpdated = model.DateOperation;
                transaction.Details = model.Description;
            }
            _transactionService.Commit(true);
            _bankAccountService.UpdateYearlyBalance(account.ID);
            return Json(serializer.Serialize(new { Success = true, Message = "" }));
        }

        public ActionResult DeleteTransaction(Guid id, int month, int year)
        {
            var orgId = User.Identity.GetOrgId();
            var transaction =
                _transactionService.GetAll().FirstOrDefault(t => t.ID == id && t.OrganizationId == orgId);
            if (transaction != null)
            {
                var accountId = transaction.BankId;
                _transactionService.Remove(transaction, true);
                return RedirectToAction("Transactions", new { accountId, month, year });
            }
            return HttpNotFound("Bad transaction");
        }

        public PartialViewResult Advances(int? year)
        {
            var orgId = User.Identity.GetOrgId();
            var org = _organisationService.Get(orgId);
            ViewBag.AdvanceRate = org.AdvancesRate;
            ViewBag.VatRate = org.TaxRate;
            year = year ?? DateTime.UtcNow.Year;
            var from = new DateTime((int)year, 1, 1);
            var to = new DateTime((int)year, 12, DateTime.DaysInMonth((int)year, 12));
            var advances = GetAdvances(orgId, from, to);

            return PartialView("_Advances", advances);
        }

        private List<AdvanceViewModel> GetAdvances(int orgId, DateTime from, DateTime to)
        {
            return
                _advanceService.GetAll()
                    .Where(a => a.OrganizationId == orgId && a.DateAction > from && a.DateAction < to && !a.Deleted)
                    .OrderBy(a => a.DateCreated)
                    .Select(a => new AdvanceViewModel
                    {
                        Id = a.ID,
                        AdvanceAmount = a.AdvanceAmount,
                        VatAmount = a.TaxAmount,
                        Credit = a.Credit,
                        Amount = a.Amount,
                        CreatedDate = a.DateCreated,
                        Description = a.Description,
                        InvoiceNumber = a.InvoiceNumber,
                        InvoiceId = a.InvoiceId,
                        DocumentType = a.ForDocumentType,
                        SupplierInvoiceNumber = a.SupplierInvoiceNumber
                    }).ToList();
        }

        //[ValidateAjax]
        [HttpPost]
        public ActionResult AddAdvances(AdvanceViewModel model)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (ModelState.IsValid)
            {
                var organization = _organisationService.Get(User.Identity.GetOrgId());

                var account =
                    _bankAccountService.GetDefaultAccount(User.Identity.GetOrgId());
                if (account == null)
                    return HttpNotFound(new { Success = false, Message = "Default account not set" }.ToString());

                var advance = new Advance
                {
                    Amount = model.Amount,
                    Credit = model.Credit,
                    DateCreated = model.CreatedDate,
                    DateAction = model.CreatedDate,
                    OrganizationId = account.OrganizationID,
                    Description = model.Description,
                    AdvanceAmount = model.Credit ? model.Amount / (1 + organization.TaxRate / 100) * (organization.AdvancesRate / 100) : (double?)null,
                    //vat
                    TaxAmount = model.Credit
                        ? (model.Amount / (organization.TaxRate + 100) * organization.TaxRate)
                        : -(model.Amount / (organization.TaxRate + 100) * organization.TaxRate)
                };

                _advanceService.Create(advance, true);
                var vatPayDateDate = organization.GetVatPayDay(model.CreatedDate);
                _transactionService.UpdateAutoVatTransaction(account.ID, vatPayDateDate, advance.TaxAmount.Value);

                if (model.Credit)
                {
                    var advancePayDate = organization.GetAdvancesPayDay(model.CreatedDate);
                    _transactionService.UpdateAutoAdvanceTransaction(account.ID, advancePayDate, advance.AdvanceAmount.Value);
                }
                

                return Json(serializer.Serialize(new { Success = true, Message = "" }));
            }

            return Json(serializer.Serialize(new { Success = false, Message = ViewData.ModelState.SerializeError() }));
        }

        [HttpPost]
        public ActionResult ClearAdvances(DateTime? from, DateTime? to)
        {
            if (ModelState.IsValid)
            {
                var organization = _organisationService.Get(User.Identity.GetOrgId());
                var advancesToDelete =
                    organization.Advances.Where(
                        a =>
                            !a.Deleted && a.DateCreated >= from && a.DateCreated <= to).ToList();

                foreach (var advance in advancesToDelete)
                {
                    advance.Deleted = true;
                }
                _organisationService.Commit(true);
            }
            return RedirectToAction("Transactions", new { showTaxesFirst = true });
        }

        public async Task<ActionResult> Summary(int year)
        {
            var model = new BankSummaryViewModel();
            var organisation = _organisationService.Get(User.Identity.GetOrgId());

            model.Banks = organisation.BankAccounts.Where(b => !b.IsDeleted).Select(b => new BankSummary
            {
                Id = b.ID,
                BankName = b.BankName.Name,
                Limit = b.Limit,
                AccountNumber = b.AccountNumber,
                IsDefault = b.Active
            }).ToList();

            foreach (var bank in model.Banks)
            {
                for (short i = 0; i < 12; i++)
                {
                    bank.Values[i] = await _bankAccountService.GetBalance(bank.Id,
                        new DateTime(year, i + 1, DateTime.DaysInMonth(year, i + 1), 23, 59, 59));
                }
            }
            ViewBag.Year = year;
            return View(model);
        }

        private int MonthDiff(DateTime lValue, DateTime rValue)
        {
            return (lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year);
        }



    }
}