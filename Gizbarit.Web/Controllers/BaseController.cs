﻿using System;
using System.Web;
using System.Web.Mvc;
using Gizbarit.DAL.Common;
using Gizbarit.Utils;

namespace Gizbarit.Web.Controllers
{
    public class BaseController : Controller
    {
        protected int OrgId;
        public BaseController()
        {
            
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = User.Identity;
            if ((string)filterContext.RouteData.Values["controller"] != "Login")
            {
                CheckLogggedIn(filterContext);
            }

            if ((string)filterContext.RouteData.Values["controller"] == "Login")
            {
                if (User.Identity.IsAuthenticated)
                {
                    
                    Response.Redirect("~/Dashboard");
                }
            }

            //HttpCookie cookie = Request.Cookies[".ASPXAUTH"];
            //if (!cookie.Value.IsEmpty() && Request.Cookies["WasInvalidated"] == null)
            //{
            //    cookie.Expires = DateTime.Now.AddDays(2);
            //    Response.Cookies.Set(cookie);
            //    Response.Cookies.Add(new HttpCookie("WasInvalidated", "true"));
            //}

            //LanguageUi(filterContext);
            //getPageModel(filterContext);

        }

        public bool CheckLogggedIn(ActionExecutingContext filterContext)
        {
            if (!User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult("/Login/");
                InvalidateAspAuthCookie();
                return false;
            }
            var sc = Request.Cookies["sc"];
            if (sc == null)
            {
                filterContext.Result = new RedirectResult("/Login/");
                InvalidateAspAuthCookie();
                return false;
            }

            var scValue = Helper.Unprotect(sc.Value, "auth");
            if (scValue != DateTime.Now.Date.Ticks.ToString())
            {
                filterContext.Result = new RedirectResult("/Login/");
                InvalidateAspAuthCookie();
                return false;
            }

            OrgId = System.Web.HttpContext.Current.User.Identity.GetOrgId();
            return true;
        }

        private void InvalidateAspAuthCookie()
        {
            HttpCookie cookie = Request.Cookies[".ASPXAUTH"];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(cookie);
            }
        }
    }
}