﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Models;
using Gizbarit.DAL.Providers.IO;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Web.Quartz.Jobs;
using Gizbarit.Web.Quartz.Services;

namespace Gizbarit.Web.Controllers
{
    public class RegisterController : Controller
    {
        private readonly IStorage _storage = StorageFactory.Create();
        private readonly IOrganisationService _organisationService;
        private readonly IBankNameService _bankNameService;
        private readonly IBankAccountService _bankAccountService;
        private readonly IUserNotifyService _userNotifyService;

        public RegisterController(IOrganisationService organisationService, IBankNameService bankNameService,
            IBankAccountService accountService, IUserNotifyService userNotifyService)
        {
            _organisationService = organisationService;
            _bankNameService = bankNameService;
            _bankAccountService = accountService;
            _userNotifyService = userNotifyService;
        }

        // GET: Register
        public ActionResult Index()
        {
            var businesTypes = _organisationService.GetBusinessTypes();
            var bankNames = _bankNameService.GetAll().ToList();

            var model = new RegisterModel();

            model.SetBankNames(bankNames);
            model.SetBusinesTypes(businesTypes);

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                if (_organisationService.IsUniqueBusinessId(model.AccountProfile.BusinessId))
                {
                    model.AccountProfile.TempLogoId = model.TempLogoId;
                    model.AccountProfile.LogoExtension = model.LogoExtension;

                    var organisationDto = model.AccountProfile.ToOrganizationEntity();
                    var organisation = _organisationService.Create(organisationDto, true);

                    foreach (var bankAccountViewModel in model.BankAccounts)
                    {
                        var account = bankAccountViewModel.ToBankAccountEntity();
                        organisation.BankAccounts.Add(account);
                        _organisationService.Commit(true);
                        _bankAccountService.SetYearlyBalance(DateTime.Now.Year, account.ID, account.InitialBalance);
                    }
                    organisation.DocumentNumberings.Add(model.DocumentNumbers.ToDocumentNumbering(model.IsSmallBusines()));
                    _organisationService.Commit(true);

                    _userNotifyService.AddOrUpdateJob<SendNotificationJob>(organisation);

                    return View("Success");
                }
                ModelState.AddModelError("", Translations.Errors.BusinessNumberExists);
            }
            var businesTypes = _organisationService.GetBusinessTypes();
            var bankNames = _bankNameService.GetAll().ToList();
            model.SetBankNames(bankNames);
            model.SetBusinesTypes(businesTypes);
            return View(model);
        }
        
        public ActionResult Success()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            bool isSavedSuccessfully = true;
            string fName = String.Empty;
            var tempId = Request["tempId"];
            try
            {
                foreach (string itemFile in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[itemFile];
                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = $"{tempId}{System.IO.Path.GetExtension(file.FileName)}";
                        var savedFileName = Path.Combine(Server.MapPath("~/Content/Logos/"), fileName);
                        file.SaveAs(savedFileName);

                        _storage.Write(Path.Combine("logos", fileName), file.InputStream);
                    }
                }
            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }

            if (isSavedSuccessfully)
            {
                return Json(new { Message = fName });
            }
            return Json(new { Message = "Error in saving file" });
        }

    }
}