﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Facades.Document;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Models.Documents.Related;
using Gizbarit.DAL.Models.Organisation;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.Web.Filters;
using Gizbarit.Utils;
using Quartz.Spi;

namespace Gizbarit.Web.Controllers
{
    public partial class DocumentsController : BaseController
    {
        private readonly IOrganisationService _organizationService;
        private readonly IDocumentService _documentService;
        private readonly IDocumentItemService _documentItemService;
        private readonly ITransactionService _transactionService;
        private readonly IAdvanceService _advanceService;
        private readonly IBankAccountService _bankAccountService;
        private readonly IPaymentTypeService _paymentTypeService;
        private readonly IPaymentService _paymentService;

        private readonly DocumentUtils _documentUtils;

        private readonly Currency _currency;
        private readonly IDocumentFacade _documentFacade;

        public DocumentsController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService, IAdvanceService advanceService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService,
            IDocumentFacade documentFacade,
            ICatalogService catalogService
            )
        {
            _organizationService = organizationService;
            _documentService = documentService;
            _documentItemService = documentItemService;
            _transactionService = transactionService;
            _advanceService = advanceService;
            _bankAccountService = bankAccountService;
            _paymentTypeService = paymentTypeService;
            _paymentService = paymentService;
            _currency = currencyService.Get("ILS");
            _documentFacade = documentFacade;

            _documentUtils = new DocumentUtils(_organizationService, _documentService, _documentItemService,
                _transactionService, _advanceService, _bankAccountService, _paymentTypeService, _paymentService, catalogService, currencyService);
        }

        //public ActionResult Delete(Guid id, string redirect)
        //{
        //    var organisation = _organizationService.Get(User.Identity.GetOrgId());
        //    var document = organisation.Documents.FirstOrDefault(d => d.ID == id);
        //    if (document == null)
        //        return HttpNotFound("Document not found");
        //    _documentService.Remove(document, true);
        //    return RedirectToAction("PriceOffers");
        //}



        [HttpPost]
        public PartialViewResult GetLinkedPriceOffers(int clientId, bool isExport = false)
        {
            var organisation = _organizationService.Get(User.Identity.GetOrgId());
            var documents =
                organisation.Documents.Where(d => d.DocumentType == DocumentType.PriceOffer && !d.IsPaid && d.ClientID == clientId && d.IsExport == isExport)
                    .OrderByDescending(d => d.Number)
                    .Select(d => new LinkedPreliminaryInvoice
                    {
                        BaseDocument = new BaseDocument
                        {
                            Id = d.ID,
                            DocumentType = d.DocumentType,
                            Subject = d.Subject,
                            DateCreated = d.DateCreated,
                            Number = d.Number,
                            PriceWithTax = d.Total,
                            IsTaxIncluded = d.IsTaxIncluded
                        },
                        OfferExtras = d.InternalComments,
                        PaymentDueDateDays = Getter.GetPaymentDueDays(d),
                        IncludedCatalogItems = d.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList()
                    }).ToList();

           
            ViewBag.DocumentTypeName = (isExport)?Translations.Menu.ExportCommercialOffer:Translations.Documents.PriceOffers;
            return PartialView("~/Views/Documents/Shared/LinkedDocuments.cshtml", documents);
        }

        [HttpPost]
        public PartialViewResult GetLinkedPreliminaryInvoices(int clientId, bool showPaid = true, bool isExport = false)
        {
            var documents =
                _documentService.GetAll(isExport).Where(d => d.DocumentType == DocumentType.PreliminaryInvoice && !d.IsPaid && d.ClientID == clientId);
            if (!showPaid)
                documents = documents.Where(d => d.PreliminaryInvoice.Paid == 0);

            var model = documents.OrderByDescending(d => d.Number).ToList()
                    .Select(d => new LinkedPreliminaryInvoice
                    {
                        BaseDocument = new BaseDocument
                        {
                            Id = d.ID,
                            DocumentType = d.DocumentType,
                            Subject = d.Subject,
                            DateCreated = d.DateCreated,
                            Number = d.Number,
                            PriceWithTax = d.Total,
                            IsTaxIncluded = d.IsTaxIncluded
                        },
                        OfferExtras = d.InternalComments,
                        IncludedCatalogItems = d.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList(),
                        Paid = _documentService.GetDocumentReceiptsSum(d.ID),
                        PaymentDueDateDays = Getter.GetPaymentDueDays(d),
                    }).ToList();
            ViewBag.DocumentTypeName = (isExport)?Translations.Menu.ExportProformaInvoice:Translations.Documents.PreliminaryInvoices;
            return PartialView("~/Views/Documents/Shared/LinkedDocuments.cshtml", model);
        }

        public PartialViewResult GetLinkedDocumentsForReceipt(int clientId, Guid? selectedDocumentIdStr)
        {
            var org = _organizationService.Get(User.Identity.GetOrgId());
            var model = new LinkedDocuments();

            if (selectedDocumentIdStr != null)
            {
                model.SelectedDocumentItem =
                    _documentService.GetAll()
                        .Include(d => d.Client)
                        .Where(d => d.OrganizationID == org.ID && d.ID == selectedDocumentIdStr)
                        .OrderByDescending(d => d.Number)
                        .ToList()
                        .Select(d => new LinkedDocumentReceipt
                        {
                            Id = d.ID,
                            Number = d.Number,
                            DocumentType = d.DocumentType,
                            TotalPrice = d.TotalwithoutTax,
                            PriceWithTax = d.Total,
                            Paid =
                                (d.DocumentType == DocumentType.PreliminaryInvoice
                                    ? d.PreliminaryInvoice.Paid
                                    : d.Invoice.Paid) + _documentService.GetInvoiceTotalPrepaymentSum(d.ID)
                        }).FirstOrDefault();
            }

            model.PreliminaryInvoices =
                _documentService.GetAll()
                    .Where(
                        d =>
                            d.OrganizationID == org.ID && d.ClientID == clientId && !d.IsPaid &&
                            d.DocumentType == DocumentType.PreliminaryInvoice)
                                .OrderByDescending(d => d.Number)
                                .Select(d => new LinkedDocumentReceipt
                                {
                                    Id = d.ID,
                                    TotalPrice = d.TotalwithoutTax,
                                    PriceWithTax = d.Total,
                                    Number = d.Number,
                                    Paid = d.PreliminaryInvoice.Paid
                                }).ToList();

            model.TaxInvoices =
                _documentService.GetAll()
                    .Where(
                        d =>
                            d.OrganizationID == org.ID && d.ClientID == clientId && !d.IsPaid &&
                            d.DocumentType == DocumentType.TaxInvoice)
                    .OrderByDescending(d => d.Number)
                    .ToList()
                    .Select(d => new LinkedDocumentReceipt
                    {
                        Id = d.ID,
                        TotalPrice = d.TotalwithoutTax,
                        PriceWithTax = d.Total,
                        Number = d.Number,
                        //Paid = d.Invoice.Paid + _documentService.GetInvoiceTotalPrepaymentSum(d.ID)  
                        Paid =  _documentService.GetInvoiceTotalPrepaymentSum(d.ID) //TODO Validate this does't crash prepaid logic https://trello.com/c/oRJ1WRg3
                    }).ToList();

            return PartialView("~/Views/Documents/Shared/LinkedDocumentsForReceipt.cshtml", model);
        }


        [ValidateAjax]
        [HttpPost]
        public JsonResult GetClientsByName(string clientName)
        {
            var org = _organizationService.Get(User.Identity.GetOrgId());
            var clients = org.Clients.Where(c => c.Active && c.Name.ToLower().Contains(clientName.ToLower())).OrderBy(c => c.Name).ToList();
            if (clients.Any())
            {
                var result = clients.Select(c => new ClientViewModel
                {
                    Id = c.ID,
                    Name = c.Name,
                    Address = c.Address,
                    City = c.City,
                    Email = c.Email,
                    Fax = c.Fax,
                    Phone = c.Phone,
                    RetainerAmount = c.RetainerAmount ?? 0,
                    UniqueId = c.UniqueId,
                    Zip = c.Zip,
                    Cell = c.Cell,

                    AdditionEmails = c.ClientEmailContacts.Select(m => m.Email),
                    AdditionPhones = c.ClientPhoneContacts.Select(m => m.Phone)
                });
                return Json(result);
            }
            return null;
        }

        public ActionResult GetDocument(Guid id)
        {
            var org = _organizationService.Get(User.Identity.GetOrgId());
            var document = org.Documents.FirstOrDefault(d => d.ID == id);
            if (document == null)
                return HttpNotFound();

            switch (document.DocumentType)
            {
                case DocumentType.PreliminaryInvoice:
                    return RedirectToAction("Edit", "PreliminaryInvoice", new { id = document.ID });
                case DocumentType.PriceOffer:
                    return RedirectToAction("Edit", "PriceOffer", new { id = document.ID });
                case DocumentType.TaxInvoice:
                    return RedirectToAction("Edit", "TaxInvoice", new { id = document.ID });
                case DocumentType.Receipt:
                    return RedirectToAction("Edit", "Receipt", new { id = document.ID });
                case DocumentType.TaxInvoiceReceipt:
                    return RedirectToAction("Edit", "InvoiceRecipt", new { id = document.ID });
                case DocumentType.Waybill:
                    return RedirectToAction("Edit", "Waybill", new { id = document.ID });
            }
            return HttpNotFound();
        }

        public async Task<ActionResult> Send(Guid id, string customText = "", bool isCopyRequested = false)
        {
            HttpNotFound("Error. Document not found");
           
            var document = _documentService.Get(id);
            if (document == null)
                return HttpNotFound();

            //_documentFacade.SendByMail(id, customText, isCopyRequested);

            LockDocumentForEditing(document.ID, DocumentStatusType.Send);

            switch (document.DocumentType)
            {
                case DocumentType.PreliminaryInvoice:
                    return RedirectToAction("Edit", "PreliminaryInvoice", new { id = document.ID, wasSend = true });
                case DocumentType.PriceOffer:
                    return RedirectToAction("Edit", "PriceOffer", new { id = document.ID, wasSend = true });
                case DocumentType.TaxInvoice:
                    return RedirectToAction("Edit", "TaxInvoice", new { id = document.ID, wasSend = true });
                case DocumentType.Receipt:
                    return RedirectToAction("Edit", "Receipt", new { id = document.ID, wasSend = true });
                case DocumentType.TaxInvoiceReceipt:
                    return RedirectToAction("Edit", "InvoiceRecipt", new { id = document.ID, wasSend = true });
                case DocumentType.Waybill:
                    return RedirectToAction("Edit", "Waybill", new { id = document.ID });
            }
            return HttpNotFound("Error. Document not found");
        }

        public ActionResult Print(Guid id, bool isCopyRequested = false)
        {
           // var document = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == id);
            var document = _documentService.Get(id);
            if (document == null)
                return HttpNotFound();

            var model = _documentFacade.GetForPrint(id, isCopyRequested);
            var result = File(model.GetStream(), model.Type);
            
            //LockDocumentForEditing(id, DocumentStatusType.Printed);

            return result;
        }

        [HttpPost]
        public ActionResult SetDocumentPaidStatus(Guid id, bool isPaid)
        {
           // var document = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == id);
            var document = _documentService.Get(id);
            if (document != null)
            {
                _documentService.ChangeDocumentPaidStatus(id, isPaid);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        public ActionResult LockDocumentForEditing(Guid id, DocumentStatusType status = DocumentStatusType.Printed)
        {
            //var document =
            //   _documentService.GetUserDocuments()
            //       .Include(d => d.Payments)
            //       .Include(d => d.Organization)
            //       .First(d => d.ID == id);

            var document = _documentService.Get(id); //when lazy load fields Payments and Organization included
                        

            //TODO: Check it and move to facade
            if (status != DocumentStatusType.Printed || status != DocumentStatusType.Send)
            {
                status = DocumentStatusType.Printed;
            }

            if (document != null)
            {
                document.Status = status;
                ProcessDocumentBanking(document);

                _documentService.Commit(true);
                return RedirectToAction("GetDocument", "Documents", new { id = document.ID });
            }
            return HttpNotFound();
        }

        private void ProcessDocumentBanking(Document document)
        {
            if (!document.IsPaymentProcessed)
            {
                var org = _organizationService.GetCurrent();
                switch (document.DocumentType)
                {
                    case DocumentType.TaxInvoice:
                        _documentUtils.AddTaxesOnDocumentCreate(document.ToBaseDocument(org), document.Number, document.DocumentType);
                        break;
                    case DocumentType.Receipt:
                        ProcessPayments(document);
                        break;
                    case DocumentType.TaxInvoiceReceipt:
                        _documentUtils.AddTaxesOnDocumentCreate(document.ToBaseDocument(org), document.Number, document.DocumentType);
                        ProcessPayments(document);
                        break;
                }
                document.IsPaymentProcessed = true;
            }
            RecalculatePaymentDate(document);
        }

        private void RecalculatePaymentDate(Document document)
        {
            switch (document.DocumentType)
            {
                case DocumentType.Receipt:
                    {
                        var refDocumentId = document.Receipt.ReferenceDocument;
                        if (refDocumentId.HasValue)
                        {
                            var refDocument = _documentService.Get(refDocumentId.Value);
                            if (refDocument == null)
                                return;
                            refDocument.PaymentDueDate = document.DateCreated;
                            _documentService.Commit(true);
                        }
                        return;
                    }
                case DocumentType.TaxInvoiceReceipt:
                    {
                        var refDocuments = document.DocumentReferences.Where(t=>t.Type == DocumentType.PreliminaryInvoice).ToList();
                        foreach (var refD in refDocuments)
                        {
                            var refDocument = _documentService.Get(refD.BaseDocumentId);
                            if (refDocument == null)
                                return;
                            refDocument.PaymentDueDate = document.DateCreated;
                            _documentService.Commit(true);
                        }
                        return;
                    }
            }
        }

        private void ProcessPayments(Document document)
        {
            var bankAccount = _bankAccountService.GetDefaultAccount(User.Identity.GetOrgId());
            foreach (var payment in document.Payments.Where(p => p.PaymentType == 1 || p.PaymentType == 2))
            {
                var refDocument = document.DocumentReferences.FirstOrDefault();

                _transactionService.Create(new BankTransaction
                {
                    OrganizationId = User.Identity.GetOrgId(),
                    ID = Guid.NewGuid(),
                    Amount = Math.Abs(payment.Amount),
                    BankId = bankAccount.ID,
                    Credit = payment.Amount > 0,
                    DateInserted = payment.Date,
                    Details =
                        String.Format("{0} {1} {2} ({3})",
                            Translations.Bank.Payment,
                            Translations.Errors.PaymentForReceipt,
                            document.Number,
                            document.Client.Name),
                    DateUpdated = payment.Date,
                    CurrencyId = "ILS"
                });

            }
            _transactionService.Commit(true);

        }
    }
}