﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.Web.Util.Extensions;
using Newtonsoft.Json;

// ReSharper disable CheckNamespace
namespace Gizbarit.Web.Controllers
{
    public class PriceOfferController : BaseDocumentController
    {
        protected IDocumentProviderFactory _pdfFactory;
        public PriceOfferController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            IDocumentProviderFactory pdfFactory,
            ICatalogService catalogService) :
            base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
            this._pdfFactory = pdfFactory;
        }


        public async Task<ActionResult> Index(bool wasSend = false, bool isExport = false)
        {
            var documents = await
                _documentService.GetUserDocuments(null, isExport).Where(d => d.DocumentType == DocumentType.PriceOffer)
                .Include(d => d.Client)
                .OrderByDescending(d => d.Number)
                .ToListAsync();

            var organisation = _organizationService.GetCurrent();

            var result = documents.Select(d => d.ToBaseDocument(organisation)).ToList();
            ViewBag.WasSend = wasSend;
            ViewBag.IsExport = isExport;
            return View("~/Views/Documents/PriceOffer/PriceOffers.cshtml", result);
        }

        public ActionResult Create(bool isExport = false)
        {
            var model = InitPriceOfferViewModel();
            if (isExport)
            {
                model.BaseDocument.IsExport = true;
                model.BaseDocument.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();
              
            }

            return View("~/Views/Documents/PriceOffer/PriceOffer.cshtml", model);
        }

        public ActionResult Edit(Guid id, bool print = false, bool wasSend = false)
        {
            var document = _documentService.Get(id);
            if (document == null)
                return HttpNotFound("Document not found");

            var organisation = _organizationService.GetCurrent();
            var model = new PriceOfferViewModel
            {
                BaseDocument = document.ToBaseDocument(organisation),
                CatalogItems = Getter.GetCatalogItems(_organizationService),
                IncludedCatalogItems = document.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList(),
                PaymentDueDateDays = (document.PaymentDueDate-document.DateCreated).Value.Days,
                OfferExtras = document.InternalComments
            };
            model.BaseDocument.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();
            //model.BaseDocument.Print = print;
            //model.BaseDocument.Send = send;

            ViewBag.WasSend = wasSend;
            return View("~/Views/Documents/PriceOffer/PriceOffer.cshtml", model);
        }

        [HttpPost]
        public ActionResult Edit(PriceOfferViewModel model)
        {
            _logger.Debug(JsonConvert.SerializeObject(model));
           
            if (ModelIsValid(model))
            {
                _documentUtils.AddClient(model.BaseDocument.ClientInfo);

                if (model.BaseDocument.IsExport)
                {
                    var currencyFromDb = _currencyService.Get(model.BaseDocument.Currency);
                    if (currencyFromDb != null)
                        _currency = currencyFromDb;
                }

                var document = model.ToDocument(_currency);

                Document currentDocument;

                var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                    (_documentService.Get(model.BaseDocument.Id) == null);

                if (isNewDocument)
                {
                    currentDocument = document;
                    InitPriceOffer(currentDocument);
                    currentDocument.Organization = _organizationService.GetCurrent();
                    currentDocument.Client =
                        _organizationService.GetCurrent().Clients.FirstOrDefault(c => c.ID == currentDocument.ClientID);
                    currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.PriceOffer);

                    _documentService.Create(currentDocument, true);
                }
                else
                {
                    // currentDocument = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == document.ID);
                    currentDocument = _documentService.Get(document.ID);
                    if (currentDocument == null)
                        return HttpNotFound("Document not found");
                    if (currentDocument.InvoiceReceipt != null)
                    {
                        currentDocument.InvoiceReceipt.Paid = 0;
                    }
                    
                    currentDocument.Update(document);
                    _documentUtils.CleanDocumentItems(currentDocument);
                }
                _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);

                _documentService.Update(currentDocument, true);
                return Edit(currentDocument.ID, model.BaseDocument.Print, model.BaseDocument.Send);
            }

            model.BaseDocument.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();
            return View("~/Views/Documents/PriceOffer/PriceOffer.cshtml", model);
        }

        [NonAction]
        public PriceOfferViewModel InitPriceOfferViewModel()
        {
            return new PriceOfferViewModel
            {
                BaseDocument = _documentUtils.InitBaseDocument(DocumentType.PriceOffer),
                CatalogItems = Getter.GetCatalogItems(_organizationService),
            };
        }

        [NonAction]
        public bool ModelIsValid(PriceOfferViewModel model)
        {
            DocumentUtils.FilterModelState(ModelState, new[] { "CatalogItems" });
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.PriceOffer,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }
            }
            return ModelState.IsValid;
        }

        [NonAction]
        public void InitPriceOffer(Document d)
        {
            d.PriceOffer = new PriceOffer();
        }
    }
}