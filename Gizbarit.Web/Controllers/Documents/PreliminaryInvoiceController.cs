﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.Web.Util.Extensions;
using Newtonsoft.Json;

// ReSharper disable CheckNamespace
namespace Gizbarit.Web.Controllers
{
    public  class PreliminaryInvoiceController : BaseDocumentController
    {
        protected IDocumentProviderFactory _pdfFactory;

        public PreliminaryInvoiceController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            IDocumentProviderFactory pdfFactory,
            ICatalogService catalogService) :
            base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
            this._pdfFactory = pdfFactory;
        }

        public async Task<ActionResult> Index(bool wasSend = false, bool isExport = false)
        {
            var documents = await
       _documentService.GetUserDocuments(null,isExport).Where(d => d.DocumentType == DocumentType.PreliminaryInvoice)
                .Include(d => d.Client)
                .OrderByDescending(d => d.Number)
                .ToListAsync();

            var organisation = _organizationService.GetCurrent();

            var result = documents.Select(d => d.ToBaseDocument(organisation)).ToList();
            ViewBag.IsExport = isExport;
            ViewBag.WasSend = wasSend;

            return View("~/Views/Documents/PreliminaryInvoice/PreliminaryInvoices.cshtml", result);
        }

        public ActionResult Create(bool isExport = false)
        {
            var model = InitPreliminaryInvoiceViewModel();

            if (isExport)
            {
                model.BaseDocument.IsExport = true;
                model.BaseDocument.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();

            }

            return View("~/Views/Documents/PreliminaryInvoice/PreliminaryInvoice.cshtml", model);
        }

        public ActionResult Edit(Guid id, bool print = false, bool wasSend = false)
        {
            var document = _documentService.Get(id);
            if (document == null)
                return HttpNotFound("Document not found");

            var organisation = _organizationService.GetCurrent();

            var model = new PreliminaryInvoiceViewModel
            {
                BaseDocument = document.ToBaseDocument(organisation),
                CatalogItems = Getter.GetCatalogItems(_organizationService),
                IncludedCatalogItems = document.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList(),
                PaymentDueDateDays = Getter.GetPaymentDueDays(document),
                OfferExtras = document.InternalComments
            };

            model.BaseDocument.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();

            ViewBag.WasSend = wasSend;
            return View("~/Views/Documents/PreliminaryInvoice/PreliminaryInvoice.cshtml", model);
        }


        [HttpPost]
        public ActionResult Edit(PreliminaryInvoiceViewModel model)
        {
            _logger.Debug(JsonConvert.SerializeObject(model));
            if (!ModelIsValid(model))
            {
                model.BaseDocument.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();
                return View("~/Views/Documents/PreliminaryInvoice/PreliminaryInvoice.cshtml", model);
            }

            _documentUtils.AddClient(model.BaseDocument.ClientInfo);

            if (model.BaseDocument.IsExport)
            {
                var currencyFromDb = _currencyService.Get(model.BaseDocument.Currency);
                if (currencyFromDb != null)
                    _currency = currencyFromDb;
            }

            

            var document = model.ToDocument(_currency);

            Document currentDocument;

            var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                                (_documentService.Get(model.BaseDocument.Id) == null);

            if (isNewDocument)
            {
                currentDocument = document;
                InitPreliminaryInvoice(currentDocument);
                currentDocument.Organization = _organizationService.GetCurrent();
                currentDocument.Client =
                    _organizationService.GetCurrent().Clients.FirstOrDefault(c => c.ID == currentDocument.ClientID);
                currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.PreliminaryInvoice);

                _documentService.Create(currentDocument, true);
            }
            else
            {
                currentDocument = _documentService.Get(document.ID);
                if (currentDocument == null)
                    return HttpNotFound("Document not found");
                currentDocument.Update(document);
                _documentUtils.CleanDocumentItems(currentDocument);
                _documentService.Commit(true);
            }
            _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);

            var distinctCatalogItems =
                model.IncludedCatalogItems.Where(i => i.DocumentId != null && i.DocumentId != currentDocument.ID)
                    .GroupBy(i => i.DocumentId)
                    .Select(g => g.First());
            if (distinctCatalogItems.Any())
            {
                foreach (var item in distinctCatalogItems)
                {
                    _documentUtils.AddDocumentReferences(currentDocument, item.DocumentId,
                        DocumentType.PriceOffer,
                        DocumentType.PreliminaryInvoice);
                }
            }
            _documentService.Update(currentDocument, true);

            return Edit(currentDocument.ID, model.BaseDocument.Print, model.BaseDocument.Send);
        }

        [NonAction]
        public PreliminaryInvoiceViewModel InitPreliminaryInvoiceViewModel()
        {
            return new PreliminaryInvoiceViewModel
            {
                BaseDocument = _documentUtils.InitBaseDocument(DocumentType.PreliminaryInvoice),
                CatalogItems = Getter.GetCatalogItems(_organizationService),
            };
        }

        [NonAction]
        public bool ModelIsValid(PreliminaryInvoiceViewModel model)
        {
            DocumentUtils.FilterModelState(ModelState, new[] { "CatalogItems" });
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.PreliminaryInvoice,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }
            }
            return ModelState.IsValid;
        }

        [NonAction]
        public void InitPreliminaryInvoice(Document d)
        {
            d.PreliminaryInvoice = new PreliminaryInvoice {IsAutoCreated = false};
        }
    }
}