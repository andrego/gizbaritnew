﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.Web.Util.Extensions;
using Newtonsoft.Json;

// ReSharper disable CheckNamespace
namespace Gizbarit.Web.Controllers
{
    public class TaxInvoiceController : BaseDocumentController
    {
        private IDocumentProviderFactory _pdfFactory;

        public TaxInvoiceController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            IDocumentProviderFactory pdfFactory,
            ICatalogService catalogService) :
            base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
            this._pdfFactory = pdfFactory;
        }

        public async Task<ActionResult> Index(bool wasSend = false, bool isExport = false)
        {
            var documents = await
                 _documentService.GetUserDocuments(null,isExport).Where(d => d.DocumentType == DocumentType.TaxInvoice)
                 .Include(d => d.Client)
                 .OrderByDescending(d => d.Number)
                 .ToListAsync();
            var organisation = _organizationService.GetCurrent();
            var result = documents.Select(d => d.ToBaseDocument(organisation)).ToList();
            ViewBag.IsExport = isExport;
            ViewBag.WasSend = wasSend;
            return View("~/Views/Documents/TaxInvoice/TaxInvoices.cshtml", result);
        }

        public ActionResult Create(bool isExport = false)
        {

            var model = new TaxInvoiceViewModel
            {
                BaseDocument = _documentUtils.InitBaseDocument(DocumentType.TaxInvoice),
                CatalogItems = Getter.GetCatalogItems(_organizationService)
            };

            if (isExport)
            {
                model.BaseDocument.IsExport = true;
                model.BaseDocument.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();

            }

            return View("~/Views/Documents/TaxInvoice/TaxInvoice.cshtml", model);
        }

        public ActionResult Edit(Guid id, bool print = false, bool wasSend = false, bool sendCopy = false)
        {
            

            var document = _documentService.Get(id);
            if (document == null)
                return HttpNotFound("Document not found");

            var organisation = _organizationService.GetCurrent();

            var model = new TaxInvoiceViewModel
            {
                BaseDocument = document.ToBaseDocument(organisation),
                CatalogItems = Getter.GetCatalogItems(_organizationService),
                IncludedCatalogItems = document.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList(),
                PaymentDueDateDays = Getter.GetPaymentDueDays(document),
                OfferExtras = document.InternalComments
            };
            model.BaseDocument.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();
            //model.BaseDocument.Print = print;
            //model.BaseDocument.Send = send;
            //model.BaseDocument.SendCopy = sendCopy;
            ViewBag.WasSend = wasSend;
            return View("~/Views/Documents/TaxInvoice/TaxInvoice.cshtml", model);
        }

        [HttpPost]
        public ActionResult Edit(TaxInvoiceViewModel model)
        {
            
            _logger.Debug(JsonConvert.SerializeObject(model));
           
            if (ModelIsValid(model))
            {
                _documentUtils.AddClient(model.BaseDocument.ClientInfo);
                // add Currency To Document
                if (model.BaseDocument.IsExport)
                {
                    var currencyFromDb = _currencyService.Get(model.BaseDocument.Currency);
                    if(currencyFromDb != null)
                        _currency = currencyFromDb;
                }

                var document = model.ToDocument(_currency);

                Document currentDocument;

                var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                                    (_documentService.Get(model.BaseDocument.Id) == null);  

                if (isNewDocument)
                {
                    currentDocument = document;
                    InitTaxInvoice(currentDocument);
                    currentDocument.Organization = _organizationService.GetCurrent();
                    currentDocument.Client =
                                           _organizationService.GetCurrent().Clients.FirstOrDefault(c => c.ID == currentDocument.ClientID);
                    currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.TaxInvoice);

                    _documentService.Create(currentDocument, true);
                    
                    //TODO do just on print document
                    //_documentUtils.AddTaxesOnDocumentCreate(model.BaseDocument, document.Number, DocumentType.TaxInvoice);
                }
                else
                {
                   
                    currentDocument = _documentService.Get(document.ID);
                    if (currentDocument == null)
                        return HttpNotFound("Document not found");

                    if (currentDocument.IsOriginalGenerated())
                        return RedirectToAction("Index");

                    currentDocument.Update(document);
                    _documentUtils.CleanDocumentItems(currentDocument);
                }

                _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);

                var distinctCatalogItems =
                        model.IncludedCatalogItems.Where(i => i.DocumentId != null && i.DocumentId != currentDocument.ID)
                            .GroupBy(i => i.DocumentId)
                            .Select(g => g.First());
                if (distinctCatalogItems.Any())
                {
                    foreach (var item in distinctCatalogItems)
                    {
                        _documentUtils.AddDocumentReferences(currentDocument, item.DocumentId,
                            DocumentType.TaxInvoice,
                            DocumentType.PreliminaryInvoice);
                    }
                }
                _documentService.Update(currentDocument, true);

                return Edit(currentDocument.ID, model.BaseDocument.Print, model.BaseDocument.Send, model.BaseDocument.SendCopy);
            }
            model.BaseDocument.CurrencyList = _currencyService.GetAll().Select(m => m.ID).ToList();
            return View("~/Views/Documents/TaxInvoice/TaxInvoice.cshtml", model);
        }

        [NonAction]
        public bool ModelIsValid(TaxInvoiceViewModel model)
        {
            DocumentUtils.FilterModelState(ModelState, new[] { "CatalogItems" });
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.TaxInvoice,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }
            }
            return ModelState.IsValid;
        }

        [NonAction]
        public void InitTaxInvoice(Document d)
        {
            d.Invoice = new Invoice();
        }
    }
}