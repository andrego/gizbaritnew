﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Utils;
using Newtonsoft.Json;

// ReSharper disable CheckNamespace
namespace Gizbarit.Web.Controllers
{
    public class WaybillController : BaseDocumentController
    {
        private IDocumentProviderFactory _pdfFactory;

        public WaybillController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            IDocumentProviderFactory pdfFactory,
            ICatalogService catalogService) :
            base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
            this._pdfFactory = pdfFactory;
        }

        public async Task<ActionResult> Index()
        {
            var documents = await
                            _documentService.GetUserDocuments().Where(d => d.DocumentType == DocumentType.Waybill)
                            .Include(d => d.Client)
                            .OrderByDescending(d => d.Number)
                            .ToListAsync();

            var organisation = _organizationService.GetCurrent();

            var result = documents.Select(d => d.ToBaseDocument(organisation)).ToList();

            ViewBag.IsExport = false;
            return View("~/Views/Documents/Waybill/Waybills.cshtml", result);
        }

        public ActionResult Create()
        {
            var model = InitWayBillViewModel();
            return View("~/Views/Documents/Waybill/Waybill.cshtml", model);
        }

        public ActionResult Edit(Guid id, bool print = false)
        {
            var document = _documentService.GetUserDocuments().Include(d=>d.DocumentItems).FirstOrDefault(d => d.ID == id);
            if (document == null)
                return HttpNotFound("Document not found");

            var organisation = _organizationService.GetCurrent();

            var model = new WayBillViewModel()
            {
                BaseDocument = document.ToBaseDocument(organisation),
                CatalogItems = Getter.GetCatalogItems(_organizationService),
                IncludedCatalogItems = document.DocumentItems.Select(di => di.ToDocumentItemViewModel()).ToList(),
            };
            model.BaseDocument.Print = print;

            return View("~/Views/Documents/Waybill/Waybill.cshtml", model);
        }

        [HttpPost]
        public ActionResult Edit(WayBillViewModel model)
        {
            _logger.Debug(JsonConvert.SerializeObject(model));
            if (!ModelIsValid(model))
            {
                return View("~/Views/Documents/Waybill/Waybill.cshtml", model);
            }

            _documentUtils.AddClient(model.BaseDocument.ClientInfo);
            var document = model.ToDocument(_currency);

            Document currentDocument;

            var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                                !_documentService.GetUserDocuments()
                                    .Any(x => x.ID == model.BaseDocument.Id);

            if (isNewDocument)
            {
                currentDocument = document;
                InitWayBill(currentDocument);
                currentDocument.Organization = _organizationService.GetCurrent();
                currentDocument.Client =
                    _organizationService.GetCurrent().Clients.FirstOrDefault(c => c.ID == currentDocument.ClientID);
                currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.Waybill);

                _documentService.Create(currentDocument, true);
            }
            else
            {
                currentDocument = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == document.ID);
                if (currentDocument == null)
                    return HttpNotFound("Document not found");


                if (currentDocument.IsOriginalGenerated())
                    return RedirectToAction("Index");

                currentDocument.Update(document);
                    
                _documentUtils.CleanDocumentItems(currentDocument);
            }

            if (!CheckTaxCheme(model, document))
            {
                ModelState.AddModelError("",
                    Translations.Errors.DifferentTaxInclusionScheme);
                return View("~/Views/Documents/Waybill/Waybill.cshtml", model);
            }

            _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);

            _documentService.Update(currentDocument, true);
            _documentUtils.ProcessToPreliminaryInvoice(currentDocument);
            return Edit(currentDocument.ID, model.BaseDocument.Print);
        }

        private bool CheckTaxCheme(WayBillViewModel model, Document document)
        {
            var thisMonthPreliminaryInvoice =
                _documentService.GetAutoCreatedPreliminaryInvoice(model.BaseDocument.ClientInfo.Id,
                        model.BaseDocument.DateCreated);

            var isNewDocument = (thisMonthPreliminaryInvoice == null);
            if (isNewDocument)
                return true;

            return thisMonthPreliminaryInvoice.IsTaxIncluded == document.IsTaxIncluded;
        }

        

        [NonAction]
        public WayBillViewModel InitWayBillViewModel()
        {
            return new WayBillViewModel
            {
                BaseDocument = _documentUtils.InitBaseDocument(DocumentType.Waybill),
                CatalogItems = Getter.GetCatalogItems(_organizationService)
            };
        }

        [NonAction]
        public void InitWayBill(Document d)
        {
            d.Waybill = new Waybill();
        }

        [NonAction]
        public bool ModelIsValid(WayBillViewModel model)
        {
            DocumentUtils.FilterModelState(ModelState, new[] { "CatalogItems" });
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.Waybill,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }
            }
            return ModelState.IsValid;
        }
    }
}