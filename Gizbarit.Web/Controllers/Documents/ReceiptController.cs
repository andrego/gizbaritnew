﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.Utils;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Quartz.Spi;

// ReSharper disable CheckNamespace
namespace Gizbarit.Web.Controllers
{
    public class ReceiptController : BaseDocumentController
    {
        private IDocumentProviderFactory _pdfFactory;
        public ReceiptController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            IDocumentProviderFactory pdfFactory,
            ICatalogService catalogService) :
            base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
            this._pdfFactory = pdfFactory;
        }

        public async Task<ActionResult> Index(bool wasSend = false)
        {
            var documents = await
                _documentService.GetUserDocuments().Where(d => d.DocumentType == DocumentType.Receipt)
                .Include(d => d.Client)
                .OrderByDescending(d => d.Number)
                .ToListAsync();

            var smallBusinessReceipts =
               await _documentService.GetUserDocuments()
                   .Where(d => d.DocumentType == DocumentType.TaxInvoiceReceipt && d.IsSmallBusiness)
                   .Include(d => d.Client)
                   .OrderByDescending(d => d.Number)
                   .ToListAsync();

            documents.AddRange(smallBusinessReceipts);

            var result = documents.Select(d => d.ToReceiptViewModelContainer()).ToList();

            foreach (var document in result)
            {
                if (document.ReferenceDocumentId != Guid.Empty)
                {
                    var refDoc = 
                        _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == document.ReferenceDocumentId);

                    if (refDoc == null)
                        continue;

                    if (refDoc.DocumentType == DocumentType.PreliminaryInvoice)
                    {
                        document.PreliminaryInvoice = refDoc.Number;
                    }
                    if (refDoc.DocumentType == DocumentType.TaxInvoice)
                    {
                        document.TaxInvoice = refDoc.Number;
                    }
                }
            }
            ViewBag.WasSend = wasSend;
            ViewBag.IsExport = false;
            return View("~/Views/Documents/Receipt/Receipts.cshtml", result);
        }

        public ActionResult Create()
        {
            var model = InitReceiptViewModel();
            return View("~/Views/Documents/Receipt/Receipt.cshtml", model);
        }

        public ActionResult Edit(Guid id, bool print = false, bool wasSend = false, bool sendCopy = false)
        {
            var document =
                _documentService.GetUserDocuments()
                    .Include(d => d.Client)
                    .Include(d => d.Payments)
                    .Include(d => d.Receipt)
                    .FirstOrDefault(d => d.ID == id);
            if (document == null)
            {
                return HttpNotFound("Document not found");
            }
            var organisation = _organizationService.Get(User.Identity.GetOrgId());

            var model = new ReceiptViewModel
            {
                BaseDocument = document.ToBaseDocument(organisation),
                PaymentTypes = Getter.GetPaymentTypes(_paymentTypeService),
                Payments = document.Payments.ToPayments()
            };

            if (document.Receipt.ReferenceDocument != null)
            {
                var refDoc = organisation.Documents.FirstOrDefault(d => d.ID == document.Receipt.ReferenceDocument);
                if (refDoc != null)
                {
                    model.SelectedDocumentItem = new BaseDocument
                    {
                        Id = refDoc.ID,
                        Number = refDoc.Number,
                        DocumentType = refDoc.DocumentType
                    };
                }
            }
            else
            {
                model.OtherDescription = document.ExternalComments;
            }

            //model.BaseDocument.Print = print;
            //model.BaseDocument.Send = send;
            //model.BaseDocument.SendCopy = sendCopy;
            ViewBag.WasSend = wasSend;

            return View("~/Views/Documents/Receipt/Receipt.cshtml", model);
        }

        [HttpPost]
        public ActionResult Edit(ReceiptViewModel model)
        {
            _logger.Debug(JsonConvert.SerializeObject(model));
            if (!ModelIsValid(model))
            {
                return View("~/Views/Documents/Receipt/Receipt.cshtml", model);
            }

            _documentUtils.AddClient(model.BaseDocument.ClientInfo);

            double deduction = 0;
            Document currentDocument;
            var document = model.ToDocument(_currency);
            var isNewDocument = model.BaseDocument.Id == Guid.Empty ||
                               !_documentService.GetUserDocuments()
                                                .Any(x => x.ID == model.BaseDocument.Id);

            //var document = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == model.BaseDocument.Id);
            if (isNewDocument) //new document
            {
                currentDocument = document;
                Document refDocument = null;

                if (!String.IsNullOrEmpty(model.OtherDescription))
                {
                    currentDocument.ExternalComments = model.OtherDescription;
                }
                else
                {
                    refDocument = _documentService.Get(model.SelectedDocumentItem.Id);
                    if (refDocument == null)
                        return HttpNotFound("Document not found");

                    try
                    {
                        switch (refDocument.DocumentType)
                        {
                            case DocumentType.TaxInvoice:
                                deduction = ProcessTaxInvoiceDocument(currentDocument, refDocument, model.PaymentsTotal);
                                break;
                            case DocumentType.PreliminaryInvoice:
                                deduction = ProcessPreliminaryinvoice(currentDocument, refDocument, model.PaymentsTotal);
                                break;
                        }
                    }
                    catch (InvalidProgramException)
                    {
                        ModelState.AddModelError("",
                        String.Format(
                            Translations.Errors.SumPaymentsHasToBeLess + ": {0}",
                            refDocument.Total));
                        return View("~/Views/Documents/Receipt/Receipt.cshtml", model);
                    }
                }

                currentDocument.Receipt = new Receipt()
                {
                    ReferenceDocument = refDocument?.ID,
                    Deduction = deduction
                };
                currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.Receipt);
                _documentService.Create(currentDocument, true);
            }
            else
            {
                currentDocument = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == document.ID);
                if (currentDocument == null)
                    return HttpNotFound("Document not found");
                if (document.IsOriginalGenerated())
                    return RedirectToAction("Index");

                if (!currentDocument.ExternalComments.IsNullOrWhiteSpace())
                {
                    currentDocument.ExternalComments = model.OtherDescription;
                }
                else
                {
                    var refDocument = _documentService.Get(model.SelectedDocumentItem.Id);

                    if (refDocument == null)
                        return HttpNotFound("Document not found");

                    try
                    {
                        switch (refDocument.DocumentType)
                        {
                            case DocumentType.TaxInvoice:
                                deduction = ProcessTaxInvoiceDocument(currentDocument, refDocument, model.PaymentsTotal);
                                break;
                            case DocumentType.PreliminaryInvoice:
                                deduction = ProcessPreliminaryinvoice(currentDocument, refDocument, model.PaymentsTotal);
                                break;
                        }
                    }
                    catch (InvalidProgramException)
                    {
                        ModelState.AddModelError("",
                        String.Format(
                            Translations.Errors.SumPaymentsHasToBeLess + ": {0}",
                            refDocument.Total));
                        return View("~/Views/Documents/Receipt/Receipt.cshtml", model);
                    }

                    currentDocument.Receipt.Deduction = deduction;
                }
                currentDocument.Update(document);
                _documentService.Update(currentDocument, true);
            }

            SavePayments(model, currentDocument);

            return Edit(currentDocument.ID, model.BaseDocument.Print, model.BaseDocument.Send, model.BaseDocument.SendCopy);

        }


        private void SavePayments(ReceiptViewModel model, Document document)
        {
            #region paymentProcessing

            var documentPayments = document.Payments.ToList();
            foreach (var payment in documentPayments)
            {
                _paymentService.Remove(payment, true);
            }

            foreach (var payment in model.Payments)
            {
                _paymentService.Create(new Payment
                {
                    Amount = payment.Sum,
                    DocumentID = document.ID,
                    PaymentType = payment.PaymentType,
                    Date = payment.DateOperation,
                    BankName = payment.BankName,
                    CreditCardType = payment.CreditCardType,
                    PaymentNumber = payment.CheckNumber,
                    BranchName = payment.BranchName,
                    AccountNumber = payment.AccountNumber,
                    PayerID = document.ClientID
                });
            }
            _paymentTypeService.Commit(true);

            #endregion
        }




        [NonAction]
        public bool ModelIsValid(ReceiptViewModel model)
        {
            DocumentUtils.FilterModelState(ModelState, new[] { "CatalogItems", "BaseDocument.Subject", "SelectedDocumentItem" });
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.Receipt,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }

                if ((model.SelectedDocumentItem == null || model.SelectedDocumentItem.Id == default(Guid)) &&
                    String.IsNullOrEmpty(model.OtherDescription))
                {
                    ModelState.AddModelError("", Translations.Errors.SelectSourceDocument);
                }

                if (model.Payments.Any(p => p.PaymentType == 0))
                {
                    ModelState.AddModelError("", Translations.Errors.NoPaymentMethodSelected);
                }
            }
            return ModelState.IsValid;

        }

        private double ProcessTaxInvoiceDocument(Document document, Document referenceDocument, double totalPayments)

        {
            if (!referenceDocument.IsPaid)
            {
                double amountToReduce = document.IsNew() ? 0 : document.Total;

                if (referenceDocument.Invoice.Paid - amountToReduce + totalPayments > referenceDocument.Total)
                {
                    throw new InvalidProgramException();
                }

                referenceDocument.Invoice.Paid -= amountToReduce;

                referenceDocument.Invoice.Paid += totalPayments;

                var deduction = referenceDocument.Total - referenceDocument.Invoice.Paid;

                if (referenceDocument.Invoice.Paid == referenceDocument.Total)
                {
                    referenceDocument.IsPaid = true;
                }

                if (document.DocumentReferences.All(d => d.BaseDocumentId != referenceDocument.ID))
                {
                    document.DocumentReferences.Add(new DocumentReference
                    {
                        BaseDocumentId = referenceDocument.ID,
                        Type = referenceDocument.DocumentType,
                        CreditAmount = deduction
                    });
                }

                return deduction;
            }
            return 0;

        }

        private double ProcessPreliminaryinvoice(Document document, Document referenceDocument, double totalPayments)
        {
            double amountToReduce = 0;
            if (!document.IsNew())
            {
                amountToReduce = document.Total;
            }

            if (referenceDocument.PreliminaryInvoice.Paid - amountToReduce + totalPayments > referenceDocument.Total)
            {
                throw new InvalidProgramException();
            }

            referenceDocument.PreliminaryInvoice.Paid -= amountToReduce;
            referenceDocument.PreliminaryInvoice.Paid += totalPayments;

            var deduction = referenceDocument.Total - referenceDocument.PreliminaryInvoice.Paid;
            if (referenceDocument.PreliminaryInvoice.Paid == referenceDocument.Total)
            {
                referenceDocument.IsPaid = true;
            }
            if (document.DocumentReferences.All(d => d.BaseDocumentId != referenceDocument.ID))
            {
                document.DocumentReferences.Add(new DocumentReference
                {
                    BaseDocumentId = referenceDocument.ID,
                    Type = referenceDocument.DocumentType,
                    CreditAmount = deduction
                });
            }
            return deduction;
        }

        private enum PaymentType
        {
            Payment,
            Prepayment
        }

        [NonAction]
        public ReceiptViewModel InitReceiptViewModel()
        {
            return new ReceiptViewModel
            {
                BaseDocument = _documentUtils.InitBaseDocument(DocumentType.Receipt),
                PaymentTypes = Getter.GetPaymentTypes(_paymentTypeService),
            };
        }
    }
}