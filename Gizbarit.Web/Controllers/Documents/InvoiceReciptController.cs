﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Facades.Document;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.Utils;
using Newtonsoft.Json;

// ReSharper disable CheckNamespace
namespace Gizbarit.Web.Controllers
{
    public class InvoiceReciptController : BaseDocumentController
    {
        protected IDocumentFacade _documentFacade;
        public InvoiceReciptController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService,
            IDocumentFacade documentFacade,
            ICatalogService catalogService) :
            base(organizationService, currencyService,
                documentService, documentItemService,
                transactionService,
                bankAccountService, paymentTypeService,
                paymentService, advanceService, catalogService)
        {
            this._documentFacade = documentFacade;
        }

        public async Task<ActionResult> Index(bool wasSend = false)
        {
            var querry = _documentService.GetUserDocuments()
                .Where(d => d.DocumentType == DocumentType.TaxInvoiceReceipt)
                .Include(d => d.Client);

            querry = User.Identity.GetOrgType() == Enums.BusinessType.Exemption
                ? querry.Where(d => d.IsSmallBusiness)
                : querry.Where(d => !d.IsSmallBusiness);

            var documents = await querry.OrderByDescending(d => d.Number)
                .ToListAsync();
            
            var result = documents.Select(d => d.ToInvoiceReceiptViewModelContainer()).ToList();

            ViewBag.WasSend = wasSend;
            ViewBag.IsExport = false;
            return View("~/Views/Documents/InvoiceReceipt/InvoiceReceipts.cshtml", result);
        }

        public ActionResult Create()
        {
            var model = InitInvoiceReceiptViewModel();
            return View("~/Views/Documents/InvoiceReceipt/InvoiceReceipt.cshtml", model);
        }

        public ActionResult Edit(Guid id, bool print = false, bool wasSend = false, bool sendCopy = false)
        {
            var document = _documentService.GetUserDocuments().Include(d => d.Payments).FirstOrDefault(d => d.ID == id);
            if (document == null)
                return HttpNotFound("Document not found");

            var organisation = _organizationService.GetCurrent();
            var model = new InvoiceReceiptViewModel
            {
                BaseDocument = document.ToBaseDocument(organisation),
                CatalogItems = Getter.GetCatalogItems(_organizationService),
                IncludedCatalogItems =
                    _documentItemService.GetAll()
                        .Where(di => di.DocumentID == document.ID)
                        .ToList()
                        .Select(di => di.ToDocumentItemViewModel())
                        .ToList(),
                PaymentDueDateDays = Getter.GetPaymentDueDays(document),
                PaymentTypes = Getter.GetPaymentTypes(_paymentTypeService),
                Payments = document.Payments.ToPayments(),
                OfferExtras = document.InternalComments
            };
            ViewBag.WasSend = wasSend;

            return View("~/Views/Documents/InvoiceReceipt/InvoiceReceipt.cshtml", model);
        }

        [HttpPost]
        public ActionResult Edit(InvoiceReceiptViewModel model)
        {
            _logger.Debug(JsonConvert.SerializeObject(model));
            if (!ModelIsValid(model))
            {
                return View("~/Views/Documents/InvoiceReceipt/InvoiceReceipt.cshtml", model);
            }

            _documentUtils.AddClient(model.BaseDocument.ClientInfo);

            var document = model.ToDocument(_currency, Request.UserHostAddress);

            Document currentDocument;
            var isNewDocument = model.BaseDocument.Id == Guid.Empty || 
                                !_documentService.GetUserDocuments()
                                                 .Any(x=>x.ID == model.BaseDocument.Id);

            if (isNewDocument)
            {
                currentDocument = document;
                InitInvoiceReceipt(currentDocument);
                currentDocument.Organization = _organizationService.GetCurrent();
                currentDocument.Client =
                    _organizationService.GetCurrent().Clients.FirstOrDefault(c => c.ID == currentDocument.ClientID);
                currentDocument.Number = _documentService.GetLastDocumentNumber(DocumentType.TaxInvoiceReceipt);

                if (User.Identity.GetOrgType() == Enums.BusinessType.Exemption)
                {
                    currentDocument.IsSmallBusiness = true;
                }

                _documentService.Create(currentDocument, true);

            }
            else
            {
                currentDocument = _documentService.GetUserDocuments().FirstOrDefault(d => d.ID == document.ID);
                if (currentDocument == null)
                    return HttpNotFound("Document not found");

//                    if (currentDocument.IsOriginalGenerated())
//                        return RedirectToAction("Index");

                currentDocument.Update(document);
                if (currentDocument.InvoiceReceipt != null)
                {
                    currentDocument.InvoiceReceipt.Paid = 0;
                }

                _documentUtils.CleanDocumentItems(currentDocument);
                _documentUtils.CleanDocumentPayments(currentDocument);
                   
            }

            _documentUtils.AddDocumentItems(currentDocument, model.IncludedCatalogItems);

            var distinctCatalogItems =
                model.IncludedCatalogItems.Where(i => i.DocumentId != null && i.DocumentId != currentDocument.ID)
                    .GroupBy(i => i.DocumentId)
                    .Select(g => g.First());
            if (distinctCatalogItems.Any())
            {
                foreach (var item in distinctCatalogItems)
                {
                    _documentUtils.AddDocumentReferences(currentDocument, item.DocumentId,
                        DocumentType.PriceOffer,//TODO check it!
                        DocumentType.PreliminaryInvoice);
                }
            }
            else if (currentDocument.DocumentReferences.Count > 0)
            {
                currentDocument.InvoiceReceipt.Paid += currentDocument.DocumentReferences.Sum(r => r.CreditAmount) ?? 0;
            }

            var bankAccount = _bankAccountService.GetDefaultAccount(OrgId);

            foreach (var payment in model.Payments)
            {
                currentDocument.InvoiceReceipt.Paid += payment.Sum;

                _documentUtils.AdDocumentPayments(payment, currentDocument.ID,
                    currentDocument.ClientID);

            }

            currentDocument.InvoiceReceipt.Deduction = currentDocument.Total -
                                                       currentDocument.InvoiceReceipt.Paid;

            _documentService.Update(currentDocument, true);

            return Edit(currentDocument.ID, model.BaseDocument.Print, model.BaseDocument.Send, model.BaseDocument.SendCopy);
        }

        [NonAction]
        public void InitInvoiceReceipt(Document d)
        {
            d.InvoiceReceipt = new InvoiceReceipt
            {
                Deduction = d.Total
            };
        }

        [NonAction]
        public bool ModelIsValid(InvoiceReceiptViewModel model)
        {
            DocumentUtils.FilterModelState(ModelState, new[] { "CatalogItems" });
            if (ModelState.IsValid)
            {
                if (model.BaseDocument.Id == Guid.Empty &&
                    !_documentUtils.VerifyDocumentCreationDate(DocumentType.TaxInvoiceReceipt,
                        model.BaseDocument.DateCreated))
                {
                    ModelState.AddModelError("", Translations.Errors.CreationDateGreater);
                }

                if (model.BaseDocument.PriceWithTax != model.PaymentsTotal)
                {
                    ModelState.AddModelError("", Translations.Errors.SumPaymentsHasToBeLess);
                }
            }
            return ModelState.IsValid;
        }

        [NonAction]
        public InvoiceReceiptViewModel InitInvoiceReceiptViewModel()
        {
            return new InvoiceReceiptViewModel
            {
                BaseDocument = _documentUtils.InitBaseDocument(DocumentType.TaxInvoiceReceipt),
                CatalogItems = Getter.GetCatalogItems(_organizationService),
                PaymentTypes = Getter.GetPaymentTypes(_paymentTypeService)
            };
        }
    }
}