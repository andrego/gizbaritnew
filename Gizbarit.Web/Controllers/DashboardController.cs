﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Models;
using Gizbarit.DAL.Models.Account.Bank;
using Gizbarit.DAL.Models.Documents;
using Gizbarit.DAL.Models.Graph;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils;
using Gizbarit.Web.Util.Helpers;

namespace Gizbarit.Web.Controllers
{
    public class DashboardController : BaseController
    {
        private readonly IBankAccountService _bankAccountService;
        private readonly IOrganisationService _organisationService;
        private readonly IDocumentService _documentService;

        public DashboardController(IBankAccountService bankAccountService, IOrganisationService organisationService, IDocumentService documentService)
        {
            _bankAccountService = bankAccountService;
            _organisationService = organisationService;
            _documentService = documentService;
        }
        // GET: Dashboard
        public ActionResult Index(int? accountId = null)
        {
            var account = accountId == null
                ? _bankAccountService.GetDefaultAccount(User.Identity.GetOrgId())
                : _bankAccountService.Get(accountId.Value);

            ViewBag.AccountId = account?.ID;
            return View();
        }

        public ActionResult Graph(int? accountId=null)
        {
            var account = accountId == null
                ? _bankAccountService.GetDefaultAccount(User.Identity.GetOrgId())
                : _bankAccountService.Get(accountId.Value);

            if (account == null)
                return PartialView(new List<GraphDataElement>());

            var model = AsyncHelpers.RunSync<List<GraphDataElement>>(() => GraphDataHelper.ComputeDaysData(_bankAccountService, account.ID));
            ViewBag.CurrentAccount = account.ToAccountMiniViewModel();
            ViewBag.Accounts = _bankAccountService.GetUserAccounts(User.Identity.GetOrgId()).ToList().Select(a=> new SelectListItem
            {
                Value = a.ID.ToString(),
                Text = String.Format("{0}-{1}", a.BankName.Name, a.AccountNumber)
            }).ToList();

            return PartialView(model);
        }

        public ActionResult CheckIncomeApproach()
        {
            var currentYear = DateTime.Now.Year;
            var receiptsSumm =
                _documentService.GetUserDocuments()
                    .Where(
                        d => d.DocumentType == DocumentType.Receipt || d.DocumentType == DocumentType.TaxInvoiceReceipt && d.DateCreated.Year == currentYear)
                    .Sum(d => (double?)d.Total);
            return PartialView(receiptsSumm);
        }

        public async Task<ActionResult> AccountStatus()
        {
            var accounts = _bankAccountService.GetUserAccounts(OrgId).Where(a=>!a.IsDeleted).ToList();
            Dictionary<BankAccountMiniViewModel, double> todayDues = new Dictionary<BankAccountMiniViewModel, double>();
            Dictionary<BankAccountMiniViewModel, double> tomorrowDues = new Dictionary<BankAccountMiniViewModel, double>();
            Dictionary<BankAccountMiniViewModel, AccountException> limitExceeds = new Dictionary<BankAccountMiniViewModel, AccountException>();

            for (int i = 0; i < accounts.Count; i++)
            {
                var account = accounts[i];
                var todayDue = await _bankAccountService.GetBalance(account.ID, DateTime.Today);
                var tomorrowDue = await _bankAccountService.GetBalance(account.ID, DateTime.Today.AddDays(1));

                var limitExceed = await _bankAccountService.GetLimitExceedInfo(account.ID);

                todayDues.Add(account.ToAccountMiniViewModel(), todayDue);
                tomorrowDues.Add(account.ToAccountMiniViewModel(), tomorrowDue);
                if(limitExceed!= null)
                    limitExceeds.Add(account.ToAccountMiniViewModel(), limitExceed);
            }
            ViewBag.TodayDues = todayDues;
            ViewBag.TomorrowDues = tomorrowDues;
            ViewBag.LimitExceeds = limitExceeds;
            return PartialView("_AccountStatus");
        }

        [HttpPost]
        public async Task<ActionResult> Search(DocumentSearchModel model)
        {
            var organisation = _organisationService.Get(User.Identity.GetOrgId());

            model.Query = model.Query != null
                ? model.Query.TrimEnd().ToLower()
                : String.Empty;
            
            var found = 
                _documentService.GetAll()
                    .Where(
                        d => d.OrganizationID == organisation.ID && (
                            d.Subject.ToLower().Contains(model.Query) || d.Client.Name.ToLower().Contains(model.Query) ||
                            d.Number.ToString().Contains(model.Query) || d.DocumentItems.Any(i=>i.Name.ToLower().Contains(model.Query))));

            found = model.DateStart == null
                ? found.Where(d => d.DateCreated.Year >= DateTime.Now.Year)
                : found.Where(d => d.DateCreated >= model.DateStart.Value);

            found = model.DateEnd != null
                ? found.Where(d => d.DateCreated <= model.DateEnd)
                : found;
            var result = await found.ToListAsync();

            var searchResultModel = result.Select(f => new DocumentSearchResult
            {
                Id = f.ID,
                Number = f.Number,
                Subject = f.Subject,
                ClientName = f.Client.Name,
                Type = f.DocumentType,
                Date = f.DateCreated
            });
            ViewBag.Query = model.Query;
            ViewBag.DateStart = model.DateStart;
            ViewBag.DateEnd = model.DateEnd;

            return View(searchResultModel);
        }



        public ActionResult PreliminaryInvoices()
        {
            var currentYear = DateTime.Now.Year;
            var orgId = User.Identity.GetOrgId();

            var preliminaryInvoices =
                _documentService.GetAll().Include(d => d.Payments).Include(d => d.Client)
                    .Where(
                        d =>
                            d.OrganizationID == orgId &&
                            (d.DocumentType == DocumentType.PreliminaryInvoice ||
                             ((d.DocumentType == DocumentType.TaxInvoice || d.DocumentType == DocumentType.TaxInvoiceReceipt) &&
                              (d.DocumentReferences.Count == 0 || d.DocumentReferences.FirstOrDefault().Type != DocumentType.PreliminaryInvoice))));

            preliminaryInvoices = preliminaryInvoices.Where(d => d.DateCreated.Year == currentYear);

            var model = preliminaryInvoices.OrderByDescending(d => d.DateCreated)
                .ToList().Select(d => d.ToSettlementsViewModel(_documentService));

           return PartialView(model);
        }
    }
}