﻿using System;
using System.IO;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Models.Account;
using Gizbarit.DAL.Providers.IO;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils;

namespace Gizbarit.Web.Controllers
{
    public class OrganisationController : BaseController
    {
        private readonly IStorage _stroage = StorageFactory.Create();
        private readonly IOrganisationService _organisationService;
        public OrganisationController(IOrganisationService organisationService)
        {
            _organisationService = organisationService;
        }
        public ActionResult Index()
        {
            var organization = _organisationService.Get(User.Identity.GetOrgId());
            if(organization == null)
                return HttpNotFound();
            var model = organization.ToBaseAccountProfile();
            var businesTypes = _organisationService.GetBusinessTypes();
            model.SetBusinessTypes(businesTypes);
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(BaseAccountProfile model)
        {
            var organization = _organisationService.Get(User.Identity.GetOrgId());
            if (organization == null)
                return HttpNotFound();
            if (!ModelState.IsValid)
            {
                model.SetBusinessTypes(_organisationService.GetBusinessTypes());
                return View(model);
            }

            organization.Name = model.BusinessName;
            organization.NameEng = model.BusinessNameEng;
            if (organization.UniqueID != model.BusinessId)
            {
                if (!_organisationService.IsUniqueBusinessId(model.BusinessId))
                {
                    model.SetBusinessTypes(_organisationService.GetBusinessTypes());
                    ModelState.AddModelError("", Translations.Errors.BusinessNumberExists);
                    return View(model);
                }
            }
            
            if (organization.UniqueID != "123456789" && organization.UniqueID !="987654321")
            {
                
                organization.UniqueID = model.BusinessId;
                organization.Password = model.Password;
            }
           
            organization.Email = model.Email;
            organization.AccountingMethod = (short)model.VatPeriodId;
            organization.AdvancesAccountingMethod = (short) model.TaxPeriodId;
            organization.AdvancesRate = model.IncomeTax;
            organization.Address = model.Street;
            organization.AddressEng = model.StreetEng;
            organization.City = model.City;
            organization.Fax = model.Fax;
            organization.Zip = model.Zip;
            organization.Fax = model.Fax;
            organization.Phone1 = model.Phone;
            organization.FirstName = model.FirstName;
            organization.LastName = model.LastName;
            organization.BusinessTypeId = model.BusinesTypeId;

            organization.RegisterType = (Enums.RegisterType) model.RegisterTypeId;
            organization.TaxRate = (Enums.BusinessType) model.BusinesTypeId == Enums.BusinessType.Exemption ? 0 : 17;
            organization.AdvancesAccountingMethod = (short) model.TaxPeriodId;
            organization.AccountingMethod = (short) model.VatPeriodId;
            organization.AdvancesRate = model.IncomeTax;

            if (System.IO.File.Exists(Path.Combine(Server.MapPath("~/Content/Logos/"),
                $"{model.TempLogoId}.{model.LogoExtension}")))
            {
                var pathToLogo = $"{model.TempLogoId}.{model.LogoExtension}";
                organization.PathToLogo = pathToLogo;
            }
            _organisationService.Commit(true);

            Helper.SetAuthCookie(organization.ToLoginUserVerifyResult(), false);

            return RedirectToAction("Index");
        }
    }
}