﻿using System;
using System.Collections.Generic;

using System.Globalization;
using System.Linq;

using System.Web.Mvc;
using Gizbarit.DAL.Common;

using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Payment;
using Gizbarit.DAL.Models.CustomerPayment;

using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;

using Gizbarit.Utils;


namespace Gizbarit.Web.Controllers
{
    public class CustomerPaymentController : BaseDocumentController
    {
        public CustomerPaymentController(IOrganisationService organizationService, 
            ICurrencyService currencyService, 
            IDocumentService documentService, 
            IDocumentItemService documentItemService, 
            ITransactionService transactionService, 
            IBankAccountService bankAccountService, 
            IPaymentTypeService paymentTypeService, 
            IPaymentService paymentService, 
            IAdvanceService advanceService, 
            ICatalogService catalogService) : 
                base(organizationService, 
                    currencyService, 
                    documentService, 
                    documentItemService, 
                    transactionService, 
                    bankAccountService, 
                    paymentTypeService, 
                    paymentService, 
                    advanceService, 
                    catalogService)
        {
        }
       

        public ActionResult Create(bool wasSend = false)
        {
            var model = new CustomerPaymentViewModel();
            model.BaseDocument = _documentUtils.InitBaseDocument(DocumentType.PreliminaryInvoice);
            model.PaymentDay = DateTime.Now;

            ViewBag.WasSend = wasSend;
            return View("~/Views/CustomerPayment/CustomerPayment.cshtml", model);
        }

        public ActionResult Edit(CustomerPaymentViewModel model)
        {
            
            if(model.BaseDocument.TotalPrice < model.PaymentSum || model.PaymentSum < 0)
                ModelState.AddModelError("PaymentSum", @"The  payment amount can't be more than the balance for payment in this document");

           
            FilterModeState(ModelState, new[] { "BaseDocument.Subject" });

            if (ModelState.IsValid)
            {
                
                var document = _documentService.GetAll(true).FirstOrDefault(m => m.ID == model.BaseDocument.Id);
                
                if (document != null)
                {
                     document.Payments.Add(new Payment()
                        {
                            PaymentType = 3,
                            Amount = model.PaymentSum,
                            Date = model.PaymentDay
                        });
                  


                    var bank = _bankAccountService.GetDefaultAccount(User.Identity.GetOrgId());// need check bankAccount !!!!

                    //not sure for add transaction every time. Maybe we must update exist bank transaction for CustomerPayment 
                    document.BankTransactions.Add(new BankTransaction()
                    {
                        Credit = true,
                        Amount = model.PaymentSum,
                        Currency = _currencyService.Get(model.BaseDocument.Currency),
                        DateInserted = model.PaymentDay,
                        DateUpdated = DateTime.Now,
                        ID = Guid.NewGuid(),
                        OrganizationId = User.Identity.GetOrgId(),

                        Details = $"{document.Client.Name}, {document.DocumentType.ToString()}: {document.Number}",                           
                        BankId = bank.ID 
                    });

                    if (document.Total <= document.Payments.Sum(m => m.Amount))
                    {
                        document.IsPaid = true;
                    }

                    _documentService.Update(document, true);
                }
                 return RedirectToAction("Create", "CustomerPayment", new {wasSend = true});   
            }
            return View("~/Views/CustomerPayment/CustomerPayment.cshtml", model);

        }

        protected void FilterModeState(ModelStateDictionary modelState, string[] keysToNotValidate)
        {
            List<string> keysTobeRemoved = new List<string>();

            if (keysToNotValidate != null && keysToNotValidate.Any())
            {
                foreach (var key in keysToNotValidate)
                {
                    keysTobeRemoved.AddRange(
                        modelState.Where(x => String.Compare(key, 0, x.Key, 0, key.Length) == 0)
                            .Select(x => x.Key)
                            .ToList());
                }
            }
            foreach (var item in keysTobeRemoved)
            {
                modelState.Remove(item);
            }
        }

        public JsonResult GetPaymentDocuments(int clientId)
        {

            var listDocumentNumbers = _documentService.GetAll(true).Where(m => m.ClientID == clientId &&
                                                                (m.DocumentType == DocumentType.PreliminaryInvoice || m.DocumentType == DocumentType.TaxInvoice) &&
                                                                               (m.Payments.Count == 0 || (m.Total > m.Payments.Sum(k => k.Amount)) ) && !m.IsPaid)
                .OrderBy(m => m.DocumentType)
                .ThenByDescending(m => m.Number)
                .ToList();


            var documentDictionary = new Dictionary<string, string>();
            listDocumentNumbers.ForEach(element =>
            {
              documentDictionary[element.ID.ToString()] = $"{element.DocumentType.ToString()} - {element.Number}";
            });
           

            return Json(new {list = documentDictionary}, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetDocument(string documentId)
        {
            if (!documentId.IsEmpty())
            {
                var document = _documentService.GetAll(true).FirstOrDefault(m => m.ID.ToString() == documentId);
                if (document != null)
                {
                    
                    var total = document.Total - document.Payments.Sum(m => m.Amount);
                    var referenceDocuments = document.DocumentReferences.SelectMany(m => m.BaseDocument.Payments).ToList();
                    double referencePaymentSum = 0;
                    if (referenceDocuments.Count() != 0)
                    {
                        referencePaymentSum = referenceDocuments.Sum(m => m.Amount);
                    }
                    total -= referencePaymentSum;
                    string [] result = {(total).ToString(CultureInfo.InvariantCulture), document.Currency.ID};
                    return Json(new { result}, JsonRequestBehavior.AllowGet);
                }
            }
            return Json( new {result=""}, JsonRequestBehavior.AllowGet);
        }
       
    }
}