﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages.Html;
using Gizbarit.DAL.Common;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Catalog;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Facades.Document;
using Gizbarit.DAL.Models.Store;
using Gizbarit.DAL.Providers.Store;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Store;
using Gizbarit.DAL.Utils;
using Gizbarit.Utils;
using Gizbarit.Web.Filters;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using WebGrease.Css.Ast.Selectors;

namespace Gizbarit.Web.Controllers
{
    public class StoreController : BaseController
    {
        private readonly IOrganisationService _organisationService;
        private readonly IStoreCatalogItemsService _storeCatalogItemsService;
        private readonly IStoreOrderDetailsService _storeOrderDetailsService;
        private readonly IDocumentFacade _documentFacade;

        public StoreController(IOrganisationService organisationService,
            IStoreCatalogItemsService storeCatalogItemsService,
            IStoreOrderDetailsService storeOrderDetailsService,
            IDocumentFacade documentFacade)
        {
            _organisationService = organisationService;
            _storeCatalogItemsService = storeCatalogItemsService;
            _storeOrderDetailsService = storeOrderDetailsService;
            _documentFacade = documentFacade;
        }

        // GET: Store
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Stock()
        {
            var modelList = GetStock();

            return View(modelList);
        }

        public ActionResult Balance(int? supplierId)
        {
            var org = _organisationService.GetCurrent();
            var suppliers = org.Suppliers.Select(s => new System.Web.Mvc.SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString()
            }).ToList();

            ViewBag.SelectedSupplierName = supplierId == null ? null : suppliers.FirstOrDefault(s => s.Value == supplierId.ToString())?.Text;
            ViewBag.Suppliers = suppliers;

            var catalogItemsGroups =
                org.StoreInvoices.OrderByDescending(c => c.Date)
                    .Where(i => supplierId == null || i.SupplierId == supplierId.Value)
                    .SelectMany(i => i.Items).GroupBy(c => c.CatalogItemId);

            var modelList = new List<StoreCatalogItemViewModel>();
            foreach (var catalogItemGroup in catalogItemsGroups)
            {
                var item = catalogItemGroup.First();
                var modelItem = item.ToViewModel();
                modelItem.Quantity = catalogItemGroup.Sum(i => i.Quantity);
                modelList.Add(modelItem);
            }
            return View(modelList);
        }

        [HttpPost]
        public ActionResult Balance(List<StoreCatalogItemViewModel> items)
        {
            var org = _organisationService.GetCurrent();
            if (!ModelState.IsValid)
            {
                var suppliers = org.Suppliers.Select(s => new System.Web.Mvc.SelectListItem
                {
                    Text = s.Name,
                    Value = s.ID.ToString()
                }).ToList();
                ViewBag.Suppliers = suppliers;
                return View(items);
            }
            var storeError = items.Any(i => i.UsedQuantity != null && i.UsedQuantity > i.Quantity);

            if (storeError)
            {
                ModelState.AddModelError("", Translations.Stock.WarehouseBalanceExceeded);

                var suppliers = org.Suppliers.Select(s => new System.Web.Mvc.SelectListItem
                {
                    Text = s.Name,
                    Value = s.ID.ToString()
                }).ToList();
                ViewBag.Suppliers = suppliers;
                return View(items);
            }

            foreach (var catalogItem in items)
            {
                var currentItem = _storeCatalogItemsService.Get(catalogItem.Id);
                if (currentItem == null)
                    continue;
                if (currentItem.StoreInvoice.Organization.ID != org.ID)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                currentItem.UsedQuantity = catalogItem.UsedQuantity;
            }
            _storeCatalogItemsService.Commit(true);

            return RedirectToAction("Balance");
        }


        public ActionResult Pricing()
        {
            var org = _organisationService.GetCurrent();
            var catalogItems =
                org.StoreInvoices.OrderByDescending(c => c.Date)
                    .SelectMany(i => i.Items)
                    .GroupBy(i => i.CatalogItemId);
            var list = new List<StoreCatalogItemViewModel>();
            foreach (var item in catalogItems)
            {
                var catalogItem = item.FirstOrDefault();
                var tempItem = catalogItem.ToViewModel();
                tempItem.Quantity = item.Sum(i => i.Quantity);
                list.Add(tempItem);
            }
            return View(list);
        }

        public double? CatalogItemCount(string code)
        {
            if (string.IsNullOrEmpty(code))
                return null;

            var org = _organisationService.GetCurrent();
            var catalogItem = org.CatalogItems.FirstOrDefault(c => c.Code == code);

            if (catalogItem == null)
                return null;

            var catalogItemsCount =
                org.StoreInvoices.SelectMany(i => i.Items.Where(o => o.CatalogItemId == catalogItem.ID));

            if (!catalogItemsCount.Any())
                return null;
            var totalQuantity = catalogItemsCount.Sum(i => i.Quantity);
            var usedQuantity = org.Documents
                        .Where(
                            d =>
                                d.DocumentType == DocumentType.Waybill ||
                                d.DocumentType == DocumentType.TaxInvoice ||
                                d.DocumentType == DocumentType.TaxInvoiceReceipt)
                                .Where(d => d.IsOriginalGenerated())
                        .Sum(
                            d => d.DocumentItems.Where(i => i.Code == catalogItem.Code).Sum(cc => cc.Quantity));


            return totalQuantity-usedQuantity;
        }

        [HttpPost]
        public ActionResult Pricing(List<StoreCatalogItemViewModel> items)
        {
            if (!ModelState.IsValid)
                return View(items);

            var org = _organisationService.GetCurrent();
            foreach (var item in items)
            {
                var currentItem = _storeCatalogItemsService.Get(item.Id);
                if (currentItem == null)
                    continue;
                if (currentItem.StoreInvoice.Organization.ID != org.ID)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                currentItem.CatalogItem.SellingPrice = item.Price;
                currentItem.CatalogItem.Price = item.Price;

            }
            _storeCatalogItemsService.Commit(true);

            return RedirectToAction("Pricing");
        }

        public PartialViewResult GetStoreData(int supplierId, StoreInvoiceType type, int? invoiceNumber, DateTime? invoiceDate)
        {
            var org = _organisationService.GetCurrent();
            ViewBag.CatalogItems = Getter.GetCatalogItems(_organisationService);
            var supplier = org.Suppliers.FirstOrDefault(c => c.ID == supplierId);
            if (invoiceNumber.HasValue && invoiceDate.HasValue)
            {
                var invoice =
                    org.StoreInvoices.FirstOrDefault(
                        i =>
                            i.Number == invoiceNumber.Value && i.Date == invoiceDate.Value && i.SupplierId == supplierId);
                if (invoice == null)
                    return PartialView("_StoreDataPartial",
                        new StoreDataViewModel
                        {
                            Type = type,
                            SupplierId = supplierId,
                            InvoiceNumber = invoiceNumber.Value,
                            InvoiceDate = invoiceDate.Value
                        });


                var invoiceItems = invoice.Items.ToList().Select(i => i.ToViewModel()).ToList();

                return PartialView("_StoreDataPartial",
                    new StoreDataViewModel
                    {
                        SupplierId = supplierId,
                        Type = invoice.Type,
                        Description = invoice.Description,
                        InvoiceNumber = invoice.Number,
                        InvoiceDate = invoice.Date,
                        Items =
                            !invoiceItems.Any()
                                ? new List<StoreCatalogItemViewModel> { new StoreCatalogItemViewModel() }
                                : invoiceItems
                    });
            }

            if (supplier == null && supplierId != 0)
                return null;

            return PartialView("_StoreDataPartial",
                new StoreDataViewModel {Type = type, SupplierId = supplierId, InvoiceNumber = invoiceNumber ?? 0});
        }

        [HttpPost]
        public ActionResult EditStoreData(StoreDataViewModel model)
        {
            var org = _organisationService.GetCurrent();
            var supplier = org.Suppliers.FirstOrDefault(s => s.ID == model.SupplierId);

            if (supplier == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var storeInvoice = supplier.StoreInvoices.FirstOrDefault(i => i.Number == model.InvoiceNumber && i.Date == model.InvoiceDate);
            if (storeInvoice == null)
            {
                storeInvoice = new StoreInvoice
                {
                    Date = model.InvoiceDate.Value,
                    Number = model.InvoiceNumber,
                    Organization = org,
                    Type = model.Type,
                    Description = model.Description
                };
                supplier.StoreInvoices.Add(storeInvoice);

            }
            else
            {
                foreach (var item in storeInvoice.Items.ToList())
                {
                    _storeCatalogItemsService.Remove(item, true);
                }
            }
            switch (model.Type)
            {
                case StoreInvoiceType.Invoice:
                    {
                        var existingInvoice =
                        supplier.Invoices.FirstOrDefault(
                             w =>
                                 w.Number == model.InvoiceNumber &&
                                 w.Date.Year == model.InvoiceDate.Value.Year &&
                                 w.Date.Month == model.InvoiceDate.Value.Month);

                        if (existingInvoice == null)
                        {
                            supplier.Invoices.Add(new SupplierInvoice
                            {
                                Date = model.InvoiceDate.Value,
                                Number = model.InvoiceNumber,
                                Description = model.Description,
                                OrganizationID = org.ID,
                                IsConfirmed = false,
                                Amount = model.GetItemsPriceWithVat()
                            });

                        }
                        else
                        {
                            existingInvoice.Amount = model.GetItemsPriceWithVat();
                        }
                        break;
                    }
                case StoreInvoiceType.Waybill:
                    {
                        var existingWaybill =
                            org.SupplierWaybills.FirstOrDefault(
                                w =>
                                    w.Number == model.InvoiceNumber && w.SupplierID == model.SupplierId &&
                                    w.Date.Year == model.InvoiceDate.Value.Year &&
                                    w.Date.Month == model.InvoiceDate.Value.Month);

                        if (existingWaybill == null)
                        {
                            org.SupplierWaybills.Add(new SupplierWaybill
                            {
                                Number = model.InvoiceNumber,
                                SupplierID = supplier.ID,
                                Date = model.InvoiceDate.Value,
                                SupplyDescription = model.Description,
                                Amount = model.GetItemsPriceWithVat()
                            });
                        }
                        else
                        {
                            existingWaybill.Amount = model.GetItemsPriceWithVat();
                        }
                        break;
                    }
            }


            foreach (var itemGroup in model.Items.GroupBy(g => g.Code))
            {
                var item = itemGroup.First();
                var catalogItem = org.CatalogItems.FirstOrDefault(c => c.Code == item.Code);
                if (catalogItem == null)
                {
                    catalogItem = new CatalogItem
                    {
                        Name = item.Name,
                        Code = item.Code,
                        Active = true,
                        SupplierPrice = item.SupplierPrice
                    };
                    org.CatalogItems.Add(catalogItem);
                }
                else
                {
                    catalogItem.SupplierPrice = item.SupplierPrice;
                }

                storeInvoice.Items.Add(new StoreCatalogItem
                {
                    CatalogItem = catalogItem,
                    Quantity = itemGroup.Sum(p => p.Quantity),
                });
            }
            _organisationService.Commit(true);
            return GetStoreData(model.SupplierId, model.Type, (int?)model.InvoiceNumber, model.InvoiceDate);
        }

        [ValidateAjax]
        [HttpPost]
        public JsonResult GetStoreInvoices(int supplierId, bool waybill=false)
        {
            var org = _organisationService.Get(User.Identity.GetOrgId());
            var supplier = org.Suppliers.FirstOrDefault(c => c.ID == supplierId);
            if (supplier != null)
            {
                return waybill?
                    Json(
                        supplier.Waybills.OrderByDescending(s => s.Number)
                            .Select(i => new { Id = i.ID, Number = i.Number, Date = i.Date.ToString("dd.MM.yyyy") })):
                    Json(
                        supplier.StoreInvoices.OrderByDescending(s => s.Number).Where(i=>i.Type==StoreInvoiceType.Invoice)
                            .Select(i => new { Id = i.ID, Number = i.Number, Date = i.Date.ToString("dd.MM.yyyy") }));
            }
            return null;
        }

        public ActionResult Order(int? id, bool? isSended = null)
        {
            var org = _organisationService.GetCurrent();
            ViewBag.BusinessInfo = org.ToBaseAccountShort();
            ViewBag.CatalogItems = Getter.GetCatalogItems(_organisationService);
            int number = 0;
            if (id == null)
            {
                number =
                org.Suppliers.SelectMany(s => s.Orders).OrderByDescending(o => o.Number).FirstOrDefault()?.Number ?? 0;
                return View(++number);
            }
            var order = org.Suppliers.SelectMany(s => s.Orders).FirstOrDefault(o => o.ID == id);
            if (order == null)
                return HttpNotFound();

            number = order.Number;
            ViewBag.Supplier = order.Supplier.ToSupplierViewModel();
            ViewBag.Orders = order.OrderDetails.ToList().Select(c => new StoreCatalogItemViewModel
            {
                Name = c.CatalogItem.Name,
                Code = c.CatalogItem.Code,
                Quantity = c.Quantity
            });

            if (isSended.HasValue)
            {
                ViewBag.DocumentSended = isSended.Value;
            }

            return View(number);
        }

        [HttpPost]
        public ActionResult Order(OrderDetailsViewModel model)
        {
            var order = AddOrUpdate(model);
            try
            {
                switch (model.Action)
                {
                    case DocumentActionType.Print:
                        return Json(new { id = order.ID });

                    case DocumentActionType.Send:
                       // _documentFacade.SendByMail(order, model.CultureCode);
                        order.Status = DocumentStatusType.Send;
                        _organisationService.Commit(true);
                        return RedirectToAction("Order", new { id = order.ID, isSended = true });
                }

                if (Request.IsAjaxRequest())
                {
                    return Json(new {Id=order.ID});
                }
            }
            catch (Exception ex)
            {
                // TODO: Write to log
                // return new HttpStatusCodeResult(HttpStatusCode.BadRequest, e.Message);
            }
            return RedirectToAction("Order", new { id = order.ID});
        }

        public ActionResult Orders()
        {
            var org = _organisationService.Get(User.Identity.GetOrgId());
            var orders = org.Suppliers.SelectMany(s => s.Orders).ToList();

            return View(orders);
        }

        [HttpPost]
        public ActionResult SetOrderProcessedStatus(int id, bool wasProcessed)
        {
            var order = _organisationService.GetCurrent()
                .Suppliers
                .SelectMany(m => m.Orders.Where(k => k.ID == id))
                    .FirstOrDefault();
            //var order = _organisationService.GetCurrent().Suppliers.Select(i => i.Orders.FirstOrDefault(o => o.ID == id)).FirstOrDefault();

            if (order != null)
            {
                order.WasProcessed = wasProcessed;
                _organisationService.Commit(true);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        [HttpPost]
        public ActionResult SetDocumentStatus(int id, bool wasSend)
        {
            var order = _organisationService.GetCurrent()
                .Suppliers
                .SelectMany(m => m.Orders.Where(k=>k.ID == id))
                    .FirstOrDefault();

            if (order != null)
            {
                order.Status = (wasSend) ? DocumentStatusType.Send : DocumentStatusType.Initial; //?????
                _organisationService.Commit(true);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        public ActionResult PrintOrder(int id, string cultureCode = "he")
        {
            var org = _organisationService.GetCurrent();
            var order = org.Suppliers.SelectMany(s => s.Orders).FirstOrDefault(o => o.ID == id);
            var model = _documentFacade.GetForPrint(order, cultureCode);

            if (order == null)
            {
                return RedirectToAction("Orders");
            }

            var result = File(model.GetStream(), model.Type);

            return result;
        }

        public ActionResult SendOrder(OrderDetailsViewModel model)
        {
            var isSended = false;
            try
            {
                var order = AddOrUpdate(model);

                _documentFacade.SendByMail(order, model.CultureCode);

                model.Id = order.ID;
                isSended = true;
            }
            catch (Exception e)
            {
                isSended = false;
            }

            return RedirectToAction("Order", new { id = model.Id, sended = isSended });
        }

        public ActionResult SendStock(string email = null)
        {
            var isSend = false;
            try
            {
                var org = _organisationService.GetCurrent();
                var storeStockInfo = new StoreStockInfo()
                {
                    Organization = org,
                    Invoices = org.StoreInvoices,
                    Email = email ?? org.BookKeepingEmail ?? Request.Cookies["bkemail"]?.Value,
                    BccEmail = Request.Cookies["bccemail"]?.Value
                };

                var bkemail = new HttpCookie("bkemail", storeStockInfo.Email);
                var bccEmail = new HttpCookie("bccemail", storeStockInfo.BccEmail);

                bkemail.Expires = DateTime.UtcNow.AddMonths(1);
                bccEmail.Expires = DateTime.UtcNow.AddMonths(1);

                Response.Cookies.Add(bkemail);
                Response.Cookies.Add(bccEmail);

                _documentFacade.SendByMail(storeStockInfo);
                ViewBag.DocumentSended = true;
            }
            catch (Exception e)
            {
                // TODO: Write to log
                ViewBag.DocumentSended = false;
            }

            var model = GetStock();

            return View("Stock", model);
        }

        public ActionResult PrintStock(string email = null)
        {

            var org = _organisationService.GetCurrent();
            var storeStockInfo = new StoreStockInfo()
            {
                Organization = org,
                Invoices = org.StoreInvoices,
                Email = email ?? org.BookKeepingEmail ?? Request.Cookies["bkemail"]?.Value,
                BccEmail = Request.Cookies["bccemail"]?.Value
            };
            var model = _documentFacade.GetForPrint(storeStockInfo);

            var bkemail = new HttpCookie("bkemail", storeStockInfo.Email);
            var bccEmail = new HttpCookie("bccemail", storeStockInfo.BccEmail);

            bkemail.Expires = DateTime.UtcNow.AddMonths(1);
            bccEmail.Expires = DateTime.UtcNow.AddMonths(1);

            Response.Cookies.Add(bkemail);
            Response.Cookies.Add(bccEmail);

            var result = File(model.GetStream(), model.Type);

            return result;
        }


        private Order AddOrUpdate(OrderDetailsViewModel model)
        {
            var org = _organisationService.GetCurrent();
            var supplier = org.Suppliers.FirstOrDefault(s => s.ID == model.SupplierId);
            if (supplier == null)
            {
                throw new KeyNotFoundException($"Supplier with id :{model.SupplierId} not found.");
            }

            var order = supplier.Orders.FirstOrDefault(o => o.ID == model.Id);
            if (order == null)
            {
                var number = org.Suppliers.SelectMany(s => s.Orders).OrderByDescending(o => o.Number).FirstOrDefault()?.Number ?? 0;
                order = new Order
                {
                    Date = DateTime.Now,
                    Number = ++number,
                    WasProcessed = false,
                };
                supplier.Orders.Add(order);
            }
            else
            {
                foreach (var item in order.OrderDetails.ToList())
                {
                    _storeOrderDetailsService.Remove(item, true);
                }
            }

            foreach (var item in model.Items)
            {
                var catalogItem = org.CatalogItems.FirstOrDefault(c => c.Code == item.Code);
                if (catalogItem == null)
                {
                    catalogItem = new CatalogItem
                    {
                        Name = item.Name,
                        Code = item.Code,
                        Active = true
                    };
                    org.CatalogItems.Add(catalogItem);
                }
                order.OrderDetails.Add(new OrderDetail
                {
                    CatalogItem = catalogItem,
                    Quantity = item.Quantity,
                });
            }


            supplier.Email = supplier.Email.IsEmpty() ? model.Email : supplier.Email;
            _organisationService.Commit(true);

            return order;
        }

        private List<StoreCatalogItemViewModel> GetStock()
        {
            var org = _organisationService.GetCurrent();

            org.BookKeepingEmail = org.BookKeepingEmail.IsEmpty() ? Request.Cookies["bkemail"]?.Value : org.BookKeepingEmail;
            ViewBag.BusinessInfo = org.ToBaseAccountShort();

            var catalogItemsGroups =
                org.StoreInvoices.OrderByDescending(c => c.Date)
                    .SelectMany(i => i.Items).GroupBy(c => c.CatalogItemId);

            var modelList = new List<StoreCatalogItemViewModel>();
            foreach (var catalogItemGroup in catalogItemsGroups)
            {
                var item = catalogItemGroup.First();
                var modelItem = item.ToViewModel();
                modelItem.Quantity = catalogItemGroup.Sum(i => i.Quantity);
                modelList.Add(modelItem);
            }

            return modelList;
        }
    }
}