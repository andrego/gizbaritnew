﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Antlr.Runtime.Misc;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Client;
using Gizbarit.DAL.Models.Organisation;
using Gizbarit.DAL.Services.Clients;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils;
using iTextSharp.text;

namespace Gizbarit.Web.Controllers
{
    public class ClientsController : BaseController
    {
        private readonly IOrganisationService _organizationService;
        private readonly IClientService _clientService;
        private readonly IClientPhoneContactService _clientPhoneContactService;
        private readonly IClientEmailContactService _clientEmailContactService;

        public ClientsController(IOrganisationService organisationService, IClientService clientService, IClientEmailContactService clientEmailContactService, IClientPhoneContactService clientPhoneContactService)

        {
            _organizationService = organisationService;
            _clientService = clientService;
            _clientEmailContactService = clientEmailContactService;
            _clientPhoneContactService = clientPhoneContactService;
        }

        public ActionResult Index()
        {
            int orgId = User.Identity.GetOrgId();
            var organisation = _organizationService.Get(orgId);
            var clients = organisation.Clients.Where(c=>c.Active).Select(c => new ClientViewModel
            {
                Id = c.ID,
                Name = c.Name,
                Address = c.Address,
                City = c.City,
                Email = c.Email,
                Fax = c.Fax,
                Phone = c.Phone,
                RetainerAmount = c.RetainerAmount ?? 0,
                UniqueId = c.UniqueId,
                Zip = c.Zip,
                Cell = c.Cell
            }).ToList();

            if (!clients.Any())
            {
                clients = new List<ClientViewModel> {new ClientViewModel {Id = -1} };
            }

            return View(clients);
        }

        [HttpPost]
        public ActionResult Index(List<ClientViewModel> clients)
        {
            if (ModelState.IsValid)
            {
                int orgId = User.Identity.GetOrgId();
                var organisation = _organizationService.Get(orgId);

                foreach (var client in clients)
                {
                    if (client.Id != -1)
                    {
                        var currentClient = organisation.Clients.FirstOrDefault(c => c.ID == client.Id);
                        if(currentClient == null)
                            return HttpNotFound(Translations.Errors.ClientNotFound);

                        currentClient.Name = client.Name;
                        currentClient.UniqueId = client.UniqueId;
                        currentClient.Address = client.Address;
                        //currentClient.Phone = client.Phone;
                        //currentClient.Email = client.Email;
                        currentClient.RetainerAmount = client.RetainerAmount;

                        _organizationService.Commit(true);
                    }
                    else
                    {
 
                        organisation.Clients.Add(new Client
                        {
                            Active = true,
                            DateCreated = DateTime.UtcNow,
                            Name = client.Name,
                            UniqueId = client.UniqueId,
                            Address = client.Address,
                            ClientEmailContacts = new [] {new ClientEmailContact() {Email = client.Email, IsActive = true} },
                            ClientPhoneContacts = new [] {new ClientPhoneContact() {Phone = client.Phone, IsActive = true} },
                           // Phone = client.Phone,
                           // Email = client.Email,
                            RetainerAmount = client.RetainerAmount
                        });


                        _organizationService.Commit(true);
                    }
                }
                return RedirectToAction("Index");
            }
            return View(clients);
        }

        public ActionResult Delete(int clientId)
        {
            if(clientId == -1)
                return RedirectToAction("Index");

            int orgId = User.Identity.GetOrgId();
            var organisation = _organizationService.Get(orgId);
            var client = organisation.Clients.FirstOrDefault(c => c.ID == clientId);
            if (client != null)
            {
                client.Active = false;
                _clientService.Commit(true);
                return RedirectToAction("Index");
            }
            return HttpNotFound(Translations.Errors.ClientNotFound);

        }

        public PartialViewResult DeleteContact(int contactId, int clientId, string contactName)
        {
            var client = _clientService.Get(clientId);
            if (client == null)
                return GetClientContacts(clientId);

            switch (contactName)
            {

                case "email":
                    {
                        var emailContact = client.ClientEmailContacts.FirstOrDefault(m => m.ID == contactId);
                        if (emailContact != null && !emailContact.IsActive)
                        {
                            _clientEmailContactService.Remove(emailContact);
                            _clientEmailContactService.Commit(true);
                        }
                        else
                        {
                            return null;
                        }
                        break;
                    }
                case "phone":
                    {
                        var phoneContact = client.ClientPhoneContacts.FirstOrDefault(m => m.ID == contactId);
                        if (phoneContact != null && !phoneContact.IsActive)
                        {
                            _clientPhoneContactService.Remove(phoneContact);
                            _clientPhoneContactService.Commit(true);
                        }
                        else
                        {
                            return null;
                        }
                        break;
                    }
            }
            

            return GetClientContacts(clientId);
        }

        public JsonResult SaveClientContacts(int clientId, string [] emails, string [] phones)
        {
            var client = _clientService.Get(clientId);
            if (client == null)
            {
                return Json("Client not found", JsonRequestBehavior.DenyGet);
            }

            foreach (var item in client.ClientEmailContacts.ToList())
            {
                _clientEmailContactService.Remove(item);
            }
            
            foreach (var item in client.ClientPhoneContacts.ToList())
            {
                _clientPhoneContactService.Remove(item);
            }
            
            for (var i = 0; i < emails.Length; i++)
            {
                _clientEmailContactService.Create(new ClientEmailContact()
                {
                    ClientId = clientId,
                    Email = emails[i],
                    IsActive = (i==0)
                });
            }
         
            for (var i = 0; i < phones.Length; i++)
            {
                _clientPhoneContactService.Create(new ClientPhoneContact()
                {
                    ClientId = clientId,
                    Phone = phones[i],
                    IsActive = (i==0)
                });
            }

            _clientEmailContactService.Commit(true);
            _clientPhoneContactService.Commit(true);
            return Json("Contacts saved success", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetClientContacts(int clientId)
        {
           
            var client = _clientService.Get(clientId);
            var resultModel = new ClientContactsViewModel()
            {
                ClientName = "New Client",
                ClientId = -1,
                Emails = new List<ClientContactView>(),
                Phones = new List<ClientContactView>()
            };

            var emptyContact = new ClientContactView() { Contact = "", Id = 0 };

            if (client == null)
            {
                resultModel.Emails.Add(emptyContact);
                resultModel.Phones.Add(emptyContact);
                return PartialView("~/Views/Clients/ClientContactsPartial.cshtml", resultModel);
            }

            resultModel.ClientName = client.Name;
            resultModel.ClientId = client.ID;

            if (client.ClientEmailContacts.Count == 0)
            {
                resultModel.Emails.Add(emptyContact);
            }
            else
            {
                foreach (var item in client.ClientEmailContacts)
                {
                    resultModel.Emails.Add(new ClientContactView() {Id = item.ID, Contact = item.Email});
                }
            }

            if (client.ClientPhoneContacts.Count == 0)
            {
                resultModel.Phones.Add(emptyContact);
            }
            else
            {
                foreach (var item in client.ClientPhoneContacts)
                {
                    resultModel.Phones.Add(new ClientContactView() {Id = item.ID, Contact = item.Phone});
                }
            }


            return PartialView("~/Views/Clients/ClientContactsPartial.cshtml", resultModel);

        }

        public ActionResult VerifyExist(string uniqueId)
        {
            var org = _organizationService.GetCurrent();
            var exists = org.Clients.Any(c => c.UniqueId == uniqueId && c.Active);
            if(exists)
                return new HttpStatusCodeResult(HttpStatusCode.Found);
            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }
    }
}