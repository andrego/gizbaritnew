﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Gizbarit.DAL.Common;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Models;
using Gizbarit.DAL.Services.Content;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils.Email;
using NLog;
using Org.BouncyCastle.Crypto.Tls;
using reCAPTCHA.MVC;

namespace Gizbarit.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMailService _mailService;
        private readonly IOrganisationService _organisationService;
        private readonly IContentNodeService _contentNodeService;

        public HomeController(IMailService mailService, 
            IOrganisationService organisationService,
            IContentNodeService contentNodeService)
        {
            _mailService = mailService;
            _organisationService = organisationService;
            _contentNodeService = contentNodeService;

        }
        public ActionResult TermsOfUse()
        {
            return GetContent("TermsOfUse");
        }

        public ActionResult Video(Guid? id)
        {
            var culture = GetCulture();
            var model = id.HasValue ? _contentNodeService.Get(id.Value) : _contentNodeService.Get("Video", culture);

            if (model.IsNull())
            {
                return RedirectPermanent("~/");
            }

            model.Childs = model.Childs ?? new List<ContentNode>();

            if (id.HasValue && model.Childs.Any(x => x.ID == id))
            {
                var item =  model.Childs.Single(x => x.ID == id);

                item.Selected = true;
            }

            return id.HasValue ? View("VideoDetail", model) : View("VideoIndex", model);
        }

        public ActionResult ContactUs()
        {
            var model = new ContactModel();
            if (User.Identity.IsAuthenticated)
            {
                var user = _organisationService.GetCurrent();
                model.Email = user.Email;
                model.Phone = user.Phone1;
                model.Name = $"{user.FirstName} {user.LastName}";
            }
            return View(model);
        }

        public ActionResult AboutUs()
        {
          return GetContent("AboutUs");
        }

        public ActionResult Gizbarit()
        {
            return GetContent("Gizbarit");
        }

        public ActionResult Prices()
        {
            return GetContent("Prices");
        }

        public ActionResult Index()
        {
            return GetContent("Index");
        }
        [HttpPost]
        [CaptchaValidator]
        public async Task<ActionResult> Contact(ContactModel model, bool captchaValid)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _mailService.SendMailToAdmin(model.Name, $"{model.Email}, {model.Phone}", model.Message);
                    return RedirectToAction("ContactUsSent");
                }
                catch (Exception ex)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }
            return View("ContactUs", model);
        }

        public ActionResult ContactUsSent()
        {
            return View();
        }

        public ActionResult ChangeCulture(string lang)
        {
            string returnUrl = Request.RawUrl;
            List<string> cultures = new List<string>() { "ru", "he", "en", "fr" };
            if (!cultures.Contains(lang))
            {
                lang = "en";
            }
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
                cookie.Value = lang; 
            else
            {

                cookie = new HttpCookie("lang")
                {
                    HttpOnly = false,
                    Value = lang,
                    Expires = DateTime.Now.AddYears(1)
                };
            }
            Response.Cookies.Add(cookie);
            return RedirectToAction("Index");
        }

        private string GetCulture()
        {
            var cultures = new HashSet<string>() { "ru", "he", "en", "fr" };
            var cookie = Request.Cookies["lang"];

            return cookie == null || !cultures.Contains(cookie.Value) ? "he" : cookie.Value;
        }

        private ActionResult GetContent(string viewName)
        {
            var culture = GetCulture();
            var model = _contentNodeService.Get(viewName, culture);

            return model == null ? View(viewName) : View("Content", model);
        }
    }
}
