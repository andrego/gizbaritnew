﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Models;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils;

namespace Gizbarit.Web.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private readonly IOrganisationService _organisationService;

        public LoginController(IOrganisationService organisationService)
        {
            _organisationService = organisationService;
        }
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userCookie = LoginInternal(model.BusinessNumber, model.Password);

                    if (userCookie != null)
                    {
                        return RedirectToAction("Index", "Dashboard");
                    }
                }
                catch (AccessViolationException)
                {
                    ModelState.AddModelError("", Translations.Login.TrialExpired);
                    return View();
                }
                catch (MemberAccessException)
                {
                    ModelState.AddModelError("", Translations.Login.PaidExpired);
                    return View();
                }


            }
            ModelState.AddModelError("", Translations.Login.WrongPassword);
            return View();
        }

        [HttpPost]
        public ActionResult Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();

                RedirectToAction("Index", "Login");
            }

            return RedirectToAction("Index", "Login");
        }

        private string LoginInternal(string businessNumber, string password, bool isPersistent = false)
        {
            var org = _organisationService.Login(businessNumber, password);

            if (org == null)
            {
                return null;
            }

            if (org.TrialPeriod && org.TrialPeriodExpirationDate.Date < DateTime.Now.Date)
            {
                throw new AccessViolationException("TrialPeriodExpirationDate is fired");
            }

            if (!org.TrialPeriod && org.PaidPeriodExpirationDate.Date < DateTime.Now.Date)
            {
                throw new MemberAccessException("PaidPeriodExpirationDate is fired");
            }

            _organisationService.UpdateLastAccessedInfo(org);
            return Helper.SetAuthCookie(org.ToLoginUserVerifyResult(), isPersistent).Value;
        }
    }
}