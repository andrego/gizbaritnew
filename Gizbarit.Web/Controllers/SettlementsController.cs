﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Models.Settlements;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Supliers;
using Gizbarit.DAL.Utils;
using Gizbarit.Web.Filters;
using Gizbarit.Utils;
using Gizbarit.Web.Util.Extensions;
using Quartz.Spi;
using Advance = Gizbarit.DAL.Entities.Bank.Advance;

namespace Gizbarit.Web.Controllers
{
    public class SettlementsController : BaseController
    {
        private readonly IOrganisationService _organisationService;
        private readonly IPaymentService _paymentService;
        private readonly IDocumentService _documentService;
        private readonly ISupplierInvoiceService _supplierInvoiceService;
        private readonly IBankAccountService _bankAccountService;
        private readonly ITransactionService _transactionService;

        public SettlementsController(IOrganisationService organisationService, IDocumentService documentService,
            IPaymentService paymentService, ISupplierInvoiceService supplierInvoiceService,
            IBankAccountService bankAccountService, ITransactionService transactionService)
        {
            _organisationService = organisationService;
            _documentService = documentService;
            _paymentService = paymentService;
            _supplierInvoiceService = supplierInvoiceService;
            _bankAccountService = bankAccountService;
            _transactionService = transactionService;
        }

        // GET: Settlements
        public ActionResult Index(int? year, int? month, int? clientId, bool isExport = false)
        {
            var currentYear = year ?? DateTime.Now.Year;
            var orgId = User.Identity.GetOrgId();

            var organisation = _organisationService.Get(orgId);

            var preliminaryInvoices =
                _documentService.GetAll(isExport).Include(d => d.Payments).Include(d => d.Client)
                    .Where(
                        d =>
                            d.OrganizationID == orgId &&
                            (d.DocumentType == DocumentType.PreliminaryInvoice ||
                             ((d.DocumentType == DocumentType.TaxInvoice || d.DocumentType == DocumentType.TaxInvoiceReceipt) &&
                              (d.DocumentReferences.Count == 0 || d.DocumentReferences.FirstOrDefault().Type != DocumentType.PreliminaryInvoice))));
    
            if (clientId != null)
            {
                preliminaryInvoices = preliminaryInvoices.Where(d => d.ClientID == clientId);

                ViewBag.CurrentClientName =
                    organisation.Clients.Where(c => c.ID == clientId).Select(c => c.Name).FirstOrDefault();
            }

            var pastYearPreliminaries =
                preliminaryInvoices.Where(d => d.DateCreated.Year == currentYear - 1)
                    .ToList().Select(d => d.ToSettlementsViewModel(_documentService));

            preliminaryInvoices = preliminaryInvoices.Where(d => d.DateCreated.Year == currentYear)
                .Where(d=> month == null || d.DateCreated.Month == month);
       

            var result = new SettlementsWithCustommerViewModelWrapper
            {
                Clients = organisation.Clients.Select(c => c.ToClientViewModel()),
                Items =
                    preliminaryInvoices.OrderByDescending(d => d.DateCreated)
                         .ToList().Select(d => d.ToSettlementsViewModel(_documentService)),
                PastYearDeduction = pastYearPreliminaries.Sum(p => p.Deduction)
            };

            ViewBag.Year = currentYear;
            ViewBag.Month = month;
            ViewBag.ClientId = clientId;

            result.IsExport = isExport;


            return View(result);
        }

        public ActionResult PerCustommer(int? year)
        {
            var currentYear = year ?? DateTime.Now.Year;
            ViewBag.Year = currentYear;
            var orgId = User.Identity.GetOrgId();

            var organisation = _organisationService.Get(orgId);
            var documentsByClient =
                organisation.Documents.Where(
                    d => d.DateCreated.Year == currentYear && (
                        (d.DocumentType == DocumentType.PreliminaryInvoice && d.DocumentReferences.Count == 0) ||
                        d.DocumentType == DocumentType.TaxInvoice ||
                        d.DocumentType == DocumentType.TaxInvoiceReceipt))
                    .OrderBy(o => o.Client.Name)
                    .GroupBy(g => g.ClientID);

            var paymentsByClient = _paymentService.GetAll().Where(d => d.Date.Year == currentYear && d.PayerID != null);
            var model = new List<SettlemetsByClient>();
            foreach (var documents in documentsByClient)
            {
                var client = organisation.Clients.FirstOrDefault(c => c.ID == documents.Key);
                if (client != null)
                {
                    var clientPayments = paymentsByClient.Where(p => p.PayerID == documents.Key).ToList();

                    model.Add(new SettlemetsByClient
                    {
                        ClientId = client.ID,
                        ClientName = client.Name,
                        ClientAddress = client.Address,
                        ClientPhone = client.Phone,
                        TotalPaid = clientPayments.Sum(p => p.Amount),
                        TotalInvoice = documents.Sum(d => d.Total)
                    });
                }
            }
            return View(model);

        }

        public ActionResult WaybillsSummary(int? year)
        {
            ViewBag.Year = year ?? DateTime.Now.Year;
            return View();
        }

        public ActionResult AccordingToSupplier(int? year)
        {
            ViewBag.Year = year ?? DateTime.Now.Year;
            return View();
        }

        public ActionResult TaxInvoice(int? year)
        {
            ViewBag.Year = year ?? DateTime.Now.Year;
            return View();
        }

        public async Task<ActionResult> SummaryBySupplier(int? year, string sortOrder)
        {
            var currentYear = year ?? DateTime.Now.Year;
            var orgId = User.Identity.GetOrgId();

            ViewBag.SupplierSortParam = sortOrder == "Supplier" ? "Supplier_desc" : "Supplier";
            ViewBag.DateSortParam = sortOrder == "Date" ? "Date_desc" : "Date";
            ViewBag.DeductionSortParam = sortOrder == "Deduction" ? "Deduction_desc" : "Deduction";

            var query =
                _supplierInvoiceService.GetAll().Include(s => s.Supplier).Include(s => s.Receipts)
                    .Where(si => si.OrganizationID == orgId && si.IsConfirmed && si.Date.Year == currentYear);
            ViewBag.CurrentSort = sortOrder;

            switch (sortOrder)
            {
                case "Supplier":
                    query = query.OrderBy(si => si.Supplier.Name);
                    break;
                case "Supplier_desc":
                    query = query.OrderByDescending(si => si.Supplier.Name);
                    break;
                case "Date":
                    query = query.OrderBy(si => si.Date);
                    break;
                case "Date_desc":
                    query = query.OrderByDescending(si => si.Date);
                    break;
                case "Deduction":
                    query = query.OrderBy(si => si.Amount - si.Receipts.Sum(r => r.Amount));
                    break;
                case "Deduction_desc":
                    query = query.OrderByDescending(si => si.Amount - si.Receipts.Sum(r => r.Amount));
                    break;
                default:
                    query = query.OrderByDescending(si => si.Date);
                    break;
            }
            var result = await query.ToListAsync();
            var model = result.Select(r => new SummaryBySupplier
            {
                SupplierId = r.SupplierID,
                SupplierName = r.Supplier.Name,
                InvoiceDate = r.Date,
                InvoiceAmount = r.Amount,
                DeliverySubject = r.Description,
                InvoiceNumber = r.Number,
                Paid = r.Receipts.Sum(s => s.Amount)
            });
            ViewBag.Year = currentYear;
            return View(model);
        }

        public async Task<ActionResult> Waybills(int? year, int? clientId)
        {
            var currentYear = year ?? DateTime.Now.Year;
            var currentClientId = clientId ?? 0;
            var orgId = User.Identity.GetOrgId();
            var organisation = _organisationService.Get(orgId);

            var model = new SettlementsWaybillWrapper
            {
                Clients = organisation.Clients.Select(c => c.ToClientViewModel())
            };

            if (currentClientId != 0)
            {
                var documents = await _documentService.GetAll()
                    .Include(d => d.Client).Where(d => d.OrganizationID == orgId
                                                       && d.DocumentType == DocumentType.Waybill &&
                                                       d.DateCreated.Year == currentYear &&
                                                       d.Client.ID == currentClientId).ToListAsync();

                model.Waybils = documents.Select(d => d.ToWaybillViewModel(_documentService)).ToList();
            }

            ViewBag.Year = currentYear;
            ViewBag.SelectedClientId = currentClientId;

            return View(model);
        }


        [HttpPost]
        public ActionResult EditSupplierWaybills(SupplierWaybillContainer container)
        {
            if (ModelState.IsValid)
            {
                var organisation = _organisationService.Get(User.Identity.GetOrgId());
                var supplier = organisation.Suppliers.FirstOrDefault(s => s.ID == container.SupplierId);
                if (supplier == null)
                    return HttpNotFound(Translations.Errors.SupplierNotFound);

                foreach (var waybill in container.Waybills.Where(w => w.IsActive == 1))
                {
                    if (waybill.Id == -1) //new waybill
                    {
                        organisation.SupplierWaybills.Add(new SupplierWaybill
                        {
                            Amount = waybill.Amount,
                            Date = waybill.Date,
                            Number = waybill.Number,
                            SupplyDescription = waybill.SupplyDescription,
                            SupplierID = supplier.ID,
                        });
                    }
                    else
                    {
                        var waybillToEdit = organisation.SupplierWaybills.FirstOrDefault(w => w.ID == waybill.Id);
                        if (waybillToEdit != null)
                        {
                            waybillToEdit.Amount = waybill.Amount;
                            waybillToEdit.Date = waybill.Date;
                            waybillToEdit.Number = waybill.Number;
                            waybillToEdit.SupplyDescription = waybill.SupplyDescription;
                        }
                    }
                }


                var supplierInvoice =
                    organisation.SupplierInvoices.FirstOrDefault(
                        i => i.Date.Year == container.Year && i.Date.Month == container.Month && i.SupplierID == supplier.ID);

                if (supplierInvoice == null)
                {
                    supplierInvoice = new SupplierInvoice
                    {
                        Date = new DateTime(container.Year, container.Month, 1),
                        SupplierID = supplier.ID,
                        IsConfirmed = true
                    };
                    var waybillWithDescription =
                        container.Waybills.FirstOrDefault(w => w.IsActive == 1 && w.SupplyDescription.Length > 0);
                    if (waybillWithDescription != null)
                        supplierInvoice.Description = waybillWithDescription.SupplyDescription;
                    organisation.SupplierInvoices.Add(supplierInvoice);
                }
                else
                {
                    if(supplierInvoice.IsConfirmed)
                    return GetWaybillBySupplierId(container.SupplierId, container.Year, container.Month);
                }

                supplierInvoice.IsConfirmed = container.Waybills.Where(w => w.IsActive == 1).Sum(w => w.Amount) ==
                                       container.Invoice.Total;
                if (supplierInvoice.IsConfirmed)
                {
                    if (ValidateSupplierInvoiceViewModel(container.Invoice))
                    {
                        supplierInvoice.Amount = container.Invoice.Total.Value;
                        supplierInvoice.Number = container.Invoice.Number.Value;
                        supplierInvoice.Date = container.Invoice.Date.Value;
                        supplierInvoice.IsConfirmed = true;

                        organisation.Advances.Add(new Advance
                        {
                            Amount = supplierInvoice.Amount,
                            Credit = false,
                            DateCreated = supplierInvoice.Date,
                            DateAction = supplierInvoice.Date,
                            AdvanceAmount = null, //advance
                            TaxAmount = -(supplierInvoice.Amount / (organisation.TaxRate + 100) * organisation.TaxRate), //vat
                            Description = String.Format("{1}, invoice:{0}", supplierInvoice.Number, supplier.Name),

                        });
                    }
                    else
                    {
                        return PartialView("SupplierWaybillPartial", container);
                    }
                }
                _organisationService.Commit(true);
                return GetWaybillBySupplierId(container.SupplierId, container.Year, container.Month);
            }
            return PartialView("SupplierWaybillPartial", container);
        }

        public ActionResult GetSupplierReceiptContainer(int invoiceId, int supplierId)
        {
            var organisation = _organisationService.Get(User.Identity.GetOrgId());
            var container = new SupplierReceiptContainer();
            var invoice = organisation.SupplierInvoices.FirstOrDefault(i => i.ID == invoiceId);
            if (invoice == null)
            {
                container.Invoice = new SupplierInvoiceViewModel
                {
                    Id = -1
                };
                container.Receipts = new List<SupplierReceiptViewModel>();
                container.SupplierId = supplierId;
            }
            else
            {
                container.Invoice = invoice.ToSupplierInvoiceViewModel();
                container.SupplyDescription = invoice.Description;
                container.Receipts =
                    organisation.SupplierReceipts.Where(sr => sr.SupplierInvoiceID == invoice.ID)
                        .Select(r => r.ToSupplierReceiptViewModel()).ToList();
                // invoice.Receipts;
            }

            return PartialView("SupplierReceiptPartial", container);
        }

        [HttpPost]
        public ActionResult EditSupplierReceipts(SupplierReceiptContainer container)
        {
            if (ModelState.IsValid)
            {
                bool newInvoice = false;
                foreach (var receipt in container.Receipts)
                {
                    if (receipt.Amount != null && (receipt.Date == null || receipt.Number == String.Empty))
                    {
                        ModelState.AddModelError("", Translations.Errors.EnterInvoiceNumber);
                        return PartialView("SupplierReceiptPartial", container);
                    }
                }
                var isNew = false;

                var organisation = _organisationService.Get(User.Identity.GetOrgId());
                var supplier = organisation.Suppliers.FirstOrDefault(s => s.ID == container.SupplierId);

                if (supplier == null)
                {
                    ModelState.AddModelError("", Translations.Errors.SupplierNotFound);
                    return PartialView("SupplierReceiptPartial", container);
                }

                var invoice = organisation.SupplierInvoices.FirstOrDefault(i => i.ID == container.Invoice.Id);
                if (invoice == null)
                {
                    if (ValidateSupplierInvoiceViewModel(container.Invoice))
                    {
                        invoice = new SupplierInvoice
                        {
                            Amount = container.Invoice.Total.Value,
                            Date = container.Invoice.Date.Value,
                            IsConfirmed = true,
                            Description = container.SupplyDescription,
                            Number = container.Invoice.Number.Value,
                            SupplierID = container.SupplierId
                        };
                        organisation.SupplierInvoices.Add(invoice);
                    }
                    else
                    {
                        return PartialView("SupplierReceiptPartial", container);
                    }
                    newInvoice = true;
                }

                var bankAccount = _bankAccountService.GetDefaultAccount(User.Identity.GetOrgId());
                var activeReceipts = container.Receipts.Where(
                    r => r.Id == -1 && r.Amount != null & r.Date != null && r.Number != String.Empty).ToList();

                foreach (
                    var receipt in activeReceipts)
                {


                    organisation.SupplierReceipts.Add(new SupplierReceipt
                    {
                        SupplierID = container.SupplierId,
                        SupplierInvoiceID = invoice.ID,
                        Number = receipt.Number,
                        // ReSharper disable once PossibleInvalidOperationException
                        PaymentDate = (DateTime)receipt.Date,
                        // ReSharper disable once PossibleInvalidOperationException
                        Amount = (double)receipt.Amount,

                    });

                    organisation.BankTransactions.Add(new BankTransaction
                    {
                        ID = Guid.NewGuid(),
                        Amount = (double)receipt.Amount,
                        BankId = bankAccount.ID,
                        Credit = false,
                        DateInserted = (DateTime)receipt.Date,
                        Details = String.Format("{0} - {1} - {2} {3}", supplier.Name, invoice.Description, Translations.Settlements.Invoice, invoice.Number),
                        DateUpdated = (DateTime)receipt.Date,
                        CurrencyId = "ILS"
                    });
                }

                if (newInvoice)
                {
                    var advance = new Advance
                    {
                        Amount = invoice.Amount,
                        Credit = false,
                        SupplierInvoiceNumber = (int?)invoice.Number,
                        DateCreated = invoice.Date,
                        DateAction = invoice.Date,
                        Description = String.Format(Translations.Errors.TaxForSupplier, supplier.Name, invoice.Number),
                        TaxAmount = -(invoice.Amount / (organisation.TaxRate + 100) * organisation.TaxRate) //vat
                    };
                    organisation.Advances.Add(advance);
                    if (!advance.Credit)
                    {
                        var vatPayDateDate = organisation.GetVatPayDay(invoice.Date);
                        _transactionService.UpdateAutoVatTransaction(bankAccount.ID, vatPayDateDate, advance.TaxAmount.Value);
                    }
                }

                _organisationService.Commit(true);

                container = new SupplierReceiptContainer
                {
                    SupplierId = invoice.SupplierID,
                    Invoice = invoice.ToSupplierInvoiceViewModel(),
                    Receipts = organisation.SupplierReceipts.Where(r => r.SupplierInvoiceID == invoice.ID)
                        .Select(r => r.ToSupplierReceiptViewModel())
                };
                return PartialView("SupplierReceiptPartial", container);
            }
            return PartialView("SupplierReceiptPartial", container);
        }


        [HttpPost]
        public PartialViewResult GetWaybillBySupplierId(int id, int? year, int? month)
        {
            var currentYear = year ?? DateTime.UtcNow.Year;
            var currentMonth = month ?? DateTime.UtcNow.Month;

            var organisation = _organisationService.Get(User.Identity.GetOrgId());

            var waybillContainer = new SupplierWaybillContainer
            {
                Waybills = organisation.SupplierWaybills.Where(
                    w => w.SupplierID == id && w.Date.Year == currentYear && w.Date.Month == currentMonth).Select(w =>
                        new SupplierWaybillViewModel
                        {
                            Id = w.ID,
                            Number = w.Number,
                            Amount = w.Amount,
                            Date = w.Date,
                            SupplyDescription = w.SupplyDescription
                        }).ToList(),
                Invoice = organisation.SupplierInvoices.Where(
                    i => i.SupplierID == id && i.Date.Year == currentYear && i.Date.Month == currentMonth).Select(i => new SupplierInvoiceViewModel
                    {
                        Id = i.ID,
                        Number = i.Number,
                        Total = i.Amount,
                        Date = i.Date,
                        IsConfirmed = i.IsConfirmed
                    }).FirstOrDefault() ?? new SupplierInvoiceViewModel { IsConfirmed = false },
                Month = currentMonth,
                Year = currentYear,
                SupplierId = id
            };

            return PartialView("SupplierWaybillPartial", waybillContainer);
        }

        [ValidateAjax]
        [HttpPost]
        public JsonResult GetSupplierByName(string supplierName)
        {
            var org = _organisationService.Get(User.Identity.GetOrgId());
            var suppliers = org.Suppliers.Where(c => c.Name.ToLower().Contains(supplierName.ToLower())).ToList();
            if (suppliers.Any())
            {
                var result = suppliers.Select(c => c.ToSupplierViewModel());
                return Json(result);
            }
            return null;
        }

        [ValidateAjax]
        [HttpPost]
        public JsonResult GetSupplierInvoices(int supplierId, int year)
        {
            var org = _organisationService.Get(User.Identity.GetOrgId());
            var supplier = org.Suppliers.FirstOrDefault(c => c.ID == supplierId);
            if (supplier != null)
            {
                return
                    Json(
                        supplier.Invoices.Where(i => i.Date.Year == year)
                            .Select(
                                i =>
                                    new
                                    {
                                        Id = i.ID,
                                        Amount = i.Amount,
                                        Number = i.Number,
                                        Date = i.Date.ToString("dd.MM.yyyy")
                                    }));
            }
            return null;
        }



        [ValidateAjax]
        [HttpPost]
        public ActionResult GetSupplierInvoicesPartial(int supplierId, int? year = null, string sortOrder = null)
        {
            year = year ?? DateTime.Now.Year;

            var org = _organisationService.Get(User.Identity.GetOrgId());
            var supplier = org.Suppliers.FirstOrDefault(c => c.ID == supplierId);
            if (supplier != null)
            {
                ViewBag.NumberSortParam = sortOrder == "Number" ? "Number_desc" : "Number";
                ViewBag.DateSortParam = sortOrder == "Date" ? "Date_desc" : "Date";
                ViewBag.AmountSortParam = sortOrder == "Amount" ? "Amount_desc" : "Amount";
                ViewBag.PaidSortParam = sortOrder == "Paid" ? "Paid_desc" : "Paid";
                ViewBag.DeductionSortParam = sortOrder == "Deduction" ? "Deduction_desc" : "Deduction";

                var querry =
                    _supplierInvoiceService.GetAll().Include(si => si.Receipts).Where(si => si.SupplierID == supplierId && si.Date.Year == year);
                ViewBag.CurrentSort = sortOrder;

                switch (sortOrder)
                {
                    case "Date":
                        querry = querry.OrderBy(o => o.Date);
                        break;
                    case "Date_desc":
                        querry = querry.OrderByDescending(o => o.Date);
                        break;
                    case "Number":
                        querry = querry.OrderBy(o => o.Number);
                        break;
                    case "Number_desc":
                        querry = querry.OrderByDescending(o => o.Number);
                        break;
                    case "Amount":
                        querry = querry.OrderBy(o => o.Amount);
                        break;
                    case "Amount_desc":
                        querry = querry.OrderByDescending(o => o.Amount);
                        break;
                    case "Paid":
                        querry = querry.OrderBy(o => o.Receipts.Sum(r => r.Amount));
                        break;
                    case "Paid_desc":
                        querry = querry.OrderByDescending(o => o.Receipts.Sum(r => r.Amount));
                        break;
                    case "Deduction":
                        querry = querry.OrderBy(o => o.Amount - o.Receipts.Sum(r => r.Amount));
                        break;
                    case "Deduction_desc":
                        querry = querry.OrderByDescending(o => o.Amount - o.Receipts.Sum(r => r.Amount));
                        break;
                }
                var result = querry.ToList().Select(i => new SupplierInvoiceDetailsViewModel
                {
                    Number = i.Number,
                    Date = i.Date,
                    Subject =
                        org.SupplierWaybills.FirstOrDefault(
                            w => w.Date.Year == i.Date.Year && w.Date.Month == i.Date.Month)?.SupplyDescription,
                    Amount = i.Amount,
                    Paid = i.Receipts.Sum(r => r.Amount)
                });
                ViewBag.SupplierId = supplierId;
                ViewBag.Year = year;
                return PartialView("SupplierInvoicesPartial", result);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult EditSupplier(SupplierViewModel supplier)
        {
            if (ModelState.IsValid)
            {
                var organisation = _organisationService.Get(User.Identity.GetOrgId());
                Supplier currentSupplier = null;
                if (supplier.Id == 0) //new supplier
                {
                    currentSupplier = new Supplier
                    {
                        Name = supplier.Name,
                        Address = supplier.Address,
                        Phone = supplier.Phone,
                        Email = supplier.Email
                    };
                    organisation.Suppliers.Add(currentSupplier);
                    supplier.Id = currentSupplier.ID;
                }
                else
                {
                    currentSupplier = organisation.Suppliers.FirstOrDefault(s => s.ID == supplier.Id);
                    if (currentSupplier != null)
                    {
                        currentSupplier.Name = supplier.Name;
                        currentSupplier.Phone = supplier.Phone;
                        currentSupplier.Address = supplier.Address;
                        currentSupplier.Email = supplier.Email;
                    }
                }
                _organisationService.Commit(true);
                if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Contains("Store"))
                    return PartialView("EditSupplierPartialWithEmail", currentSupplier.ToSupplierViewModel());

                return PartialView("EditSupplierPartial", currentSupplier.ToSupplierViewModel());
            }
            if(Request.UrlReferrer != null && Request.UrlReferrer.ToString().Contains("Store"))
                return PartialView("EditSupplierPartialWithEmail", supplier);
            return PartialView("EditSupplierPartial", supplier);
        }

        private bool ValidateSupplierInvoiceViewModel(SupplierInvoiceViewModel model)
        {
            if (model.Number == null)
            {
                ModelState.AddModelError("", Translations.Errors.EnterInvoiceNumber);
            }
            if (model.Date == null)
            {
                ModelState.AddModelError("", Translations.Errors.SelectInvoiceDate);
            }
            if (model.Total == null)
            {
                ModelState.AddModelError("", Translations.Errors.InvoiceTotalRequired);
            }
            return ModelState.IsValid;
        }

        private void HideUnhideDocument(SettlementsWithCustommerViewModelWrapper container)
        {
            List<Guid> elementsToUpdate = new List<Guid>();
            var groups = container.Items.Where(i => i.Invoice != Guid.Empty).GroupBy(g => g.Invoice);
            foreach (var group in groups)
            {
                var maxPreliminary = group.OrderByDescending(x => x.PreliminaryInvoiceNumber).First();
                if (maxPreliminary.Deduction == 0)
                {
                    elementsToUpdate.Add(maxPreliminary.PreliminaryInvoice);
                }
            }
            container.Items.Where(i => elementsToUpdate.Contains(i.PreliminaryInvoice))
                    .ForEach(x => x.SetMainDocument());
        }
    }
}