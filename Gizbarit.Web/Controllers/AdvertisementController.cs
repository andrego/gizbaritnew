﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using AdminResources;
using AutoMapper;
using Gizbarit.DAL.Entities.Advertisement;
using Gizbarit.DAL.Models.Advertisement;
using Gizbarit.DAL.Services;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.Utils;
using Gizbarit.Web.ProfilesMap;
using Microsoft.Ajax.Utilities;

namespace Gizbarit.Web.Controllers
{
    public class AdvertisementController : Controller
    {
        private readonly IOrganisationService _organisationService;
        private readonly IAdvertisementService _advertisementService;

        public AdvertisementController(IOrganisationService organisationService, 
            IAdvertisementService advertisementService)
        {
            _organisationService = organisationService;
            _advertisementService = advertisementService;
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AdvertisementProfile>();
                cfg.AddProfile<OrganizationInfoProfile>();
                cfg.AddProfile<AdvertisementViewProfile>();
            });
        }

        public ActionResult Show(string searchString)
        {
            var cultureName = Thread.CurrentThread.CurrentCulture.Name;
            var locale = cultureName.Split('-')[0].ToLower();
            var advLang = GetLanguageFromLocale(locale);
            searchString = searchString?.ToLower();

            var allAdvertisements = _advertisementService.GetActive().Where(m => m.Language == advLang)
                .Where(m=> searchString == null || (m.Text.ToLower().Contains(searchString)|| m.Title.ToLower().Contains(searchString)))
                .ToList();
            IEnumerable<AdvertisementModelView> model =
                Mapper.Map<IEnumerable<Advertisement>, IEnumerable<AdvertisementModelView>>(allAdvertisements);

            return PartialView("AdvertisementPartial",model);
        }

        // GET: Advertisement
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ChangeAdvertLanguage(AdvertiseLanguages language)
        {
            var org = _organisationService.GetCurrent();
            AdvertisementModelView model =
                Mapper.Map<Advertisement, AdvertisementModelView>(
                    org.Advertisements.Where(m => m.Language == language).FirstOrDefault());

            if (model == null)
            {
                if (org.OrganizationInfo != null)
                    model = Mapper.Map<OrganizationInfo, AdvertisementModelView>(org.OrganizationInfo);
               
                if(model == null)
                    model = new AdvertisementModelView();
            }
            
            return PartialView("EditPartial", model);
        }

        [Authorize]
        public ActionResult Edit()
        {
            var org = _organisationService.GetCurrent();
            if (org.UniqueID == "123456789" || org.UniqueID == "987654321")
                return HttpNotFound();

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(AdvertisementModelView model)
        {

            if (ModelState.IsValid)
            {
                var organization = _organisationService.Get(User.Identity.GetOrgId());
                var advOfLanguage = 
                    organization.Advertisements.FirstOrDefault(m => m.Language == model.Language);
               
                if ( advOfLanguage!= null)
                {
                    if (model.LogoExtension == null)
                    {
                        string[] tempLogo = advOfLanguage.PathToLogo.Split('.');
                        model.TempLogoId = tempLogo[0];
                        model.LogoExtension = tempLogo[1];
                    }
                    _advertisementService.Remove(advOfLanguage, true);
                    _advertisementService.Commit(true);
                 } 

                OrganizationInfo orgInfo = Mapper.Map<AdvertisementModelView, OrganizationInfo>(model);
                Advertisement adv = Mapper.Map<AdvertisementModelView, Advertisement>(model);

                if (System.IO.File.Exists(Path.Combine(Server.MapPath("~/Content/Logos/"),
                    $"{model.TempLogoId}.{model.LogoExtension}")))
                {
                    var pathToLogo = $"{model.TempLogoId}.{model.LogoExtension}";
                    adv.PathToLogo = pathToLogo;
                    model.PathToLogo = pathToLogo;
                }

               
                organization.Advertisements.Add(adv);
                if (organization.OrganizationInfo == null)
                    organization.OrganizationInfo = orgInfo;
                else
                     Mapper.Map<OrganizationInfo, OrganizationInfo>(orgInfo, organization.OrganizationInfo);
                _organisationService.Commit(true);
      
                ViewBag.Message = "Updated!";
                return PartialView("EditPartial", model);
            }
            ViewBag.Message = "Error!";
            return PartialView("EditPartial", model);
        }

        private AdvertiseLanguages GetLanguageFromLocale(string locale)
        {
            switch (locale)
            {
                case "ru":
                    return AdvertiseLanguages.RU;
                case "he":
                    return AdvertiseLanguages.HE;
                default:
                    return AdvertiseLanguages.EN;
            }
        }
    }
}