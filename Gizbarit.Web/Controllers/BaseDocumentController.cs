﻿using Gizbarit.DAL.Entities;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.Web.Util;
using NLog;

namespace Gizbarit.Web.Controllers
{
    public class BaseDocumentController:BaseController
    {
        protected readonly IOrganisationService _organizationService;
        protected readonly IDocumentService _documentService;
        protected readonly IDocumentItemService _documentItemService;
        protected readonly ITransactionService _transactionService;
        protected readonly IBankAccountService _bankAccountService;
        protected readonly IPaymentTypeService _paymentTypeService;
        protected readonly IPaymentService _paymentService;
        protected readonly IAdvanceService _advanceService;
        protected readonly ICurrencyService _currencyService;

        protected Currency _currency;
        protected readonly DocumentUtils _documentUtils;

        protected readonly Logger _logger = LogManager.GetCurrentClassLogger();


        protected bool IsSend { get; set; }
        protected bool IsPrint { get; set; }
        protected bool IsSendCopy { get; set; }

        public BaseDocumentController(IOrganisationService organizationService, ICurrencyService currencyService,
            IDocumentService documentService, IDocumentItemService documentItemService,
            ITransactionService transactionService,
            IBankAccountService bankAccountService, IPaymentTypeService paymentTypeService,
            IPaymentService paymentService, IAdvanceService advanceService, ICatalogService catalogService)
        {
            _organizationService = organizationService;
            _documentService = documentService;
            _documentItemService = documentItemService;
            _transactionService = transactionService;
            _bankAccountService = bankAccountService;
            _paymentTypeService = paymentTypeService;
            _paymentService = paymentService;
            _advanceService = advanceService;
            _currencyService = currencyService;

            _currency = currencyService.Get("ILS");
            _documentUtils = new DocumentUtils(_organizationService, _documentService, _documentItemService,
                _transactionService, _advanceService, _bankAccountService, _paymentTypeService, _paymentService, catalogService, currencyService);
        }

        
    }
}