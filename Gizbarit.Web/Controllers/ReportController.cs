﻿using System;
﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gizbarit.DAL.Common;
using Gizbarit.DAL.Converters;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Facades.Report;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Entities.Content;
using Gizbarit.DAL.Models.Report;

namespace Gizbarit.Web.Controllers
{
    public class ReportController : BaseController
    {
        private readonly IOrganisationService _organizationService;
        private readonly IReportFacade _reportFacade;

        public ReportController(
            IOrganisationService organizationService,
            IReportFacade reportFacade
            )
        {
            _organizationService = organizationService;
            _reportFacade = reportFacade;
        }
    
        [HttpGet]
        public ActionResult Index()
        {
            var org = _organizationService.Get(OrgId);
            var model = ReportViewModel.Create(org);
            model.Email = Request.Cookies["bkemail"]?.Value;
            model.BccEmail = Request.Cookies["bccemail"]?.Value;

            return View("Index", model);
        }

        public ActionResult Index(ReportViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", model);
            }
            var organisation = _organizationService.GetCurrent();
            ICollection<Document> documents;

            if (model.InvoiceSelected)
            {
                documents = _reportFacade.GetDocuments(new DocumentReportSpecification()
                {
                    DocumentType = DocumentType.TaxInvoice,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate
                });

                model.Invoices = documents.Select(d => d.ToBaseDocument(organisation)).ToList();
            }

            if (model.ReciptSelected)
            {

                documents = _reportFacade.GetDocuments(new DocumentReportSpecification()
                {
                    DocumentType = DocumentType.Receipt,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate
                });

                model.Recipts = documents.Select(d => d.ToBaseDocument(organisation)).ToList();
            }

            if (model.InvoiceReciptSelected)
            {

                documents = _reportFacade.GetDocuments(new DocumentReportSpecification()
                {
                    DocumentType = DocumentType.TaxInvoiceReceipt,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate
                });
                model.InvoiceRecipts = documents.Select(d => d.ToBaseDocument(organisation)).ToList();
            }

            model.IsVatCharge = organisation.IsVatCharge;

            SetCoockie(model);

            return View("Preview", model);
        }

        [HttpGet]
        public ActionResult Download(ReportViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", model);
            }

            var spec = CreateSpecification(model);
            var content = _reportFacade.GetForPrint(spec);

            SetCoockie(model);

            return File(content.GetStream(), content.Type, content.Name);
        }

        public ActionResult Send(ReportViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", model);
            }

            var spec = CreateSpecification(model);
            var organisation = _organizationService.GetCurrent();

            _reportFacade.SendByMail(spec);

            model.IsVatCharge = organisation.IsVatCharge;
            model.IsSended = true;

            SetCoockie(model);

            return View("Index", model);
        }


        [HttpGet]
        public ActionResult TaxAuthority()
        {
            var model = new TaxAuthorityReportViewModel
            {
                FromDate = DateTime.UtcNow.Date.AddYears(-1),
                ToDate = DateTime.UtcNow.Date
            };

            return View("TaxAuthorityReport", model);
        }

        [HttpPost]
        public ActionResult TaxAuthority(TaxAuthorityReportViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("TaxAuthorityReport", model);
            }

            var spec = new ReportSpecification()
            {
                FromDate = model.FromDate,
                ToDate = model.ToDate
            };

            model.Contents = _reportFacade.GetTaxAuthorityReport(spec);

            return View("TaxAuthorityReport", model);
        }

        [HttpGet]
        public ActionResult DocumentsCombined()
        {
            var model = new TaxAuthorityReportViewModel
            {
                FromDate = DateTime.UtcNow.Date.AddYears(-1),
                ToDate = DateTime.UtcNow.Date
            };
        
            return View("DocumentsCombined", model);
        }


        [HttpPost]
        public ActionResult DocumentsCombined(TaxAuthorityReportViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("DocumentsCombined", model);
            }

            var organisation = _organizationService.GetCurrent();

            var spec = new ReportSpecification()
            {
                FromDate = model.FromDate,
                ToDate = model.ToDate,
                OrganizationId = organisation.ID,
            };

            model.Content = _reportFacade.GetDocumentsCombinedReport(spec);


            return File(model.Content.GetStream(), model.Content.Type, model.Content.Name);
         //   return View("DocumentsCombined", model);
        }


        private static ReportSpecification CreateSpecification(ReportViewModel model)
        {
            var spec = new ReportSpecification()
            {
                ToDate = model.ToDate,
                FromDate = model.FromDate
            };

            if (model.InvoiceSelected)
            {
                spec.DocumentTypes.Add(DocumentType.TaxInvoice);               
            }

            if (model.ReciptSelected)
            {
                spec.DocumentTypes.Add(DocumentType.Receipt);
            }

            if (model.InvoiceReciptSelected)
            {
                spec.DocumentTypes.Add(DocumentType.TaxInvoiceReceipt);
            }

            if (model.PriceOfferSelected)
            {
                spec.DocumentTypes.Add(DocumentType.PriceOffer);
            }

            if (model.PreliminaryInvoiceSelected)
            {
                spec.DocumentTypes.Add(DocumentType.PreliminaryInvoice);
            }

            if (model.WaybillSelected)
            {
                spec.DocumentTypes.Add(DocumentType.Waybill);
            }

            spec.Recipients.Add(model.Email);

            if (model.BccEmail.IsNotEmpty())
            {
                spec.Recipients.Add(model.BccEmail);
            }

            return spec;
        }

        private void SetCoockie(ReportViewModel model)
        {
            var email = new HttpCookie("bkemail", model.Email);
            var bccEmail = new HttpCookie("bccemail", model.BccEmail);

            email.Expires = DateTime.UtcNow.AddMonths(1);
            bccEmail.Expires = DateTime.UtcNow.AddMonths(1);

            Response.Cookies.Add(email);
            Response.Cookies.Add(bccEmail);
        }
    }
}