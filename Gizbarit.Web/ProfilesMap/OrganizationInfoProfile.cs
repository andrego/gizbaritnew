﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Gizbarit.DAL.Entities.Advertisement;
using Gizbarit.DAL.Models.Advertisement;

namespace Gizbarit.Web.ProfilesMap
{
    public class OrganizationInfoProfile:Profile
    {
        public override string ProfileName => "OrganizationInfoProfile";

        public OrganizationInfoProfile()
        {
            CreateMap<AdvertisementModelView, OrganizationInfo>();
            CreateMap<OrganizationInfo, AdvertisementModelView>();

            CreateMap<OrganizationInfo, OrganizationInfo>()
                .ForMember(e => e.ID, opt => opt.Ignore())
                .ForMember(e => e.Organization, opt => opt.Ignore());

        }
    }
}