﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Gizbarit.DAL.Entities.Organization;
using Gizbarit.DAL.Models.Advertisement;

namespace Gizbarit.Web.ProfilesMap
{
    public class AdvertisementViewProfile:Profile
    {
        public override string ProfileName => "AdvertisementViewProfile";

        public AdvertisementViewProfile()
        {
            CreateMap<DAL.Entities.Organization.Organization, AdvertisementModelView>()
                .ForMember(e => e.Phone, opt => opt.MapFrom(el => el.Phone1)) //not sure for choosing right number
                .ForMember(e => e.Site, opt => opt.MapFrom(el => el.OrganizationInfo.Site))
                .ForMember(e => e.Facebook, opt => opt.MapFrom(el => el.OrganizationInfo.Facebook))
                .ForMember(e => e.Instagram, opt => opt.MapFrom(el => el.OrganizationInfo.Instagram))
                .ForMember(e => e.LinkedIn, opt => opt.MapFrom(el => el.OrganizationInfo.LinkedIn))
                .ForMember(e => e.Language, opt => opt.Ignore())
                .ForMember(e => e.PathToLogo, opt => opt.Ignore())
                .ForMember(e => e.Text, opt => opt.Ignore())
                .ForMember(e => e.Title, opt => opt.Ignore());


        }
    }
}