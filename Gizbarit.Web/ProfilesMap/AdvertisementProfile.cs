﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Gizbarit.DAL.Entities.Advertisement;
using Gizbarit.DAL.Models.Advertisement;


namespace Gizbarit.Web.ProfilesMap
{
    public class AdvertisementProfile:Profile
    {
        public override string ProfileName => "AdvertisementProfile";

        public AdvertisementProfile()
        {
            CreateMap<AdvertisementModelView, Advertisement>();

           
            CreateMap<Advertisement, AdvertisementModelView>()
                .ForMember(el => el.Email, opt => opt.MapFrom(m => m.Organization.Email))
                .ForMember(el => el.Facebook, opt => opt.MapFrom(m => m.Organization.OrganizationInfo.Facebook))
                .ForMember(el => el.Name, opt => opt.MapFrom(m => m.Organization.Name))
                .ForMember(el => el.Fax, opt => opt.MapFrom(m => m.Organization.Fax))
                .ForMember(el => el.Instagram, opt => opt.MapFrom(m => m.Organization.OrganizationInfo.Instagram))
                .ForMember(el => el.LinkedIn, opt => opt.MapFrom(m => m.Organization.OrganizationInfo.LinkedIn))
                .ForMember(el => el.Site, opt => opt.MapFrom(m => m.Organization.OrganizationInfo.Site))
                .ForMember(el => el.Phone, opt => opt.MapFrom(m => m.Organization.Phone1));

        }
    }
}