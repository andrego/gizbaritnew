﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Gizbarit.DAL;
using Gizbarit.DAL.Interfaces;
using Gizbarit.Identity.Providers;
using Gizbarit.Web.Providers;
using Gizbarit.Web.Quartz;
using Gizbarit.Web.Util;
using NLog;
using NLog.LayoutRenderers;
using Quartz;
using Quartz.Impl;

namespace Gizbarit.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// ID User temp
        /// </summary>
        private static string _idUser = null;
        /// <summary>
        /// id Sesion temp
        /// </summary>
        private static string _idSesionUser = null;
        /// <summary>
        /// Id sesio
        /// </summary>
        private static string _sessionId = null;

        protected void Application_Start()
        {
            LayoutRenderer.Register("level-ordinal",
               (logEvent) => logEvent.Level.Ordinal);

            AreaRegistration.RegisterAllAreas();
            ModelBinders.Binders.Add(typeof(double), new DoubleModelBinder());
            ModelBinders.Binders.Add(typeof(double?), new DoubleModelBinder());

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SetupIocContainer();

            //GizbratDataContext.Seed();

            QuarzSchedulerService.Instatnce.Start(DependencyResolver.Current.GetService<IJobsProvider>());

            ModelBinders.Binders.Add(typeof(DateTime), new DateTimeBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new NullableDateTimeBinder());

            BundleTable.EnableOptimizations = false;
        }

    protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string cultureName = "";
            var languageCookie = HttpContext.Current.Request.Cookies["lang"];

            cultureName = languageCookie != null ? languageCookie.Value : "he";

            List<string> cultures = new List<string>() { "ru", "he", "en", "fr" };
            if (!cultures.Contains(cultureName))
            {
                cultureName = "he";
            }
            var specificCulture = CultureInfo.CreateSpecificCulture(cultureName);
            specificCulture.NumberFormat.NumberDecimalSeparator = ".";
            specificCulture.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = specificCulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);

        }

        /// <summary>
        /// DI for background jobs with InstancePerDependency lifetime scope (sure InstancePerRequest don't allow)
        /// </summary>
        private static void RegisterJobServices()
        {
            var builder = new ContainerBuilder();
#if TEST
            string dbConnectionString = ConfigurationManager.ConnectionStrings["TestConnection"].ConnectionString;
#elif DEBUG
            string dbConnectionString = ConfigurationManager.ConnectionStrings["DebugConnection"].ConnectionString;
#else
            string dbConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
#endif

            builder.RegisterType<DataAccessFactory<GizbratDataContext>>().As<IDataAccessFactory>().WithParameter("connectionString", dbConnectionString).SingleInstance();
            builder.Register(c => c.Resolve<IDataAccessFactory>().CreateUnitOfWork()).As<IUnitOfWork>();

            builder.RegisterAssemblyTypes(Assembly.Load("Gizbarit.DAL"))
                .Where(
                    t => t.Name.EndsWith("Service") || t.Name.EndsWith("Factory") || t.Name.EndsWith("EntityExpression"))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(Assembly.Load("Gizbarit.Utils"))
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Service") && t.Name != "QuarzSchedulerService")
                .AsImplementedInterfaces()
                .InstancePerDependency();

            // Schedule
            builder.Register(x => new StdSchedulerFactory().GetScheduler()).As<IScheduler>();
            // Schedule jobs
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(x => typeof(IJob).IsAssignableFrom(x));

            IContainer container = builder.Build();
            QuarzSchedulerService.Instatnce.SetDiContainer(container);
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                var identity = new CustomIdentity(HttpContext.Current.User.Identity);
                var principal = new CustomPrincipal(identity);
                HttpContext.Current.User = principal;
            }
        }

        public override void Init()
        {
            base.Init();
            if (_idUser == null) _idUser = "New_" + Guid.NewGuid().ToString().Replace("-", "0");
        }

        private static void SetupIocContainer()
        {
           
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterModule<AutofacModuleConfig>();

            RegisterJobServices();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
            _sessionId = Session.SessionID;

            HttpCookie hc = new HttpCookie("SessionID");
            hc.Value = _sessionId;
            Request.Cookies.Add(hc);
        }
    }

    public class DateTimeBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, value);

            DateTime dateTime;
            if (DateTime.TryParseExact(value.AttemptedValue, "dd.MM.yyyy", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out dateTime))
            {
                return (object) dateTime;
            }
            if (DateTime.TryParseExact(value.AttemptedValue, "dd/MM/yyyy", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out dateTime))
            {
                return (object)dateTime;
            }

            return null;
        }
    }

    public class NullableDateTimeBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, value);

            if (value == null)
                return null;

            DateTime dateTime;
            if (DateTime.TryParseExact(value.AttemptedValue, "dd.MM.yyyy", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out dateTime))
            {
                return (object)dateTime;
            }
            if (DateTime.TryParseExact(value.AttemptedValue, "dd/MM/yyyy", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out dateTime))
            {
                return (object)dateTime;
            }

            return null;
        }
    }
}
