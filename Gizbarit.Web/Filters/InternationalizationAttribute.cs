﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Gizbarit.Web.Filters
{
    public class InternationalizationAttribute : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string cultureName = "";
            var languageCookie = HttpContext.Current.Request.Cookies["lang"];

            cultureName = languageCookie != null ? languageCookie.Value : "he";

            List<string> cultures = new List<string>() {"ru", "he", "en", "fr"};
            if (!cultures.Contains(cultureName))
            {
                cultureName = "he";
            }
            var specificCulture = CultureInfo.CreateSpecificCulture(cultureName);
            specificCulture.NumberFormat.NumberDecimalSeparator = ".";
            specificCulture.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = specificCulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);
        }
    }
}