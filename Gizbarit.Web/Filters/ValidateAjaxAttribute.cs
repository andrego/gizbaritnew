﻿using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Gizbarit.Web.Filters
{
    public class ValidateAjaxAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
                return;

            var modelState = filterContext.Controller.ViewData.ModelState;
            if (!modelState.IsValid)
            {
                var errorModel =
                        from x in modelState.Keys
                        where modelState[x].Errors.Count > 0
                        select new
                        {
                            errors = modelState[x].Errors.Select(y => y.ErrorMessage).FirstOrDefault()
                                                      
                        };
                filterContext.Result = new JsonResult()
                {
                    Data = new {Success = false, Message=errorModel}
                };
                filterContext.HttpContext.Response.StatusCode =
                                                      (int)HttpStatusCode.BadRequest;
            }
        }
    }
}