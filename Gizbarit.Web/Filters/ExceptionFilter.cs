﻿using System;
using System.Web;
using System.Web.Mvc;
using Gizbarit.Utils;
using NLog;

namespace Gizbarit.Web.Filters
{
    public class ExceptionFilter: IExceptionFilter
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public void OnException(ExceptionContext exceptionContext)
        {
            if (!exceptionContext.ExceptionHandled)
            {
                var logEvent = new LogEventInfo(LogLevel.Error, HttpContext.Current.Request.RawUrl, "");
                try
                {
                    logEvent.Properties.Add("orgid", HttpContext.Current.User.Identity.GetOrgId());
                }
                catch (Exception ex)
                {
                    logEvent.Properties.Add("orgid", 0);
                }
                _logger.Error(exceptionContext.Exception.ToString);
            }
        }
    }
}