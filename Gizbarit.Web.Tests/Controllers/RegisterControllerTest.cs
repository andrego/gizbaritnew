﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gizbarit.DAL.Models;
using Gizbarit.DAL.Services.Bank;
using NUnit.Framework;
using Rhino.Mocks;

namespace Gizbarit.Web.Tests.Controllers
{
    [TestFixture]
    public class RegisterControllerTest
    {


        [Test]
        [Ignore("Ignore a fixture")]
        public void ValidateModel()
        {
            var model = new RegisterModel {AccountProfile = {LastName = null}};

            var isValid = Validator.TryValidateObject(model, new ValidationContext(model, null, null), new List<ValidationResult>(), true);

            Assert.False(isValid);
        }

        [Test]
        public void RhinoTest()
        {
            MockRepository  moqrep = new MockRepository();
            IBankAccountService service = moqrep.DynamicMock<IBankAccountService>();
            var time = DateTime.Now;
            using (moqrep.Record())
            {
                service.GetBalance(1, time);
            }

            service.GetBalance(1, time);
            moqrep.Verify(service);
        }
    }

  
}
