﻿using System;
using System.Net;
using System.Net.Mail;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Services.Bank;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Other;
using Gizbarit.DAL.Utils;
using Gizbarit.Web.Controllers;
using Gizbarit.Web.Util;
using NUnit.Framework;
using Rhino.Mocks;

namespace Gizbarit.Web.Tests.Controllers
{
    [TestFixture]
    public class InvoiceReceiptTests
    {

        delegate long LongDelegate();

        private IOrganisationService _organizationService;
        private IDocumentService _documentService;
        private IDocumentItemService _documentItemService;
        private ITransactionService _transactionService;
        private IAdvanceService _advanceService;
        private IBankAccountService _bankAccountService;
        private IPaymentTypeService _paymentTypeService;
        private IPaymentService _paymentService;
        private ICurrencyService _curencyService;

        private DocumentsController controller;

        [SetUp]
        public void InitRepos()
        {
            _organizationService = MockRepository.GenerateStub<IOrganisationService>();
            _curencyService = MockRepository.GenerateStub<ICurrencyService>();
            _documentService = MockRepository.GenerateStub<IDocumentService>();
            _paymentService = MockRepository.GenerateStub<IPaymentService>();
            _paymentTypeService = MockRepository.GenerateStub<IPaymentTypeService>();
            _bankAccountService = MockRepository.GenerateStub<IBankAccountService>();
            _advanceService = MockRepository.GenerateStub<IAdvanceService>();
            _transactionService = MockRepository.GenerateStub<ITransactionService>();
            _documentItemService = MockRepository.GenerateStub<IDocumentItemService>();

            //controller = new DocumentsController(_organizationService, _curencyService, _documentService,
            //    _documentItemService, _transactionService, _advanceService, _bankAccountService, _paymentTypeService,
            //    _paymentService);
        }

        [Test]
        public void GetPaymentDueDaysWhenPaymentDueDateIsNull()
        {
            var d = new Document {};

            var due = Getter.GetPaymentDueDays(d);

            Assert.IsTrue(due == 0);


        }

        [Test]
        public void GetPaymentDueDaysWhenPaymentDueDateIsNotNull()
        {
            var date = DateTime.Now;
            var d = new Document {DateCreated = date, PaymentDueDate = date.AddDays(10)};

            var due = Getter.GetPaymentDueDays(d);

            Assert.IsTrue(due == 10);
        }


        [Test]
        public void MailTest()
        {
            var client = new SmtpClient("smtp.sendgrid.net", 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("azure_560e0eddb8c8c3894384df319a3fa867@azure.com", "odessa58"),
                EnableSsl = true
            };

            client.Send("denisabg@gmail.com", "d0535318380@gmail.com", "test", "test");
        }
    }
}
