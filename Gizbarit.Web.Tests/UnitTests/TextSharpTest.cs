﻿using System.IO;
using System.Security.Cryptography.X509Certificates;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

namespace Gizbarit.Web.Tests.UnitTests
{
    [TestClass]
    public class TextSharpTest
    {
        [TestMethod]
        public void SignTest()
        {
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\";

            //var pbxFile = Path.Combine(dataDir, @"certificates\gizbaritcustomer.pfx");
            var pbxFile = Path.Combine(dataDir, @"certificates\next-pki12-test.pfx");
            var inFile = dataDir + @"test.pdf";
            //var inFile = dataDir + @"hebrew-test-out.pdf";
            var outFile = dataDir + @"test-out.pdf";
            //var outFile = dataDir + @"signed_test-out.pdf";
            var reader = new PdfReader(inFile);
            var stamper = PdfStamper.CreateSignature(reader, new FileStream(outFile, FileMode.Create), '\0');
            var stamp = stamper.SignatureAppearance;


            //TODO: Make full details
            stamp.Reason = "חתימה אלקטרונית  -- מערכת עסקית גיזברית";
            stamp.Location = "gizbarit.co.il";
            stamp.CertificationLevel = 2;
            stamp.SignDate = System.DateTime.Now;

            var cert = new X509Certificate2(pbxFile, "odessa58", X509KeyStorageFlags.Exportable);

            var pk = DotNetUtilities.GetKeyPair(cert.PrivateKey).Private;

            IExternalSignature es = new PrivateKeySignature(pk, "SHA256");

            var cp = new X509CertificateParser();

            //Org.BouncyCastle.X509.X509Certificate[]
            var chain = new[]
            {
                cp.ReadCertificate(cert.RawData)
            };

            // Sign the PDF file and finish up.
            MakeSignature.SignDetached(stamp, es, chain, null, null, null, 0, CryptoStandard.CMS);
              stamper.Close();
        }




    }

}
