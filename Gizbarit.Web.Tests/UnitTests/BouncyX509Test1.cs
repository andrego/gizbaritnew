﻿using System;
using System.Collections;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Gizbarit.DAL.Entities.Content;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

namespace Gizbarit.Web.Tests.UnitTests
{
    [TestClass]
    public class BouncyX509Test1
    {


        [TestMethod]
        public void GenerateCertificate()
        {
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\Certificates\\";
            var outFile = dataDir + @"next-pki12-test.pfx";
            var caFile = dataDir + @"gizbarit.v3.pfx";

            // Root
            const string password = "odessa1958";
            var issuerCertificate = new X509Certificate2();
            issuerCertificate.Import(caFile, password, X509KeyStorageFlags.PersistKeySet);
            var issuerCert = DotNetUtilities.FromX509Certificate(issuerCertificate);

            var kpgen = new RsaKeyPairGenerator();
            kpgen.Init(new KeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), 2048));
            var kp = kpgen.GenerateKeyPair();

            var gen = new X509V3CertificateGenerator();
            var subjectName = new X509Name("CN= בודקת אפשרות עברית של גיזברית , O= tester@mail.com");
            var issuerName = issuerCert?.SubjectDN;
            var serialNo = BigInteger.ProbablePrime(120, new Random());

            gen.SetSignatureAlgorithm(issuerCert?.SigAlgName.Replace("-", string.Empty));
            gen.SetSerialNumber(serialNo);
            gen.SetSubjectDN(subjectName);
            gen.SetSubjectUniqueID(new bool[] { });
            gen.SetIssuerDN(issuerName);
            gen.SetIssuerUniqueID(new bool[] { });
            gen.SetNotAfter(DateTime.Now.AddYears(1));
            gen.SetNotBefore(DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)));
            gen.SetPublicKey(kp.Public);

            //gen.CopyAndAddExtension(X509Extensions.AuthorityKeyIdentifier, false, issuerCert);
            gen.AddExtension(X509Extensions.AuthorityKeyIdentifier.Id,
                false,
                new AuthorityKeyIdentifier(
                    SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(kp.Public),
                    new GeneralNames(new GeneralName(issuerCert?.IssuerDN)),
                    serialNo));
            gen.CopyAndAddExtension(X509Extensions.AuthorityInfoAccess.Id, false, issuerCert);
            gen.CopyAndAddExtension(X509Extensions.BasicConstraints.Id, false, issuerCert);
            gen.CopyAndAddExtension(X509Extensions.KeyUsage.Id, false, issuerCert);
            gen.CopyAndAddExtension(X509Extensions.ExtendedKeyUsage.Id, false, issuerCert);
            gen.CopyAndAddExtension(X509Extensions.SubjectKeyIdentifier.Id, false, issuerCert);
            //gen.CopyAndAddExtension(X509Extensions.SubjectAlternativeName.Id, true, issuerCert);
            var newCert = gen.Generate(kp.Private);
            newCert.CheckValidity();
            newCert.Verify(kp.Public);

            // Convert BouncyCastle X509 Certificate to .NET's X509Certificate
            var cert = DotNetUtilities.ToX509Certificate(newCert);
            var certBytes = cert.Export(X509ContentType.Pkcs12, "password");

            // Convert X509Certificate to X509Certificate2
            var cert2 = new X509Certificate2(certBytes, "password");

            // Convert BouncyCastle Private Key to RSA
            var rsaPriv = DotNetUtilities.ToRSA(kp.Private as RsaPrivateCrtKeyParameters);

            // Setup RSACryptoServiceProvider with "KeyContainerName" set
            var csp = new CspParameters
            {
                KeyContainerName = "KeyContainer"
            };

            var rsaPrivate = new RSACryptoServiceProvider(csp);

            // Import private key from BouncyCastle's rsa
            rsaPrivate.ImportParameters(rsaPriv.ExportParameters(true));

            // Set private key on our X509Certificate2
            cert2.PrivateKey = rsaPrivate;

            // Export Certificate with private key
            File.WriteAllBytes(outFile, cert2.Export(X509ContentType.Pkcs12, "password"));



            //var outCert = DotNetUtilities.ToX509Certificate(newCert).Export(X509ContentType.Pkcs12, "aaa");

            //File.WriteAllBytes(outFile, outCert);

        }

        [TestMethod]
        public void GenerateCertificate_Test_ValidCertificate()
        {
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\Certificates\\";
            var outFile = dataDir + @"next-pki12-test.pfx";
            var caFile = dataDir + @"gizbarit.v3.pfx";

            // Arrange
            //const string issuerName = "מישהט מעניין בעברית";

            // Root
            const string password = "odessa1958";
            var issuerCertificate = new X509Certificate2(caFile, password);


            //var issuerCertificate = CertificateGenerator.GetCert(caFile);
            var subjectName = "CN= טורף זאב  <mail@mail.com>";
            var issuerName = issuerCertificate.SubjectName.Format(true);
            // Act
            var gizbarit = CertificateGenerator.GenerateCertificate(subjectName, issuerName);

            // Assert
            //var cert = new X509Certificate2(gizbarit, "password");

            //subjectName = "CN= טורף זאב  <mail@mail.com>";
            //issuerName = cert.SubjectName.Format(true);

//            var custCertificate = CertificateGenerator.GenerateCertificate(subjectName, issuerName);
//            var certParser = new X509CertificateParser();

//            var chain = new[]
//{
//                    certParser.ReadCertificate(cert.RawData)
//                };

            File.WriteAllBytes(outFile, gizbarit);
            
        }
    }

    public static class CertificateGenerator
    {
       
        public static byte[] GenerateCertificate(string subjectName, string issuerName)
        {
            var kpgen = new RsaKeyPairGenerator();

            kpgen.Init(new KeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), 2048));

            var kp = kpgen.GenerateKeyPair();

            var certificateGenerator = new X509V3CertificateGenerator();

            //var certName = new X509Name(subjectName);
            //var issuerName = new X509Name(issuerName);
            var serialNo = BigInteger.ProbablePrime(120, new Random());

            certificateGenerator.SetSerialNumber(serialNo);
            certificateGenerator.SetSubjectDN(new X509Name(subjectName));
            certificateGenerator.SetIssuerDN(new X509Name(issuerName));

            certificateGenerator.SetNotAfter(DateTime.Now.AddYears(1));
            certificateGenerator.SetNotBefore(DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)));
            
            certificateGenerator.SetSignatureAlgorithm("SHA256withRSA");
            certificateGenerator.SetPublicKey(kp.Public);

            certificateGenerator.AddExtension(
                X509Extensions.AuthorityKeyIdentifier.Id,
                false,
                new AuthorityKeyIdentifier(
                    SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(kp.Public),
                    new GeneralNames(new GeneralName(new X509Name(subjectName))),
                    serialNo));

            certificateGenerator.AddExtension(
                X509Extensions.ExtendedKeyUsage.Id,
                false,
                new ExtendedKeyUsage(
                    new ArrayList()
                    {
                        new DerObjectIdentifier("1.3.6.1.5.5.7.3.1")
                    }));
            certificateGenerator.AddExtension(X509Extensions.BasicConstraints.Id, true, new BasicConstraints(true));
            //gen.addExtension(X509Extensions.AuthorityKeyIdentifier, false,new AuthorityKeyIdentifierStructure(caCert));
            var newCert = certificateGenerator.Generate(kp.Private);
            return DotNetUtilities.ToX509Certificate(newCert).Export(System.Security.Cryptography.X509Certificates.X509ContentType.Pkcs12, "password");
        }

        public static CertificateContent GetCert(string fileName)
        {

            //var certPath = Path.Combine(signFolder, fileName);

            var cert = new CertificateContent()
            {
                Name = fileName
            };

            if (File.Exists(fileName))
            {
                cert.Data = File.ReadAllBytes(fileName);
            }

            return cert;
        }


    }
}
