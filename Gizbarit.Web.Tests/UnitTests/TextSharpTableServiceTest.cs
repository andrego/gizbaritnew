﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using Gizbarit.DAL;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Services.Document;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Store;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Document = iTextSharp.text.Document;
using Image = iTextSharp.text.Image;

namespace Gizbarit.Web.Tests.UnitTests
{
    [TestClass]
    public class TextSharpTableServiceTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var db = new GizbratDataContext();
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\Certificates\\";
            //var caFile = dataDir + @"gizbarit.v3.pfx";
            //var certFile = dataDir + @"next-pki12-test-1.pfx";

            // Root
            //const string password = "odessa1958";
            //var issuerCertificate = new X509Certificate2();
            //issuerCertificate.Import(caFile, password, X509KeyStorageFlags.PersistKeySet);
            // var issuerCert = DotNetUtilities.FromX509Certificate(issuerCertificate);
            // var certBytes = issuerCertificate.Export(X509ContentType.Pkcs12, password);

            //var certificateContent = new CertificateContent
            //{
            //    Data = issuerCertificate.RawData,
            //    Password = password,
            //    Expired = DateTime.Now.AddYears(1)
            //};

            var uow = new UnitOfWork(db);
            IStoreCatalogItemsService service = new StoreCatalogItemsServise(uow ); 
            //IOrganisationService service = new OrganisationService(uow);
            //var org = orgService.Get(26);

            //var certFactory = new BouncyCertificateFactory();
            //var certificate = certFactory.Create(org, certificateContent);
            //File.WriteAllBytes(certFile, certificate.Data);


            //var doc = service.Get(new Guid("81D340F6-2A18-4E29-9C7F-65A44D4B791C"));      //Invoice-Reciept ע.מ
            //var doc = service.Get(new Guid("88b28b28-a117-4152-93db-59f313bc94c6"));      //   Reciept ע.פ   !!!!!
            //var provider = new PdfInvoiceRecieptProvider();

            //var doc = service.Get(new Guid("c3fe66ac-cd7f-4ee3-8e17-fe11a09947ac"));      //Invoice ע.מ   
            //var provider = new PdfInvoiceProvider();

            //var doc = service.Get(new Guid("88b28b28-a117-4152-93db-59f313bc94c6"));      //Reciept ע.מ
            //var provider = new PdfRecieptProvider();


            //            var doc = service.Get(new Guid("39f8b8c9-b05b-4576-a95b-cb7e299988f3"));      //PreliminaryReciept ע.מ
            //var doc = service.Get(new Guid("92a56f0b-9e34-4272-895c-030c6a7aab6b"));        //PreliminaryReciept ע.פ   
            //            var provider = new PdfPreliminaryInvoiceProvider();

            //var doc = service.Get(new Guid("8b67f446-1292-4be8-b53f-8613bbe5f84a"));      //Waybill ע.מ
            //var doc = service.Get(new Guid("c796e6ce-d73d-4bcf-aded-a07503851a3b"));        //Waybill ע.פ   
            //var provider = new PdfWaybillProvider();
            var doc = service.Get(33);
            //var doc = service.Get(new Guid("b666e3fd-6d30-4b23-bf63-994cceb90c50"));      //PriceOffer ע.מ
            ////var doc = service.Get(new Guid("bf5d3823-4734-437e-b6dd-4f8d6c87e31a"));        //PriceOffer ע.פ   
            var provider = new PdfPriceOfferProvider();

            var outFile = dataDir + @"next-hebrew-test-out"  + ".pdf";

            var arcs = new DocumentProviderArgs()
            {
                StampType = DocumentStampType.Copy
            };
            var content = provider.Create(doc,arcs);
            

            //File.WriteAllBytes(outFile, content.Data);

            var acrobatReader = new Process
            {
                StartInfo =
                {
                    FileName = outFile
                }
            };

            acrobatReader.Start();

        }


        protected byte[] AddDigitalLogo(byte[] bytes, Image logo)
        {
            using (var stream = new MemoryStream())
            {
                var reader = new PdfReader(bytes);

                using (var stamper = new PdfStamper(reader, stream))
                {
                    var pdfContentByte = stamper.GetOverContent(1);

                    var image = Image.GetInstance(logo);
                    image.SetAbsolutePosition(74, 680);
                    image.ScalePercent(13);
                    pdfContentByte.AddImage(image);
                    stamper.Close();
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }


        protected static void SendEmail(MemoryStream stream)
        {
            var sender = new MailAddress("webmaster@gizbarit.co.il");
            var reciever = new MailAddress("DenisA@gizbarit.co.il");
            var email = new MailMessage(sender, reciever);
            var attach = new Attachment(stream, new System.Net.Mime.ContentType("application/pdf"));
            email.Attachments.Add(attach);
            var mailSender = new SmtpClient("Gmail-Server");
            mailSender.Send(email);
        }



        private static byte[] AddPageNumber(byte[] bytes)
        {
            var blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (var stream = new MemoryStream())
            {
                var reader = new PdfReader(bytes);
                using (var stamper = new PdfStamper(reader, stream))
                {
                    var pages = reader.NumberOfPages;
                    for (var i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(pages + "-מ " + (i) + " דף", blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }



        private static byte[] AddPageBoorders(byte[] bytes)
        {
            var blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (var stream = new MemoryStream())
            {
                var reader = new PdfReader(bytes);
                using (var stamper = new PdfStamper(reader, stream))
                {
                    var pages = reader.NumberOfPages;
                    for (var i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase((i + 1) + "/" + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }


        protected static void AddBorder(iTextSharp.text.Document document, PdfWriter writer)
        {
            //Add border to page
            var content = writer.DirectContent;
            var rectangle = new Rectangle(document.PageSize);
            rectangle.Left += document.LeftMargin / 2;
            rectangle.Right -= document.RightMargin / 2;
            rectangle.Top -= document.TopMargin / 2;
            rectangle.Bottom += document.BottomMargin / 2;
            content.SetColorStroke(BaseColor.BLACK);
            content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
            content.Stroke();

        }


        public void AddingHeaderFooter(PdfWriter writer)
        {
            Font font = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);
            PdfContentByte canvas = writer.DirectContent;
            ColumnText.ShowTextAligned(
              canvas, Element.ALIGN_LEFT,
              new Phrase("Header", font), 10, 810, 0
            );
            ColumnText.ShowTextAligned(
              canvas, Element.ALIGN_LEFT,
              new Phrase("Footer", font), 10, 10, 0
            );
        }


        public Document AddingHeader(Document doc)
        {
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\";

            var arialFontFile = dataDir + @"fonts\ARIALUNI.TTF";
            var gishaFontFile = dataDir + @"fonts\GISHABD.TTF";
            //var aBgishaFontFile = dataDir + @"fonts\GISHA.TTF";

            var arialFont = BaseFont.CreateFont(arialFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var gishaFont = BaseFont.CreateFont(gishaFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            //var aBgishaFont = BaseFont.CreateFont(aBgishaFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            var arial = new Font(arialFont, 12, Font.NORMAL, BaseColor.BLACK);
            var gisha = new Font(gishaFont, 12, Font.NORMAL, BaseColor.BLACK);
            var aBgisha = new Font(gishaFont, 15, Font.NORMAL, BaseColor.BLACK);

            //var colorEven = new BaseColor(200, 200, 200);

            var document = doc;
            var dt = new DataTable();

            var widths = new[] {20f};
            var table = new PdfPTable(1);

            table.SetWidths(widths);
            table.WidthPercentage = 100;
            table.DefaultCell.NoWrap = false;
            table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            var cell = Create("מקור", aBgisha);
            cell.RunDirection = 2;
            cell.Colspan = dt.Columns.Count;
            table.AddCell(cell);

            cell = Create("         ", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create("Olga", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create(",         ", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create("036667890", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create("         ", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;
            table.AddCell(cell);


            cell = Create("מספר עסק: 123456789", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            var dateNow = DateTime.Now.ToString("dd/MM/yyyy");
            cell = Create(dateNow, arial);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create("חשבונית מס קבלה מספר 109", aBgisha);
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create("לכבוד:", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create("WEB", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;


            cell = Create("תל אביב", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create("072-5674839", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create("515467367", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            cell = Create("         ", arial);
            table.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            table.AddCell(cell);

            document.Add(table);

            return document;
        }


        public void AddingFooter(PdfWriter writer)
        {
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\";

            var arialFontFile = dataDir + @"fonts\ARIALUNI.TTF";

            var arialFont = BaseFont.CreateFont(arialFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var arial = new Font(arialFont, 8, Font.NORMAL, BaseColor.BLACK);

            var footer = new Paragraph("מסמך מיוצר על ידי מערכת נהול עסקים גיזברית www.gizbarit.co.il", arial)
            {
                Alignment = Element.ALIGN_CENTER

            };
            var footerTbl = new PdfPTable(1)
            {
                TotalWidth = 500,
                HorizontalAlignment = Element.ALIGN_CENTER,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL
            };
            footerTbl.DefaultCell.NoWrap = false;

            var cell = new PdfPCell(footer)
            {
                Border = 0,
                //PaddingLeft = 10,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                NoWrap = false
            };

            footerTbl.AddCell(cell);
            footerTbl.WriteSelectedRows(0, -1, 50, 30, writer.DirectContent);
        }

        public PdfPTable BorderTable(PdfPTable table)
        {
            var innerTable = new PdfPTable(1);
            var widths = new[] { 10f };

            var housing = new PdfPCell(innerTable)
            {
                Padding = 0f
            };
            innerTable.SetWidths(widths);
            innerTable.WidthPercentage = 100;
            innerTable.DefaultCell.NoWrap = false;
            innerTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;

            table.AddCell(housing);

            return table;
        }

        public void AddingTable(PdfWriter stream, Document doc, DataTable dt)
        {
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\";

            var arialFontFile = dataDir + @"fonts\ARIALUNI.TTF";
            var gishaFontFile = dataDir + @"fonts\GISHABD.TTF";
            //var aBgishaFontFile = dataDir + @"fonts\GISHA.TTF";

            var arialFont = BaseFont.CreateFont(arialFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var gishaFont = BaseFont.CreateFont(gishaFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            //var aBgishaFont = BaseFont.CreateFont(aBgishaFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            var arial = new Font(arialFont, 12, Font.NORMAL, BaseColor.BLACK);
            var gisha = new Font(gishaFont, 12, Font.NORMAL, BaseColor.BLACK);
            var aBgisha = new Font(gishaFont, 15, Font.NORMAL, BaseColor.BLACK);

            //var colorEven = new BaseColor(200, 200, 200);

            var document = doc;

            var widths = new[] { 3f, 3f, 3f, 10f, 1.5f };
            var table = new PdfPTable(dt.Columns.Count);

            table.SetWidths(widths);
            table.WidthPercentage = 100;
            table.DefaultCell.NoWrap = false;
            table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            

            var cell = Create("תיאור פעילות: ffff", aBgisha);
            cell.Colspan = dt.Columns.Count;
            cell.Border = 13;

            table.AddCell(cell);

            cell=EmptyCell();
            cell.Colspan = dt.Columns.Count;
            
            cell.Border = 12;
            table.AddCell(cell);


            foreach (DataColumn c in dt.Columns)
            {
                var dataCell = Create(c.ColumnName, gisha);
                dataCell.Border = 13;
                table.AddCell(dataCell);
                dataCell.NoWrap = false;
            }

            foreach (DataRow row in dt.Rows)
            {
                foreach (var column in row.ItemArray)
                {
                    var dataCell = Create(column, arial);
                    dataCell.Border = 12;
                    table.AddCell(dataCell);
                    dataCell.NoWrap = false;
                }
            }

            cell = Create("          ", arial);
            cell.Colspan = dt.Columns.Count;
            cell.Border = 14;
            table.AddCell(cell);

            cell = Create("          ", arial);
            cell.Colspan = dt.Columns.Count;
            cell.Border = 0;
            table.AddCell(cell);

            document.Add(table);

            table = new PdfPTable(3);
            widths = new[] {15f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 95;

            var innerTable = new PdfPTable(2);
            widths = new[] { 10f, 10f };

            var housing = new PdfPCell(innerTable)
            {
                Padding = 0f
            };

            innerTable.SetWidths(widths);
            innerTable.WidthPercentage = 100;
            innerTable.DefaultCell.NoWrap = false;
            innerTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;

            cell = Create(" 35,000 ש'ח ", gisha);
            cell.Border = 0;
            innerTable.AddCell(cell);

            cell = Create("סה'כ לתשלום", gisha);
            cell.Border = 0;
            innerTable.AddCell(cell);

            cell = Create(" 6,300 ש'ח", gisha);
            cell.Border = 0;
            innerTable.AddCell(cell);

            cell = Create("מע'מ 18", gisha);
            cell.Border = 0;
            innerTable.AddCell(cell);

            cell = Create(" 41,300 ש'ח ", gisha);
            cell.Border = 0;
            innerTable.AddCell(cell);

            cell = Create("סה'כ כולל מע'מ:", gisha);
            cell.Border = 0;
            innerTable.AddCell(cell);


            table.AddCell(housing);


            cell = Create("        ", gisha);
            cell.Border = 0;
            table.AddCell(cell);

            cell = Create("        ", gisha);
            cell.Border = 0;
            table.AddCell(cell);


            cell = Create("         ", aBgisha);
            cell.Colspan = dt.Columns.Count;
            innerTable.AddCell(cell);
            table.AddCell(cell);

            document.Add(table);

        }

        public void AddingCashTable(PdfWriter stream, Document doc, DataTable dt)
        {
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\";

            var arialFontFile = dataDir + @"fonts\ARIALUNI.TTF";
            var gishaFontFile = dataDir + @"fonts\GISHABD.TTF";
            //var aBgishaFontFile = dataDir + @"fonts\GISHA.TTF";

            var arialFont = BaseFont.CreateFont(arialFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var gishaFont = BaseFont.CreateFont(gishaFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            //var aBgishaFont = BaseFont.CreateFont(aBgishaFontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            var arial = new Font(arialFont, 12, Font.NORMAL, BaseColor.BLACK);
            var gisha = new Font(gishaFont, 12, Font.NORMAL, BaseColor.BLACK);
            var aBgisha = new Font(gishaFont, 15, Font.NORMAL, BaseColor.BLACK);

            //var colorEven = new BaseColor(200, 200, 200);

            var document = doc;

            var widths = new[] { 5f, 5f, 5f, 5f, 5f, 5f, 5f, 2f };
            var innerTable = new PdfPTable(dt.Columns.Count);

            innerTable.SetWidths(widths);
            innerTable.WidthPercentage = 100;
            innerTable.DefaultCell.NoWrap = false;
            innerTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

        //    document.Add(table);

            var cell = Create("פריטי תשלום", aBgisha);
            cell.Colspan = dt.Columns.Count;
            innerTable.AddCell(cell);

            cell = Create("         ", aBgisha);
            cell.Colspan = dt.Columns.Count;
            innerTable.AddCell(cell);


            foreach (DataColumn c in dt.Columns)
            {
                var dataCell = Create(c.ColumnName, gisha);
                innerTable.AddCell(dataCell);
                dataCell.NoWrap = false;
            }

            foreach (DataRow row in dt.Rows)
            {
                foreach (var column in row.ItemArray)
                {
                    var dataCell = Create(column, arial);
                    innerTable.AddCell(dataCell);
                    dataCell.NoWrap = false;
                }
            }

            cell = Create("          ", arial);
            cell.Border = 0;
            innerTable.AddCell(cell);
            cell.Colspan = dt.Columns.Count;

            document.Add(innerTable);

            document.PageCount = 1;
            //fOOTER
            innerTable = new PdfPTable(dt.Columns.Count);

            innerTable.SetWidths(widths);
            innerTable.WidthPercentage = 100;
            innerTable.DefaultCell.NoWrap = false;
            innerTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            for (var i = 0; i < 4; i++)
            {
                cell = Create("          ", arial);
                cell.Border = 0;
                cell.Colspan = dt.Columns.Count;
                innerTable.AddCell(cell);
            }

            cell = Create("חתימה", arial);
            cell.Border = 2;
            cell.Colspan = dt.Columns.Count;
            innerTable.AddCell(cell);

            document.Add(innerTable);



        }

        private static PdfPCell Create(object data, Font font)
        {
            var phrase = new Phrase(data.ToString(), font);
            var cell = new PdfPCell(phrase)
            {
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                Border = 0,
                NoWrap = false,
                BackgroundColor = BaseColor.WHITE
            };

            return cell;
        }
        private static DataTable GetTable()
        {
            var table = new DataTable();

            table.Columns.Add("פריט", typeof(string));
            table.Columns.Add("שם פריט", typeof(string));
            table.Columns.Add("מחיר פריט", typeof(string));
            table.Columns.Add("כמות פריט", typeof(string));
            table.Columns.Add("מחיר כולל", typeof(string));

            for (var i = 1; i < 5; i++)
            {
                table.Rows.Add(
                    i + " ", 
                    i + " שם פריט", 
                    " 35,000 ש'ח ", 
                    "1 ", 
                    " 35,000 ש'ח ");
            }

            return table;
        }

        private static DataTable GetCashTable()
        {
            var table = new DataTable();

            table.Columns.Add("מס", typeof(string));
            table.Columns.Add("סוג תשלום", typeof(string));
            table.Columns.Add("המחאה", typeof(string));
            table.Columns.Add("מס' חשבון", typeof(string));
            table.Columns.Add("סניף", typeof(string));
            table.Columns.Add("בנק", typeof(string));
            table.Columns.Add("סכום", typeof(string));
            table.Columns.Add("תאריך", typeof(string));

            for (var i = 1; i < 3; i++)
            {
                table.Rows.Add(
                    i + " ", 
                    i + " סוג תשלום",
                    i + "המחאה ",
                    i + "מס' חשבון ", 
                    i + "סניף ",
                    i + "בנק ",
                    i + "סכום ",
                    DateTime.Now.ToString("dd/MM/yyyy"));
            }

            return table;
        }



        private static PdfPCell EmptyCell()
        {
            var phrase = new Phrase("  ");
            var cell = new PdfPCell(phrase)
            {
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                Border = 0,
                NoWrap = false,
                BackgroundColor = BaseColor.WHITE
            };

            return cell;
        }
    }
}
