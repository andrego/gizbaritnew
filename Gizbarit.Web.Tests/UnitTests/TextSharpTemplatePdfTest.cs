﻿using System.Diagnostics;
using System.IO;
using Gizbarit.DAL;
using Gizbarit.DAL.Services.Document;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Gizbarit.Web.Tests.UnitTests
{
    [TestClass]
    public class TextSharpTemplatePdfTest
    {
        [TestMethod]
        public void TemplateHebrewTextTest()
        {

            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\";

            //var fontFile = dataDir + @"fonts\ARIALUNI.TTF";
            var fontFile = dataDir + @"fonts\GISHA.TTF";
            //var fontFile = dataDir + @"fonts\GISHABD.TTF";
            var outFile = dataDir + @"hebrew-test-out.pdf";

            var textFrase = "מה קורה? אני התחלתי להיות עברי";
            //Declare a itextSharp document 
            var document = new Document(PageSize.A4);
            var uow = new UnitOfWork(new GizbratDataContext());
            IDocumentService service = new DocumentService(uow);
            //Create our file stream and bind the writer to the document and the stream 
            PdfWriter.GetInstance(document, new FileStream(outFile, FileMode.Create));

            //Open the document for writing 
            document.Open();

            //Add a new page 
            document.NewPage();

            //Reference a Unicode font to be sure that the symbols are present. 
            var bfArialUniCode = BaseFont.CreateFont(fontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            //Create a font from the base font 
            var hebFont = new Font(bfArialUniCode, 12);


            //Use a table so that we can set the text direction 
            var table = new PdfPTable(numColumns: 2);

            //Ensure that wrapping is on, otherwise Right to Left text will not display 
            table.DefaultCell.NoWrap = false;

            ////Create a regex expression to detect hebrew or arabic code points 
            //const string regexMatchArabicHebrew = @"[\u0600-\u06FF,\u0590-\u05FF]+";
            //if (Regex.IsMatch("מה קורה ", regexMatchArabicHebrew, RegexOptions.IgnoreCase))
            //{
            //    table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            //}

            table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            //Create a cell and add text to it 
            var text = new PdfPCell(new Phrase(textFrase, hebFont));
            //Ensure that wrapping is on, otherwise Right to Left text will not display 
            text.NoWrap = false;


            //Add the cell to the table 
            table.AddCell(text);

            text = new PdfPCell(new Phrase("עוד עברית", hebFont));
            table.AddCell(text);

            //Add the table to the document 
            document.Add(table);


            //Adding external rows to Table
            var secondTable = CreateTable();
            document.Add(CreateTable());
            document.Add(secondTable);
            //document.Add(secondTable);
            //document.Add(secondTable);
            //document.Add(secondTable);
            //document.Add(secondTable);
            //document.Add(secondTable);
            //document.Add(secondTable);
            //document.Add(secondTable);
            //document.Add(secondTable);
            //document.Add(secondTable);


            //Close the document 
            document.Close();

            //Launch the document if you have a file association set for PDF's 
            var acrobatReader = new Process
            {
                StartInfo = { FileName = outFile }
            };

            acrobatReader.Start();
        }


        private PdfPTable CreateTable()
        {
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\";

            var fontFile = dataDir + @"fonts\ARIALUNI.TTF";
            //var fontFile = dataDir + @"fonts\GISHA.TTF";
            //var fontFile = dataDir + @"fonts\GISHABD.TTF";

            var bf = BaseFont.CreateFont(fontFile, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font gisha = new Font(bf, 12, Font.BOLD, BaseColor.WHITE);


            PdfPTable table = new PdfPTable(2);
            table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            ////////////////
            PdfPCell cell = new PdfPCell(new Phrase("מזהה", gisha));
            cell.BackgroundColor = BaseColor.BLACK;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("שם", gisha));
            cell.BackgroundColor = BaseColor.BLACK;
            table.AddCell(cell);
            ////////////////

            gisha = new Font(bf, 12, Font.NORMAL, BaseColor.BLACK);
            var colorEven = new BaseColor(200, 200, 200);

            ///////////////////
            cell = new PdfPCell(new Phrase("123", gisha));
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("שלמה גולדברג", gisha));
            table.AddCell(cell);
            ///////////////////

            ///////////////////
            cell = new PdfPCell(new Phrase("5555", gisha));
            cell.BackgroundColor = colorEven;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("יוסי גולדברג", gisha));
            cell.BackgroundColor = colorEven;
            table.AddCell(cell);
            ///////////////////

            ///////////////////
            cell = new PdfPCell(new Phrase("8874", gisha));
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("נועם שפר", gisha));
            table.AddCell(cell);
            ///////////////////

            ///////////////////
            cell = new PdfPCell(new Phrase("31334", gisha));
            cell.BackgroundColor = colorEven;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("חגי כהן", gisha));
            cell.BackgroundColor = colorEven;
            table.AddCell(cell);
            ///////////////////
            return table;
        }
    }
}
