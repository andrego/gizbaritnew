﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Gizbarit.DAL;
using Gizbarit.DAL.Entities.Documents;
using Gizbarit.DAL.Entities.Settlements;
using Gizbarit.DAL.Entities.Store;
using Gizbarit.DAL.Providers.Document;
using Gizbarit.DAL.Providers.Store;
using Gizbarit.DAL.Services.Organisation;
using Gizbarit.DAL.Services.Store;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Document = iTextSharp.text.Document;

namespace Gizbarit.Web.Tests.UnitTests
{
    [TestClass]
    public class StoreUnitTest
    {
        [TestMethod]
        public void StoreOrderTestMethod()
        {

            var db = new GizbratDataContext();
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent?.Parent;

            var service = new GizbratDataContext();
            var orderFromDb = service.Orders.FirstOrDefault(x=>x.ID == 1);

            var provider = new StoreOrderPdfProvider();
            var args = new DocumentProviderArgs()
            {
                StampType = DocumentStampType.None
            }; 

            var result = provider.Create(orderFromDb, args);

            var outFile = fileDir +$@"order{orderFromDb.Number}.pdf";

            File.WriteAllBytes(outFile, result.Data);

            //Launch the document if you have a file association set for PDF's 
            var acrobatReader = new Process
            {
                StartInfo = { FileName = outFile }
            };

            acrobatReader.Start();
           // Assert.IsNotNull(result);

        }

        [TestMethod]
        public void StoreOrderEnglishTestMethod()
        {

            var db = new GizbratDataContext();
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent?.Parent;

            var service = new GizbratDataContext();
            var orderFromDb = service.Orders.FirstOrDefault(x => x.ID == 1);

            var provider = new StoreOrderPdfProviderEn();
            var args = new DocumentProviderArgs()
            {
                StampType = DocumentStampType.None
            };

            var result = provider.Create(orderFromDb, args);

            var outFile = fileDir + $@"order{orderFromDb.Number}_eng.pdf";

            File.WriteAllBytes(outFile, result.Data);

            //Launch the document if you have a file association set for PDF's 
            var acrobatReader = new Process
            {
                StartInfo = { FileName = outFile }
            };

            acrobatReader.Start();
            // Assert.IsNotNull(result);

        }



        [TestMethod]
        public void StoreStockTestMethod()
        {
            var db = new GizbratDataContext();
            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent?.Parent;

            var service = new GizbratDataContext();
            var organization = service.Organizations.Find(26);
            var stockFromDb = organization.StoreInvoices;

            var provider = new StoreStockPdfProvider();
            var args = new DocumentProviderArgs()
            {
                StampType = DocumentStampType.None
            };

            
            var context = new StoreStockInfo
            {
                Organization = organization,
                Invoices = stockFromDb
            };

            var result = provider.Create(context, args);
            var outFile = fileDir + $@"storeStock{organization.UniqueID}.pdf";

            File.WriteAllBytes(outFile, result.Data);

            //Launch the document if you have a file association set for PDF's 
            var acrobatReader = new Process
            {
                StartInfo = { FileName = outFile }
            };

            acrobatReader.Start();
            // Assert.IsNotNull(result);

        }
    }
}
