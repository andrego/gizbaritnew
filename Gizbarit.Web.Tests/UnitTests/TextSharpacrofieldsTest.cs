﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using iTextSharp.text.pdf;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Gizbarit.Web.Tests.UnitTests
{
    [TestClass]
    public class TextSharpAcrofieldsTest
    {
        [TestMethod]
        public void TestMethod1()
        {

            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\";
            var inFile = dataDir + @"hebrew-test-out.pdf";
            var oFile = dataDir + @"result.pdf";
            var pdfReader = new PdfReader(inFile);

            //using (var outFile = new FileStream(dataDir + @"result.pdf", FileMode.Create))
            //{
                
            //    var pdfStamper = new PdfStamper(pdfReader, outFile);

            //    var fields = pdfStamper.AcroFields;
            //    //rest of the code here
            //    fields.SetField("n°1", "value");
            //    //...
            //    pdfStamper.Close();
            //    pdfReader.Close();
            //}

            var sb = new StringBuilder();
            foreach (var de in pdfReader.AcroFields.Fields)
            {
                sb.Append(de.Key.ToString() + Environment.NewLine);
            }

            //Launch the document if you have a file association set for PDF's 
            var acrobatReader = new Process
            {
                StartInfo =
                {
                    FileName = oFile
                }
            };

            acrobatReader.Start();
        }

        [TestMethod]
        public void TestMethodFlattening()
        {

            var fileDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            var dataDir = fileDir?.FullName + "\\Data\\";
            var inFile = dataDir + @"hebrew-test-out.pdf";
            var outFile = dataDir + @"acrofield-test-out.pdf";

            var reader = new PdfReader(inFile);

            var stream = new FileStream(outFile, FileMode.Create, FileAccess.Write);

            var stamp = new PdfStamper(reader, stream);

            var field = new TextField(stamp.Writer, new iTextSharp.text.Rectangle(40, 500, 360, 530), "some_text");

            // add the field here, the second param is the page you want it on         
            stamp.AddAnnotation(field.GetTextField(), 1);

            stamp.FormFlattening = true; // lock fields and prevent further edits.

            stamp.Close();
        }
    }
}
