﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gizbarit.DAL.Entities.Bank;
using Gizbarit.DAL.Services.Bank;

namespace Gizbarit.Web.Tests.ServicesFakes
{
    public class TestBankNamesService : IBankNameService
    {
        public BankName Get(int id)
        {
            return new BankName
            {
                ID = id,
                Name = "Test"
            };
        }

        public BankName Create(BankName newItem, bool shouldBeCommited = false)
        {
            if (!shouldBeCommited)
                throw new ArgumentException("Please set shouldBeCommited to true");

            newItem.ID = 1;
            return new BankName();
        }

        public void Update(BankName item, bool sholdBeCommited = false)
        {
            throw new NotImplementedException();
        }

        public void Update(BankName item)
        {
            throw new NotImplementedException();
        }

        public IQueryable<BankName> GetAll()
        {
            return (IQueryable<BankName>)new List<BankName>
            {
                new BankName {ID = 1, Name = "Type1"},
                new BankName {ID = 2, Name = "Type3"}
            };
        }

        public void Remove(BankName removeItem, bool shouldBeCommited = false)
        {
            throw new NotImplementedException();
        }

        public void Commit(bool saveChanges = false)
        {
            throw new NotImplementedException();
        }
    }
}
