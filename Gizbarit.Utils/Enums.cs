﻿namespace Gizbarit.Utils
{
    public class Enums
    {
        public enum RegisterType
        {
            Company,
            Private,
            Partnership,
            NonProfit
        }

        public enum BusinessType
        {
            Ordinary = 1,
            Exemption = 2
        }
    }
}
