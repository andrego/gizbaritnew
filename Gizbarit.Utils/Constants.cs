﻿namespace Gizbarit.Utils
{
    public static class Constants
    {
        public const string TrialEndJobGroupKey = "UserNotifyTrialEndJobs";
        public const string TrialEndJobGroupKey3 = "UserNotifyTrialEndJobs3";
        public const string ExpirationJobGroupKey = "ExpirationJobs";
        public const string ExpirationJobGroupKey3 = "ExpirationJobs3";
        public const string DisableTrialModeGroupKey = "DisableTrialMode";
    }
}
