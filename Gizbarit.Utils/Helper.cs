﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Security;

namespace Gizbarit.Utils
{
    public static class Helper
    {
        public static string Seperator = "|@|";

        public static Enums.BusinessType GetOrgType(this IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                var claim =
                    claimsIdentity.Claims.FirstOrDefault(c => c.Type == "BusinessType");

                if (claim == null)
                    throw new NullReferenceException("Claim.BusinessType is empty");

                return (Enums.BusinessType) int.Parse(claim.Value);
            }
            return (Enums.BusinessType) int.Parse(identity.GetProperty(LoginUserVerifyResult.PropertieId.BusinessType));
        }

        public static int GetOrgId(this IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                var claim =
                    claimsIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

                if(claim == null)
                    throw new NullReferenceException("Claim.NameIdentifier is empty");

                return int.Parse(claim.Value);
            }
            return int.Parse(identity.GetProperty(0));
        }

        public static string GetUserFirstName(this IIdentity identity)
        {
           return identity.GetProperty(LoginUserVerifyResult.PropertieId.FirstName);
        }

        public static string GetProperty(this IIdentity identity, LoginUserVerifyResult.PropertieId pID)
        {
            var properties = identity.Name.Split(new string[] { Seperator }, StringSplitOptions.None);
            return properties[(int)pID];
        }

        public static HttpCookie SetAuthCookie(LoginUserVerifyResult user, bool isPersistent)
        {
            HttpCookie cookie = FormsAuthentication.GetAuthCookie(user.ToString(), isPersistent);

            var sc = new HttpCookie("sc")
            {
                Expires = DateTime.MaxValue,
                Value = Protect(DateTime.Now.Date.Ticks.ToString(), "auth")
            };

            if (isPersistent)
            {
                cookie.Expires = DateTime.Now.AddDays(7);
            }

            HttpContext.Current.Response.Cookies.Add(cookie);
            HttpContext.Current.Response.Cookies.Add(sc);

            return cookie;
        }

        public static string GetLastVisit()
        {
            HttpCookie lastVisitCookie = new HttpCookie("LastVisit") { Value = DateTime.Now.ToString("dd/MM/yyyy") + " " + DateTime.Now.ToShortTimeString() };
            if (HttpContext.Current.Response.Cookies["LastVisit"].Value == null)
            {
                HttpContext.Current.Response.Cookies.Remove("LastVisit");
                HttpContext.Current.Response.Cookies.Add(lastVisitCookie);
            }

            string lastVisit = HttpContext.Current.Request.Cookies["LastVisit"].Value;
            HttpContext.Current.Response.Cookies.Add(lastVisitCookie);
            return lastVisit;
        }

        public static DateTime SetCurrentTime(this DateTime date)
        {
            var now = DateTime.Now;
            var ts = new TimeSpan(now.Hour, now.Minute, now.Second);
            return date.Date + ts;
        }

        public static string Protect(string text, string purpose)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            byte[] stream = Encoding.Unicode.GetBytes(text);
            byte[] encodedValue = MachineKey.Protect(stream, purpose);
            return HttpServerUtility.UrlTokenEncode(encodedValue);
        }

        public static string Unprotect(string text, string purpose)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            byte[] stream = HttpServerUtility.UrlTokenDecode(text);
            byte[] decodedValue = MachineKey.Unprotect(stream, purpose);
            return Encoding.Unicode.GetString(decodedValue);
        }

        public static string GetWebUrl(this string url)
        {
            return new UriBuilder(url).Uri.AbsoluteUri;
        }
    }
}
