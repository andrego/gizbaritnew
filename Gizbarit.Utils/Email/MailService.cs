﻿using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Gizbarit.Utils.Email
{
    public class MailService : IMailService 
    {
        readonly string _smtpAdmin = ConfigurationManager.AppSettings["smtpAdmin"];
        readonly string _adminFrom = ConfigurationManager.AppSettings["AdminFrom"];
        readonly string _adminPass = ConfigurationManager.AppSettings["AdminPass"];
        readonly string _from = ConfigurationManager.AppSettings["EmailSender"];
        readonly string _pass = ConfigurationManager.AppSettings["PasswordToEmailSender"];
        readonly string _smtpServer = ConfigurationManager.AppSettings["smtpServer"];

        public async Task SendMailAsync(string recepientEmail, string subject, string body)
        {
            var contact = new EmailMessage
            {
                Destination = recepientEmail,
                Subject = subject,
                Body = body
            };
            await SendAsync(contact);
        }

        public async Task SendMailToAdmin(string name, string contacts, string body)
        {
            var mail = new MailMessage(_adminFrom, _adminFrom)
            {
                Subject = $"Обращение с сайта от {(name.Length > 0 ? name : "пользователя")} ({contacts})",
                Body = body,
                IsBodyHtml = true,
            };

            var client = new SmtpClient(_smtpAdmin, 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_adminFrom, _adminPass),
                EnableSsl = true
            };

            await client.SendMailAsync(mail);
        }

        private async Task SendAsync(EmailMessage message)
        {
            var mail = new MailMessage(_from, message.Destination)
            {
                Subject = message.Subject,
                Body = message.Body,
                IsBodyHtml = true,
            };

            await SendAsync(mail);
        }

        public void Send(MailMessage mail)
        {
            var client = CreateClient();

            client.Send(mail);
        }

        public async void SendFromAdmin(MailMessage message)
        {
            var client = new SmtpClient(_smtpAdmin, 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_adminFrom, _adminPass),
                EnableSsl = true
            };

            await client.SendMailAsync(message);
        }

        public async Task SendAsync(MailMessage mail)
        {
            var client = new SmtpClient(_smtpServer, 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_from, _pass),
                EnableSsl = true
            };

            await client.SendMailAsync(mail);
        }


        private SmtpClient CreateClient()
        {
            var client = new SmtpClient(_smtpServer, 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_from, _pass),
                EnableSsl = true
            };

            return client;
        }
    }

    public class EmailMessage
    {
        /// <summary>
        /// Destination, i.e. To email, or SMS phone number
        /// 
        /// </summary>
        public virtual string Destination { get; set; }
        /// <summary>
        /// Subject
        /// </summary>
        public virtual string Subject { get; set; }
        /// <summary>
        /// Message contents
        /// 
        /// </summary>
        public virtual string Body { get; set; }

        public ICollection<Attachment> Attachments { get; set; } = new List<Attachment>();
    }

}
