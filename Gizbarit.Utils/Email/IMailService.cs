﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace Gizbarit.Utils.Email
{
    public interface IMailService
    {
        Task SendMailAsync(string recepientEmail, string subject, string body);

        void Send(MailMessage message);
        void SendFromAdmin(MailMessage message);
        
        Task SendAsync(MailMessage message);

        Task SendMailToAdmin(string name, string email, string body);
    }
}
