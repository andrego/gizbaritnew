﻿using System;

namespace Gizbarit.Utils
{
    public class LoginUserVerifyResult
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int BusinessType { get; set; }
        public short AccountingMethod { get; set; }
        public System.DateTime ExpirationDate { get; set; }
        public int Quota { get; set; }
        public Enums.RegisterType RegisterType { get; set; }

        public enum PropertieId
        {
            Id = 0,
            Email,
            FirstName,
            LastName,
            BusinessType,
            AccountingMethod,
            RegisterType,
        }

        public LoginUserVerifyResult()
        {

        }

        public LoginUserVerifyResult(string str)
        {
            var properties = str.Split(new string[] { "|@|" }, StringSplitOptions.None);

            Email = properties[(int)PropertieId.Email];
            FirstName = properties[(int)PropertieId.FirstName];
            LastName = properties[(int)PropertieId.LastName];
            BusinessType = Byte.Parse(properties[(int)PropertieId.BusinessType]);
            ID = int.Parse(properties[(int)PropertieId.Id]);
            RegisterType = (Enums.RegisterType) int.Parse(properties[(int) PropertieId.RegisterType]);

        }

        public override string ToString()
        {
            return string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}",
                "|@|", ID, Email, FirstName, LastName, BusinessType, AccountingMethod, (int)RegisterType);
        }
    }
}
