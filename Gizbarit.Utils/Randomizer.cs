﻿using System;
using System.Linq;

namespace Gizbarit.Utils
{
    public static class Randomizer
    {
        public static string GetRandomString(int length)
        {
            if (length <= 0)
            {
                throw new ArgumentException("Value must be greater than 0");
            }
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random(DateTime.Now.Millisecond);

            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}