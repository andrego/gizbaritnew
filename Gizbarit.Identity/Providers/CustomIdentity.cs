﻿using System;
using System.Security.Principal;
using System.Web.Security;

namespace Gizbarit.Identity.Providers
{
    /// <summary>
    /// An identity object represents the user on whose behalf the code is running.
    /// </summary>
    [Serializable]
    public class CustomIdentity : IIdentity
    {
        #region Properties

        public IIdentity Identity { get; set; }

        public string GetUserId => ID.ToString();

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int BusinessType { get; set; }
        public Nullable<double> TaxRate { get; set; }
        public short AccountingMethod { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int Quota { get; set; }
        public string UserRoleName { get; set; }

        #endregion

        #region Implementation of IIdentity

        /// <summary>
        /// Gets the name of the current user.
        /// </summary>
        /// <returns>
        /// The name of the user on whose behalf the code is running.
        /// </returns>
        public string Name { get { return Identity.Name; } }

        /// <summary>
        /// Gets the type of authentication used.
        /// </summary>
        /// <returns>
        /// The type of authentication used to identify the user.
        /// </returns>
        public string AuthenticationType { get { return Identity.AuthenticationType; } }

        /// <summary>
        /// Gets a value that indicates whether the user has been authenticated.
        /// </summary>
        /// <returns>
        /// true if the user was authenticated; otherwise, false.
        /// </returns>
        public bool IsAuthenticated { get { return Identity.IsAuthenticated; } }

        #endregion

        #region Constructor

        public CustomIdentity(IIdentity identity)
        {
            Identity = identity;

            CustomMembershipUser customMembershipUser = (CustomMembershipUser)Membership.GetUser(identity.Name);
            if (customMembershipUser != null)
            {
                UserRoleName = customMembershipUser.UserRoleName;

                ID = PropertyProfile.CurrentUsers.ID;
                FirstName = PropertyProfile.CurrentUsers.FirstName;
                LastName = PropertyProfile.CurrentUsers.LastName;
                Email = PropertyProfile.CurrentUsers.Email;
                BusinessType = PropertyProfile.CurrentUsers.BusinessType;
                AccountingMethod = PropertyProfile.CurrentUsers.AccountingMethod;
                ExpirationDate = PropertyProfile.CurrentUsers.ExpirationDate;
                Quota = PropertyProfile.CurrentUsers.Quota;
            }
        }

        #endregion


    }

    public static class Extesions
    {
        public static string GetUserId(this CustomIdentity identity)
        {
            return identity.GetUserId;
        }
    }
}
