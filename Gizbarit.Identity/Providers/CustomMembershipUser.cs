﻿using System;
using System.Web.Security;
using Gizbarit.Utils;

namespace Gizbarit.Identity.Providers
{
    public class CustomMembershipUser : MembershipUser
    {
        #region Properties

        public string Email { get; set; }

        public string UserRoleName { get; set; }

        #endregion

        public  CustomMembershipUser(LoginUserVerifyResult user, string Role)
            : base("MyMembershipProvider", user.Email, user.ID, "", string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now)
        {
            Email = user.Email;
            UserRoleName = Role;
        }
    }
}
