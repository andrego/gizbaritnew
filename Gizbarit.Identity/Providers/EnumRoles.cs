﻿using Gizbarit.Utils;

namespace Gizbarit.Identity.Providers
{
    public enum Roles
    {
        None = 0,
        Administrator = 1,
        Organizations = 2,
        Client = 3,
        Anonimos = 1000
    }

    public class PropertyProfile
    {

        public static LoginUserVerifyResult CurrentUsers { set; get; }
    }
}
